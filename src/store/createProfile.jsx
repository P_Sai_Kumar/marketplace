const initialState = {

    jobDetails:{
        category:{
            category_name:'',
            description:'',

        },
        location:{
            city:{
                name:'',
            }
        },
        modified_on:'',
        start_date:'',
        end_date:'',
        positions:'',
        max_bill_rate:null,
        min_bill_rate:null,
        is_ot_exempt:null,
        is_expense_allowed:null,
        duration: {
            total_months: null,
            weeks: null,
            days: null,
            years: null
        },
        allowed_expense:null,
    },
    search:{
        Category:null,
        SubCategory:null,
        Location:null,
        freeText:null,
        result:{

                category: "",
                certificates: [
                    {
                        certificateName: "",
                        createdAt: "",
                        createdBy: "",
                        description: "",
                        expiryYear: null,
                        id: null,
                        issueYear: null,
                        issuer: "",
                        updatedAt: null
                        
                    }
                ],
                createdAt: "",
                createdBy: "",
                customUser: null,
                dateOfBirth: "",
                description: "",
                educations: [{
                    id: null,
                    degreeName: "",
                    institute: "",
                    yearOfPassing: null,
                    marks: null,
                    marksType: null,
                    grade: null,
                    startDate: "",
                    endDate: "",
                    isComplete: null,
                    degreeType: null,
                    description: null,
                    createdBy: "",
                    createdAt: "",
                    updatedBy: "",
                    updatedAt: "",
                    majorSubject: null,
                    minorSubject: null
                }],
                employments: [{
                    company:{},
                    companyName: "",
                    createdAt: "",
                    createdBy: "",
                    description: "",
                    employeeLocations: null,
                    endDate: "",
                    id: null,
                    isCurrent: null,
                    jobTitle: "",
                    lastSalary: null,
                    locations: [
                        {
                            city: "",
                            country: "",
                            createdAt: "",
                            createdBy: "",
                            id: null,
                            pincode: null,
                            state: "",
                            updatedAt: "",
                            updatedBy: "",
                        }
                    ],
                    startDate: "",
                    updatedAt: "",
                    updatedBy: "",
                }],
                files: [{
                    id: null,
                    path: "",
                    fileformat: "",
                    filetype: "",
                    tag: "",
                    isDefault: null,
                    isResume: null,
                    isProfilePic: null,
                    createdBy: "",
                    createdAt: "",
                    updatedBy: "",
                    updatedAt: ""
                }],
                firstName: "",
                id: "",
                isActive: null,
                jobPreferences: 
                   [ {
                    availableFrom: "",
                    availableTo: "",
                    createdAt: "",
                    
                    dailyRate: null,
                    employmentType: "",
                    engagementType: "",
                    fieldValues: [],
                    hourPerDay: null,
                    hourPerWeek: null,
                    hourlyRate: null,
                    id: null,
                    isActive: null,
                    locationPrefrences:[ {
                        createdAt: "",
                        createdBy: "",
                        id: null,
                        location:{
                        city: "",
                        country: "",
                        createdAt: "",
                        createdBy: "",
                        id: null,
                        pincode: null,
                        state: "",
                        updatedAt: "",
                        updatedBy: ""
                        },
                        prefrenceOrder: 1,
                        updatedAt: "",
                        updatedBy: ""
                    }],
                    locationType: "",
                    monthlyRate: null,
                    subCategory: {
                        createdAt: "",
                        createdBy: "",
                        id: null,
                        isActive: null,
                        isParent: null,
                        name: "",
                        updatedAt: "",
                        updatedBy: "",
                    },
                    updatedAt: "",
                    updatedBy: "",
                }],
                lastName: "",
                middleName: "",
                portfolios: [{}],
                primaryPhone: "",
                refereces: [{}],
                skills: [{
                    createdAt: "",
                    createdBy: "",
                    id: null,
                    skillName: "",
                    updatedAt: "",

                    
                }],
                updatedAt: "",
                updatedBy: "",
                user: null,
                email:"",
                workerLocation:"",
                status:""

        }
    },
    routePath:"",
    isSkillsEditable:false,
    isSummaryEditable:false,
    CategorySelected : 0,
    isBasicDetailsFilled:false,
    isAdditionalDetailsFilled:false,
    isEmploymentDetailsFilled:false,
    token: null,
    modalSelected : '',
    deleteModalSelected:'',
    viewProfileLoader : false,
    employeeConditions: false,
    vendorDefaultDetails:false,
    candidateSearchResult:null,
    jobSearchResult:null,
    employerCandidates: null,
    viewDetails: null,
    candidateSearchFilters:{
        fields:{
            Category:'',
            SubCategory:{SubCategory:'',inValid:false},
            Skill:{Skill:'',inValid:false},
            Designation:{Designation:'',inValid:false},
            Location:{Location:'',inValid:false},
        },
        errors:{
            Designation:"",
            Location:""
        },
        errorsdup : {
            Designation:"",
            Location:""
        },
        formValid: false,

    },
    jobSearchFilters:{
        fields:{
            Skill:{Skill:'',inValid:false},
            Designation:{Designation:'',inValid:false},
            Location:{Location:'',inValid:false},
        },
        errors:{
            Designation:"",
            Location:""
        },
        errorsdup : {
            Designation:"",
            Location:""
        },
        formValid: false,

    },
    vendorBasicDetails:{
        fields:{
            Name: {Name : '', inValid : false},
            Email: {Email : '', inValid : false},
            PhoneNumber: {PhoneNumber : '', inValid : false},
            profilePic: '',
            coverPhoto:'',
            EmployerName:{EmployerName:'',inValid : false},
            EmployeeID:{EmployeeID:'',inValid : false},
            EmployerWebsite:{EmployerWebsite:'',inValid : false},
        },
        errors : {
            Name: "",
            Email: "",
            PhoneNumber: "",
            EmployerName: "",
            EmployerWebsite : "",
        },
        errorsdup : {
            Name: "Enter valid Name",
            Email: "Enter valid email",
            PhoneNumber: "Enter valid mobile number",
            EmployerName: "Enter valid Name",
            EmployerWebsite : "Enter valid URL",
        },
        formValid: false,
    },
    vendorCandidates:{
        fields:{
            Id:{Id:''},
            FirstName:{FirstName:''},
            LastName:{LastName:''},
            Category:{Category:''},
            SubCategory:{SubCategory:''},
            Location:{Location:''},
            SavedJobs:{SavedJobs:'0'},
            AppliedJobs:{AppliedJobs:'0'},
            ProfilePic:{ProfilePic:''}

        },
        vendorCandidates:[{
            Id:{Id:''},
            FirstName:{FirstName:''},
            LastName:{LastName:''},
                Category:{Category:''},
                SubCategory:{SubCategory:''},
                Location:{Location:''},
                SavedJobs:{SavedJobs:'0'},
                AppliedJobs:{AppliedJobs:'0'},
                ProfilePic:{ProfilePic:''}
        }]
    },
    vendorCandidateDetails:{
        fields:{
            Category: "",
            Sub_Category: {Sub_Category: ""},
            ID_Proof: {ID_Proof : '',},
            Language: {Language : '',},
            CurrentLocation: {CurrentLocation : '',},
            JobStatus : {JobStatus : "Casually Looking"},
            AvailabilityStatus : {AvailabilityStatus : "Service notice period"},
            AvailableFrom : {AvailableFrom : "",},
            AvailableTill : {AvailableTill : ""},
            WorkType : {WorkType : ""},
            EmploymentType : {EmploymentType : ""},
            WorkLocation : {WorkLocation : ""},
            LocationPreference : {LocationPreference : ""},
            WorkingHours : {WorkingHours : ""},
            Currency : {Currency:"Canadian Dollar($)"},
            RateType : {RateType : ""}
        }
    },employerBasicDetails:{        
        fields:{            
            Name: {Name : '', inValid : false},            
            Email: {Email : '', inValid : false},            
            PhoneNumber: {PhoneNumber : '', inValid : false},            
            profilePic: '',            
            coverPhoto:'',            
            EmployerName:{EmployerName:'',inValid : false},            
            EmployeeID:{EmployeeID:'',inValid : false},            
            EmployerWebsite:{EmployerWebsite:'',inValid : false},        
        },        
        errors : {            
            Name: "",            
            Email: "",            
            PhoneNumber: "",            
            EmployerName: "",            
            EmployerWebsite : "",        
        },        
        errorsdup : {            
            Name: "Enter valid Name",            
            Email: "Enter valid email",            
            PhoneNumber: "Enter valid mobile number",            
            EmployerName: "Enter valid Name",            
            EmployerWebsite : "Enter valid URL",        
        },        
        formValid: false,    
    },


    skills : {
        skills : ["Python", "Java", "Graphic design"],
        edit : {id : null}
    },
    portfolio : {
        portfolio : ['url1', 'url2'],
        edit : {id : null}
    },
    employmentQues : {
        fields : {
            JobStatus : {JobStatus : "Casually Looking"},
            AvailabilityStatus : {AvailabilityStatus : "Service notice period"},
            AvailableFrom : {AvailableFrom : "",},
            AvailableTill : {AvailableTill : ""},
            WorkType : {WorkType : ""},
            EmploymentType : {EmploymentType : ""},
            WorkLocation : {WorkLocation : ""},
            LocationPreference : {LocationPreference : ""},
            WorkingHours : {WorkingHours : ""},
            Currency : {Currency:""},
            Rate : {Rate : ""},
            RateType : {RateType : ""}
        },
        formValid: true,
    },
    fields:{
        fields:{
        Name: {Name : '', inValid : false},
        LastName : {LastName: "",inValid : false},
        Email: {Email : '', inValid : false},
        PhoneNumber: {PhoneNumber : '', inValid : false},
        profilePic: '',
        coverPhoto:'',
        summary:'',
        resume:'',
        resumeContentType:'',
        Gender: {Gender : '', inValid : false},
        DOB: {DOB : '', inValid : false},
        Category: "",
        Sub_Category: {Sub_Category: ""},
        ID_Proof: {ID_Proof : '',},
        ID_Code: {ID_Code : '',},
        // Status: {Status : '',},
        Language: {Language : '',},
        CurrentLocation: {CurrentLocation : '',},
        },
        errors : {
            Name: "",
            LastName: "",
            Email: "",
            PhoneNumber: "",
            DOB: "",
        },
        errorsdup : {
            Name: "Enter valid first name",
            LastName: "Enter valid last name",
            Email: "Enter valid email",
            PhoneNumber: "Enter valid mobile number",
            DOB: "Enter Date of birth",
        },
        formValid: false,
    },
    educationalDetails :{
        fields : {
            Degree : {Degree : "", invalid : "false"},
            PassingYear : {PassingYear : "", invalid : 'false'},
            CurrentlyStudying : {CurrentlyStudying : false},
            University : {University : "", invalid : 'false'},
            Grade : {Grade : "", invalid : 'false'},
        },

        educationalDetails : [{
            Degree : {Degree : "", invalid : "false"},
            PassingYear : {PassingYear : "", invalid : 'false'},
            CurrentlyStudying : {CurrentlyStudying : false,},
            University : {University : "", invalid : 'false'},
            Grade : {Grade : "", invalid : 'false'},
        },],
        formValid: false,
        edit : {id : null},
        errors : {
            Degree: "Enter valid Degree",
            PassingYear: "Enter valid PassingYear",
            University: "Enter valid University",
            Grade: "Enter valid grade",
        },
        errorsdup : {
            Degree: "Enter valid Degree",
            PassingYear: "Enter valid PassingYear",
            University: "Enter valid University",
            Grade: "Enter valid grade",
        },
    },
    workDetails : {
        fields:{
            EmployerName : {EmployerName : "", invalid : "false"},
            Designation : {Designation : "", invalid : "false"},
            StartDate : {StartDate : "", invalid : "false"},
            EndDate : {EndDate : "", invalid : "false"},
            WorkLocation : {WorkLocation : "", invalid : "false"},
            CurrentlyWorking:{CurrentlyWorking : true,},
        },
        workDetails : [{
            EmployerName : {EmployerName : "TCS Company", invalid : "false"},
            Designation : {Designation : "UI Designer", invalid : "false"},
            StartDate : {StartDate : "2016-06-01", invalid : "false"},
            EndDate : {EndDate : "2018-06-01", invalid : "false"},
            WorkLocation : {WorkLocation : "Hyderabad", invalid : "false"},
            CurrentlyWorking:{CurrentlyWorking : false,},
        }],
        edit : {id : null},
        errors : {
            EmployerName: "Enter valid Name",
            Designation: "Enter valid Designation",
            // StartDate: "Enter valid Date",
            EndDate:"",
            WorkLocation: "Enter valid Work Location"
        },
        errorsdup : {
            EmployerName: "Enter valid Name",
            Designation: "Enter valid Designation",
            // StartDate: "Enter valid Date",
            EndDate: "Enter valid Date",
            WorkLocation: "Enter valid Work Location"
        },
        formValid: false,
    },
    certifications : {
        fields:{
            Name : {Name : "", invalid : "false"},
            Issuer : {Issuer : "", invalid : "false"},
            IssueYear : {IssueYear : "", invalid : "false"},
            ExpiryYear : {ExpiryYear : "", invalid : "false"},
        },
        certifications : [{
            Name : {Name : "", invalid : "false"},
            Issuer : {Issuer : "", invalid : "false"},
            IssueYear : {IssueYear : "", invalid : "false"},
            ExpiryYear : {ExpiryYear : "",invalid : "false"},
        }],
        edit : {id : null},
        errors : {
            Name: "Enter valid Name",
            Issuer: "Enter valid Issuer",
            IssueYear: "Enter valid IssueYear",
            ExpiryYear: "Enter valid ExpiryYear",
        },
        errorsdup : {
            Name: "Enter valid Name",
            Issuer: "Enter valid Issuer",
            IssueYear: "Enter valid IssueYear",
            ExpiryYear: "Enter valid ExpiryYear",
        },
        formValid: false,
    },
    recommendations : {
        fields:{
            Name : {Name : "", invalid : "false"},
            Email : {Email : "", invalid : "false"},
            PhoneNumber : {PhoneNumber : "", invalid : "false"},
        },
        recommendations : [{
            Name : {Name : "", invalid : "false"},
            Email : {Email : "", invalid : "false"},
            PhoneNumber : {PhoneNumber : "", invalid : "false"},
        }],
        edit : {id : null},
        errors : {
            Name: "Enter valid Name",
            Email: "",
            PhoneNumber: "",
        },
        errorsdup : {
            Name: "Enter valid Name",
            Email: "Enter valid email",
            PhoneNumber: "Enter valid mobile number",
        },
        formValid: false,
    },
}

const reducer = (state = initialState, action) => {
    // const newState = {...state}
    const newState = JSON.parse(JSON.stringify(state))

    switch (action.type){
        case "CANDIDATE_SEARCH_RESULT":
            newState.candidateSearchResult=action.data
            
            break;

        case "JOB_SEARCH_RESULT":
            newState.jobSearchResult=action.data
            break;

        case "JOB_DETAILS":
            newState.jobDetails=action.item
            break;

        case "ROUTE_PATH":
            newState.search=action.data
            break;
        case "SEARCH_FILTERS":
                newState.search=action.data
                break;

        case "SEARCH_RESULT":
                newState.search.result=action.item
                break;
        case "TOGGLE_SKILLS":
            newState.isSkillsEditable=!(newState.isSkillsEditable)
            break
        case "TOGGLE_SUMMARY":
            newState.isSummaryEditable=!(newState.isSummaryEditable)
            break
        case "CHANGE_CATEGORY" : 
                // console.log(action.val)
                // newState.CategorySelected = action.val;
                if(action.name&&action.name==='vendor')
                {
                    newState.vendorCandidateDetails.fields['Category'] = action.item
                }
                else if(action.name && action.name==='searchFilter')
                newState.candidateSearchFilters.fields['Category']=action.item
                else{
                    newState.fields.fields['Category'] = action.item
                }
                
                // let f={...newState.fields.fields}
                // f['Category']=action.val;
                // if(action.val  ==='HealthCare'){
                //     f['Sub_Category']='Administration'
                // }
                // else if(action.val==='Blue Collar'){
                //     f['Sub_Category']='Driver'
                // }
                // else{
                //     f['Sub_Category']='FrontEnd'
                // }
                // newState.fields.fields=f
                break;

        case "CHANGE_FIELD":
                let f1={...newState[action.data].fields}
                f1[action.name]=action.val;
                newState[action.data].fields=f1
                if(action.name==='CurrentlyWorking')
                {
                    if(action.val===true)
                    {
                        newState[action.data].errors['EndDate']=""
                    }
                }
                break;

        case "ADDITIONAL_DETAILS":
                newState.goToAdditionalDetails = true;
                break;

        case "IS_FORM_VALID":
                var keys = Object.keys(newState[action.data].errors);
                var c = 0
                for (var i=0; i < keys.length; i++){
                    var t = keys[i]
                    if(newState[action.data].errors[t] !== ""){
                        if(action.data==='workDetails'&&t==='EndDate'&&newState[action.data].fields.CurrentlyWorking.CurrentlyWorking)
                        {
                            newState[action.data].errors['EndDate']=""
                        }
                        else if(action.data==='recommendations'&&t==='Email'&&newState[action.data].fields.Email.Email==="")
                        {
                            newState[action.data].errors['Email']=""
                        }
                        else if(action.data==='recommendations'&&t==='PhoneNumber'&&newState[action.data].fields.PhoneNumber.PhoneNumber==="")
                        {
                            newState[action.data].errors['PhoneNumber']=""
                        }
                        else{
                        c = 1
                        newState[action.data].fields[t].inValid = true
                        console.log(newState[action.data].errors[t])
                        }
                    }
                }
                if ( c === 0){
                    newState[action.data].formValid = true
                }
                // console.log(newState.fields.formValid)
                break;

        case "CHANGE_ERROR_STATE":
                if (action.val){
                    newState[action.data].errors[action.field] = ""
                } else {
                    newState[action.data].errors[action.field] = newState[action.data].errorsdup[action.field]
                    newState[action.data].formValid = false;
                }
                break;

        case "ADD_DETAILS":
            let edit = newState[action.data].edit.id
            if (edit !== null){
                newState[action.data][action.data] = newState[action.data][action.data].map((item, id) => {if(item.id===edit){return action.val} else{return item}})
                newState[action.data].edit.id = null
                newState[action.data].errors = newState[action.data].errorsdup
            } else {
                newState[action.data][action.data] = newState[action.data][action.data].concat(action.val)
                console.log(newState[action.data][action.data])
            }
            break;
        case "DELETE_DETAILS":
            newState[action.name][action.name] = newState[action.name][action.name].filter((item, id) => id !== action.id)
             
            break;

        case "EDIT_DETAILS":
            var obj1 = newState[action.name][action.name].filter((val) => {if(val.id.id === action.id){return (val)}})
            // var obj1 = JSON.parse(JSON.stringify(newState[action.name][action.name].));
            // var ob = Object.assign({}, newState.educationalDetails.educationalDetails[action.id])
            newState[action.name].fields = obj1[0]
            Object.keys(newState[action.name].errors).map((val) => {newState[action.name].errors[val] = ""})
            // newState.educationalDetails.errors = {}
            newState[action.name].edit.id = action.id
            break;

        case "RESET_FORM":
            if(action.data)
            {
            var obj = JSON.parse(JSON.stringify(newState[action.data].fields));
            Object.keys(obj).map((val, id) => { 
                    if(typeof(obj[val][val]) == "boolean"){obj[val][val] = false}
                    else {
                        if(obj[val][val])
                        obj[val][val] = ""
                        else
                        obj[val]=""
                    }
                    })
            Object.keys(newState[action.data].fields).map((val, id) => {
                if(newState[action.data].fields[val].inValid)
                newState[action.data].fields[val].inValid = "false"
            })
            newState[action.data].fields = obj
            newState[action.data].formValid = false;
            newState[action.data].errors = newState[action.data].errorsdup
            }
            break;

        case "EDIT_ACTION":
            if(action.data)
            {
            newState[action.data].edit.id = null
            newState[action.data].errors = newState[action.data].errorsdup
            }
            break;

        case "CANDIDATE_RESPONSE":
            newState.employerCandidates = [...action.val]
            // data = [action.val][0].map((item, id) => {
            //     return ({
            //         firstName : item.firstName

            //     })
            //     })
                // newState.vendorBasicDetails.fields = data[0]
            // console.log(b)
            // newState.employerCandidates = true
            // newState.employerCandidates = JSON.parse(JSON.stringify(b))
            // console.log(newState.employerCandidates)
            // console.log(newState.employerCandidates)
            // newState.employerCandidates[0].firstName = "charan"
            // console.log(newState.employerCandidates)
            // console.log(b)
            // newState.employerCandidates = JSON.parse(JSON.stringify(action.val))
            // console.log(newState.employerCandidates)
            break;

        case "MAP_DATABASE_TO_LOCAl":
            var data = null
            switch (action.name){
                case "educationalDetails" : data = [action.res][0].map((item, id) => {
                    return ({id : {id : item.id},Degree : {Degree : item.degreeName, invalid : "false"},
                                    PassingYear : {PassingYear : item.yearOfPassing, invalid : 'false'},
                                    CurrentlyStudying : {CurrentlyStudying : item.isComplete, invalid : 'false'},
                                    University : {University : item.institute, invalid : 'false'},
                                    Grade : {Grade : item.marks, invalid : 'false'}})
                    })
                    break;
                case "certifications" : data = [action.res][0].map((item, id) => {
                    return ({id : {id : item.id},
                        Name : {Name : item.certificateName, invalid : "false"},
                        Issuer : {Issuer : item.issuer, invalid : "false"},
                        IssueYear : {IssueYear : item.issueYear, invalid : "false"},
                        ExpiryYear : {ExpiryYear : item.expiryYear,invalid : "false"},
                    })
                    })
                    break;
                case 'workDetails': data = [action.res][0].map((item, id) => {
                    return ({id : {id : item.id},
                            EmployerName : {EmployerName : item.companyName, invalid : "false"},
                            Designation : {Designation : item.jobTitle, invalid : "false"},
                            StartDate : {StartDate : item.startDate, invalid : "false"},
                            EndDate : {EndDate : item.endDate, invalid : "false"},
                            WorkLocation : {WorkLocation : item.employeeLocations, invalid : "false"},
                            CurrentlyWorking:{CurrentlyWorking : item.isCurrent,},
                    })
                    })
                    break;
                case 'candidateSearchFilters': data = [action.res].map((item, id) => {
                    return ({id : {id : item.id},
                        Category:item.Category,
                        SubCategory:{SubCategory:item.SubCategory,inValid:"false"},
                        Skill:{Skill:item.Skill,inValid:"false"},
                        Designation:{Designation:item.Designation,inValid:"false"},
                        Location:{Location:item.Location,inValid:"false"},
                    })
                    })
                    break;
                case 'jobSearchFilters': data = [action.res].map((item, id) => {
                    return ({id : {id : item.id},    
                        Skill:{Skill:item.Skill,inValid:"false"},
                        Designation:{Designation:item.Designation,inValid:"false"},
                        Location:{Location:item.Location,inValid:"false"},
                    })
                    })
                    break;
                case "recommendations": data = [action.res][0].map((item, id) => {
                    return ({id : {id : item.id},Name : {Name : item.name, invalid : "false"},
                    Email : {Email : item.email, invalid : "false"},
                    PhoneNumber : {PhoneNumber : item.phone, invalid : "false"},
                    })
                    })
                    break;
                case "skills":  data = [action.res][0].map((item, id) => {
                    return ({id : item.id,
                            skillName : item.skillName,
                            createdBy: item.createdBy,
                            createdAt: item.createdAt,
                            updatedBy: item.updatedBy,
                            updatedAt: item.updatedAt
                        })
                    })
                    break;
                    
                case "portfolio": data = [action.res][0].map((item, id) => {
                    return ({id : item.id, item : item.portfolioURL
                    })
                    })
                    break;
                case "fields" : data = [action.res].map((item, id) => {
                    return ({Name : {Name : item.firstName, invalid : "false"},
                                    LastName : {LastName: item.lastName, invalid : "false"},
                                    Email : {Email : item.email, invalid : 'false'},
                                    PhoneNumber : {PhoneNumber : item.primaryPhone, invalid : 'false'},
                                    CurrentLocation : {CurrentLocation : item.workerLocation},
                                    Gender: {Gender : item.gender, inValid : false},
                                    DOB: {DOB : item.dateOfBirth, inValid : false},
                                    profilePic:'',
                                    coverPhoto:'',
                                    summary:newState.fields.fields.summary?newState.fields.fields.summary:'',
                                    resume:newState.fields.fields.resume,
                                    resumeContentType:newState.fields.fields.resumeContentType,
                                    Category: "",
                                    Sub_Category: {Sub_Category: ""},
                                    ID_Proof: {ID_Proof : item.idProof,},
                                    ID_Code: {ID_Code : item.idCode,},
                                    // Status: {Status : item.status,},
                                    Language: {Language : item.language,},

                            
                                        })
                    
                    })

                
                    data = data[0]
                    console.log(action.res2)
                    if (action.res2){
                        data["Category"] = action.res2.parent.name
                        data["Sub_Category"]["Sub_Category"] = action.res2.name 
                    }
                    
                    break;

                    case "employerBasicDetails" : data = [action.res].map((item, id) => {
                        return ({
                            Name : {Name : item.name, invalid : "false"},
                            PhoneNumber : {PhoneNumber : item.phoneNumber, invalid : 'false'},   
                            Email: {Email : item.email, invalid : 'false'},
                            profilePic: item.pic?item.picContentType+', '+item.pic:'',
                            coverPhoto: item.coverPhoto?item.coverPhotoContentType+','+item.coverPhoto:'',
                            EmployerName:{EmployerName:item.employeeName,invalid : 'false'},
                            EmployeeID:{EmployeeID:item.employeeId,invalid : 'false'},
                            EmployerWebsite:{EmployerWebsite:item.employeeWebsite,invalid : 'false'},
                        })
                        })
                        newState.employerBasicDetails.fields = data[0]
                        break;

                case "vendorBasicDetails" : data = [action.res].map((item, id) => {
                    return ({
                        Name : {Name : item.name, invalid : "false"},
                            PhoneNumber : {PhoneNumber : item.phonenumber, invalid : 'false'},   
                            Email: {Email : item.email, invalid : 'false'},
                            profilePic: item.pic?item.picContentType+', '+item.pic:'',
                            coverPhoto: item.coverPhoto?item.coverPhotoContentType+','+item.coverPhoto:'',
                            EmployerName:{EmployerName:item.employerName,invalid : 'false'},
                            EmployeeID:{EmployeeID:item.employeeID,invalid : 'false'},
                            EmployerWebsite:{EmployerWebsite:item.employerWebsite,invalid : 'false'},

                    })
                    })
                    newState.vendorBasicDetails.fields = data[0]
                    break;
                case "vendorCandidateDetails":data = [action.res.jobPreference[action.res.jobPreference.length-1]].map((item, id) => {
                    console.log(item)
                    let rateType='Select'
                    if(item.dailyRate!==0)
                    {
                        rateType='Daily Rate'
                    }
                    if(item.hourlyRate!==0)
                    {
                        rateType='Hourly Rate'
                    }
                    if(item.monthlyRate!==0)
                    {
                        rateType='Monthly Rate'
                    }
                    if(item.yearlyRate!==0)
                    {
                        rateType='Yearly Rate'
                    }
                    return ({
                        Category: "",
                        Sub_Category: {Sub_Category: ""},
                        ID_Proof: {ID_Proof : '',},
                        Language: {Language : '',},
                        CurrentLocation: {CurrentLocation : '',},
                        JobStatus : {JobStatus : item.jobSearchStatus},
                        AvailabilityStatus : {AvailabilityStatus : item.availabilityStatus===''?"Service notice period":item.availabilityStatus},
                        AvailableFrom : {AvailableFrom : item.availableFrom},
                        AvailableTill : {AvailableTill : item.availableTo},
                        WorkType : {WorkType : item.engagementType},
                        EmploymentType : {EmploymentType : item.employmentType},
                        Currency : {Currency : item.currencyType},
                        RateType : {RateType : rateType},
                        WorkLocation : {WorkLocation : item.locationType},
                        LocationPreference : {LocationPreference : ""},
                        WorkingHours : {WorkingHours : item.hourPerDay},
                                        })
                    })
                    try{
                        if(action.res.locationpreference[action.res.locationpreference.length-1][0].location.city)
                        {
                            data[data.length-1].LocationPreference={LocationPreference : action.res.locationpreference[action.res.locationpreference.length-1][0].location.city}
                        }
                    }
                    catch(err){}
                    console.log(data)
                    newState.vendorCandidateDetails.fields=data[data.length-1];
                    if(action.res.category && action.res.category.length>0 && action.res.category[action.res.category.length-1])
                    {
                        newState.vendorCandidateDetails.fields["Sub_Category"]={"Sub_Category":action.res.category[action.res.category.length-1].name}
                        newState.vendorCandidateDetails.fields["Category"]=action.res.category[action.res.category.length-1].parent.name
                    }
                    if(action.res.worker.idProof)
                    {
                        newState.vendorCandidateDetails.fields["ID_Proof"]={"ID_Proof":action.res.worker.idProof}
                    }
                    if(action.res.worker.language)
                    {
                        newState.vendorCandidateDetails.fields["Language"]={"Language":action.res.worker.language}
                    }
                    if(action.res.worker.workerLocation)
                    {
                        newState.vendorCandidateDetails.fields["CurrentLocation"]={"CurrentLocation":action.res.worker.workerLocation}
                    }
                    break;

                case "vendorCandidates": data=[action.res][0].map((item, id)=>{
                                        return({
                                            Id:{Id:item.id},
                                            FirstName:{FirstName:item.firstName},
                                            LastName:{LastName:item.lastName},
                                            Category:{Category:item.Category},
                                            SubCategory:{SubCategory:(item.jobPreferences[0] && item.jobPreferences[0].subCategory?item.jobPreferences[0].subCategory.name:"")},
                                            Location:{Location:item.workerLocation},
                                            SavedJobs:{SavedJobs:item.vmsjobsaves.length},
                                            AppliedJobs:{AppliedJobs:item.vmsjobsubmits.length},
                                            ProfilePic:{ProfilePic:item.profile_pic?item.profile_pic.picContentType+","+item.profile_pic.pic:''}

                                        })
                                        })
                                        newState.vendorCandidates.fields=data[data.length-1];
                                        break;
                                        
                

        
                case "employmentDetails" : data = [action.res.jobPreference[action.res.jobPreference.length-1]].map((item, id) => {
                    console.log(item)
                    let rate=0
                    let rateType='Select'
                    if(item.dailyRate!==0)
                    {
                        rate=item.dailyRate;
                        rateType='Daily Rate'
                    }
                    if(item.hourlyRate!==0)
                    {
                        rate=item.hourlyRate;
                        rateType='Hourly Rate'
                    }
                    if(item.monthlyRate!==0)
                    {
                        rate=item.monthlyRate;
                        rateType='Monthly Rate'
                    }
                    if(item.yearlyRate!==0)
                    {
                        rate=item.yearlyRate;
                        rateType='Yearly Rate'
                    }
                    return ({JobStatus : {JobStatus : item.jobSearchStatus},
                        AvailabilityStatus : {AvailabilityStatus : item.availabilityStatus===''?"Service notice period":item.availabilityStatus},
                        AvailableFrom : {AvailableFrom : item.availableFrom},
                    AvailableTill : {AvailableTill : item.availableTo},
                    WorkType : {WorkType : item.engagementType},
                    EmploymentType : {EmploymentType : item.employmentType},
                    Currency : {Currency : item.currencyType},
                    Rate : {Rate : rate},
                    RateType : {RateType : rateType},
                    WorkLocation : {WorkLocation : item.locationType},
                    LocationPreference : {LocationPreference : ""},
                    WorkingHours : {WorkingHours : item.hourPerDay},
                                        })
                    })
                    try{
                        if(action.res.locationpreference[action.res.locationpreference.length-1][0].location.city)
                        {
                            data[data.length-1].LocationPreference={LocationPreference : action.res.locationpreference[action.res.locationpreference.length-1][0].location.city}
                        }
                    }
                    catch(err){}
                    console.log(data)
                    newState.employmentQues.fields=data[data.length-1];
                    break;
                default:
                    break
            }
            if(action.name!=='employmentDetails' && action.name!=="vendorBasicDetails" && action.name!=="vendorCandidateDetails" && action.name!=="employerBasicDetails")
            {
                newState[action.name][action.name] = data
            }
            // newState.viewProfileLoader = true
            break;

        case "CHANGE_MODAL":
            if(action.modal==='delete')
            {
                newState.deleteModalSelected=action.modal;
            }
            else{
                newState.modalSelected=action.modal;
            }
            break
        case "ON_FILLED":
            if(action.data==='basic')
            {
                newState.isBasicDetailsFilled=true;
            }
            else if(action.data==='additional')
            {
                newState.isAdditionalDetailsFilled=true;
            }
            else{
                newState.isEmploymentDetailsFilled=true;
            }
            break;
        
        case "GET_EMAIL":
            newState[action.data].fields.Email.Email = localStorage.getItem("email")
            break;

        case "TOGGLE_PLAY":
            newState.play = !newState.play
            break;

        case "VIEW_PROFILE_LOADER":
            newState.viewProfileLoader = true
        case "EMPLOYEE_CONDITIONS":
            newState.employeeConditions = !newState.employeeConditions
            break;
        case "VENDOR_DEFAULT_DETAILS":
                newState.vendorDefaultDetails = !newState.vendorDefaultDetails
                break;
        
        case "VIEW_DETAILS":
            newState.viewDetails = action.data
            break;
        

        default :
            break;

        
    }
    return newState
}

export default reducer;