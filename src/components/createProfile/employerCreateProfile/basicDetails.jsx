import React from 'react';
import '../createprofile.css';
import { connect } from 'react-redux';
import { compose } from "redux";
import { withRouter } from 'react-router-dom';
import axios from 'axios';
// import $ from 'jquery';

import UploadImage from '../../uploadImage/uploadImage';
import Input from '../../input/input';
import SideNav from '../sideNav';
import Header from '../../header/Header';
import BasicDetailsModal from './basicDetailsModal';

class EmployerBasicDetails extends React.Component{

    

    render(){

    return (
        <div>
            
        <Header />
        <section className="mainbgColor create-profile-section">
        <div className="container-fluid">
            <div className="row mobMar0">
            <SideNav page='basicDetails' employer={true}/>
                    <div className="col-md-9 mobPadd0">
                        <div className="CreateProfileForm">
                            <div className="profileHeadSec">
                               <h4 class="MobDisNone">Create Profile</h4>

                                <h6 class="disNoneDesk disBlockMob">Basic Details</h6>
                            </div>

                            <BasicDetailsModal/>

                            {/* <div className="FormSec basicDetails">
                                <form>
                                    <div className="form-row flexColRev">
                                        <div className="col-md-9">
                                            <Input 
                                            divClass="form-group" label="Name" 
                                            config = {{className :"form-control" ,
                                                    placeholder : "Enter your Name", 
                                                    type:"name"}}
                                            value = {this.props.fields.Name.Name}
                                            change={this.handleChange.bind(this,"Name" ,{required : true, name : true})}
                                            inValid = {this.props.fields.Name.inValid}
                                            error = {this.props.errors.Name}
                                            elementType="input" 
                                            />

                                            <div className="form-row">
                                                <Input 
                                                divClass="form-group col-md-6" label="Email Address" 
                                                config = {{className :"form-control" ,
                                                    placeholder : "user@domain.com", 
                                                    type:"text"}}
                                                elementType="input" 
                                                value = {this.props.fields.Email.Email}
                                                change={this.handleChange.bind(this,"Email" ,
                                                {required : true, email : true})} 
                                                error = {this.props.errors.Email}
                                                inValid = {this.props.fields.Email.inValid}
                                                />

                                                <Input 
                                                divClass="form-group col-md-6" label="Phone Number" 
                                                config = {{className :"form-control" ,
                                                    placeholder : "+91 XXX-XXX-XXXX", 
                                                    type:"text"}}
                                                elementType="input" 
                                                value = {this.props.fields.PhoneNumber.PhoneNumber}
                                                inValid = {this.props.fields.PhoneNumber.inValid}
                                                error = {this.props.errors.PhoneNumber}
                                                change={this.handleChange.bind(this,"PhoneNumber",{required : true, mobile : true})}/>

                                            </div>
                                            <div className="form-row">
                                                <Input 
                                                divClass="form-group col-md-6" label="Employer Name" 
                                                config = {{className :"form-control" ,
                                                        placeholder : "Enter Employer Name",
                                                        type:"text"}}
                                                elementType="input" 
                                                value = {this.props.fields.EmployerName.EmployerName}
                                                change={this.handleChange.bind(this,"EmployerName",{required : true, name : true})}
                                                inValid = {this.props.fields.EmployerName.inValid}
                                                error = {this.props.errors.EmployerName}/>
                                                <Input 
                                                    divClass="form-group col-md-6" label="Employee ID" 
                                                    config = {{className :"form-control" ,placeholder : "Enter Employee ID", type: "text"}}
                                                    elementType="input"
                                                    value = {this.props.fields.EmployeeID.EmployeeID}
                                                    inValid = {this.props.fields.EmployeeID.inValid}
                                                    change={this.handleChange.bind(this,"EmployeeID",{required : true})}
                                                    />
                                            </div>
                                        </div>
                                        <UploadImage type='pic' file={this.props.fields.profilePic} onChange={this.handleChange.bind(this,"profilePic", {})}/>
                                        </div>
                                    <div className="form-row">
                                        <Input 
                                        divClass="form-group col-md-9" label="Employer Website" 
                                        config = {{className :"form-control" ,
                                                   placeholder : "Enter URL",
                                                   type:"text"}}
                                        elementType="input" 
                                        value = {this.props.fields.EmployerWebsite.EmployerWebsite}
                                        change={this.handleChange.bind(this,"EmployerWebsite",{url: true})}
                                        inValid = {this.props.fields.EmployerWebsite.inValid}
                                        error = {this.props.errors.EmployerWebsite}/>
                                         
                                    </div>
                                </form>
                            </div> */}
                        
                        </div>
                        {/* <div className="btn-group NextFormButtons">
                            <button className="common-btn commonBlueBtn" onClick = {() => {this.props.checkFormIsValid(); setTimeout(() => this.handleSubmit(),5)}}>Save & Next</button>
                        </div> */}
                        
                    </div>
                    </div>
            </div>
        </section>
        </div>
        
    )
    }
}

const mapStateToProps = state => {
    return {
        fields: state.employerBasicDetails.fields,
        errors : state.employerBasicDetails.errors,
        formValid : state.employerBasicDetails.formValid,
    }
}

const mapDispatchToProps = dispatch => {
    return {
        changeState : (name,val)=> dispatch({type:"CHANGE_FIELD",name:name,val:val,data : 'employerBasicDetails'}),
        changeErrorState : (field, val) => dispatch({type : "CHANGE_ERROR_STATE", field : field, val : val, data : 'employerBasicDetails'}),
        checkFormIsValid : () => dispatch({type: "IS_FORM_VALID", data : 'employerBasicDetails'}), 
        onFilled : () => dispatch({type: "ON_FILLED", data : 'basic'}),
        getEmail : () => dispatch({type: "GET_EMAIL", data : 'employerBasicDetails'}),
        mapDatabaseToLocal : (name,res) => dispatch({type : "MAP_DATABASE_TO_LOCAl", name:name, res: res})
    }
}


export default compose(withRouter, connect(mapStateToProps, mapDispatchToProps))(EmployerBasicDetails);