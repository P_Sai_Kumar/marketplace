import React from "react";
import { connect } from 'react-redux';
import axios from 'axios'
import Certifications from "./certifications";
import EducationalDetails from "./educationalDetails";
import WorkHistory from "./workHistory";
import Recommendations from "./recommendations";
import EditEmploymentModal from "../../editEmploymentModal";
import EditBasicDetailsModal from "../../editBasicDetailsModal";
import CandidateSearchFilter from "../../searchCandidate/candidateSearchFilters";
import JobSearchFilter from "../../searchJob/jobSearchFilter";
import EditBasicDetails from "../vendorCreateProfile/editBasicDetails";
import BasicDetailsModal from "../employerCreateProfile/basicDetailsModal";
import EditCandidateDetails from "../vendorCreateProfile/editCandidateDetails";
import SaveForCandidates from "../../searchJob/saveForCandidates.jsx";

class Modal extends React.Component {
    WorkerID=localStorage.getItem("VendorID")?sessionStorage.getItem("WorkerID"):localStorage.getItem("WorkerID")
    saveSummary=()=>
    {
        if(this.props.summary!=='')
            {
                let photoID=localStorage.getItem("VendorID")?sessionStorage.getItem('photoID'):localStorage.getItem('photoID')
                if(photoID==null)
                {
                    axios.post('api/photos',{worker:{id:Number(this.WorkerID)},mimetype:this.props.summary}) 
                        .then((res)=>{
                            console.log(res)
                            localStorage.getItem("VendorID")?sessionStorage.setItem('photoID',res.data.id):localStorage.setItem('photoID',res.data.id)
                        })
                }
                else{
                    axios.patch('api/photos/'+photoID,{id:Number(photoID),worker:{id:Number(this.WorkerID)},mimetype:this.props.summary},{headers : {'Content-Type': 'application/merge-patch+json'}}) 
                    .then((res)=>{
                        console.log(res)
                    }) 
                }
            }
    }
    render(){
        let modal=null
        console.log(this.props.modalSelected)
        switch(this.props.modalSelected)
        {
            case "educationalDetails":
                modal=(<EducationalDetails/>)
                break
            case "certifications":
                modal=(<Certifications/>)
                break
            case "workDetails":
                modal=(<WorkHistory />)
                break
            case "recommendations":
                modal=(<Recommendations vendor={this.props.vendor}/>)
                break
            case "employmentDetails":
                modal=(<EditEmploymentModal/>)
                break
            case "basicDetails":
                modal=(<EditBasicDetailsModal/>)
                break
            case 'summary':
                modal=(<div className="modal-content">
                <div className="modal-header">
                <h5 className="modal-title" id="exampleModalLongTitle">Summary</h5>
                </div>
                <div className="modal-body"> 
                <textarea className='form-control' cols='100' value={this.props.summary} onChange={(e)=>{this.props.changeState('summary',e.target.value)}}></textarea>
                </div>
                <div className="modal-footer">
                <div className="btn-group NextFormButtons ModalNextFormButtons ">
                    <button className="common-btn commonBlueBtn" data-dismiss='modal' onClick={()=>{this.saveSummary()}}>Save</button>
                </div>
                </div>
                </div>)
                break
            case "candidateSearchFilters":
                
                modal=(<CandidateSearchFilter />)
                break
            case "jobSearchFilters":
                modal=(<JobSearchFilter />)
                break
            
            case "editVendorDefualtDetails":
                console.log("modal")
                modal=(<EditCandidateDetails />)
                setTimeout(() => {
                   
                },2000);
                break

            case "editVendorBasicDetails":
                modal=(<EditBasicDetails />)
                break
            case "editEmployerBasicDetails":
                modal=(<div className="modal-content"><BasicDetailsModal/></div>)
                break
            case "saveForCandidates":
                modal=(<SaveForCandidates />)
                break
            default:
                break
        }

        return (
            <span>
                {modal}
            </span>
        )
    }
}

const mapStateToProps = state => {
    return {
        modalSelected : state.modalSelected,
        summary:state.fields.fields.summary,
    }
}

const mapDispatchToProps = dispatch => {
    return {
        changeModal : (modal)=> dispatch({type:"CHANGE_MODAL", modal : modal}),
        changeState : (name,val)=> dispatch({type:"CHANGE_FIELD",name:name,val:val,data : 'fields'}),
    }
}
export default connect(mapStateToProps,mapDispatchToProps)(Modal)