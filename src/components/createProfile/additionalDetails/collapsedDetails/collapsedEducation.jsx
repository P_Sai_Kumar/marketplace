import React from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import education from '../../../../imgaes/education.png'
import DeleteModal from '../deleteModal'
class CollapsedEducation extends React.Component{

    headers = {
        'Content-Type': 'application/json',
      }
    WorkerID=localStorage.getItem("VendorID")?sessionStorage.getItem("WorkerID"):localStorage.getItem("WorkerID")
    componentWillMount(){
        axios.get('api/educations/worker/'+this.WorkerID, {headers : this.headers}).then((res) => 
        {
            this.props.mapDatabaseToLocal(res.data)
        })
    }

    onEducationDelete = async (id) => {
        await axios.delete('api/educations/'+id, {headers : this.headers}).then((res) => console.log(res))


        // console.log()
        await axios.get('api/educations/worker/'+this.WorkerID, {headers : this.headers}).then((res) => 
        {
            this.props.mapDatabaseToLocal(res.data)
        })
        // this.props.deleteDetails(this.props.id);
    }

    render (){
        return (
            <div class="addedDetail paddX15 disF">
                    <div class="addedDetailImg">
                        {/* <img src={education}></img> */}
                        <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAABmJLR0QA/wD/AP+gvaeTAAAMHUlEQVRoge1ZaXBc1ZX+7n2vXy/qTXJrc2uXbEu2kdRy4cgrTrEFEwIDgUkFkgKHCWSCSYZhqARsqW1jqEpmSIpKxXY2klCVsJlhhsF2cCDBxmYI1mLZsmRr75YsqSW1eu9+680PYcUttUxLtjPzw19V/znvnHvO98659/S5D7iGa/g/BGO0fPfJlfPRdzY03wj3nwyX43aFu91cuO1UeTq6JB2lQnfzNkLJTo9WK8BNlFQ6i90nHBy47xp5+lUNrETQcRBlleh4zh+TpAM6Svf0bHMdnw+Rwh0t7xJgtafRZf8sXT6dBb3Lu39Q0FlydCaJ8t0nV8qqttWko3fLCnPkWI1sRX4mqcy1wWYQIKkahiajWR2jgQc6fcH7lzzXxojGBkRVe0vPxf/j3La1Q5fyK5noA4Y4c6YTY1oZmYab8UW0+Ss8pd/iKb2eMejKHVZU5dlRkW2FwNFLmk/GRPRNhNHpCzKPP0x4SkVZ044omvZbz/a634EQbV7xzIdI7g9P5ghReWuGTv81halFFoOA5Xk2sizXjjyraZ5v4m/QGMNoOI6zowGcGQmykCiBp2QiLiuHCLgXBhpqWy6bSMHOv9zAM/4RvcBvlhXVVpBpZsvz7WRZtg1mvW6BoV8aUUnBgD+CrrEQOzcaIIwwlQFtkqS+YoHhp+3uFZF5ESnZ0fyRBtTrOIp1ZblYU5oDnl66ZK40GANODfvxQfcIAjERAMBloKTvybqBuWxmbfb+qu4bijrL72PAg594xtYc6xkx5dpMbFm2jZQ6LJdVTpfCcCiG4VAMveMR1u8PQ9XAOAIPCHmXMfYNUeKNl7JPa4/oo8qDBr3wVVVVqykhpCQrAxU5NlQ4bLAaFlZqkzERvf4w+icizOsPk6ikgid0UGLaASbjF95drk8u6BbuaJZVHX/d+aerOxdM5GKs/MHJjsc35FcutupwqDPEPugOEhCK0iwLyrOtKHdYoOe5OQPv84fRPx6GZzICgafYUGrGLZU2rCkx49Z9nWwkElkz2LDm45m2SUTcjE/Vy1L2kSJ301PQKOfZ6Xp+5jOzQHH7cjtuX24nDMBZXxxHesI42DnODrQPkEVmAyt3WEmR3YyxSAK94yF4g1FkGXlsLLfg8Q2ZWF1ciCxTWi1sdmyk9Szb0TLmbXTVfyYRQuhGjdN4ALOIJOkBqMwxojLHiG+uySGyytA0GCV/7g7jw/4Rdl2ugdz9+UVYXVyMTOPCAp8JBjwBjpucKU+5+kCj64sLcaLjCOqLzagvNgPzbbZpwtvo+q9U8r/vuXoVcY3I/zek3CNlu0/uFwjZzMD8USV2d6oj8WqiyN30FAge8jSuqkrXZhaRsp2te7KMwj98cUUh8UxGFh/tGTkKQLjw/C+eCBjYnAuKiobxqIKuMRF9/gSq800oyhSQlcGDI3Pv/7ikTj/UOHQTRs6kSyIlEYGnd6wvyyVOewac9gwc6/PxTnfr0iF37bmEgpc/6Ao/8kFXONmIgsRlLUvRmElRGWFgoIRAYww94yIYGAgIBA4qR2lYT2lopl8CEjPYHR0AMLh91ZsA3rwsIpKmNZ8673cuzbHBG4hC1RgbaqzphhvofrrmOQDPXaxfsqvpxzzht5Y6LCTfYiSfeMZxX10pLAYd9rd04dh3VuBX/+vD3o98eHRdLve7pnGbLyzzUVnb0rfN9fp8gp0Xkcxs5Sveidipf3+vrYznqSyK7JFUA8+qfSdM4XFdu9UoFN+xsohYDTrsOdqB+6+vwGKbCcGEBGCqmXyjPgcRScXBMwEc/lYVeeuU3+w+NPhqxe6WW7qfcf3TlSCyoKZV8WKXlQUjE2UOK39PbSk4SvBWWz/Meh1uWjY1mQYTEva3dOH4d1YAABSN4Qv7OvGvm/JxW5Ud3oCEL//6HPxR1du9rbYolR+nu3VphkDeFhV1qZ7nxiQVX+/fXnMole6Cjl8SiZ20GAQ+lJAgqioSioqusRA2VuTPacNTgu23OLHvIx8DgPGoDFllMOtpYcWulhdm6he8cDzLqMOZ1UXZSx+qX4qblzqzeYoDRe7W9VeESOmu1l/YDELxo+ursCzPjr1HO/CHjkFU5tg+c2bfUGbFYEAk33vbg4df6cVP7ynF6w8ugSDQ784MkIsad5YtstIbluTDactAbeEibKzII3od2XPZRCpe7LJSSrbcVV1MOEqwoSwP919fgY6RIHonwjjhGUcoLs2y0xhwejiGH75/HmFRw8nzUbz/7eWoLzGjwmHA1vW5xGYgv58RWI3TnpFU+vkWI3iOS1mG8/pLqgVCL1Xk2kmu5W/DWrbZAAKGu6pL0Dbkx4e9I5AVDSaBR1iUseknZ+CLyHDaBNxWZcfztxdif9sksxm46SAfXJ2Nvcd8Bc6G5huHdta996mYzGw7hBAwaCn39SwiTvcnX+Y4PtOz3fXzmc90OmFzfXFukmwkFIfdpEdxlhnFWWYAgKioGA7F8c7pfrz8QAXyrTrwdMp/IK7CfWgwKRgDT/GPdVl46ePx7wN4DwvArNLiCX2UqeyxmfLiXSdLVVU1OO2mJPlYJIF8S/I4rec5ZJoE6DiCQrswTQIA7EYOOo7CH0se8m5eaodJ4NZdLPNHRPSNh6d/w8HYnERmZWSgcdVNqRQ1KJtzrBZGZyQ8LiuwGIRUJnPCZuRYIK6Si6fEWqcJUVGZvismGv7YMTpZ2zGaPEOpTPswLSJzgWhYYtbrkFDUJHlYVGDg6Sy5qKjQGBBMJMsBIMPAYSgoYVFGsnsdR1D1oyP5Hf+ycbi30eUG4E47vnQVi3a0Nuk41Bl0yZcLoqKBAtDxyVXKGENcUuEwz35XgbgKk0AhcMnux6MK4pqyeaTx+oPpxnUBaWeEQjlc7shy3esqTfJ+pGcEHCFYV5Z8CMzs7Bfjjl+eY7tvKyDVi5P3W+XzrZAz5AWNDGn3EY1yPYGEOEtu5DnEpJRfGuZEMC4TuzE5s3FZA2PA4BNr/fNa7FOknREtI75/Ikx+JqsadBd1cKPAYzAQZZhHmQZiKjJnXAd9PBCBUc9NzwfOhuYbMwz8G5Ki2CnlGE9xNsAsm3zPlI2mWjPtjAw+sdbPczTU70++S861GDEcik2TEBUV3skIunwhRCUNhzoC6BiNQ1anhrHhkAwDT2DRJ2fkUGcQobhy6AIJo547fHOl0/7kjdV4bGMVqc7PqrQh7Fm170RyPX6KeXV2UZVfOtoz8viSbOt04A6zARFRwccDY2gbmkAgJsFhMTITT4moMPzn6UnWN54g50MyVhWZWXWegVQ7zUkZ9McU/Hf7JKOU2wkAGXr+jZuqFpNaZxaAqb506/IChBKS0OUL/gbAvZdFpF+te1KItH27fyLMlyyyAADahyehqAwdw5P4QlUBCuxmEAISTEh4takL++6dOhxisob3u4Jk+wEvBJ6STl8clTlTjfTFoyMgQGvPMzWnAUBWVfvy3Nlf21YuzsJAILIpVWzz+/frJoqksafebBtgIVHGGy19ON47ihuW5MKo51lhphlzjeUmHcWty+xgjOCxDXm4/+VuvHxiDMf6wnitxa+FI3T6LRNKmaTO/nglqio0hSUunwiA/u21P5JV9c97jpwBpcDDa5fhcyU58PgjJPoZp9c7ZwJwFZjwtVUOvPPNSuw77sOW3/cgKmlbvM9e13NBjwfpPNaTvKdVjeFY7yiTNO1nV4QIAHQ/XXOTxtiwL5xgMUkBTynqChw40O6Z0yYqqdh1eAj/vG6q3/RPiAgmVKYo7KCn0fWbi3WDerapbdgvvtbUi46RAFqHJrD3WAeLy8q5gca6XVeESJG7ub14R9OW3u2uxYGo+PbeDztZy9AE1pfn4XwwjjMjgZR22w54saHUjGU5Ruz4wyC2vNLLgqL6cG9j3eaZuqP/VuPrkvz27rHg6/9z2jP2bseQdyIiNpz9fk3lXHHN+4qcEDKqgRsDgJ4G152FO1rufK/z/G+Pdo9YKnNt5J3TXsQlBWU5U4dBMKGi4aAHbefjqC+2YO2L7YxpaI8p+JK3oa5vTkfuzyf6gPvSjmu+ROZCYUPrQ3od2QrCamRVoxdWJ4yAEAaLkRfDCfWwzBLPXo2by6ty9Q/3nwwFnHUzx/h7dEj8pLvhcx9dFT/XcA1XH38FSPcnaGfJ7/sAAAAASUVORK5CYII="/>
                    </div>

                    <div class="addedDetailContent">
                    <p>{this.props.val.Degree.Degree}</p>
                    <span>{this.props.val.University.University}{this.props.val.Grade.Grade>0 ? ", CGPA - "+this.props.val.Grade.Grade: ""}{this.props.val.PassingYear.PassingYear ? ", Passing year - "+this.props.val.PassingYear.PassingYear: ""}</span>
                    </div>
                    <div class="actionBtnsMobile disNoneDesk disBlockMob">
                        <div class="MoreActions dropdown show">
                        <a class=" dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <svg xmlns="http://www.w3.org/2000/svg" height="20px" viewBox="0 0 24 24" width="20px" fill="#1C1C1C">
                            <path d="M0 0h24v24H0z" fill="none"/><path d="M6 10c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2zm12 0c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2zm-6 0c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2z"/>
                            </svg>
                        </a>
                        
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuLink">
                            <a class="dropdown-item" data-toggle="modal" data-target="#enterDetails" onClick={() => {this.props.editDetails(this.props.id.id);this.props.changeModal(this.props.modal)}}>Edit</a>
                            <a class="dropdown-item" data-toggle="modal" data-target="#deleteEducation" onClick={()=>{localStorage.setItem('deleteData','education');localStorage.setItem('deleteID',this.props.id.id);this.props.changeModal('delete')}}>Delete</a>
                        </div>
                        </div>
                    </div>
                    <div class="actionBtns MobDisNone">
                    <a class="editDetails" data-toggle="modal" data-target="#enterDetails" title="Edit" onClick={() => {this.props.editDetails(this.props.id.id);this.props.changeModal(this.props.modal)}}>
                    <svg xmlns="http://www.w3.org/2000/svg" height="18px" viewBox="0 0 24 24" width="18px" fill="#007BFF">
                        <path d="M0 0h24v24H0z" fill="none"/><path d="M3 17.25V21h3.75L17.81 9.94l-3.75-3.75L3 17.25zM20.71 7.04c.39-.39.39-1.02 0-1.41l-2.34-2.34c-.39-.39-1.02-.39-1.41 0l-1.83 1.83 3.75 3.75 1.83-1.83z"/>
                    </svg>
                    </a>
                    
                    <a data-toggle="modal" data-target="#deleteEducation" class="removeDetail" title="Delete" onClick={()=>{localStorage.setItem('deleteData','education');localStorage.setItem('deleteID',this.props.id.id);this.props.changeModal('delete')}}>
                    <svg xmlns="http://www.w3.org/2000/svg" height="20px" viewBox="0 0 24 24" width="20px" fill="#f67980">
                        <path d="M0 0h24v24H0z" fill="none"/><path d="M6 19c0 1.1.9 2 2 2h8c1.1 0 2-.9 2-2V7H6v12zM19 4h-3.5l-1-1h-5l-1 1H5v2h14V4z"/>
                    </svg>
                    </a>
                </div>
                <div className="modal fade" id="deleteEducation" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div className="modal-dialog modal-dialog-centered multistepModal CreateProfileModal modal-confirm" role="document">
                        <DeleteModal id={(this.props.id)?this.props.id.id:null} onEducationDelete={this.onEducationDelete} data='education'/>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        token: state.token,
        workerID: state.workerID
    }
}

const mapDispatchToProps = dispatch => {
    return {
        editDetails : (id) => dispatch({type : "EDIT_DETAILS", id : id, name:'educationalDetails'}),
        deleteDetails : (id) => dispatch({type : "DELETE_DETAILS", id : id,name:'educationalDetails'}),
        changeModal : (modal)=> dispatch({type:"CHANGE_MODAL", modal : modal}),
        mapDatabaseToLocal : (res) => dispatch({type : "MAP_DATABASE_TO_LOCAl", name:'educationalDetails', res: res})
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CollapsedEducation);