import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import UploadImage from '../../../uploadImage/uploadImage';
import useRecorder from "../../../recorder/useRecorder";
import AudioRecorder from "../../../recorder/audioRecorder"

const CollapsedDocuments=(props)=>{
     
    // let [audioURL, isRecording, startRecording, stopRecording]=useRecorder()
    useEffect(()=>
    {
        let WorkerID=localStorage.getItem("VendorID")?sessionStorage.getItem("WorkerID"):localStorage.getItem("WorkerID")
        axios.get('api/files/worker/'+WorkerID)
                .then(async(res)=>{
                console.log(res.data);
                if(res.data.length!==0)
                {
                  for(var i=0; i< res.data.length; i++){
                    if(res.data[i].filenameContentType.includes("audio")){
                    //   let res1 = await fetch(res.data[i].filenameContentType+', '+res.data[i].audioresume);
                    //   let blob = await res1?.blob();
                    //   this.audioURL = URL.createObjectURL(blob)
                    localStorage.setItem("audioURL",res.data[i].filenameContentType+', '+res.data[i].audioresume)
                    } else if(res.data[i].filenameContentType.includes("video")){
                      
                    } else if(res.data[i].filenameContentType.includes("application")){
                      localStorage.setItem('fileID',res.data[i].id)
                        props.changeState('resume',res.data[i].filenameContentType+', '+res.data[i].filename)
                        let res1 = await fetch(res.data[i].filenameContentType+', '+res.data[i].filename);
                        let blob = await res1?.blob();
                        localStorage.setItem('resumeURL',URL.createObjectURL(blob))
                        let resumeContentType
                     if(res.data[i].filenameContentType.includes('pdf'))
                     {
                        resumeContentType='Pdf'
                     }
                     else if(res.data[i].filenameContentType.includes('docs'))
                     {
                        resumeContentType='Docs'
                     }
                     else{
                        resumeContentType='Doc'
                     }
                     props.changeState('resumeContentType',resumeContentType)
                    }
                  }
                        
                }
                })
    },[])
    
        return(
            <div>
                {/* <div class="flex-1 flex-center">
                    <div class={isRecording ? "btn-record active":"btn-record"} onClick={isRecording ? stopRecording:startRecording}>
                        <i class="icn-record"> <i class="icn-record-inner"></i> 
                        <svg width="14px" height="19px" viewBox="0 0 14 19" version="1.1" xmlns="http://www.w3.org/2000/svg"> <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"> 
                        <path d="M7,12 C8.66,12 9.99,10.66 9.99,9 L10,3 C10,1.34 8.66,0 7,0 C5.34,0 4,1.34 4,3 L4,9 C4,10.66 5.34,12 7,12 Z M12.3,9 C12.3,12 9.76,14.1 7,14.1 C4.24,14.1 1.7,12 1.7,9 L0,9 C0,12.41 2.72,15.23 6,15.72 L6,19 L8,19 L8,15.72 C11.28,15.24 14,12.42 14,9 L12.3,9 Z" id="mic" fill="#FFFFFF" fill-rule="nonzero"></path> </g> 
                        </svg> </i>
                    </div>
                    
                </div>
                <div>
                    <audio src={localStorage.getItem("audioURL")} id="audio" controls >
                        <source  type="audio/mp3"/>
                    </audio>
                    
                </div> */}
                <AudioRecorder />
                {/* <UploadImage type='resume' fileContentType={props.fields.resumeContentType} file={localStorage.getItem("resumeURL")}/> */}
            </div>
            
        )
}
const mapStateToProps = state => {
    return {
        fields: state.fields.fields,
    }
  }
const mapDispatchToProps = dispatch => {
    return {
        changeState : (name,val)=> dispatch({type:"CHANGE_FIELD",name:name,val:val,data : 'fields'}),
        mapDatabaseToLocal : (name,res) => dispatch({type : "MAP_DATABASE_TO_LOCAl", name:name, res: res})
    }
}
export default connect(mapStateToProps,mapDispatchToProps)(CollapsedDocuments)