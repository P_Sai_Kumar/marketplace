import React from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import DeleteModal from '../deleteModal'

class CollapsedRecommendations extends React.Component{

    headers = {
        'Content-Type': 'application/json',
      }
    WorkerID=localStorage.getItem("VendorID")?(this.props.vendor?localStorage.getItem("WorkerID"):sessionStorage.getItem("WorkerID")):localStorage.getItem("WorkerID")
    componentDidMount(){
        axios.get('api/refereces/worker/'+this.WorkerID, {headers : this.headers}).then((res) => 
        {
            this.props.mapDatabaseToLocal(res.data)
        })
    }

    onRecommendationDelete = async (id) => {
        await axios.delete('api/refereces/'+id, {headers : this.headers}).then((res) => console.log(res))


        axios.get('api/refereces/worker/'+this.WorkerID, {headers : this.headers}).then((res) => 
        {
            this.props.mapDatabaseToLocal(res.data)
        })
        // this.props.deleteDetails(this.props.id);
    }
    render (){
        return (
            <div class="addedDetail paddX15 disF">
                    <div class="addedDetailImg">
                        <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAABmJLR0QA/wD/AP+gvaeTAAAICElEQVRoge2aWWxU1xnHf9+5M2Njmy0FAsQ2SyAtjRCJSgMBEhxEiBcSQYR5qJJ0eSiEHQJqSF78UKWIEgyGkK0qUpdUgraJIDZbCxYgIKVASkraAMGAXcJasDHGs9z79WFmzNieGd9rjNpK+b/MnXO+5f+fe+9ZvjPwNf63IF0VaFp5Ta+wWE8qOgHMCGAYaB8gJ2bSCFxBOI3qP9VhXzgU2funVx+s74r8dyWkqOJUBnbmDERfFJgMWB5D2MAuRH6t0vyHbQuGBzvLpVNCCjbWZGbdsOao8IrAwFhzEOUQRvYYOB62nZNZAevi9ezwLYDet/zZIdsZgCMPqWEkqpOAsUAg5v8vVV11u5fzTvUPhzTfcyFF5bVFIroOeBBA4aig70mATZVzBl33Eqtkw7neGmImmFmgj8YInQbmVS7K3+EllmshBRtrMrMbrNWqvBxzPYbwWtXCvO1eEiaFqhStqSsU9GcIo2LE1jsmuNTt4+ZKyHNrztwfwVcJfAe4jejy7AH56zfPFLvz7NujoEx9Wb1q5wNvAJnA4Ug4NHXnsmGXO/LtUEhhec1gS6xdGh2FvsAxpVVL8j5LZV9SfnaEItNUZIpAPjAAUOASKvsN8vbHi3MPpss5taJulOM4m4HhwClH7SnbFw8522khz605c7+Nb39UBJ9k+EzJh/NyryWzfXbVhT62L7IS+D5g0oRVUX5qbF/F1qUDr6YyenbVhT62P1KJ8hhwKhIOTUh3Z1IKKdhYk5lVb+0n+jgdavJnPl09t19jMtspPz/dz+8P7FZ4OI2Atoioyi/tiH/JzmX9byXl8NblnKxI859jYg6rCT6R6p1J+ctl1VvlURH6RYbPTE0loqxMjc8f+KNHEQA+Ef2xzx/aUVp2IpDMoHpuv0Yr7CsBTgHfFc14M1WwpEIK19YWA7OB2zimNNXjBPCXXnXTgfGeJLTG+Kbe3Rek6ty6dOBV1MwEmlHmlqw5/0wyu3ZCSlfXdjOq66Lf9NV0L3YMP3DPOTnUkXnp+qsW534KvAagsK6o4lRGW5t2QhqNzgGGghzLHpj/VnoGKuBM9MQ6GUQHlZSfG5rOpOlG3jrgODBcNDC7bX8rIQUbazKBJQCCLu9onihad7EPSHfPxJMjrZDqMok4IssBUFnW9q60EpJd75sRWzsdcbNEEKvZ6yIxJRwkpyOb7Qtyt4EcAx4QO+P5xL5WQhR9Kfop77pJHmmyHQ9c08IItzs0ElFE34te82Ir//hFyYZzvYFJQDBDI5vdJM90et4mOmvfNYxln3NjZ9uyCQgBT09e8WXPFv/4hROUiYAFeuCjxUNuuAm65Sd9bwJ13ignhXMzh7RLkDh2LMn7N3AI8PkDvifj7S1CjDABQDDVHkn83aN9ewgnPO1BVPcAiOGJeFOLEBX5VvRT/+aRRo1H+yTE+KsXc2NMlGOMMyS+7KoPAYjjnHQbsKxMDfB8h4YdozAWyxXCdoxjjDO0HrW+ARBR65LbgAd71PUC+ru1T4MBsViuYHyB2CpY7mtpS+jPAejR0JB0cZgMV7Mv3QQXw2bHCBpfc9IVcHI0NsQuesRbXN/OZDgya3RYYe/dxIhhTycrKC3bkEQhjQANPXp0OMMmwoi+DjR1gkQcTZY6r3tx8IWyuwMo3GzhkdB/DSDgj/TzErRy4aAjRuQFLz6JMCIvbF08+KgXn0gg3A9A0JYdZqKQ0wC2Y33TKxlbmquAK179gEvdrjdUevayJTpaiTkdb7ojRPgHgKiM8hp324LhQRFWeiakrNxc9nDIq5uYGEd1Po+33ZkQHfZFYzsFngkBFzMvrwVPE9vhpvq8is7kwtGn4A5nSBCS6Tf7gAjIuGnlNa7H9DiOzBodVmOmu7U3jkyvLpOI1zzPrK69D+FxIJwhTnshH87LvYayG8gIiq/UawKAyxkXXU+mjQ25rm0TYRmnFAgIuitxcdt6HjH6GwAhXhb1hu6R/HZ76ZToeyXTcwJVidaJwVHzQWJX642VhDYpXAB9tHhtbaHXPN2dpr73wjaOwoq6olixuy6nvqHVnqmVkG0LhgeNUA6AoysKytTnOkl5zWDbNr9za2/b5oPi9WcGubUvKFOfQVcAqOqbbUe7dksUR4LrgDMIo7r1PJ+2TAMwdXXtY8Vrz71vxDoBjHFLDBhLxPd58ZrzvyiqqBvbkXFWz9oFKCOBUzn1jRva9ictmRaurS02qh8DQQcZs31R3vHE/midN/w9VH4UPwa4awifoWyMhEO/bVvjLS6vewRxDgIZiBQnO8pIWfstXnP+bWA2wkkr7Bu/denAq7Eq+XJgOndOmroaYZCPUHmjanHup0UVX/U1TviAwjCB9ZWL8ucnc0oppHR1bbdbllbHCsiHgKNEy6h3tWL2AAd4H3gEGKPowds9nUmptsRpjxWK36rpT9g6AAzpep6ecMZHZNyWRUNTzj1pf92quUMuRmxrYuxc77+FGstyJqcTAS4ek52vPFBrWUwEPukyai6h6EH89rit8wd3WOBw9bxvmZ9/oamnXQC8SxcV5DqAAhswoaeq5g656MbB8/F08draQlQ3cO/emy9BX65aNGiXFyfPI1DVwrztaoIjiFbtv/LqnwoKFxBdlH3j5re9ioC7/AtHadmJwK3ePWao6ksSrRv7PYYIAbtRfnUp6/Lvj8waHe4sly77U03JhnO9naBMFPRxREYCg4keTcf3NjeI3sGzihwX0YPi171e/y3xNf5f8B9yaO+LOAmK0QAAAABJRU5ErkJggg=="/>
                    </div>

                    <div class="addedDetailContent">
                    <p>{this.props.val.Name.Name}</p>
                    <span>{this.props.val.Email.Email} {this.props.val.PhoneNumber.PhoneNumber}</span>
                    </div>
                    <div class="actionBtnsMobile disNoneDesk disBlockMob">
                        <div class="MoreActions dropdown show">
                        <a class=" dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <svg xmlns="http://www.w3.org/2000/svg" height="20px" viewBox="0 0 24 24" width="20px" fill="#1C1C1C">
                            <path d="M0 0h24v24H0z" fill="none"/><path d="M6 10c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2zm12 0c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2zm-6 0c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2z"/>
                            </svg>
                        </a>
                        
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuLink">
                            <a class="dropdown-item" data-toggle="modal" data-target="#enterDetails" onClick={() => {this.props.editDetails(this.props.id.id);this.props.changeModal(this.props.modal)}}>Edit</a>
                            <a class="dropdown-item" data-toggle="modal" data-target="#deleteRecommendation" onClick={()=>{localStorage.setItem('deleteData','recommendation');localStorage.setItem('deleteID',this.props.id.id);this.props.changeModal('delete')}}>Delete</a>
                        </div>
                        </div>
                    </div>
                    <div class="actionBtns MobDisNone">
                    <a class="editDetails" data-toggle="modal" data-target="#enterDetails" onClick={() => {this.props.editDetails(this.props.id.id);this.props.changeModal(this.props.modal)}} title="Edit">
                    <svg xmlns="http://www.w3.org/2000/svg" height="18px" viewBox="0 0 24 24" width="18px" fill="#007BFF">
                        <path d="M0 0h24v24H0z" fill="none"/><path d="M3 17.25V21h3.75L17.81 9.94l-3.75-3.75L3 17.25zM20.71 7.04c.39-.39.39-1.02 0-1.41l-2.34-2.34c-.39-.39-1.02-.39-1.41 0l-1.83 1.83 3.75 3.75 1.83-1.83z"/>
                    </svg>
                    </a>
                    
                    <a data-toggle="modal" data-target="#deleteRecommendation" class="removeDetail" title="Delete" onClick={()=>{localStorage.setItem('deleteData','recommendation');localStorage.setItem('deleteID',this.props.id.id);this.props.changeModal('delete')}}>
                    <svg xmlns="http://www.w3.org/2000/svg" height="20px" viewBox="0 0 24 24" width="20px" fill="#f67980">
                        <path d="M0 0h24v24H0z" fill="none"/><path d="M6 19c0 1.1.9 2 2 2h8c1.1 0 2-.9 2-2V7H6v12zM19 4h-3.5l-1-1h-5l-1 1H5v2h14V4z"/>
                    </svg>
                    </a>
                </div>
                <div className="modal fade" id="deleteRecommendation" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div className="modal-dialog modal-dialog-centered multistepModal CreateProfileModal modal-confirm" role="document">
                        <DeleteModal id={(this.props.id)?this.props.id.id:null} onRecommendationDelete={this.onRecommendationDelete} data='recommendation'/>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        token: state.token // edit : state.workDetails.edit,
    }
}

const mapDispatchToProps = dispatch => {
    return {
        editDetails : (id) => dispatch({type : "EDIT_DETAILS", id : id, name:'recommendations'}),
        deleteDetails : (id) => dispatch({type : "DELETE_DETAILS", id : id,name:'recommendations'}),
        changeModal : (modal)=> dispatch({type:"CHANGE_MODAL", modal : modal}),
        mapDatabaseToLocal : (res) => dispatch({type : "MAP_DATABASE_TO_LOCAl", name:'recommendations', res: res})
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CollapsedRecommendations);