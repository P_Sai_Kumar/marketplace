import React from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import Certification from '../../../../imgaes/certification.png'
import DeleteModal from '../deleteModal'
import deleteModal from '../deleteModal';
class CollapsedCertifications extends React.Component{

    headers = {
        'Content-Type': 'application/json',
      }
    WorkerID=localStorage.getItem("VendorID")?sessionStorage.getItem("WorkerID"):localStorage.getItem("WorkerID")
    componentDidMount(){
        axios.get('api/certificates/worker/'+this.WorkerID, {headers : this.headers}).then((res) => 
        {
            this.props.mapDatabaseToLocal(res.data)
        })
    }

    onCertificateDelete = async (id) => {
        await axios.delete('api/certificates/'+id, {headers : this.headers}).then((res) => console.log(res))


        axios.get('api/certificates/worker/'+this.WorkerID, {headers : this.headers}).then((res) => 
        {
            this.props.mapDatabaseToLocal(res.data)
        })
    }

    render (){
        return (
            <div class="addedDetail paddX15 disF">
                    <div class="addedDetailImg">
                    {/* <img src={Certification}></img> */}
                     <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAABmJLR0QA/wD/AP+gvaeTAAAInklEQVRoge2Ye4xcdRXHP+d37507z85Ou93tW0JpoS1CfCWopVhQSXwQESUhKoWEGGOQgJLoHxgrkWiipBIV4yPxjfEFBgJGBS2WxFcQ2tIHYCmwpfve7c7Mzty5j9/xjzuzu6W0bHfX//ab3Oz+7u/cc77fc87vkYFFLGIRi1jEIhaxiEX8vyCdf/78eG1Tb8Xf1tvrNYEBVfpFGOrtlcGzcagP3LGebParGCm+toGtEfIFueqLR8/G7+Cg9qrSI8JKYMUrQ638RC3cvf0dSw6fJOTRPfWnjLBp83mFqgjLZ/iIgCFgQIR+YFCV4yIMqXIcGAQGkoT+lU98OaZSrOK4/hlZJVHA+GS5f+uXXMdJiQG9Iqxqk10F9KpOzfUA3lQulMGD/52sqLL/ikuLb50S8pc99YsVno7jGJf6sxduXn3+2WSrA7/e91z5pfs3TgU0Lo1Vb6Nw7O9YL0/9nMvIv/Jv3MYIE2/48HOt4tqNZ/J3Ojxz6PjhmOIFruOgysXv3lbcZ9KIXA4wWa/TP1RdNhfnAGqTNGthA4Iq0hjD738agioENcLyOdTXbQXVads5oH9gonuyXgNATMrdTUdsAWg2G9TrzfJcA2CA6gBEzalX7uRoe2qUyp6voW4emiOp7RxRqze6xHiUy12guhnaQhS5AJQwjGi1Wp61tmWMOXOfvwbiTHdrpohXw2mMAWPTtnNAHNsgDMNsFIYACGYTTOVFl6VGEQBhmNTnEgTHsyeNrcCkSR8rZ7adJZpBqw4QxTEAii6FTmtBCcDa1HcYxa1sds4tnJLu82DYBW0LEIWeGNaE82qrVisJARKbpG7b3DtCCjBDSBiHZxsgtooODa/gmRyEAklbgGs1NTDCoAcjLmSU1obhFdK7GtfIGbyeClUbAWibq54kREmY4U9EzpgzBawqVsEqTNRimoHFDzLF1U0DjirL4yaXD3pUgrS047mIx3oixp0cTSOjQabYGgrJZQ3lkosR2o/wOtJMm2NnHE8LMdRQlhmT8jcZpxjEFqtt0laxM8jPRCuwjA2na8vPjyrX9SV0xQ6uybMkD34hNVwWefS84hFbOOEm2FGtVQvUqhCr4mdPzl1HlAGMScUZAStSAuhwFahNC2kPHMcBILJSsdHs1mK9mvbq0mJfeFH5Tp9lRfBL0wYd4b4Hy9s7eyVyLhq709krO8PRyTWZejU5RUhabZ0etJEYtzxTiEIV2mVqXzVwPQ8RwbjOrBpXFcLQIgJbSt/w6F4Cfiad7DOwx4WHvfTZ46bvALIedC9hU/nrnkjqQ/X0cWYik3FFRPC8NI5AP7QrIsJBlCt936dQyAcikp2NU9vuPd8PrJcJDJlyWoGnHDhhWkfG3v7ygX1XrgLYctEfjq+P/7mOYfF5UwIZl4w3KX6mZYOWb6xVnFnkT4xQKOSDjJ/JAigcmBKCZR8C+XwB36MJzEpIbSJtq7w3HOOaNEUvGHTCNB/8610aNAobOmk7+Px1Gw48fdXkB6+4w8pRzXGuBdeQ94bioLU2U5tI6FrqnjbWTFS6Ck3HK6RChP3QaS3fPgSEhUKB1euWzWqXTxIlaCQ4Dmwp7XIp5dNqHHF48sg1A0GjkO/YLlkLl3weet9ZKOw9+qF+jjipbSnPltIu13EgaCQkyez6a/W6blMoFABCJ7IPt3OV4rEn6j/L+XJF1/LMShHUEQmBJnDCGBkXtF+QY5rwovH0UNB0jmxaL/t1YMcthNE99HTBpKBPePa+P+4yrgHHgyQEETj/IxAHcPhB+Nh7b7VcGhkKwNAJyHo3S89PvnPoiL4xm0vW20g2icM5iq5RZKW1WgG6gFyimlFFToxE/UHD/unybcUbYHrXwkSFG0vd4eaS4x5bu1bGZpUaANUKnT3dgoLWImX5EmHDVXDo1+mmcPg3qUktVKwYNVYATVXGVAA2rZf9wH7g92cK2denS+OCXd2q5Q513k0J2b5dYmDfrAV0YHiAMN6JVciBseq4Xs2OVEsm/JXQObgThVqkuJmqNYl1yLdP0zCGPPefTch2ok9K9jxuPSmk96f7yHp7qTXTtKywvH/rj2vNBIYCZXjG00zgA1t/VGOVBQeoNSHr7ZWVvzg4Xx7zFgKAZ3YRpqc7mxNWLnmhfPW2H064TotYIVZwnRZXb/vBxIry0TKb24dtGKXfLgDO7sZ2Gujw9Ruptp5lRQUcAwGwz0FHjTa03ASlINUcy6xwsQVfIbEwMA4ZZ6Osve/5+XJYECEA2vfxAwibWVpiamE0BCbai7oM5GdcOcZqYDko636+ZSHiz+4Emg0e6elh63CdcLxI3gfXgYIP+bYoVai3IE6g0YLAq7Onp2ehwi9Ma/3u1jcz4jxJw0B3GNEd1OkNDZWgjN/OVSuGsewEQxnLSLbISMYjb6E7eYtc883/zJfDwlTE6m10xZB4MO557C9UAFjdiji/PoECh/Nd9OfS6++aEHwLXTFYbgM+MV8K866I/vLmVbjOUSC9aw27UHcgkPTv0mQ3AGPOuyjZVEAxgeVxx0WEsefKNd86Nh8e899+PeczdEREAi0DnkLJwqo4RuVGyO1gRRRTTNK5lklt2x6w8un50phXRfShT+YJci8D6Y96oy40Z+QmZ3fLp+7eDqDfu303Dblsai5vYelUVcbRaJ1ce+/cfr1hvhUJcjfQEZGYl04SYVBcbpgaO7oDw/T1tmHSb1JUEO/6+VCZsxDdudMAt6CyF7gWc2w9rgZTBr59XG66u0MUuenul8jq36bmXW1xuHwexrwH9F/ArW2fc8Lcd60Lx9YDn5WP3vNI55V+/3PfJZbbMKr43o2nfuRcj0lexIrg672yc2cMPAo8qr+95X1tn3M65RfsZAdQxfDt2yfx9B+dtXGKzfdu303IJYwWi20hC4KFuTS2IYLFS+46aW28Go7uwNevLKSIRSxiEa+P/wEpiqiq6SMyUAAAAABJRU5ErkJggg=="/>   
                    </div>

                    <div class="addedDetailContent">
                    <p>{this.props.val.Name.Name}</p>
                    <span>{this.props.val.Issuer.Issuer}</span>
                    </div>
                    <div class="actionBtnsMobile disNoneDesk disBlockMob">
                        <div class="MoreActions dropdown show">
                        <a class=" dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <svg xmlns="http://www.w3.org/2000/svg" height="20px" viewBox="0 0 24 24" width="20px" fill="#1C1C1C">
                            <path d="M0 0h24v24H0z" fill="none"/><path d="M6 10c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2zm12 0c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2zm-6 0c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2z"/>
                            </svg>
                        </a>
                        
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuLink">
                            <a class="dropdown-item" data-toggle="modal" data-target="#enterDetails" onClick={() => {this.props.editDetails(this.props.id.id);this.props.changeModal(this.props.modal)}}>Edit</a>
                            <a class="dropdown-item" data-toggle="modal" data-target="#deleteCertificate" onClick={()=>{localStorage.setItem('deleteData','certificate');localStorage.setItem('deleteID',this.props.id.id);this.props.changeModal('delete')}}>Delete</a>
                        </div>
                        </div>
                    </div>
                    <div class="actionBtns MobDisNone">
                    <a class="editDetails" data-toggle="modal" data-target="#enterDetails" onClick={() => {this.props.editDetails(this.props.id.id);this.props.changeModal(this.props.modal)}} title="Edit">
                    <svg xmlns="http://www.w3.org/2000/svg" height="18px" viewBox="0 0 24 24" width="18px" fill="#007BFF">
                        <path d="M0 0h24v24H0z" fill="none"/><path d="M3 17.25V21h3.75L17.81 9.94l-3.75-3.75L3 17.25zM20.71 7.04c.39-.39.39-1.02 0-1.41l-2.34-2.34c-.39-.39-1.02-.39-1.41 0l-1.83 1.83 3.75 3.75 1.83-1.83z"/>
                    </svg>
                    </a>
                    
                    <a data-toggle="modal" data-target="#deleteCertificate" class="removeDetail" title="Delete" onClick={()=>{localStorage.setItem('deleteData','certificate');localStorage.setItem('deleteID',this.props.id.id);this.props.changeModal('delete')}}>
                    <svg xmlns="http://www.w3.org/2000/svg" height="20px" viewBox="0 0 24 24" width="20px" fill="#f67980">
                        <path d="M0 0h24v24H0z" fill="none"/><path d="M6 19c0 1.1.9 2 2 2h8c1.1 0 2-.9 2-2V7H6v12zM19 4h-3.5l-1-1h-5l-1 1H5v2h14V4z"/>
                    </svg>
                    </a>
                </div>
                 
                <div className="modal fade" id="deleteCertificate" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div className="modal-dialog modal-dialog-centered multistepModal CreateProfileModal modal-confirm" role="document">
                        <DeleteModal id={(this.props.id)?this.props.id.id:null} onCertificateDelete={this.onCertificateDelete} data='certificate'/>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        token: state.token,
        deleteModalSelected:state.deleteModalSelected
    }
}

const mapDispatchToProps = dispatch => {
    return {
        editDetails : (id) => dispatch({type : "EDIT_DETAILS", id : id, name:'certifications'}),
        deleteDetails : (id) => dispatch({type : "DELETE_DETAILS", id : id,name:'certifications'}),
        changeModal : (modal)=> dispatch({type:"CHANGE_MODAL", modal : modal}),
        mapDatabaseToLocal : (res) => dispatch({type : "MAP_DATABASE_TO_LOCAl", name:'certifications', res: res})
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CollapsedCertifications);