import React from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import Workplace from '../../../../imgaes/Workplace.png'
import DeleteModal from '../deleteModal'

class CollapsedWorkDetails extends React.Component{

    headers = {
        'Content-Type': 'application/json',
      }
    WorkerID=localStorage.getItem("VendorID")?sessionStorage.getItem("WorkerID"):localStorage.getItem("WorkerID")
    componentDidMount(){
        axios.get('api/employments/worker/'+this.WorkerID, {headers : this.headers}).then((res) => 
        {
            this.props.mapDatabaseToLocal(res.data)
        })
    }

    onWorkDetailDelete = async (id) => {
        await axios.delete('api/employments/'+id, {headers : this.headers}).then((res) => console.log(res))


        axios.get('api/employments/worker/'+this.WorkerID).then((res) => 
        {
            this.props.mapDatabaseToLocal(res.data)
        })
    }

    render (){
        return (
            <div class="addedDetail paddX15 disF">
                    <div class="addedDetailImg">
                        {/* <img src={Workplace}></img> */}
                        <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAABmJLR0QA/wD/AP+gvaeTAAAAbklEQVRIiWNgGAUEACMyx2vCo//EaNpWIMdIWBUEMJHqIlIBCzZBmAthPiLFxehgYHyAKy7IiSOa+wCrBdsK5BjxhTu6PD71gysOcMnjUz8aBwTB0I8DFB8QW+bgKquw+YTmPiCplByUZdEoIAgAB5M9EE6djkIAAAAASUVORK5CYII="/>
                    </div>

                    <div class="addedDetailContent">
                    <p>{this.props.val.Designation.Designation}</p>
                    <span>{this.props.val.EmployerName.EmployerName}, {(this.props.val.StartDate.StartDate!==null)&&this.props.val.StartDate.StartDate.split("-")[0]} - {this.props.val.CurrentlyWorking.CurrentlyWorking===false?(this.props.val.EndDate.EndDate!==null)&&this.props.val.EndDate.EndDate.split("-")[0]:'currently working'}</span>
                    </div>
                    <div class="actionBtnsMobile disNoneDesk disBlockMob">
                        <div class="MoreActions dropdown show">
                        <a class=" dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <svg xmlns="http://www.w3.org/2000/svg" height="20px" viewBox="0 0 24 24" width="20px" fill="#1C1C1C">
                            <path d="M0 0h24v24H0z" fill="none"/><path d="M6 10c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2zm12 0c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2zm-6 0c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2z"/>
                            </svg>
                        </a>
                        
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuLink">
                            <a class="dropdown-item" data-toggle="modal" data-target="#enterDetails" onClick={() => {this.props.editDetails(this.props.id.id);this.props.changeModal(this.props.modal)}}>Edit</a>
                            <a class="dropdown-item" data-toggle="modal" data-target="#deleteWorkDetail" onClick={()=>{localStorage.setItem('deleteData','workDetail');localStorage.setItem('deleteID',this.props.id.id);this.props.changeModal('delete')}}>Delete</a>
                        </div>
                        </div>
                    </div>
                    <div class="actionBtns MobDisNone">
                    <a class="editDetails" data-toggle="modal" data-target="#enterDetails" onClick={() => {this.props.editDetails(this.props.id.id);this.props.changeModal(this.props.modal)}} title="Edit">
                    <svg xmlns="http://www.w3.org/2000/svg" height="18px" viewBox="0 0 24 24" width="18px" fill="#007BFF">
                        <path d="M0 0h24v24H0z" fill="none"/><path d="M3 17.25V21h3.75L17.81 9.94l-3.75-3.75L3 17.25zM20.71 7.04c.39-.39.39-1.02 0-1.41l-2.34-2.34c-.39-.39-1.02-.39-1.41 0l-1.83 1.83 3.75 3.75 1.83-1.83z"/>
                    </svg>
                    </a>
                    
                    <a data-toggle="modal" data-target="#deleteWorkDetail" class="removeDetail" title="Delete" onClick={()=>{localStorage.setItem('deleteData','workDetail');localStorage.setItem('deleteID',this.props.id.id);this.props.changeModal('delete')}}>
                    <svg xmlns="http://www.w3.org/2000/svg" height="20px" viewBox="0 0 24 24" width="20px" fill="#f67980">
                        <path d="M0 0h24v24H0z" fill="none"/><path d="M6 19c0 1.1.9 2 2 2h8c1.1 0 2-.9 2-2V7H6v12zM19 4h-3.5l-1-1h-5l-1 1H5v2h14V4z"/>
                    </svg>
                    </a>
                </div>
                <div className="modal fade" id="deleteWorkDetail" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div className="modal-dialog modal-dialog-centered multistepModal CreateProfileModal modal-confirm" role="document">
                        <DeleteModal id={(this.props.id)?this.props.id.id:null} onWorkDetailDelete={this.onWorkDetailDelete} data='workDetail'/>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        // edit : state.workDetails.edit,
        token: state.token
    }
}

const mapDispatchToProps = dispatch => {
    return {
        editDetails : (id) => dispatch({type : "EDIT_DETAILS", id : id, name:'workDetails'}),
        deleteDetails : (id) => dispatch({type : "DELETE_DETAILS", id : id,name:'workDetails'}),
        changeModal : (modal)=> dispatch({type:"CHANGE_MODAL", modal : modal}),
        mapDatabaseToLocal : (res) => dispatch({type : "MAP_DATABASE_TO_LOCAl", name:'workDetails', res: res})
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CollapsedWorkDetails);