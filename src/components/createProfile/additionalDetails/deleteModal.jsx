import React from "react";
import { connect } from 'react-redux';
import axios from 'axios';
class DeleteModal extends React.Component {
    headers = {
        'Content-Type': 'application/json',
      }
    WorkerID=localStorage.getItem("VendorID")?sessionStorage.getItem("WorkerID"):localStorage.getItem("WorkerID")
    onDelete=async(id,data)=>{
        if(data==='certificate')
        {
            await axios.delete('api/certificates/'+id).then((res) => console.log(res))


            axios.get('api/certificates/worker/'+this.WorkerID).then((res) => 
            {
                this.props.mapDatabaseToLocal(res.data,'certifications')
            })
        }
        else if(data==='education')
        {
            await axios.delete('api/educations/'+id, {headers : this.headers}).then((res) => console.log(res))

            await axios.get('api/educations/worker/'+this.WorkerID, {headers : this.headers}).then((res) => 
            {
                this.props.mapDatabaseToLocal(res.data,'educationalDetails')
            })
        }
        else if(data==='workDetail')
        {
            await axios.delete('api/employments/'+id, {headers : this.headers}).then((res) => console.log(res))


            axios.get('api/employments/worker/'+this.WorkerID).then((res) => 
            {
                this.props.mapDatabaseToLocal(res.data,'workDetails')
            })
        }
        else if(data==='recommendation')
        {
            await axios.delete('api/refereces/'+id, {headers : this.headers}).then((res) => console.log(res))


            axios.get('api/refereces/worker/'+this.WorkerID, {headers : this.headers}).then((res) => 
            {
                this.props.mapDatabaseToLocal(res.data,'recommendations')
            })
        }
    }
    onSkillDelete=async(id,item,data)=>{
        if (data === "skill"){
            await axios.patch('api/workers/skillsremove/'+this.WorkerID, item ,{headers : {"Content-Type":"application/merge-patch+json"}}).then((res) => console.log(res))


            axios.get('api/skills-masters/worker/'+this.WorkerID, {headers : this.headers}).then((res) => 
            {
                this.props.mapDatabaseToLocal(res.data,'skills')
            })
        } else {
            await axios.delete('api/portfolios/'+id, {headers : this.headers}).then((res) => console.log(res))


            axios.get('api/portfolios/worker/'+this.WorkerID, {headers : this.headers}).then((res) => 
            {
                this.props.mapDatabaseToLocal(res.data,'portfolio')
            })

        }
    }
    render(){
        return(
            <span>
                <div class="modal-content">
                    <div class="modal-header d-flex">
                    <h5 className="modal-title" id="exampleModalLongTitle">Confirm Delete</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body" style={{textAlign:'center'}}>
                        <div class="icon-box">
                        <svg xmlns="http://www.w3.org/2000/svg" version="1.1" width="50" height="50" viewBox="0 0 256 256">
                        <g transform="translate(128 128) scale(0.72 0.72)">
                            <g style={{stroke: 'none', strokeWidth: 0, strokeDasharray: 'none', strokeLinecap: 'butt', strokeLinejoin: 'miter', strokeMiterlimit: 10, fill: 'none', fillRule: 'nonzero', opacity: 1}} transform="translate(-175.05 -175.05000000000004) scale(3.89 3.89)" >
                            <path d="M 24.005 66.995 c -0.256 0 -0.512 -0.098 -0.707 -0.293 c -0.391 -0.391 -0.391 -1.023 0 -1.414 l 41.99 -41.99 c 0.391 -0.391 1.023 -0.391 1.414 0 s 0.391 1.023 0 1.414 l -41.99 41.99 C 24.517 66.897 24.261 66.995 24.005 66.995 z" style={{stroke: 'none', strokeWidth: 1, strokeDasharray: 'none', strokeLinecap: 'butt', strokeLinejoin: 'miter', strokeMiterlimit: 10, fill: 'rgb(236,0,0)', fillRule: 'nonzero', opacity: 1}} transform=" matrix(1 0 0 1 0 0) " stroke-linecap="round" />
                            <path d="M 65.995 66.995 c -0.256 0 -0.512 -0.098 -0.707 -0.293 l -41.99 -41.99 c -0.391 -0.391 -0.391 -1.023 0 -1.414 s 1.023 -0.391 1.414 0 l 41.99 41.99 c 0.391 0.391 0.391 1.023 0 1.414 C 66.507 66.897 66.251 66.995 65.995 66.995 z" style={{stroke: 'none', strokeWidth: 1, strokeDasharray: 'none', strokeLinecap: 'butt', strokeLinejoin: 'miter', strokeMiterlimit: 10, fill: 'rgb(236,0,0)', fillRule: 'nonzero', opacity: 1}} transform=" matrix(1 0 0 1 0 0) " stroke-linecap="round" />
                            <path d="M 45 90 C 20.187 90 0 69.813 0 45 C 0 20.187 20.187 0 45 0 c 24.813 0 45 20.187 45 45 C 90 69.813 69.813 90 45 90 z M 45 2 C 21.29 2 2 21.29 2 45 c 0 23.71 19.29 43 43 43 c 23.71 0 43 -19.29 43 -43 C 88 21.29 68.71 2 45 2 z" style={{stroke: 'none', strokeWidth: 1, strokeDasharray: 'none', strokeLinecap: 'butt', strokeLinejoin: 'miter', strokeMiterlimit: 10, fill: 'rgb(236,0,0)', fillRule: 'nonzero', opacity: 1}} transform=" matrix(1 0 0 1 0 0) " stroke-linecap="round" />
                        </g>
                        </g>
                        </svg>
                        </div>						
                        <h4 class="modal-title w-100">Are you sure?</h4><br/>	
                        <p>Do you really want to delete this record? This process cannot be undone.</p>
                    </div>
                    <div class="modal-footer justify-content-center">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal" onClick={() =>{
                            if(localStorage.getItem('deleteData')==='education')
                            {
                                this.onDelete(Number(localStorage.getItem('deleteID')),'education')
                            }
                            else if(localStorage.getItem('deleteData')==='workDetail')
                            {
                                this.onDelete(Number(localStorage.getItem('deleteID')),'workDetail')
                            }
                            else if(localStorage.getItem('deleteData')==='certificate')
                            {
                                this.onDelete(Number(localStorage.getItem('deleteID')),'certificate')
                            }
                            else if(localStorage.getItem('deleteData')==='skill'||localStorage.getItem('deleteData')==='portfolio')
                            {
                                this.onSkillDelete(Number(localStorage.getItem('deleteID')),JSON.parse(localStorage.getItem('deleteItem')),localStorage.getItem('deleteData'))
                            }
                            else if(localStorage.getItem('deleteData')==='recommendation')
                            {
                                this.onDelete(Number(localStorage.getItem('deleteID')),'recommendation')
                            }
                        }}>Delete</button>
                    </div>
                </div>
            </span>
        )
    }
}
const mapStateToProps = state => {
    return {
        deleteModalSelected : state.deleteModalSelected
    }
}

const mapDispatchToProps = dispatch => {
    return {
        changeModal : (modal)=> dispatch({type:"CHANGE_MODAL", modal : modal}),
        mapDatabaseToLocal : (res,name) => dispatch({type : "MAP_DATABASE_TO_LOCAl", name:name, res: res})
    }
}
export default connect(mapStateToProps,mapDispatchToProps)(DeleteModal)