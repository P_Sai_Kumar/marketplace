import React from 'react';
import { connect } from 'react-redux';
import axios from 'axios';

import CollapsedSkills from './collapsedDetails/collapsedSkills';


class DetailsPanel2 extends React.Component{

    handleKeyPress = async (event) => {
        let WorkerID=localStorage.getItem("VendorID")?(this.props.vendor?localStorage.getItem("WorkerID"):sessionStorage.getItem("WorkerID")):localStorage.getItem("WorkerID")
        if (event.key === 'Enter') {
            console.log("Entered")

            var data = null

            const headers = {
                'Content-Type': 'application/json',
              }
            if(this.props.data === "skills"){
                data = {
                    skillName: event.target.value,
                    worker : {
                        id:  WorkerID,
                    }
                }            
                

                await axios.post('api/skills-masters', data, {headers : headers})
                .then(async(response) => 
                {console.log(response); await axios.patch("api/worker/skills/"+WorkerID,
                 data=response.data, {headers : {"Content-Type" : "application/merge-patch+json"}})}).catch((e) => console.log(e))

                axios.get('api/skills-masters/worker/'+WorkerID).then((res) => 
                {
                    console.log(res)
                    this.props.mapDatabaseToLocal(res.data, this.props.data)
                })

            } else {
                data = {
                    portfolioURL: event.target.value,
                    worker : {
                        id: WorkerID,
                    }
                }

                await axios.post('api/portfolios', data, {headers : headers})
                .then((response) => {console.log(response)}).catch((e) => console.log(e))

                axios.get('api/portfolios/worker/'+WorkerID).then((res) => 
                {
                    this.props.mapDatabaseToLocal(res.data, this.props.data)
                })
            }
            // this.props.addDetails(event.target.value, this.props.data) 
            this.refs.reset.value = '';

          }
    }


    render(){

        var variable = null

        if(this.props.data === "skills") {
            variable = this.props.skill.map((item, id) => (<CollapsedSkills item={item} key={id} id={item.id} data={this.props.data} vendor={this.props.vendor}/>))
        } else {
            variable = this.props.portfolio.map((item, id) => (<CollapsedSkills item={item.item} key={id} id={item.id} data={this.props.data}/>))
        }
        

        return (
            
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id={this.props.id} >
                    <h4 class="panel-title" data-toggle="collapse" data-parent="#accordion" href={'#'+this.props.href} aria-expanded="false" aria-controls={this.props.href}>
                    <a class="collapsed">
                        {this.props.title}
                    </a>
                    </h4>

                    <a class="addDetails" data-toggle="collapse" data-parent="#accordion" href={'#'+this.props.href} aria-expanded="false" aria-controls={this.props.href}>
                    <svg xmlns="http://www.w3.org/2000/svg" height="15px" viewBox="0 0 24 24" width="15px" fill="#007BFF">
                        <path d="M0 0h24v24H0z" fill="none"/><path d="M19 13h-6v6h-2v-6H5v-2h6V5h2v6h6v2z"/>
                    </svg>
                    </a>
        
                </div>
                <div id={this.props.href} class="panel-collapse collapse" role="tabpanel" aria-labelledby={this.props.id}>
                    <div class="panel-body">
                    <div class="AddSkillsTextbox paddX15" id="AddSkills">
                        <input type="text" class="form-control" id="inputAddPortfolio" placeholder={this.props.placeholder} onKeyPress={this.handleKeyPress} ref="reset"/>
                    </div>
                    {variable}
                    </div>
                </div>
                </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        skill : state.skills.skills,
        portfolio : state.portfolio.portfolio,
        token : state.token
    }
}

const mapDispatchToProps = dispatch => {
    return {
        addDetails : (fields, data) => dispatch({type : "ADD_DETAILS", val : fields, data : data}),
        mapDatabaseToLocal : (res, name) => dispatch({type : "MAP_DATABASE_TO_LOCAl", name: name, res: res})
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(DetailsPanel2);