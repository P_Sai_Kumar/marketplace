import React from 'react';
import './createprofile.css';
import { connect } from 'react-redux';
import { compose } from "redux";
import { Link, Redirect, useHistory, withRouter } from 'react-router-dom';
import axios from 'axios';
import $ from 'jquery';

import UploadImage from '../uploadImage/uploadImage';
import Input from '../input/input';
import SideNav from './sideNav';
import Header from '../header/Header';
import Location from '../input/location'
import ProgressBar from './ProgressBar';

class BasicDetails extends React.Component{

    state = {
        sub_options : []
    }
    subCategoryIDs = []
    CategoryOptions = []
    CategoryIDs =[]
    WorkerID=null
    handleValidation(value, rules){
        let isValid = true;
        if (rules.required) {
            isValid = value.trim() !== "" && isValid;
        }

        if (rules.name) {
            isValid = /^[A-Za-z\s]+$/.test(value) && isValid;
        }

        if (rules.email) {
            isValid = /^[^@\s]+@[^@\s]+\.[^@\s]+$/.test(value) && isValid;
        }

        if (rules.mobile) {
            isValid = /^([0-9]{10})$/.test(value) && isValid;
        }

        if (rules.select) {
            isValid = value !== "Select" && isValid;
        }
        return isValid
    }

    handleChange(field, rules, event)
    {
        if(event===null)
        {
            this.props.changeState(field,'')
            return
        }
        let k = this.handleValidation(event.target.value, rules)
        if (k){
            this.props.changeErrorState(field, true)
        } else {
            this.props.changeErrorState(field, false)
        }
        
        if(field === 'profilePic')
        {
            var reader = new FileReader();
            reader.readAsDataURL(event.target.files[0]);
            reader.onloadend =  async()=> {
            var base64String = reader.result;
            console.log(base64String.split(",")[1]);
            this.props.changeState(field,base64String)
            }
        }
        else{
            var obj = {}
            obj[field] = event.target.value 
            obj['inValid'] = !k
            this.props.changeState(field, obj)
        }
    }

    componentWillMount=async()=>{
        this.WorkerID=localStorage.getItem("VendorID")?sessionStorage.getItem('WorkerID'):localStorage.getItem("WorkerID")
        if(localStorage.getItem('newWorkerID'))
        {
            this.WorkerID=localStorage.getItem('newWorkerID')
            sessionStorage.setItem('WorkerID',localStorage.getItem('newWorkerID'))
            localStorage.removeItem('newWorkerID')
        }
        try{
            $('.modal-backdrop').hide()
            // if (sessionStorage.getItem('reloadCount')){
            //         const reloadCount = Number(sessionStorage.getItem('reloadCount'));
            //         if(reloadCount < 1) {
            //             sessionStorage.setItem('reloadCount', String(reloadCount + 1));
            //             window.location.reload();
            //             sessionStorage.removeItem('reloadCount');
            //         } else {
            //             sessionStorage.removeItem('reloadCount');
            //         }
            //     }
          } catch(err){
  
          }
        if (this.WorkerID !== null){
                await axios.get('api/workers/profile/'+this.WorkerID, {headers : this.headers}).then((res) => 
                {
                console.log(res)
                this.props.mapDatabaseToLocal("fields",res.data.worker,res.data.category[res.data.category.length-1])
                localStorage.getItem("VendorID")?sessionStorage.setItem("subCat",JSON.stringify(res.data.category[res.data.category.length-1])):localStorage.setItem("subCat",JSON.stringify(res.data.category[res.data.category.length-1]))
                if(res.data.photo) {localStorage.getItem("VendorID")?sessionStorage.setItem('photoID',res.data.photo.id):localStorage.setItem('photoID',res.data.photo.id) 
                this.props.changeState('profilePic',res.data.photo.picContentType+', '+res.data.photo.pic) }
                })
        } else {
            if(localStorage.getItem("VendorID"))
            {
                await axios.get('api/workers/profile/'+localStorage.getItem("WorkerID"), {headers : this.headers}).then((res) => 
                {
                    console.log(res)
                    this.props.mapDatabaseToLocal("fields",res.data.worker,res.data.category[res.data.category.length-1])
                    //this.props.changeState('Email',{Email : '', inValid : false})
                    // this.props.changeState('PhoneNumber',{PhoneNumber : '', inValid : false})
                    sessionStorage.setItem("subCat",JSON.stringify(res.data.category[res.data.category.length-1]))
                })
            }
            else{
                this.props.getEmail()
            }
        }

        axios.get('api/allcategories', {headers : this.headers}).then(async(res) => 
        {
            this.CategoryOptions = res.data.map((item, id) => {return item.name})
            this.CategoryIDs = res.data.map((item, id) => {return item.id})
            this.forceUpdate()
            for(let x of res.data)
            {
                if(x.name===this.props.fields.Category)
                {
                    await axios.get('api/allsubcategories/'+x.id, {headers : this.headers}).then((res) => 
                    {
                        this.setState({sub_options : res.data.map((item, id) => {return item.name})})
                        this.subCategoryIDs = res.data.map((item) => {return item})
                    })
                    this.forceUpdate()
                    break
                }
            }
        })

        // if (this.props.fields.Category !== ""){
        //     this.subCategory = <Input 
        //     divClass="form-group col-md-4" label="Sub-Category" 
        //     config = {{className :"form-control form-select" ,}}
        //     elementType="select"
        //     options = {this.state.sub_options} value={this.props.fields.Sub_Category}
        //     change={(event) => {this.handleChange("Sub_Category", {}, event); localStorage.setItem("subCat",JSON.stringify(this.subCategoryIDs[this.state.sub_options.indexOf(event.target.value)]))}}/>
        // }

    }

    // componentDidMount() {
    //     if (sessionStorage.getItem('reloadCount')){
    //         const reloadCount = sessionStorage.getItem('reloadCount');
    //         if(reloadCount < 1) {
    //             sessionStorage.setItem('reloadCount', String(reloadCount + 1));
    //             window.location.reload();
    //         } else {
    //             sessionStorage.removeItem('reloadCount');
    //         }
    //     }
    //   }
    

    handleSubmit = async () => {
        console.log(this.props.formValid)

        const headers = {
            'Content-Type': 'application/json',
            // 'Authorization': 'Bearer '+ localStorage.getItem("token")
          }

        console.log(headers)

        if (this.props.formValid){
            var data = {
                firstName: this.props.fields.Name.Name,
                lastName: this.props.fields.LastName.LastName,
                email: this.props.fields.Email.Email,
                primaryPhone: this.props.fields.PhoneNumber.PhoneNumber,
                gender: this.props.fields.Gender.Gender,
                dateOfBirth: this.props.fields.DOB.DOB,
                idProof: this.props.fields.ID_Proof.ID_Proof,
                idCode: this.props.fields.ID_Code.ID_Code,
                // status: this.props.fields.Status.Status,
                language: this.props.fields.Language.Language,
                workerLocation: this.props.fields.CurrentLocation.CurrentLocation,
                page1:true,
            }
            if(!localStorage.getItem('VendorID'))
            {
                data['user']=JSON.parse(localStorage.getItem("user"))
            }
            if(localStorage.getItem('VendorID'))
            {
                data['vendor']={id:localStorage.getItem("VendorID")}
            }
            // if (this.WorkerID == null){
            //     axios.post('api/photos',{worker:{id:Number(this.WorkerID)},pic:this.props.fields.profilePic.split(",")[1],picContentType:this.props.fields.profilePic.split(",")[0]}) 
            //     .then((res)=>{
            //         console.log(res)
            //     })

            //     }
            //     else{
            //         axios.put('api/photos/'+this.WorkerID,{worker:{id:Number(this.WorkerID)},pic:this.props.fields.profilePic.split(",")[1],picContentType:this.props.fields.profilePic.split(",")[0]}) 
            //     .then((res)=>{
            //         console.log(res)
            //     })
            //     }
              
            console.log(data)
            if(this.WorkerID == null){
                await axios.post('api/workers', data, {headers : headers})
            .then(async(response) => {console.log(response);
                if(localStorage.getItem('VendorID'))
                {
                    sessionStorage.setItem('WorkerID',response.data.id)
                    sessionStorage.setItem("Worker", JSON.stringify(response.data))
                    this.WorkerID=response.data.id
                    await axios.get('api/refereces/worker/'+localStorage.getItem("WorkerID")).then(async(res) => 
                    {
                        let d=[res.data][0].map((item, id) => {
                            return ({name :item.name,
                            email :item.email,
                            phone :item.phone,
                            worker:{id:sessionStorage.getItem('WorkerID')}
                            })
                            })
                        for(let x of d)
                        {
                            await axios.post('api/refereces',x)
                            .then((response) => {console.log(response)}).catch((e) => console.log(e))
                        }
                    })
                    await axios.get('api/skills-masters/worker/'+localStorage.getItem("WorkerID")).then(async(res) => 
                    {
                        for(let x of [res.data][0])
                        {
                        await axios.post('api/skills-masters',{skillName:x.skillName,worker:{id:sessionStorage.getItem('WorkerID')}})
                        .then(async(response) => 
                        {console.log(response); await axios.patch("api/worker/skills/"+sessionStorage.getItem('WorkerID'),
                         data=response.data, {headers : {"Content-Type" : "application/merge-patch+json"}})}).catch((e) => console.log(e))
                        }
                    })
                    await axios.get('api/workers/profile/'+localStorage.getItem("WorkerID")).then(async (res) => 
                    {
                        if (res.data.jobPreference.length !== 0){
                            console.log(res.data)
                            let d = [res.data.jobPreference[res.data.jobPreference.length-1]].map((item, id) => {
                                 
                                return ({jobSearchStatus : item.jobSearchStatus,
                                    availabilityStatus : item.availabilityStatus,
                                    engagementType : item.engagementType,
                                    employmentType : item.employmentType,
                                    currencyType : item.currencyType,
                                    monthlyRate : item.monthlyRate,
                                    dailyRate : item.dailyRate,
                                    hourlyRate : item.hourlyRate,
                                    yearlyRate : item.yearlyRate,
                                    locationType : item.locationType,
                                    hourPerDay : item.hourPerDay,
                                    worker:{id:sessionStorage.getItem('WorkerID')}
                                                    })
                                })
                            d=d[d.length-1]
                            if(sessionStorage.getItem("subCat")!=null&&sessionStorage.getItem("subCat")!='undefined')
                            {
                                d['subCategory'] = JSON.parse(sessionStorage.getItem("subCat"))
                            }
                            await axios.post('api/job-preferences',d)
                            .then(async (response) => {
                                try{
                                    if(res.data.locationpreference[res.data.locationpreference.length-1][0].location.city)
                                    {
                                        await axios.post('api/locations', {city : res.data.locationpreference[res.data.locationpreference.length-1][0].location.city})
                                        .then(async(res1) => {
                                        await axios.post('api/location-prefrences', {worker : response.data, location : res1.data, prefrenceOrder : 1})})
                                        .catch((e) => console.log(e))
                                    }
                                    else{
                                        await axios.post('api/locations', {city : ''})
                                        .then(async(res1) => {
                                        await axios.post('api/location-prefrences', {worker : response.data, location : res1.data, prefrenceOrder : 1})})
                                        .catch((e) => console.log(e))
                                    }
                                }
                                catch(err){
                                }
                            }).catch((e) => console.log(e)) 
                        }
                    })
                }
                else{
                    localStorage.setItem("Worker", JSON.stringify(response.data));
                    localStorage.setItem("WorkerID", response.data.id);
                    this.WorkerID=response.data.id
                    axios.post('api/job-preferences/', {worker:{id:localStorage.getItem("WorkerID")},subCategory: JSON.parse(localStorage.getItem("subCat"))})
                    .then(async(response) => {
                        console.log(response)
                        await axios.post('api/locations', 
                    {city : ''}, {headers : headers})
                    .then(async(res) => {
                        console.log(res);  await axios.post('api/location-prefrences', {worker : response.data, location : res.data, prefrenceOrder : 1})})
                        .catch((e) => console.log(e))
                    }).catch((e) => console.log(e))
                }}).catch((e) => console.log(e))
            // await axios.get("api/workers/get/" + localStorage.getItem("userID"),).then(
            //     (res) => {
            //          localStorage.setItem("WorkerID", res.data.id);
            //     })
            //     .catch((e) => console.log(e))
        }
            else{
                data["id"] = this.WorkerID
                await axios.patch('api/workers/'+this.WorkerID, data,{headers:{'Content-Type': 'application/merge-patch+json'}})
            .then((response) => {console.log(response);
                localStorage.getItem('VendorID')?sessionStorage.setItem('Worker',JSON.stringify(response.data)):localStorage.setItem('Worker',JSON.stringify(response.data))
            }).catch((e) => console.log(e))
            await axios.get('api/workers/profile/'+this.WorkerID).then(async (res) => 
            {
                if (res.data.jobPreference.length !== 0){
                    console.log(res.data)
                    let d = [res.data.jobPreference[res.data.jobPreference.length-1]].map((item, id) => {
                         
                        return ({jobSearchStatus : item.jobSearchStatus,
                            availabilityStatus : item.availabilityStatus,
                            engagementType : item.engagementType,
                            employmentType : item.employmentType,
                            currencyType : item.currencyType,
                            monthlyRate : item.monthlyRate,
                            dailyRate : item.dailyRate,
                            hourlyRate : item.hourlyRate,
                            yearlyRate : item.yearlyRate,
                            locationType : item.locationType,
                            hourPerDay : item.hourPerDay,
                            worker:{id:this.WorkerID}
                                            })
                        })
                    d=d[d.length-1]
                    if(localStorage.getItem('VendorID')?(sessionStorage.getItem("subCat")!=null&&sessionStorage.getItem("subCat")!='undefined'):(localStorage.getItem("subCat")!=null&&localStorage.getItem("subCat")!='undefined'))
                    {
                        d['subCategory'] = JSON.parse(localStorage.getItem('VendorID')?sessionStorage.getItem("subCat"):localStorage.getItem("subCat"))
                    }
                    await axios.post('api/job-preferences',d)
                    .then(async (response) => {
                        try{
                            if(res.data.locationpreference[res.data.locationpreference.length-1][0].location.city)
                            {
                                await axios.post('api/locations', {city : res.data.locationpreference[res.data.locationpreference.length-1][0].location.city})
                                .then(async(res1) => {
                                await axios.post('api/location-prefrences', {worker : response.data, location : res1.data, prefrenceOrder : 1})})
                                .catch((e) => console.log(e))
                            }
                            else{
                                await axios.post('api/locations', {city : ''})
                                .then(async(res1) => {
                                await axios.post('api/location-prefrences', {worker : response.data, location : res1.data, prefrenceOrder : 1})})
                                .catch((e) => console.log(e))
                            }
                        }
                        catch(err){
                        }
                    }).catch((e) => console.log(e)) 
                }
                else{
                    axios.post('api/job-preferences/', {worker:{id:this.WorkerID},subCategory: JSON.parse(localStorage.getItem('VendorID')?sessionStorage.getItem("subCat"):localStorage.getItem("subCat"))})
                    .then(async(response) => {
                        console.log(response)
                        await axios.post('api/locations', 
                    {city : ''}, {headers : headers})
                    .then(async(res) => {
                        console.log(res);  await axios.post('api/location-prefrences', {worker : response.data, location : res.data, prefrenceOrder : 1})})
                        .catch((e) => console.log(e))
                    }).catch((e) => console.log(e))
                }
            })
            }
            
                // await axios.post('http://localhost:9001/api/categories', {isParent : true, name : this.props.fields.Category.Category},
                //     {headers : headers})
                // .then((response) => 
                    
                //     {
                //     console.log(response)
                //     axios.post('http://localhost:9001/api/categories', {isParent : false, name : this.props.fields.Sub_Category.Sub_Category, parent : response.data},
                //         {headers : headers})
                //     .then((response) => {console.log(response)}).catch((e) => console.log(e))  
                // }).catch((e) => console.log(e))  
            // } else {
            //     data["id"] = this.WorkerID
            //     await axios.put('http://localhost:9001/api/workers/'+this.WorkerID, data, {headers : headers})
            // .then((response) => {console.log(response);}).catch((e) => console.log(e))
            // }            

             
            if(this.props.fields.profilePic&&this.props.fields.profilePic!=='')
            {
                let photoID=localStorage.getItem("VendorID")?sessionStorage.getItem('photoID'):localStorage.getItem('photoID')
                if(photoID==null)
                {
                    axios.post('api/photos',{worker:{id:Number(this.WorkerID)},pic:this.props.fields.profilePic.split(",")[1],picContentType:this.props.fields.profilePic.split(",")[0]}) 
                        .then((res)=>{
                            console.log(res)
                            if(localStorage.getItem("VendorID"))
                            {
                                sessionStorage.setItem('photoID',res.data.id)
                            }
                            else{
                                localStorage.setItem('photoID',res.data.id)
                            }
                        })
                }
                else{
                    axios.patch('api/photos/'+photoID,{id:Number(photoID),worker:{id:Number(this.WorkerID)},pic:this.props.fields.profilePic.split(",")[1],picContentType:this.props.fields.profilePic.split(",")[0]},{headers : {'Content-Type': 'application/merge-patch+json'}}) 
                    .then((res)=>{
                        console.log(res)
                    }) 
                }
            }
            this.props.history.push('/createProfile/additionalDetails')
            localStorage.setItem("isBasicFilled" , true)


        } else {
            this.forceUpdate()
        }
    }


    handleSubCategory = async (id , event) => {
        await axios.get('api/allsubcategories/'+id, {headers : this.headers}).then((res) => 
    {
        this.setState({sub_options : res.data.map((item, id) => {return item.name})})
        this.subCategoryIDs = res.data.map((item) => {return item})
        this.forceUpdate()
    })
    }
    
    render(){

    return (
        <div>
        <Header />
        <section className="mainbgColor create-profile-section">
        <div className="container-fluid">
            <div className="row mobMar0">
            <SideNav page='basicDetails' additionalPage = {this.props.additionalPage} employmentPage = {this.props.employmentPage}/>
                    <div className="col-md-9 mobPadd0">
                        <div className="CreateProfileForm">
                            <ProgressBar basic={true} additional={false} employment={false}/>
                            <div className="profileHeadSec">
                               {/* <h4>Create Profile</h4> */}
                               <h4 class="MobDisNone">Create Profile</h4>

                                <h6 class="disNoneDesk disBlockMob">Basic Details</h6>
                            </div>

                            <div className="FormSec basicDetails">
                                <form>
                                    <div className="form-row flexColRev">
                                        <div className="col-md-9">
                                            <div className="form-row">
                                            <Input 
                                            divClass="form-group col-md-6" label="First Name" 
                                            config = {{className :"form-control" ,
                                                    placeholder : "Enter your First Name", 
                                                    type:"name"}}
                                            value = {this.props.fields.Name.Name}
                                            change={this.handleChange.bind(this,"Name" ,{required : true, name : true})}
                                            inValid = {this.props.fields.Name.inValid}
                                            error = {this.props.errors.Name}
                                            elementType="input" 
                                            />

                                            <Input 
                                            divClass="form-group col-md-6" label="Last Name" 
                                            config = {{className :"form-control" ,
                                                    placeholder : "Enter your Last Name", 
                                                    type:"name"}}
                                            value = {this.props.fields.LastName.LastName}
                                            change={this.handleChange.bind(this,"LastName" ,{required : true, name : true})}
                                            inValid = {this.props.fields.LastName.inValid}
                                            error = {this.props.errors.LastName}
                                            elementType="input" 
                                            />
                                            </div>

                                            <div className="form-row">
                                                <Input 
                                                divClass="form-group col-md-6" label="Email Address" 
                                                config = {{className :"form-control" ,
                                                    placeholder : "user@gmail.com", 
                                                    type:"text"}}
                                                elementType="input" 
                                                value = {this.props.fields.Email.Email}
                                                change={this.handleChange.bind(this,"Email" ,
                                                {required : true, email : true})} 
                                                error = {this.props.errors.Email}
                                                inValid = {this.props.fields.Email.inValid}
                                                />

                                                <Input 
                                                divClass="form-group col-md-6" label="Phone Number" 
                                                config = {{className :"form-control" ,
                                                    placeholder : "+91 XXX-XXX-XXXX", 
                                                    type:"text"}}
                                                elementType="input" 
                                                value = {this.props.fields.PhoneNumber.PhoneNumber}
                                                inValid = {this.props.fields.PhoneNumber.inValid}
                                                error = {this.props.errors.PhoneNumber}
                                                change={this.handleChange.bind(this,"PhoneNumber",{required : true, mobile : true})}/>

                                            </div>
                                            <div className="form-group">
                                                <label htmlFor="inputGender">Gender</label>
                                            <div className="RadioBtn">
                                                    <div className="form-check form-check-inline">
                                                        <input className="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="Male" checked={this.props.fields.Gender.Gender=== "Male"} onChange={this.handleChange.bind(this,"Gender", {})}/>
                                                        <label className="form-check-label" htmlFor="inlineRadio1">Male</label>
                                                    </div>
                                                    <div className="form-check form-check-inline">
                                                        <input className="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="Female" checked={this.props.fields.Gender.Gender === "Female"} onChange={this.handleChange.bind(this,"Gender", {})}/>
                                                        <label className="form-check-label" htmlFor="inlineRadio2">Female</label>
                                                    </div>
                                                    <div className="form-check form-check-inline">
                                                        <input className="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="Other" checked={this.props.fields.Gender.Gender === "Other"} onChange={this.handleChange.bind(this,"Gender", {})}/>
                                                        <label className="form-check-label" htmlFor="inlineRadio2">Other</label>
                                                    </div>
                                            </div>
                                            </div>
                                        </div>
                                        <UploadImage type='pic' file={this.props.fields.profilePic} onChange={this.handleChange.bind(this,"profilePic", {})}/>
                                        </div>

                                    <div className="form-row">
                                        <Input 
                                        divClass="form-group col-md-4" label="Date of Birth" 
                                        config = {{className :"form-control" , 
                                                    type:"date"}}
                                        elementType="input" 
                                        value = {this.props.fields.DOB.DOB}
                                        change={this.handleChange.bind(this,"DOB",{required : true})}
                                        inValid = {this.props.fields.DOB.inValid}
                                        error = {this.props.errors.DOB}/>

                                        <Input 
                                        divClass="form-group col-md-4" label="Category" 
                                        config = {{className :"form-control form-select" ,}}
                                        elementType="select"
                                        options = {this.CategoryOptions}
                                        value = {this.props.fields.Category}
                                        change={(event) => {this.props.setCategory(event ); this.handleSubCategory(this.CategoryIDs[this.CategoryOptions.indexOf(event.target.value)])}}/>

                                        {this.props.fields.Category &&
                                        <Input 
                                        divClass="form-group col-md-4" label="Sub-Category" 
                                        config = {{className :"form-control form-select" ,}}
                                        elementType="select"
                                        options = {["select"].concat(this.state.sub_options)} 
                                        value={this.props.fields.Sub_Category.Sub_Category}
                                        change={(event) => {this.handleChange("Sub_Category", {}, event); localStorage.getItem('VendorID')?sessionStorage.setItem("subCat",JSON.stringify(this.subCategoryIDs[this.state.sub_options.indexOf(event.target.value)])):localStorage.setItem("subCat",JSON.stringify(this.subCategoryIDs[this.state.sub_options.indexOf(event.target.value)]))}}/>}

                                    </div>

                                    <div className="form-row">
                                        {/* <div className="form-row col-md-6"> */}
                                            <Input 
                                            divClass="form-group col-6 col-md-4" label="ID Proof" 
                                            config = {{className :"form-control form-select" ,}}
                                            elementType="select"
                                            options = {["Select", "SSN", "Passport"] }
                                            value = {this.props.fields.ID_Proof.ID_Proof}
                                            inValid = {this.props.fields.ID_Proof.inValid}
                                            change={this.handleChange.bind(this,"ID_Proof",{select : true})} />

                                            <Input 
                                            divClass="form-group col-6 col-md-4" label="ID Code" 
                                            config = {{className :"form-control" , type: "text"}}
                                            elementType="input"
                                            value = {this.props.fields.ID_Code.ID_Code}
                                            inValid = {this.props.fields.ID_Code.inValid}
                                            change={this.handleChange.bind(this,"ID_Code",{required : true})}
                                             />

                                        {/* </div> */}
                                            {/* <Input 
                                            divClass="form-group col-md-3" label="Status" 
                                            config = {{className :"form-control form-select" ,}}
                                            elementType="select"
                                            options = {["Select", "Actively applying", "Casually looking", "Exploring marketplace "] }
                                            value = {this.props.fields.Status.Status}
                                            inValid = {this.props.fields.Status.inValid}
                                            change={this.handleChange.bind(this,"Status",{select : true})}
                                             /> */}

                                            <Input 
                                            divClass="form-group col-md-4" label="Language" 
                                            config = {{className :"form-control form-select" ,}}
                                            elementType="select" 
                                            options = {["Select", "English", "Hindi", "Tamil", 
                                            "Telugu", "Others"] }
                                            value = {this.props.fields.Language.Language}
                                            inValid = {this.props.fields.Language.inValid}
                                            change={this.handleChange.bind(this,"Language",{select : true})}/>
                                        
                                    </div>

                                    {/* <Input 
                                    divClass="form-group" label="Current Location" 
                                    config = {{className :"form-control form-select" ,}}
                                    elementType="select" name="Location"
                                    options = {["Select", "Hyderabad", "Mumbai", "Delhi", 
                                    "Noida", "Others"] }
                                    value={this.props.fields.CurrentLocation.CurrentLocation}
                                    inValid = {this.props.fields.CurrentLocation.inValid}
                                    change={this.handleChange.bind(this,"CurrentLocation",{select : true})}/> */}
                                    <Location 
                                    divClass="form-group" label="Current Location" 
                                    config = {{className :"form-control form-select" ,}}
                                    value={this.props.fields.CurrentLocation.CurrentLocation}
                                    inValid = {this.props.fields.CurrentLocation.inValid}
                                    change={this.handleChange.bind(this,"CurrentLocation",{select : true})}/>


                                </form>
                            </div>
                        
                        </div>
                        <div className="btn-group NextFormButtons">
                            {/* <button className="common-btn commonOutlineBtn">Draft</button> */}
                            <button className="common-btn commonBlueBtn" onClick = {() => {this.props.checkFormIsValid(); setTimeout(() => this.handleSubmit(),5)}}>Save & Next</button>
                        </div>
                    </div>
                    </div>
            </div>
        </section>
        </div>
        
    )
    }
}

const mapStateToProps = state => {
    return {
        sel : state.CategorySelected,
        fields: state.fields.fields,
        errors : state.fields.errors,
        formValid : state.fields.formValid,
        token: state.token
    }
}

const mapDispatchToProps = dispatch => {
    return {
        setCategory : (event) => dispatch({type : "CHANGE_CATEGORY", item : event.target.value}),
        changeState : (name,val)=> dispatch({type:"CHANGE_FIELD",name:name,val:val,data : 'fields'}),
        goToAdditionalDetails : () => dispatch({type: "ADDITIONAL_DETAILS"}),
        changeErrorState : (field, val) => dispatch({type : "CHANGE_ERROR_STATE", field : field, val : val, data : 'fields'}),
        checkFormIsValid : () => dispatch({type: "IS_FORM_VALID", data : 'fields'}), 
        onFilled : () => dispatch({type: "ON_FILLED", data : 'basic'}),
        // workerId: (id) => dispatch({type : "SET_WORKER_ID", id : id})
        getEmail : () => dispatch({type: "GET_EMAIL", data : 'fields'}),
        mapDatabaseToLocal : (name,res,res2) => dispatch({type : "MAP_DATABASE_TO_LOCAl", name:name, res: res, res2: res2})
    }
}


export default compose(withRouter, connect(mapStateToProps, mapDispatchToProps))(BasicDetails);