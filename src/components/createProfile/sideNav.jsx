import React from 'react';
import { connect } from 'react-redux';
import axios from 'axios'

class SideNav extends React.Component {
     
    componentWillMount=async()=>
    {
        let WorkerID=localStorage.getItem("VendorID")?sessionStorage.getItem("WorkerID"):localStorage.getItem("WorkerID")
        if(this.props.vendor)
        {
            if (localStorage.getItem("VendorID") !== null){
                await axios.get('api/vendors/'+localStorage.getItem("VendorID")).then((res) => 
                {
                    res.data.page1===true && this.props.onFilled('basic')
                    res.data.page2===true && this.props.onFilled('employment')    
                })
            }
        }
        else if (WorkerID !== null){
            await axios.get('api/workers/profile/'+WorkerID).then((res) => 
            {
                console.log(res)
                res.data.worker.page1===true && this.props.onFilled('basic')
                res.data.worker.page2===true && this.props.onFilled('additional')
                res.data.worker.page3===true && this.props.onFilled('employment')
            })
        }
        else if(this.props.employer)
        {

        }
    }
    render() {
        return (
            <div className="col-md-3 MobDisNone">
                <ul className="ProfileStepTabsList">
                    <li>
                        <div className={this.props.isBasicFilled === true? "ProfileStepTab filled": (this.props.page==='basicDetails') ? "ProfileStepTab active" : "ProfileStepTab"}>
                            <h6>Basic Details</h6>
                            <span>Here you can fill your basic info</span>
                        </div>
                    </li>
                    {(this.props.vendor)?
                    <li>
                    <div className={this.props.isEmployFilled === true? "ProfileStepTab filled": (this.props.page==='candidateDetails') ? "ProfileStepTab active" : "ProfileStepTab"}>
                        <h6>Candidate Details</h6>
                        <span>Here you can fill default Vendor details</span>
                    </div>
                    </li>
                    :
                    (!this.props.employer)&&
                    <>
                    <li>
                        <div className={this.props.isAdditionalFilled === true? "ProfileStepTab filled": (this.props.page==='additionalDetails') ? "ProfileStepTab active" : "ProfileStepTab"}>
                            <h6>Additional Details</h6>
                            <span>Here you can fill your basic info</span>
                        </div>
                    </li>
                    <li>
                        <div className={this.props.isEmployFilled === true? "ProfileStepTab filled": (this.props.page==='employmentDetails') ? "ProfileStepTab active" : "ProfileStepTab"}>
                            <h6>Employment Questions</h6>
                            <span>Here you can fill your basic info</span>
                        </div>
                    </li>
                    </>}
                </ul>
            </div>

        )
    }
}

const mapStateToProps = state => {
    return {
        isBasicFilled : state.isBasicDetailsFilled,
        isAdditionalFilled: state.isAdditionalDetailsFilled,
        isEmployFilled : state.isEmploymentDetailsFilled,
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onFilled : (data) => dispatch({type: "ON_FILLED", data : data}),
    }
}
export default connect(mapStateToProps,mapDispatchToProps) (SideNav);