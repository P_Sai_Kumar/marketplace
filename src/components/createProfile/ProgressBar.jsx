import React from 'react'
class ProgressBar extends React.Component
{
    constructor(props)
    {
        super(props)
    }
    render()
    {
        return (
            <ul class="progressBar" id="progressbar">
                <li class={this.props.basic && "filledform"} id="basic-details"></li>
                {(this.props.vendor)?
                <li class={this.props.candidate && "filledform"} id="candidate-details"></li>
                :
                <>
                <li class={this.props.additional && "filledform"} id="additional-details"></li>
                <li class={this.props.employment && "filledform"} id="employnment-questions"></li>
                </>}
            </ul>
        )
    }
}
export default ProgressBar;