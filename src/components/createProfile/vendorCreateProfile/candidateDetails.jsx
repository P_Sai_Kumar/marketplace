import React from 'react'
import '../createprofile.css';
import { connect } from 'react-redux';
import axios from 'axios';
import Input from '../../input/input';
import SideNav from '../sideNav';
import Header from '../../header/Header';
import Location from '../../input/location'
import ProgressBar from '../ProgressBar';
import DetailsPanel from '../additionalDetails/detailsPanel'
import DetailsPanel2 from '../additionalDetails/detailsPanel2'
import Modal from '../additionalDetails/modal';

class VendorCandidateDetails extends React.Component
{
    state = {
        sub_options : []
    }
    subCategoryIDs = []
    CategoryOptions = []
    CategoryIDs =[]
    CurrencyOptions=[]
    componentWillMount() {
        axios.get('api/workers/profile/'+localStorage.getItem("WorkerID"),).then(async (res) => 
        {
            if (res.data.jobPreference.length !== 0){
                console.log(res.data)
                await this.props.mapDatabaseToLocal("vendorCandidateDetails",res.data)
            }
        })
        axios.get('api/job-preferences/Currency').then((res)=>{
            this.CurrencyOptions = ["Select"].concat(res.data)
            this.forceUpdate()
        })
        axios.get('api/allcategories', {headers : this.headers}).then(async(res) => 
        {
            this.CategoryOptions = res.data.map((item, id) => {return item.name})
            this.CategoryIDs = res.data.map((item, id) => {return item.id})
            this.forceUpdate()
            for(let x of res.data)
            {
                if(x.name===this.props.fields.Category)
                {
                    await axios.get('api/allsubcategories/'+x.id, {headers : this.headers}).then((res) => 
                    {
                        this.setState({sub_options : res.data.map((item, id) => {return item.name})})
                        this.subCategoryIDs = res.data.map((item) => {return item})
                    })
                    this.forceUpdate()
                    break
                }
            }
        })
    }
    preventMinus = (e) => {
        if (e.code === 'Minus') {
            e.preventDefault();
        }
    };
    handleChange(field,rules, event)
    {   
        var obj = {}
        obj[field] = event.target.value 
        this.props.changeState(field, obj)     
    }
    handleSubmit=async()=>{
        var monthlyRate = 0
        var hourlyRate = 0
        var dailyRate = 0
        var yearlyRate = 0
        var engagementType = null, employmentType = null, locationType = null
        switch (this.props.fields.RateType.RateType){
            
            case "Daily Rate":
                dailyRate = 1
                break;
            case "Monthly Rate":
                monthlyRate = 1
                break;
            case  "Hourly Rate":
                hourlyRate = 1
                break;
            case "Yearly Rate":
                yearlyRate = 1
                break;
            default:
                break;
        }

        if (this.props.fields.WorkType.WorkType){engagementType = this.props.fields.WorkType.WorkType}
        if (this.props.fields.EmploymentType.EmploymentType){employmentType = this.props.fields.EmploymentType.EmploymentType}
        if (this.props.fields.WorkLocation.WorkLocation){locationType = this.props.fields.WorkLocation.WorkLocation}
         
        var data = {
            jobSearchStatus : this.props.fields.JobStatus.JobStatus,
            availabilityStatus : this.props.fields.JobStatus.JobStatus === "Actively Applying" ? this.props.fields.AvailabilityStatus.AvailabilityStatus : "",
            availableFrom: this.props.fields.AvailableFrom.AvailableFrom,
            availableTo: this.props.fields.AvailableTill.AvailableTill,
            engagementType: engagementType,
            employmentType: employmentType,
            locationType: locationType,
            hourPerDay: Number(this.props.fields.WorkingHours.WorkingHours),
            currencyType:this.props.fields.Currency.Currency,
            monthlyRate : monthlyRate,
            dailyRate : dailyRate,
            hourlyRate : hourlyRate,
            yearlyRate : yearlyRate,
            worker : {
                id : localStorage.getItem("WorkerID"),
            },
        }
        if(localStorage.getItem("subCat")!=null&&localStorage.getItem("subCat")!='undefined')
        {
            data['subCategory'] = JSON.parse(localStorage.getItem("subCat"))
        }

        console.log(data)


        const headers = {
            'Content-Type': 'application/json',
          }
        await axios.patch('api/vendors/'+localStorage.getItem("VendorID"), {"id":localStorage.getItem("VendorID"),page2:true}, {headers : {'Content-Type': 'application/merge-patch+json'}})
          .then((response) => {console.log(response);}).catch((e) => console.log(e))
        await axios.patch('api/workers/'+localStorage.getItem("WorkerID"), {"id":localStorage.getItem("WorkerID"),idProof: this.props.fields.ID_Proof.ID_Proof,language: this.props.fields.Language.Language,workerLocation: this.props.fields.CurrentLocation.CurrentLocation}, {headers : {'Content-Type': 'application/merge-patch+json'}})
          .then((response) => {console.log(response);}).catch((e) => console.log(e))
        await axios.post('api/job-preferences', data, {headers : headers})
        .then(async (response) => {
            console.log(response)
            console.log({city : this.props.fields.LocationPreference.LocationPreference, })
            await axios.post('api/locations', 
        {city : this.props.fields.LocationPreference.LocationPreference}, {headers : headers})
        .then(async(res) => {
            console.log(res); console.log({worker : response.data, location : res.data, prefrenceOrder : 1}); await axios.post('api/location-prefrences', {worker : response.data, location : res.data, prefrenceOrder : 1})})
            .catch((e) => console.log(e))
        }).catch((e) => console.log(e))
        localStorage.setItem("isEmployFilled" , true)
        if(localStorage.getItem("RedirectAfterSignUp") === "/" || localStorage.getItem("RedirectAfterSignUp") === null){
             this.props.history.push("/vendor/viewProfile")
        } else if(localStorage.getItem("RedirectAfterSignUp")) {
            this.props.history.push(localStorage.getItem("RedirectAfterSignUp"))
        }
        
        localStorage.removeItem("RedirectAfterSignUp")
    }

    handleSubCategory = async (id , event) => {
        await axios.get('api/allsubcategories/'+id, {headers : this.headers}).then((res) => 
    {
        this.setState({sub_options : res.data.map((item, id) => {return item.name})})
        this.subCategoryIDs = res.data.map((item) => {return item})
    })
    this.forceUpdate()
    }
    render(){

        return (
            <div>
                <Header />
                <section className="mainbgColor create-profile-section">
                    <div className="container-fluid">
                        <div className="row mobMar0">
                        <SideNav page='candidateDetails' vendor={true}/>
                        <div className="col-md-9 mobPadd0">
                            <div className="CreateProfileForm">
                                <ProgressBar basic={true} candidate={true} vendor={true}/>
                                <div class="profileHeadSec">
                                    <h4 class="MobDisNone">Create Profile</h4>
                                    <h6 class="disNoneDesk disBlockMob">Employment Details </h6>
                                </div>

                                <div class="FormSec employerDetails">
                                    <form>
                                    <div class="form-row">
                                    <Input 
                                        divClass="form-group col-md-6" label="Category" 
                                        config = {{className :"form-control form-select" ,}}
                                        elementType="select"
                                        options = {this.CategoryOptions}
                                        value = {this.props.fields.Category}
                                        change={(event) => {this.props.setCategory(event ); this.handleSubCategory(this.CategoryIDs[this.CategoryOptions.indexOf(event.target.value)])}}/>
                                        {this.props.fields.Category &&
                                        <Input 
                                        divClass="form-group col-md-6" label="Sub-Category" 
                                        config = {{className :"form-control form-select" ,}}
                                        elementType="select"
                                        options = {["select"].concat(this.state.sub_options)} 
                                        value={this.props.fields.Sub_Category.Sub_Category}
                                        change={(event) => {this.handleChange("Sub_Category", {}, event); localStorage.setItem("subCat",JSON.stringify(this.subCategoryIDs[this.state.sub_options.indexOf(event.target.value)]))}}/>}
                                    </div>
                                    <div className="form-row">
                                            <Input 
                                            divClass="form-group col-md-4" label="ID Proof" 
                                            config = {{className :"form-control form-select" ,}}
                                            elementType="select"
                                            options = {["Select", "SSN", "Passport"] }
                                            value = {this.props.fields.ID_Proof.ID_Proof}
                                            inValid = {this.props.fields.ID_Proof.inValid}
                                            change={this.handleChange.bind(this,"ID_Proof",{select : true})} />
                                            <Input 
                                            divClass="form-group col-md-4" label="Language" 
                                            config = {{className :"form-control form-select" ,}}
                                            elementType="select" 
                                            options = {["Select", "English", "Hindi", "Tamil", 
                                            "Telugu", "Others"] }
                                            value = {this.props.fields.Language.Language}
                                            inValid = {this.props.fields.Language.inValid}
                                            change={this.handleChange.bind(this,"Language",{select : true})}/>
                                            <Location 
                                            divClass="form-group col-md-4" label="Current Location" 
                                            config = {{className :"form-control form-select" ,}}
                                            value={this.props.fields.CurrentLocation.CurrentLocation}
                                            inValid = {this.props.fields.CurrentLocation.inValid}
                                            change={this.handleChange.bind(this,"CurrentLocation",{select : true})}/>
                                        </div>
                                        {/* <div class="form-row">
                                            <Input 
                                            divClass="form-group col-md-6" label="Recommendation" 
                                            config = {{className :"form-control" ,
                                                    placeholder : "Add Recommendation", 
                                                    type:"text"}}
                                            value = {this.props.fields.Recommendation.Recommendation}
                                            change={this.handleChange.bind(this,"Recommendation" ,{})}
                                            elementType="input" 
                                            />
                                            <Input 
                                            divClass="form-group col-md-6" label="Skills" 
                                            config = {{className :"form-control" ,
                                                    placeholder : "Enter Skills", 
                                                    type:"text"}}
                                            value = {this.props.fields.Skills.Skills}
                                            change={this.handleChange.bind(this,"Skills" ,{})}
                                            elementType="input" 
                                            />
                                        </div> */}
                                        <div class="AdditionalDetails form-row">
                                        <div class="form-group col-md-6">
                                        <DetailsPanel id="headingSeven" title="Recommendations" href="collapseSeven" modal="recommendations" vendor={true}/>
                                        </div>
                                        <div class="form-group col-md-6">
                                        <DetailsPanel2 id="headingFive" title="Skills" href="collapseFive" data="skills" placeholder="Enter skills" vendor={true}/>
                                        </div>
                                        </div>
                                        <div className="modal fade" id="enterDetails" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                            <div className="modal-dialog modal-dialog-centered multistepModal CreateProfileModal" role="document">
                                            <Modal vendor={true}/>
                                            </div>
                                        </div>
                                        <div class="form-row">

                                        <div className="form-group">
                                            <label htmlFor="inputGender">Job Search Status</label>
                                            <div className="RadioBtn">
                                                    <div className="form-check form-check-inline">
                                                        <input className="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="Actively Applying" checked={this.props.fields.JobStatus.JobStatus === "Actively Applying"} onChange={this.handleChange.bind(this,"JobStatus",{})}/>
                                                        <label className="form-check-label" htmlFor="inlineRadio1">Actively Applying</label>
                                                    </div>
                                                    <div className="form-check form-check-inline">
                                                        <input className="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="Casually Looking" checked={this.props.fields.JobStatus.JobStatus === "Casually Looking"} onChange={this.handleChange.bind(this,"JobStatus",{})}/>
                                                        <label className="form-check-label" htmlFor="inlineRadio2">Casually Looking</label>
                                                    </div>
                                                    <div className="form-check form-check-inline">
                                                        <input className="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="Exploring Marketplace" checked={this.props.fields.JobStatus.JobStatus === "Exploring Marketplace"} onChange={this.handleChange.bind(this,"JobStatus",{})}/>
                                                        <label className="form-check-label" htmlFor="inlineRadio2">Exploring Marketplace</label>
                                                    </div>
                                            </div>
                                        </div>

                                        {this.props.fields.JobStatus.JobStatus === "Actively Applying" && <div>
                                        <div className="form-group">
                                        <label htmlFor="inputGender">Availability Status</label>
                                        <div className="RadioBtn">
                                                <div className="form-check form-check-inline">
                                                    <input className="form-check-input" type="radio" name="inlineRadioOptionsAvailable" id="inlineRadio1" value="Can join immediately" checked={this.props.fields.AvailabilityStatus.AvailabilityStatus === "Can join immediately"} onChange={this.handleChange.bind(this,"AvailabilityStatus",{})}/>
                                                    <label className="form-check-label" htmlFor="inlineRadio1">Can join immediately</label>
                                                </div>
                                                <div className="form-check form-check-inline">
                                                    <input className="form-check-input" type="radio" name="inlineRadioOptionsAvailable" id="inlineRadio2" value="Service notice period" checked={this.props.fields.AvailabilityStatus.AvailabilityStatus === "Service notice period"} onChange={this.handleChange.bind(this,"AvailabilityStatus",{})}/>
                                                    <label className="form-check-label" htmlFor="inlineRadio2">Service notice period</label>
                                                </div>
                                                <div className="form-check form-check-inline">
                                                    <input className="form-check-input" type="radio" name="inlineRadioOptionsAvailable" id="inlineRadio2" value="Not resigned yet" checked={this.props.fields.AvailabilityStatus.AvailabilityStatus === "Not resigned yet"} onChange={this.handleChange.bind(this,"AvailabilityStatus",{})}/>
                                                    <label className="form-check-label" htmlFor="inlineRadio2">Not resigned yet</label>
                                                </div>
                                        </div>
                                        </div>

                                        {/* {this.props.fields.AvailabilityStatus.AvailabilityStatus !== "Not resigned yet" && <div class="form-row">
                                        <Input 
                                        divClass="form-group col-md-6" label="Available From" 
                                        config = {{className :"form-control" ,
                                                placeholder : "Enter Date", 
                                                type:"date"}}
                                        value = {this.props.fields.AvailableFrom.AvailableFrom}
                                        change={this.handleChange.bind(this,"AvailableFrom",{})}
                                        elementType="input" 
                                        />
                                        <Input 
                                        divClass="form-group col-md-6" label="Available Till" 
                                        config = {{className :"form-control" ,
                                                placeholder : "Enter Date", 
                                                type:"date"}}
                                        value = {this.props.fields.AvailableTill.AvailableTill}
                                        change={this.handleChange.bind(this,"AvailableTill",{})}
                                        elementType="input" 
                                        />
                                        </div>
                                        } */}
                                        </div>
                                        }       
                                        </div>

                                        <div class="form-row">
                                        <div class="form-group col-md-4">
                                            <label for="inputworkType">Work Type</label>
                                            <div class="RadioBtn">
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="WorkTypeOptions" id="fullTime" value="FullTime" checked={this.props.fields.WorkType.WorkType=== "FullTime"} onChange={this.handleChange.bind(this,"WorkType",{})}/>
                                                    <label class="form-check-label" for="fullTime">Full Time</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="WorkTypeOptions" id="partTime" value="PartTime" checked={this.props.fields.WorkType.WorkType=== "PartTime"} onChange={this.handleChange.bind(this,"WorkType",{})}/>
                                                    <label class="form-check-label" for="partTime">Part Time</label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group col-md-4">
                                            <label for="inputEmployType">Employnment Type</label>
                                            <div class="RadioBtn">
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="EmploynmentTypeOptions" id="permanent" value="Permanent" checked={this.props.fields.EmploymentType.EmploymentType=== "Permanent"} onChange={this.handleChange.bind(this,"EmploymentType",{})}/>
                                                    <label class="form-check-label" for="permanent">Permanent</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="EmploynmentTypeOptions" id="inlineRadio2" value="Contractual" checked={this.props.fields.EmploymentType.EmploymentType=== "Contractual"} onChange={this.handleChange.bind(this,"EmploymentType",{})}/>
                                                    <label class="form-check-label" for="inlineRadio2">Contract</label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group col-md-4">
                                            <label for="inputWorkLocation">Work Location</label>
                                            <div class="RadioBtn">
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="inlineRadioOptions1" id="inlineRadio1" value="WorkFromHome" checked={this.props.fields.WorkLocation.WorkLocation=== "WorkFromHome"} onChange={this.handleChange.bind(this,"WorkLocation",{})}/>
                                                    <label class="form-check-label" for="inlineRadio1">Home</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="inlineRadioOptions1" id="inlineRadio2" value="WorkFromOffice" checked={this.props.fields.WorkLocation.WorkLocation=== "WorkFromOffice"} onChange={this.handleChange.bind(this,"WorkLocation",{})}/>
                                                    <label class="form-check-label" for="inlineRadio2">Office</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="inlineRadioOptions1" id="inlineRadio3" value="Both" checked={this.props.fields.WorkLocation.WorkLocation=== "Both"} onChange={this.handleChange.bind(this,"WorkLocation",{})}/>
                                                    <label class="form-check-label" for="inlineRadio3">Both</label>
                                                </div>
                                            </div>
                                        </div>
                                        </div>

                                        <div class="form-row">
                                        <Location 
                                        divClass="form-group col-md-6" label="Location Preference" 
                                        config = {{className :"form-control form-select" ,}}
                                        value={this.props.fields.LocationPreference.LocationPreference}
                                        change={this.handleChange.bind(this,"LocationPreference",{})}/>
                                        <Input 
                                        divClass="form-group col-md-6" label="Working hours" 
                                        config = {{className :"form-control",type:"number",min:0,onKeyPress:this.preventMinus}}
                                        elementType="input"
                                        value = {this.props.fields.WorkingHours.WorkingHours}
                                        change={this.handleChange.bind(this,"WorkingHours",{})}
                                        />
                                        </div>

                                        <div class="form-row">
                                        <Input 
                                        divClass="form-group col-md-6" label="Currency" 
                                        config = {{className :"form-control form-select" ,}}
                                        elementType="select"
                                        options = {this.CurrencyOptions}
                                        value = {this.props.fields.Currency.Currency}
                                        change={this.handleChange.bind(this,"Currency",{})}
                                        />
                                        <div class="form-group col-md-6">
                                            <label for="inputRateType">Rate Type</label>
                                            <div class="RadioBtn">
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="inlineRadioOptions2" id="inlineRadio1" value="Daily Rate" checked={this.props.fields.RateType.RateType=== "Daily Rate"} onChange={this.handleChange.bind(this,"RateType",{})}/>
                                                        <label class="form-check-label" for="inlineRadio1">Daily</label>
                                                    </div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="inlineRadioOptions2" id="inlineRadio2" value="Hourly Rate" checked={this.props.fields.RateType.RateType=== "Hourly Rate"} onChange={this.handleChange.bind(this,"RateType",{})}/>
                                                        <label class="form-check-label" for="inlineRadio2">Hourly</label>
                                                    </div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="inlineRadioOptions2" id="inlineRadio3" value="Monthly Rate" checked={this.props.fields.RateType.RateType=== "Monthly Rate"} onChange={this.handleChange.bind(this,"RateType",{})}/>
                                                        <label class="form-check-label" for="inlineRadio3">Monthly</label>
                                                    </div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="inlineRadioOptions2" id="inlineRadio4" value="Yearly Rate" checked={this.props.fields.RateType.RateType=== "Yearly Rate"} onChange={this.handleChange.bind(this,"RateType",{})}/>
                                                        <label class="form-check-label" for="inlineRadio4">Yearly</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div className="btn-group NextFormButtons">
                            <button className="common-btn commonBlueBtn" onClick = {() => {this.handleSubmit()}}>Save & Preview</button>
                            </div>
                        </div>
                    </div>
                </div>
                </section>
            </div>
        )
    }
}
const mapStateToProps = state => {
    return {
        fields: state.vendorCandidateDetails.fields,
    }
}

const mapDispatchToProps = dispatch => {
    return {
        setCategory : (event) => dispatch({type : "CHANGE_CATEGORY", item : event.target.value,name:'vendor'}),
        changeState : (name,val)=> dispatch({type:"CHANGE_FIELD", name:name, val:val, data : 'vendorCandidateDetails'}),
        onFilled : () => dispatch({type: "ON_FILLED", data : 'employment'}),
        mapDatabaseToLocal : (name,res,) => dispatch({type : "MAP_DATABASE_TO_LOCAl", name:name, res: res,})
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(VendorCandidateDetails);