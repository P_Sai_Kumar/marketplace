import React from 'react';
import '../createprofile.css';
import { connect } from 'react-redux';
import { compose } from "redux";
import { withRouter } from 'react-router-dom';
import axios from 'axios';

import UploadImage from '../../uploadImage/uploadImage';
import Input from '../../input/input';
import SideNav from '../sideNav';
import Header from '../../header/Header';
import ProgressBar from '../ProgressBar';

class VendorBasicDetails extends React.Component{

    handleValidation(value, rules){
        let isValid = true;
        if (rules.required) {
            isValid = value.trim() !== "" && isValid;
        }

        if (rules.name) {
            isValid = /^[A-Za-z\s]+$/.test(value) && isValid;
        }

        if (rules.email) {
            isValid = /^[^@\s]+@[^@\s]+\.[^@\s]+$/.test(value) && isValid;
        }

        if (rules.mobile) {
            isValid = /^([0-9]{10})$/.test(value) && isValid;
        }
        if(rules.url)
        {
            isValid = /((([A-Za-z]{3,9}:(?:\/\/)?)(?:[-;:&=\+\$,\w]+@)?[A-Za-z0-9.-]+|(?:www.|[-;:&=\+\$,\w]+@)[A-Za-z0-9.-]+)((?:\/[\+~%\/.\w-_]*)?\??(?:[-\+=&;%@.\w_]*)#?(?:[\w]*))?)/.test(value) && isValid; 
        }
        return isValid
    }

    handleChange(field, rules, event)
    {
        if(event===null)
        {
            this.props.changeState(field,'')
            return
        }
        let k = this.handleValidation(event.target.value, rules)
        if (k){
            this.props.changeErrorState(field, true)
        } else {
            this.props.changeErrorState(field, false)
        }
        
        if(field === 'profilePic')
        {
            var reader = new FileReader();
            reader.readAsDataURL(event.target.files[0]);
            reader.onloadend =  async()=> {
            var base64String = reader.result;
            console.log(base64String.split(",")[1]);
            this.props.changeState(field,base64String)
            }
        }
        else{
            var obj = {}
            obj[field] = event.target.value 
            obj['inValid'] = !k
            this.props.changeState(field, obj)
        }
    }

    componentWillMount=async()=>{
        if (localStorage.getItem("VendorID") != null){
                await axios.get('api/vendors/'+localStorage.getItem("VendorID"), {headers : this.headers}).then((res) => 
                {
                    console.log(res.data)
                this.props.mapDatabaseToLocal("vendorBasicDetails",res.data)
                })
        } else {
            this.props.getEmail()
        }
    }

    handleSubmit = async () => {
        console.log(this.props.formValid)

        const headers = {
            'Content-Type': 'application/json',
          }
 
        if (this.props.formValid){
            var data = {
                "email": this.props.fields.Email.Email,
                "employeeID": this.props.fields.EmployeeID.EmployeeID,
                "employerName": this.props.fields.EmployerName.EmployerName,
                "employerWebsite": this.props.fields.EmployerWebsite.EmployerWebsite,
                "name": this.props.fields.Name.Name,
                "phonenumber": this.props.fields.PhoneNumber.PhoneNumber,
                "pic" : this.props.fields.profilePic.split(",")[1],
                "picContentType":this.props.fields.profilePic.split(",")[0],
                "user" : JSON.parse(localStorage.getItem("user")),
                "page1": true,
            }
             
            console.log(data)
            if(localStorage.getItem("VendorID") == null){
                await axios.post("api/workers",{email : this.props.fields.Email.Email, primaryPhone : this.props.fields.PhoneNumber.PhoneNumber, firstName : " ", lastName : " ","user" : JSON.parse(localStorage.getItem("user"))}).then(async (res) => {
                    console.log(res)
                    localStorage.setItem("WorkerID",res.data.id)
                    data["worker"] = res.data
                    await axios.post('api/vendors', data, {headers : headers})
                    .then((response) => {console.log(response); localStorage.setItem("Vendor", JSON.stringify(response.data));localStorage.setItem("VendorID", response.data.id)}).catch((e) => console.log(e))
                })
                
        }
            else{
                data["id"] = localStorage.getItem("VendorID")
                await axios.patch('api/vendors/'+localStorage.getItem("VendorID"), data, {headers : {'Content-Type': 'application/merge-patch+json'}})
            .then((response) => {console.log(response);}).catch((e) => console.log(e))
            }
             
            
            this.props.history.push('/vendor/createProfile/candidateDetails')
            // localStorage.setItem("isBasicFilled" , true)


        } else {
            this.forceUpdate()
        }
    }

    render(){
        console.log(this.props.location.pathname)

    return (
        <div>
        {this.props.location.pathname==='/vendor/viewProfile'?null: <Header />}
        <section className="mainbgColor create-profile-section">
        <div className="container-fluid">
            <div className="row mobMar0">
            {this.props.location.pathname==='/vendor/viewProfile'?null:<SideNav page='basicDetails' vendor={true}/>}
                    <div className="col-md-9 mobPadd0">
                        <div className="CreateProfileForm">
                        {this.props.location.pathname==='/vendor/viewProfile'?null:<ProgressBar basic={true} candidate={false} vendor={true}/>}
                        {this.props.location.pathname==='/vendor/viewProfile'?null:  <div className="profileHeadSec">
                               <h4 class="MobDisNone">Create Profile</h4>

                                <h6 class="disNoneDesk disBlockMob">Basic Details</h6>
                            </div>}

                            <div className="FormSec basicDetails">
                                <form>
                                    <div className="form-row flexColRev">
                                        <div className="col-md-9">
                                            <Input 
                                            divClass="form-group" label="Name" 
                                            config = {{className :"form-control" ,
                                                    placeholder : "Enter your Name", 
                                                    type:"name"}}
                                            value = {this.props.fields.Name.Name}
                                            change={this.handleChange.bind(this,"Name" ,{required : true, name : true})}
                                            inValid = {this.props.fields.Name.inValid}
                                            error = {this.props.errors.Name}
                                            elementType="input" 
                                            />

                                            <div className="form-row">
                                                <Input 
                                                divClass="form-group col-md-6" label="Email Address" 
                                                config = {{className :"form-control" ,
                                                    placeholder : "user@domain.com", 
                                                    type:"text"}}
                                                elementType="input" 
                                                value = {this.props.fields.Email.Email}
                                                change={this.handleChange.bind(this,"Email" ,
                                                {required : true, email : true})} 
                                                error = {this.props.errors.Email}
                                                inValid = {this.props.fields.Email.inValid}
                                                />

                                                <Input 
                                                divClass="form-group col-md-6" label="Phone Number" 
                                                config = {{className :"form-control" ,
                                                    placeholder : "+91 XXX-XXX-XXXX", 
                                                    type:"text"}}
                                                elementType="input" 
                                                value = {this.props.fields.PhoneNumber.PhoneNumber}
                                                inValid = {this.props.fields.PhoneNumber.inValid}
                                                error = {this.props.errors.PhoneNumber}
                                                change={this.handleChange.bind(this,"PhoneNumber",{required : true, mobile : true})}/>

                                            </div>
                                            <div className="form-row">
                                                <Input 
                                                divClass="form-group col-md-6" label="Employer Name" 
                                                config = {{className :"form-control" ,
                                                        placeholder : "Enter Employer Name",
                                                        type:"text"}}
                                                elementType="input" 
                                                value = {this.props.fields.EmployerName.EmployerName}
                                                change={this.handleChange.bind(this,"EmployerName",{required : true, name : true})}
                                                inValid = {this.props.fields.EmployerName.inValid}
                                                error = {this.props.errors.EmployerName}/>
                                                <Input 
                                                    divClass="form-group col-md-6" label="Employee ID" 
                                                    config = {{className :"form-control" ,placeholder : "Enter Employee ID", type: "text"}}
                                                    elementType="input"
                                                    value = {this.props.fields.EmployeeID.EmployeeID}
                                                    inValid = {this.props.fields.EmployeeID.inValid}
                                                    change={this.handleChange.bind(this,"EmployeeID",{required : true})}
                                                    />
                                            </div>
                                        </div>
                                        <UploadImage type='pic' file={this.props.fields.profilePic} onChange={this.handleChange.bind(this,"profilePic", {})}/>
                                        </div>
                                    <div className="form-row">
                                        <Input 
                                        divClass="form-group col-md-9" label="Employer Website" 
                                        config = {{className :"form-control" ,
                                                   placeholder : "Enter URL",
                                                   type:"text"}}
                                        elementType="input" 
                                        value = {this.props.fields.EmployerWebsite.EmployerWebsite}
                                        change={this.handleChange.bind(this,"EmployerWebsite",{url: true})}
                                        inValid = {this.props.fields.EmployerWebsite.inValid}
                                        error = {this.props.errors.EmployerWebsite}/>
                                         
                                    </div>
                                </form>
                            </div>
                        
                        </div>
                        <div className="btn-group NextFormButtons">
                            <button className="common-btn commonBlueBtn" onClick = {() => {this.props.checkFormIsValid(); setTimeout(() => this.handleSubmit(),5)}}>{this.props.location.pathname==='/vendor/viewProfile'?'Save':'Save & Next'}</button>
                        </div>
                    </div>
                    </div>
            </div>
        </section>
        </div>
        
    )
    }
}

const mapStateToProps = state => {
    return {
        fields: state.vendorBasicDetails.fields,
        errors : state.vendorBasicDetails.errors,
        formValid : state.vendorBasicDetails.formValid,
    }
}

const mapDispatchToProps = dispatch => {
    return {
        changeState : (name,val)=> dispatch({type:"CHANGE_FIELD",name:name,val:val,data : 'vendorBasicDetails'}),
        changeErrorState : (field, val) => dispatch({type : "CHANGE_ERROR_STATE", field : field, val : val, data : 'vendorBasicDetails'}),
        checkFormIsValid : () => dispatch({type: "IS_FORM_VALID", data : 'vendorBasicDetails'}), 
        onFilled : () => dispatch({type: "ON_FILLED", data : 'basic'}),
        getEmail : () => dispatch({type: "GET_EMAIL", data : 'vendorBasicDetails'}),
        mapDatabaseToLocal : (name,res) => dispatch({type : "MAP_DATABASE_TO_LOCAl", name:name, res: res})
    }
}


export default compose(withRouter, connect(mapStateToProps, mapDispatchToProps))(VendorBasicDetails);