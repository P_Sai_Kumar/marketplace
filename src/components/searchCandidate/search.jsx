import React, { Component, useEffect, useState } from 'react';
import axios from 'axios'
import { withRouter } from 'react-router';

import { Link, useHistory, useLocation } from 'react-router-dom';
 
import { connect } from 'react-redux';
import Header from "../header/Header";
import Modal from '../createProfile/additionalDetails/modal';
import './search.css'
import $ from 'jquery'

function Search(props) {


  const [result, setresult] = useState([])

  const [userInput, setuserInput] = useState(sessionStorage.getItem("designation"))
  const [location, setlocation] = useState(sessionStorage.getItem("location"))
  const [category, setCategory] = useState(sessionStorage.getItem("category"))
  const [subCategory, setSubCategory] = useState(sessionStorage.getItem("subCategory"))
  const [skills,setSkills]=useState(sessionStorage.getItem("skills"))

  const [sugggestions, setSuggestions] = useState([])
  const [skillsSuggestion,setSkillsSuggestions]=useState([])
  const [search, setSearch] = useState(userInput)
  const [loading,setLoading]=useState(true)

  const [subOptions, setsubOptions] = useState([])
  const [categoryOptions,setCategoryOptions]=useState([]);
  const [categoryIDs,setCategoryIDs]=useState([])
  const [subCategoryIDs,setSubCategoryIDs]=useState([])

 


  useEffect(() => {
    if(props.searchResult)
    setresult(props.searchResult)
  }, [props.searchResult]);


  function getMax(data, n) {
    var tmp = {}, tops = [];
  
    // Create object with count of occurances of each array element
    data.forEach(function(item) {
        tmp[item] = tmp[item] ? tmp[item]+1 : 1;
    });
  
    // Create an array of the sorted object properties
    tops = Object.keys(tmp).sort(function(a, b) { return tmp[a] - tmp[b] });
  
    // Return last n elements in reverse order
    return tops.slice(-(n)).reverse();
  }

  function getSkills(skillsObj){
    var skills = []
    for(var i=0; i<skillsObj.length; i++){
      skills.push(skillsObj[i].skillName)
    }
    return skills
  }

  useEffect(() => {
    try {
      $('.modal-backdrop').hide();
      // $('#signUp').click();
      // window.location.reload(false);
    } catch (err) {

    }
    console.log(props.filters.Designation.Designation)
    console.log(props.filters.Location.Location)

    

    axios.get('api/allcategories').then(async(res) => 
        {
            setCategoryOptions(res.data.map((item, id) => {return item.name}))
            setCategoryIDs(  res.data.map((item, id) => {return item.id}))
            // this.forceUpdate()
            for(let x of res.data)
            {
                if(x.name===category)
                {
                    await axios.get('api/allsubcategories/'+x.id).then((res) => 
                    {
                        setsubOptions(res.data.map((item, id) => {return item.name}))
                        setSubCategoryIDs( res.data.map((item) => {return item}))
                    })
                    // this.forceUpdate()
                    break
                }
            }
        })

        var recommendedSkills

        if(props.location.pathname === "/recommendedCandidates"){
          axios.get('api/recruiters/'+localStorage.getItem('EmployerID'))
        .then(async (res)=>{
            if(res.data.workers){
            console.log(res.data.workers[0].skills)

            var skills = getSkills(res.data.workers[0].skills)
            skills = skills.concat(getSkills(res.data.workers[res.data.workers.length-1].skills))
            console.log(skills)
            recommendedSkills = getMax(skills, 3)
            // recommendedSkills = skills[0]
            console.log(recommendedSkills)
            var data = []
            for(var i=0; i<3; i++){
              await axios.get("/api/search/candidate-filters"
                    ,{
                      params: {skill: recommendedSkills[i]}
                    },{"headers": {
                      "content-type": "application/json",  
                      }
                    }
                  ).then(async (res) => {
                    data = data.concat(res.data)
                  })
            }
            //Remove duplicate candidates
            data = data.filter((thing, index, self) =>
                index === self.findIndex((t) => (
                  t.id === thing.id
                ))
            )
            setresult(data)
            setLoading(false)

                }           
        })
          
        } else {

        if (location || userInput || category || subCategory  || skills){
          var param=   { }
        if(userInput)
        param={...param,designation:userInput}
        if(location)
        param={...param,location:location}
        if(skills)
        param={...param,skill:skills}
        if(category)
        param={...param,category:category}
        if(subCategory)
        param={...param,subCategory: subCategory}
             axios.get("/api/search/candidate-filters"
              ,{
                params:param
              },{"headers": {
                "content-type": "application/json",  
                }
              }
            ).then((res) => {
              setresult(res.data)
              setLoading(false)
              
            })}
          else if(sessionStorage.getItem("candidateSearchInput"))
          {
            axios.get("api/search/workerSearchQuery/"+sessionStorage.getItem("candidateSearchInput")
            ).then(res=>{
              setSearch(sessionStorage.getItem("candidateSearchInput"))
              setresult(res.data)
              setLoading(false)
              
            })
          }
          else{
            axios.get("api/search/allworkers"
            ).then((res)=>{
              setSearch("Candidates")
              setresult(res.data.content)
              setLoading(false)
              
            })
          }

        }

  }, []);

  


  const handleSubmit = async () => {
      setLoading(true)
      setSearch(userInput)
      if (location || userInput || category || subCategory  || skills){
        var param=   { }
        if(userInput)
        param={...param,designation:userInput}
        if(location)
        param={...param,location:location}
        if(skills)
        param={...param,skill:skills}
        if(category)
        param={...param,category:category}
        if(subCategory)
        param={...param,subCategory: subCategory}
        
           axios.get("/api/search/candidate-filters"
            ,{
              params:param
            },{"headers": {
              "content-type": "application/json",  
              }
            }
          ).then((res) => {
            setresult(res.data)
            setLoading(false)
            
          })}
        else if (sessionStorage.getItem("candidateSearchInput")){
    
          axios.get("api/search/workerSearchQuery/"+sessionStorage.getItem("candidateSearchInput"),
         
          ).then((res) => {
            setresult(res.data)
            setLoading(false)
            
          })
        }
        else{
          axios.get("api/search/allworkers"
          ).then((res)=>{
            setSearch("Candidates")
            setresult(res.data.content)
            setLoading(false)
            
          })      
        }

      
    }


  const locationChange = async (event) => {
    setlocation(event.target.value)
    sessionStorage.setItem("location", event.target.value)
    
  }

  const subCategoryChange = (event) => {
    setSubCategory(event.target.value)
    sessionStorage.setItem("subCategory", event.target.value)
  }

  const skillsChange = (event) => {
    setSkills(event.target.value)
    sessionStorage.setItem("skills", event.target.value)


    if (event.target.value.trim())
      axios.get("api/search/skillsSuggestions/" + event.target.value, {
        transformRequest: (data, headers) => {
          delete headers.common['Authorization'];

        }
      }).then((res) => {
        setSkillsSuggestions(res.data)
        console.log(res.data)
      })
    else
        setSkillsSuggestions([])
  }

  const designationChange = (event) => {
    setuserInput(event.target.value)
    sessionStorage.setItem("designation", event.target.value)

    if (event.target.value.trim())
      axios.get("api/search/designationSuggestions/" + event.target.value, {
        transformRequest: (data, headers) => {
          delete headers.common['Authorization'];

        }
      }).then((res) => {
        setSuggestions(res.data)
        console.log(res.data)
      })
    else
      setSuggestions([])
  }

  const categroyChange = async (event) => {
    setCategory(event.target.value)
    handleSubCategory(categoryIDs[categoryOptions.indexOf(event.target.value)])
    
    
  }


  const reset = () => {
    
    setLoading(true)
    axios.get("api/search/allworkers"
      ).then((res)=>{
        setSearch("Candidates")
        setresult(res.data.content)
        setLoading(false)
      
      })


    setCategory("")
    setSubCategory("")
    setlocation("")
    setuserInput("")
    setSkills("")
    setSearch("Candidates")
    setsubOptions([])

    sessionStorage.setItem("candidateSearchInput","")
    sessionStorage.setItem("skills", "")
    sessionStorage.setItem("category", "")
    sessionStorage.setItem("subCategory", "")
    sessionStorage.setItem("designation", "")
    sessionStorage.setItem("location", "")
  }

  const onSuggestHandler = (text) => {
    setuserInput(text)
    sessionStorage.setItem("designation", text)
    setSuggestions([])
  }
  const onSuggestSkillsHandler = (text) => {
    setSkills(text)
    sessionStorage.setItem("skills", text)
    setSkillsSuggestions([])
  }

  const rate=(res)=>{
    if(res.jobPreferences[0]){
      var rate=null;
      if(res.jobPreferences[0].dailyRate>0)
      rate=res.jobPreferences[0].dailyRate+"/Day"
      if(res.jobPreferences[0].hourlyRate>0)
      rate=res.jobPreferences[0].hourlyRate+"/Hour"
      if(res.jobPreferences[0].monthlyRate>0)
      rate=res.jobPreferences[0].monthlyRate+"/Month"
      if(res.jobPreferences[0].yearlyRate>0)
      rate=res.jobPreferences[0].yearlyRate+"/Year"
      return rate; 
    }
  return null
  }

  const cat=(res)=>{
    var result=null;
    if(res.category)
    result=res.category
    if(res.Category)
    result=res.Category
    if(res.jobPreferences[0] && res.jobPreferences[0].subCategory){
    if(res.jobPreferences[0].subCategory.name)
    result+="/"+res.jobPreferences[0].subCategory.name
    }

    return result;
  }

  const  handleSubCategory = async (id , event) => {
    await axios.get('api/allsubcategories/'+id ).then((res) => 
   {
    setsubOptions(res.data.map((item, id) => {return item.name}))
    setSubCategoryIDs( res.data.map((item) => {return item}))
    // this.forceUpdate()  
  })
  }
  const sortBy=(event)=>{
    if(event.target.value==='a-z')
    setresult([...result.sort((a, b) => (a.firstName?a.firstName+a.lastName:'').toLowerCase().trim() > (b.firstName?b.firstName+b.lastName:'').toLowerCase().trim() ? 1:-1)])
    else if(event.target.value==='z-a')
    setresult([...result.sort((a, b) => (a.firstName?a.firstName+a.lastName:'').toLowerCase().trim() < (b.firstName?b.firstName+b.lastName:'').toLowerCase().trim() ? 1:-1)])
    else if(event.target.value==="recent")
    handleSubmit()
  }

  // try{
  //   $('.modal-backdrop').hide();
  //   } 
  // catch(err){}

 



  return (
    <div>
      <Header />
      <section class="mainbgColor view-profile-section">
        <div class="container mobPadd30" >
          <div class="row mobMar0">
            <div class="col-md-3 DisNoneMob">
              <div class="SearchFilterHead">
                <h6>Search Filter</h6> <a style={{color:"#007BFF",cursor:"pointer"}} onClick={reset}>Reset All</a>
              </div>

              <div class="searchFilter">
                <div class="CatagoryFilter">
                  <div class="form-group">
                    <label for="inputCategory">Category</label>
                    <div className="landingpageInput">
                      <select id="inputCategory" class="form-control" value={category} onChange={categroyChange}>
                        <option value="" hidden></option>
                        {
                          categoryOptions.map(item => (
                            <option value={item} key={item} id={item} >{item}</option>
                          ))
                        }
                      </select>
                      {/* <svg xmlns="http://www.w3.org/2000/svg" height="20px" viewBox="0 0 24 24" width="20px" fill="#000000"><path d="M0 0h24v24H0z" fill="none" /><path d="M16.59 8.59L12 13.17 7.41 8.59 6 10l6 6 6-6z" /></svg> */}
                    </div>
                  </div>

                  <div class="form-group  ">
                    <label for="inputSubCategory">Sub-Category</label>
                    <div className="landingpageInput">
                      <select id="inputSubCategory" class="form-control" value={subCategory} onChange={subCategoryChange}>
                        <option value="" hidden></option>
                        {
                          category && subOptions.map(item => (
                            <option value={item} key={item} id={item} >{item}</option>
                          ))
                        }
                      </select>
                      {/* <svg xmlns="http://www.w3.org/2000/svg" height="20px" viewBox="0 0 24 24" width="20px" fill="#000000"><path d="M0 0h24v24H0z" fill="none" /><path d="M16.59 8.59L12 13.17 7.41 8.59 6 10l6 6 6-6z" /></svg> */}
                    </div>
                  </div>
                </div>
                <div class="SkillsFilter">
                  <div class="form-group">
                    <label for="inputSkills">Skills</label>
                    <input type="name" id="inputSkills" class="form-control" 
                    onChange={skillsChange} value={skills} autoComplete="off" placeholder="Enter Skills." />
                    <div className="autocomplete-list">
                            {skillsSuggestion.length > 0 && skillsSuggestion.map((res, i) => (

                              <div className="autocomplete-list-options" onClick={() => onSuggestSkillsHandler(res.skillSuggestion)} >
                                {res.skillSuggestion}
                              </div>

                            ))}
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="form-group">
                      <div>
                        <label for="inputDesignation">Designation</label>
                        <div className="landingpageInput">
                          <input type="name" id="inputDesignation" class="form-control"
                          onChange={designationChange} value={userInput} autoComplete="off"
                          placeholder="Enter Designation." />


                          {/* <div className="autocomplete-list">
                            {sugggestions.length > 0 && sugggestions.map((res, i) => (

                              <div className="autocomplete-list-options" onClick={() => onSuggestHandler(res.designation)} >
                                {res.designation}
                              </div>

                            ))}
                          </div> */}
                          
                          
                        </div>




                      </div>
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="inputLocation">Location</label>
                    <div className="landingpageInput">
                      <select id="inputLocation" class="form-control" value={location} onChange={locationChange}>

                        <option value="" hidden></option>
                        <option >Mumbai</option>
                        <option>Hyderabad</option>
                        <option>Noida </option>
                      </select>
                      {/* <svg xmlns="http://www.w3.org/2000/svg" height="20px" viewBox="0 0 24 24" width="20px" fill="#000000"><path d="M0 0h24v24H0z" fill="none" /><path d="M16.59 8.59L12 13.17 7.41 8.59 6 10l6 6 6-6z" /></svg> */}

                    </div>
                   
                  </div>
                </div>

                {localStorage.getItem('EmployerID') ?
                  <div className="SkillsFilter border-top">
                    <div class="form-group">
                      <label for="companyName">Company Name</label>
                      <select id="companyName" class="form-control">
                        <option value="" hidden>Select</option>
                      </select>
                    </div>
                  </div>
                : null }

              </div>


              <button type="button" class="SearchFilterButton" onClick={handleSubmit}>Search</button>

            </div>

            <div class="col-md-9 mobPadd0">
              <div class="FilterList">
                <div class="topLine disF">
                <div class="FilterMenu">
                       <div  class="disNoneDesk disBlockMob" data-toggle="modal" data-target="#enterDetails" onClick={() => {props.changeModal('candidateSearchFilters');}}>
                        <h6 className="disF">
                          <svg xmlns="http://www.w3.org/2000/svg" enable-background="new 0 0 24 24" height="15px" viewBox="0 0 24 24" width="15px" fill="#929292"><g>
                            <path d="M0,0h24 M24,24H0" fill="none"/><path d="M7,6h10l-5.01,6.3L7,6z M4.25,5.61C6.27,8.2,10,13,10,13v6c0,0.55,0.45,1,1,1h2c0.55,0,1-0.45,1-1v-6 c0,0,3.72-4.8,5.74-7.39C20.25,4.95,19.78,4,18.95,4H5.04C4.21,4,3.74,4.95,4.25,5.61z"/><path d="M0,0h24v24H0V0z" fill="none"/></g>
                          </svg>
                          Filter
                          </h6>
                          
                        </div>
                        {/* <a href="#" onClick={reset}>Reset All</a> */}
                        
                     </div>
                  
                  <span class="disF">
                  <svg xmlns="http://www.w3.org/2000/svg" height="24px" viewBox="0 0 24 24" width="24px" fill="#929292">
                      <path d="M0 0h24v24H0z" fill="none" /><path d="M3 18h6v-2H3v2zM3 6v2h18V6H3zm0 7h12v-2H3v2z" />
                    </svg>
                    <select className="form-control h-auto py-0" name="sort-by" id="sort-by" onChange={sortBy}>
                      <option value="" hidden>Sort By</option>
                      <option value="recent">Recent</option>
                      <option value="a-z">Name (A-Z)</option>
                      <option value="z-a">Name (Z-A)</option>
                    </select>
                  </span>
                </div>

                <div className="filter-count-text">
                  <span>{result && result.length > 0 ? "1 -" : null} {result && result.length ? result.length:null} of {result.length} Results Showing for {search}</span>
                </div>

                {loading ?
                  <div class="cssload-container" style={{ alignContent: 'center' }}>
                    <div class="cssload-speeding-wheel" style={{ alignContent: 'center' }}></div>
                  </div> :

                <div class="SearchedList">

                  {
                    result.map((res) => (
                      <Link to={{ pathname: '/candidateDetails/' + res.id }} style={{ textDecoration: 'none', color: 'inherit' }}>
                        <div class="SearchResult">
                          <div class="FilteredOptions">

                            <h6 class="marB25">{res.firstName} {res.lastName} <span>{res.employments[0]?"( "+res.employments[0].jobTitle+" )":null}</span></h6>
                            {/* <ul class="searchDetails disF marB25">
                              <li>
                                <svg xmlns="http://www.w3.org/2000/svg" height="15px" viewBox="0 0 24 24" width="15px" fill="#FA852B">
                                  <path d="M0 0h24v24H0z" fill="none" /><path d="M20 6h-4V4c0-1.11-.89-2-2-2h-4c-1.11 0-2 .89-2 2v2H4c-1.11 0-1.99.89-1.99 2L2 19c0 1.11.89 2 2 2h16c1.11 0 2-.89 2-2V8c0-1.11-.89-2-2-2zm-6 0h-4V4h4v2z" />
                                </svg>
                                <span>{cat(res)?cat(res):"N/A"}</span>
                              </li>
                              <li>
                                <svg xmlns="http://www.w3.org/2000/svg" height="16px" viewBox="0 0 24 24" width="16px" fill="#FA852B">
                                  <path d="M0 0h24v24H0z" fill="none" /><path d="M20 4H4c-1.11 0-1.99.89-1.99 2L2 18c0 1.11.89 2 2 2h16c1.11 0 2-.89 2-2V6c0-1.11-.89-2-2-2zm0 14H4v-6h16v6zm0-10H4V6h16v2z" />
                                </svg>
                                <span>${rate(res)?rate(res):"N/A"}</span>
                              </li>
                              <li>
                                <svg xmlns="http://www.w3.org/2000/svg" enable-background="new 0 0 24 24" height="15px" viewBox="0 0 24 24" width="15px" fill="#FA852B"><g>
                                  <path d="M0,0h24v24H0V0z" fill="none" /></g><g><path d="M12,2c-4.2,0-8,3.22-8,8.2c0,3.32,2.67,7.25,8,11.8c5.33-4.55,8-8.48,8-11.8C20,5.22,16.2,2,12,2z M12,12c-1.1,0-2-0.9-2-2 c0-1.1,0.9-2,2-2c1.1,0,2,0.9,2,2C14,11.1,13.1,12,12,12z" /></g>
                                </svg>
                                <span>{res.workerLocation?res.workerLocation:"N/A"}</span>
                              </li>
                            </ul> */}

                            <div className="row marB25">
                              <div className="col-auto col-md-4">
                                <svg xmlns="http://www.w3.org/2000/svg" height="15px" viewBox="0 0 24 24" width="15px" fill="#FA852B">
                                  <path d="M0 0h24v24H0z" fill="none" /><path d="M20 6h-4V4c0-1.11-.89-2-2-2h-4c-1.11 0-2 .89-2 2v2H4c-1.11 0-1.99.89-1.99 2L2 19c0 1.11.89 2 2 2h16c1.11 0 2-.89 2-2V8c0-1.11-.89-2-2-2zm-6 0h-4V4h4v2z" />
                                </svg>
                                <span className="ml-2">{cat(res) ? cat(res) : "N/A"}</span>
                              </div>
                              <div className="col-auto col-md-4">
                                <svg xmlns="http://www.w3.org/2000/svg" height="16px" viewBox="0 0 24 24" width="16px" fill="#FA852B">
                                  <path d="M0 0h24v24H0z" fill="none" /><path d="M20 4H4c-1.11 0-1.99.89-1.99 2L2 18c0 1.11.89 2 2 2h16c1.11 0 2-.89 2-2V6c0-1.11-.89-2-2-2zm0 14H4v-6h16v6zm0-10H4V6h16v2z" />
                                </svg>
                                <span className="ml-2">{rate(res) ?"$"+rate(res) : "N/A"}</span>
                              </div>
                              <div className="col-auto col-md-4">
                                <svg xmlns="http://www.w3.org/2000/svg" enable-background="new 0 0 24 24" height="15px" viewBox="0 0 24 24" width="15px" fill="#FA852B"><g>
                                  <path d="M0,0h24v24H0V0z" fill="none" /></g><g><path d="M12,2c-4.2,0-8,3.22-8,8.2c0,3.32,2.67,7.25,8,11.8c5.33-4.55,8-8.48,8-11.8C20,5.22,16.2,2,12,2z M12,12c-1.1,0-2-0.9-2-2 c0-1.1,0.9-2,2-2c1.1,0,2,0.9,2,2C14,11.1,13.1,12,12,12z" /></g>
                                </svg>
                                <span className="ml-2">{res.workerLocation ? res.workerLocation : "N/A"}</span>
                              </div>
                            </div>
                          

                            <div class="skills">
                              {
                                res.skills.map((skill,index) => (
                                  index<5?(<span>{skill.skillName}</span>):(index==5?<a className="ml-2">Show more</a>:null)
                                ))
                              }
                            </div>

                          </div>
                        </div>
                      </Link>
                    ))}

                </div>
               } 
                <div className="modal fade" id="enterDetails" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">

                        <div className="modal-dialog modal-dialog-centered multistepModal CreateProfileModal" role="document">

                          <Modal />

                        </div>

                    </div>
              </div>
            </div>


          </div>
        </div>
      </section>
    </div>
  );

};
const mapStateToProps = state => {
  return {
    searchResult:state.candidateSearchResult,
    data:sessionStorage.getItem("candidateSearchFilters"),
    filters: state.candidateSearchFilters.fields,
    modalSelected:state.modalSelected,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    searchFilters: (data) => dispatch({ type: "SEARCH_FILTERS", data: data }),
    
    changeModal : (modal)=> dispatch({type:"CHANGE_MODAL", modal : modal}),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Search);
