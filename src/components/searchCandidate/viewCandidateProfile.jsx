import React from 'react';
import { connect } from 'react-redux'
import axios from 'axios';
import $ from 'jquery';

import { Link} from 'react-router-dom';
import Header from '../header/Header';
import Modal from '../user_management/modal';

import userPhotoDefault from "../../imgaes/userPhotoDefault.png";
import certification from '../../imgaes/certification.png'
import Workplace from '../../imgaes/Workplace.png'
import template from '../../imgaes/template_3.jpg'
import CollapsedCertifications from '../createProfile/additionalDetails/collapsedDetails/collapsedCertifications';
import CollapsedEducation from '../createProfile/additionalDetails/collapsedDetails/collapsedEducation'
import CollapsedWorkDetails from '../createProfile/additionalDetails/collapsedDetails/collapsedWorkDetails'
import MicrosoftTeamsImage from '../../imgaes/MicrosoftTeams-image.png'
import PdfIcon from '../../imgaes/icons/pdf_icon.jpg'
import DocIcon from '../../imgaes/icons/doc_icon.png'

import '../viewProfile/viewProfile.css'

class ViewCandidateProfile extends React.Component {

  
  workerID = null
  state={
    profilepic:null,
    coverpic:'',
    resume:'',
    resumeFileType:'',
    audio:''

  }
  
  constructor(props) {
    super(props)
    this.workerID = this.props.match.params.id;
    

  }

  componentWillMount = async () => {
    try {
      $('.modal-backdrop').hide();
      // $('#signUp').click();
      // window.location.reload(false);
    } catch (err) {

    }
    //const workerID=

    

    await axios.get('api/search/workers/' + this.workerID).then(async (res) => {

      await this.props.searchResult(res.data)

    })
    await axios.get('api/workers/profile/'+this.workerID).then(async (res) => 
        {
            if(res.data.photo) {
            localStorage.setItem('photoID',res.data.photo.id)
            this.setState({profilepic:res.data.photo.picContentType+', '+res.data.photo.pic})
          }
          if(res.data.files)
          {
            res.data.files.map(async(file)=>{
              if(file.filenameContentType.includes("image"))
             await this.setState({coverpic:file.filenameContentType+','+file.filename})
            })
          }
        })

      if(localStorage.getItem("token")!==null)
      {
        await axios.get('api/files/worker/'+this.workerID,{headers:{
          'Content-Type':'application/json',
          'Authorization':'Bearer '+localStorage.getItem("token")
        }}).then(res=>{
          if(res.data)
          {
            res.data.map(async(file)=>{
              if(file.isResume){
                let res1 = await fetch(file.filenameContentType+','+file.filename);
                let blob = await res1?.blob();
            await this.setState({resume:URL.createObjectURL(blob),resumeFileType:(file.filenameContentType.includes("pdf")?"pdf":"doc")})
                  
          }
              if(file.filenameContentType.includes("audio"))
              await this.setState({audio:file.filenameContentType+', '+file.audioresume})
            
            })
            
          }
        })
      }
   
  }

  email = () => {
    let mail = this.props.search.result.email
    if (mail) {
      let str = mail.split("@")
      let result = ""
      result = result + mail[0]

      for (let i = 0; i < (str[0].length) - 2; i++)
        result += "x"

      result += str[0][(str[0].length) - 1]
      result += "@" + str[1]

      return result
    }
    return ""
  }

  saveCandidate = async () => {
    this.props.setViewDetails(true)
    axios.patch("api/recruiters",{workers: [

      {

       id:parseInt(this.workerID)

      }

    ]}, {headers : {'Content-Type': 'application/merge-patch+json'}}).then((res)=>console.log(res))
  }

  file = () => {
    // let files = this.props.search.result.files
    // let path = ""
    // files.map((file => {
    //   if (file.isProfilePic) {
    //     path = file.path
    //   }
    // }));
    
    
  }

  workLocation=()=>{
    let employments=this.props.search.result.employments
    let location=""
    employments.map(emp=>{
      if(emp.locations.length>0 && emp.isCurrent)
      {
        location=emp.locations[0].city
      }
      
    })
    if(!location)
    location=this.props.search.result.workerLocation
    return location
  }
   rate=(res)=>{
   
      var rate="";
      var currency=res.currencyType?res.currencyType:""
      if(res.dailyRate>0)
      rate=res.dailyRate+" "+currency +"/Day"
      if(res.hourlyRate>0)
      rate=res.hourlyRate+" "+currency+"/Hour"
      if(res.monthlyRate>0)
      rate=res.monthlyRate+" "+currency+"/Month"
      if(res.yearlyRate>0)
      rate=res.yearlyRate+" "+currency+"/Year"
      return rate; 
    
    
  }

  cat=(res)=>{
    var result=null;
    if(res.category)
    result=res.category
    if(res.jobPreferences[0] && res.jobPreferences[0].subCategory){
    if(res.jobPreferences[0].subCategory.name)
    result+="/"+res.jobPreferences[0].subCategory.name
    }

    return result;
  }





  render() {

    try{
      $('.modal-backdrop').hide();
      } 
    catch(err){}
    
    console.log(this.state.resumeFileType)
    console.log(this.state.resume)

    $('.trigger-more').click(function() {
      if($(this).hasClass('collapsed')) {
        $(this).text('See Less');
      } else {
        $(this).text('See More');
      }
  });
    
    return (
      <div>
        <Header hideCandidate={true}/>
        <section class="mainbgColor view-profile-section">
          <div class="container-fluid">
            <div class="row mobMar0">
              <div class="col-md-9 mobPadd0">
                <div class="ProfileDetail">
                  <div class="ProfileBasicDetail">
                    <div class="coverPhoto" style={{backgroundImage:this.state.coverpic!=''?`url(${this.state.coverpic})`:MicrosoftTeamsImage,backgroundRepeat:'no-repeat'}}>


                    {/* {this.state.coverpic ? <img src={this.state.coverpic}  /> : <img src={MicrosoftTeamsImage}/>} */}

                    </div>
                    <div class="ViewProfileDetail marT-30">
                      <div class="ProfilePicMain">
                      {this.state.profilepic ? <img src={this.state.profilepic} class="profileImg" /> : <img src={userPhotoDefault} class="profileImg"/>}
                        {/* <img src={this.state.profilepic} class="profileImg" /> */}
                      </div>
                      <div class="profileActionButtons">

                      {/* {this.props.employmentQues.JobStatus.JobStatus==='Actively Applying'?
                            <div class="status online"><span class="onlinedot"></span>Actively Applying</div>
                            :this.props.employmentQues.JobStatus.JobStatus==='Casually Looking'?
                            <div class="status casual"><span class="onlinedot"></span>Casually Looking</div>
                            :<div class="status explore"><span class="onlinedot"></span>Exploring Marketplace</div>} */}

                      {this.props.search.result.jobPreferences[0] && this.props.search.result.jobPreferences[0].jobSearchStatus ==='Actively Applying'?
                            <div class="status online"><span class="onlinedot"></span>Actively applying</div>
                            :this.props.search.result.jobPreferences[0] && this.props.search.result.jobPreferences[0].jobSearchStatus==='Casually Looking'?
                            <div class="status casual"><span class="onlinedot"></span>Casually looking</div>
                            :<div class="status explore"><span class="onlinedot"></span>Exploring marketplace</div>}


                      </div>

                      <h2 class="FontBold">{this.props.search.result.firstName} {this.props.search.result.lastName}</h2>

                      <div class="ProfileBasic">
                        <ul class="ProfileBasicInfo">
                          <li>
                            <span>
                              <svg xmlns="http://www.w3.org/2000/svg" height="15px" viewBox="0 0 24 24" width="15px" fill="#FA852B">
                                <path d="M0 0h24v24H0z" fill="none" /><path d="M20 6h-4V4c0-1.11-.89-2-2-2h-4c-1.11 0-2 .89-2 2v2H4c-1.11 0-1.99.89-1.99 2L2 19c0 1.11.89 2 2 2h16c1.11 0 2-.89 2-2V8c0-1.11-.89-2-2-2zm-6 0h-4V4h4v2z" />
                              </svg>
                              {this.cat(this.props.search.result)?this.cat(this.props.search.result):"N/A"}
                            </span>
                          </li>
                          <li>
                            <span>
                              <svg xmlns="http://www.w3.org/2000/svg" enable-background="new 0 0 24 24" height="15px" viewBox="0 0 24 24" width="15px" fill="#FA852B"><g>
                                <path d="M0,0h24v24H0V0z" fill="none" /></g><g><path d="M12,2c-4.2,0-8,3.22-8,8.2c0,3.32,2.67,7.25,8,11.8c5.33-4.55,8-8.48,8-11.8C20,5.22,16.2,2,12,2z M12,12c-1.1,0-2-0.9-2-2 c0-1.1,0.9-2,2-2c1.1,0,2,0.9,2,2C14,11.1,13.1,12,12,12z" /></g>
                              </svg>
                              {this.props.search.result.workerLocation?this.props.search.result.workerLocation:"N/A"}
                            </span>
                          </li>
                        </ul>
                        {(localStorage.getItem("token") === null) ?
                        <div>
                          <a href='' class="CandidateContactBtn" data-toggle="modal" data-target="#hide">View Contact Details</a>
                        </div>
                        :
                        <>
                        {this.props.viewDetails? 
                        <ul class="ProfileBasicInfo">
                          <li>
                            
                              <span >

                                <svg xmlns="http://www.w3.org/2000/svg" height="15px" viewBox="0 0 24 24" width="15px" fill="#FA852B">
                                  <path d="M0 0h24v24H0z" fill="none" /><path d="M20.01 15.38c-1.23 0-2.42-.2-3.53-.56-.35-.12-.74-.03-1.01.24l-1.57 1.97c-2.83-1.35-5.48-3.9-6.89-6.83l1.95-1.66c.27-.28.35-.67.24-1.02-.37-1.11-.56-2.3-.56-3.53 0-.54-.45-.99-.99-.99H4.19C3.65 3 3 3.24 3 3.99 3 13.28 10.73 21 20.01 21c.71 0 .99-.63.99-1.18v-3.45c0-.54-.45-.99-.99-.99z" />
                                </svg>
                                +91 {this.props.search.result.primaryPhone}

                              </span>
                             
                            
                          </li>

                          <li>
                              <span>
                                <svg xmlns="http://www.w3.org/2000/svg" height="15px" viewBox="0 0 24 24" width="15px" fill="#FA852B">
                                  <path d="M0 0h24v24H0z" fill="none" /><path d="M20 4H4c-1.1 0-1.99.9-1.99 2L2 18c0 1.1.9 2 2 2h16c1.1 0 2-.9 2-2V6c0-1.1-.9-2-2-2zm0 4l-8 5-8-5V6l8 5 8-5v2z" />
                                </svg>
                                {this.props.search.result.email}
                              </span> 
                              


                            
                          </li>
                        </ul>
                        :<a style={{cursor:'pointer',color:'#0d6efd'}} class="CandidateContactBtn" onClick={() => {this.saveCandidate()}}>View Contact Details</a>}
                        </>
                        }


                      </div>

                    </div>
                  </div>

                  <div class="ProfileBasicDetail">
                    <div class="ViewProfileDetail Summary">
                      <div class="EmploynmentConditionHead">
                        <h2>Summary</h2>

                      </div>
                      <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took
                        a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into
                        electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets
                        containing Lorem Ipsum passages, and more recently with desktop publishing
                        software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                    </div>
                  </div>

                  <div class="ProfileBasicDetail">
                    <div class="ViewProfileDetail VieweditableDetails">
                      <div class="ViewProfileDetailHead disF">
                        <h2>Work History</h2>

                        <div class="expand ViewHistory">
                          {this.props.search.result.employments.length > 1 && <a style={{ textDecoration: 'none' }} role="button" class="collapsed" data-toggle="collapse" href="#ViewAllHistory" aria-expanded="false" aria-controls="ViewAllHistory">View All {'>'}</a>}
                        </div>

                      </div>

                      {this.props.search.result.employments.length > 0 ?
                        <div class="addedDetail paddX15 disF">
                          <div class="workIconImg">
                            {/* <img src={Workplace} /> */}
                            <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAABmJLR0QA/wD/AP+gvaeTAAAAbklEQVRIiWNgGAUEACMyx2vCo//EaNpWIMdIWBUEMJHqIlIBCzZBmAthPiLFxehgYHyAKy7IiSOa+wCrBdsK5BjxhTu6PD71gysOcMnjUz8aBwTB0I8DFB8QW+bgKquw+YTmPiCplByUZdEoIAgAB5M9EE6djkIAAAAASUVORK5CYII="/>
                          </div>

                          <div class="addedDetailContent">
                            <p>{this.props.search.result.employments[0]?this.props.search.result.employments[0].jobTitle:null}</p>
                            <span>
                                
                                {(localStorage.getItem("token") !== null && this.props.search.result.employments[0])?
                                
                                <Link to="/searchCandidate" className="text-decoration-none">{this.props.search.result.employments[0].companyName}</Link>
                                
                                :"Xxxxxxxxxx"}, 

                                Full Time, 
                                {this.props.search.result.employments[0].startDate} - {this.props.search.result.employments[0].isCurrent === false ? this.props.search.result.employments[0].endDate : 'currently working'}
                            </span>
                          </div>



                        </div> : null
                      }
                      <div id='ViewAllHistory' className='collapse' aria-expanded="false">


                        {this.props.search.result.employments.length > 1 && this.props.search.result.employments.slice(1).map((val, id) => (
                          <div class="addedDetail paddX15 disF">
                            <div class="addedDetailImg">
                              {/* <img src={Workplace} /> */}
                              <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAABmJLR0QA/wD/AP+gvaeTAAAAbklEQVRIiWNgGAUEACMyx2vCo//EaNpWIMdIWBUEMJHqIlIBCzZBmAthPiLFxehgYHyAKy7IiSOa+wCrBdsK5BjxhTu6PD71gysOcMnjUz8aBwTB0I8DFB8QW+bgKquw+YTmPiCplByUZdEoIAgAB5M9EE6djkIAAAAASUVORK5CYII="/>
                            </div>

                            <div class="addedDetailContent">
                              <p>{val.jobTitle}</p>
                              <span>{(localStorage.getItem("token") !== null)?val.companyName:"Xxxxxxxxxx"}, Full Time, {val.startDate} - {val.isCurrent === false ? val.endDate : 'currently working'}</span>
                            </div>



                          </div>

                        ))}

                      </div>
                    </div>
                  </div>




                  <div class="ProfileBasicDetail">
                    <div class="ViewProfileDetail VieweditableDetails">
                      <div class="ViewProfileDetailHead disF">
                        <h2>Educational Details</h2>
                        <div class="expand ViewHistory">
                          {this.props.search.result.educations.length > 1 && <a style={{ textDecoration: 'none' }} role="button" class="collapsed" data-toggle="collapse" href="#ViewAllEducationalDetails" aria-expanded="false" aria-controls="ViewAllEducationalDetails">View All {'>'}</a>}
                        </div>
                      </div>


                      {this.props.search.result.educations.length > 0 ?
                        <div class="addedDetail paddX15 disF">
                          <div class="addedDetailImg">
                            <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAABmJLR0QA/wD/AP+gvaeTAAAMHUlEQVRoge1ZaXBc1ZX+7n2vXy/qTXJrc2uXbEu2kdRy4cgrTrEFEwIDgUkFkgKHCWSCSYZhqARsqW1jqEpmSIpKxXY2klCVsJlhhsF2cCDBxmYI1mLZsmRr75YsqSW1eu9+680PYcUttUxLtjPzw19V/znvnHvO98659/S5D7iGa/g/BGO0fPfJlfPRdzY03wj3nwyX43aFu91cuO1UeTq6JB2lQnfzNkLJTo9WK8BNlFQ6i90nHBy47xp5+lUNrETQcRBlleh4zh+TpAM6Svf0bHMdnw+Rwh0t7xJgtafRZf8sXT6dBb3Lu39Q0FlydCaJ8t0nV8qqttWko3fLCnPkWI1sRX4mqcy1wWYQIKkahiajWR2jgQc6fcH7lzzXxojGBkRVe0vPxf/j3La1Q5fyK5noA4Y4c6YTY1oZmYab8UW0+Ss8pd/iKb2eMejKHVZU5dlRkW2FwNFLmk/GRPRNhNHpCzKPP0x4SkVZ044omvZbz/a634EQbV7xzIdI7g9P5ghReWuGTv81halFFoOA5Xk2sizXjjyraZ5v4m/QGMNoOI6zowGcGQmykCiBp2QiLiuHCLgXBhpqWy6bSMHOv9zAM/4RvcBvlhXVVpBpZsvz7WRZtg1mvW6BoV8aUUnBgD+CrrEQOzcaIIwwlQFtkqS+YoHhp+3uFZF5ESnZ0fyRBtTrOIp1ZblYU5oDnl66ZK40GANODfvxQfcIAjERAMBloKTvybqBuWxmbfb+qu4bijrL72PAg594xtYc6xkx5dpMbFm2jZQ6LJdVTpfCcCiG4VAMveMR1u8PQ9XAOAIPCHmXMfYNUeKNl7JPa4/oo8qDBr3wVVVVqykhpCQrAxU5NlQ4bLAaFlZqkzERvf4w+icizOsPk6ikgid0UGLaASbjF95drk8u6BbuaJZVHX/d+aerOxdM5GKs/MHJjsc35FcutupwqDPEPugOEhCK0iwLyrOtKHdYoOe5OQPv84fRPx6GZzICgafYUGrGLZU2rCkx49Z9nWwkElkz2LDm45m2SUTcjE/Vy1L2kSJ301PQKOfZ6Xp+5jOzQHH7cjtuX24nDMBZXxxHesI42DnODrQPkEVmAyt3WEmR3YyxSAK94yF4g1FkGXlsLLfg8Q2ZWF1ciCxTWi1sdmyk9Szb0TLmbXTVfyYRQuhGjdN4ALOIJOkBqMwxojLHiG+uySGyytA0GCV/7g7jw/4Rdl2ugdz9+UVYXVyMTOPCAp8JBjwBjpucKU+5+kCj64sLcaLjCOqLzagvNgPzbbZpwtvo+q9U8r/vuXoVcY3I/zek3CNlu0/uFwjZzMD8USV2d6oj8WqiyN30FAge8jSuqkrXZhaRsp2te7KMwj98cUUh8UxGFh/tGTkKQLjw/C+eCBjYnAuKiobxqIKuMRF9/gSq800oyhSQlcGDI3Pv/7ikTj/UOHQTRs6kSyIlEYGnd6wvyyVOewac9gwc6/PxTnfr0iF37bmEgpc/6Ao/8kFXONmIgsRlLUvRmElRGWFgoIRAYww94yIYGAgIBA4qR2lYT2lopl8CEjPYHR0AMLh91ZsA3rwsIpKmNZ8673cuzbHBG4hC1RgbaqzphhvofrrmOQDPXaxfsqvpxzzht5Y6LCTfYiSfeMZxX10pLAYd9rd04dh3VuBX/+vD3o98eHRdLve7pnGbLyzzUVnb0rfN9fp8gp0Xkcxs5Sveidipf3+vrYznqSyK7JFUA8+qfSdM4XFdu9UoFN+xsohYDTrsOdqB+6+vwGKbCcGEBGCqmXyjPgcRScXBMwEc/lYVeeuU3+w+NPhqxe6WW7qfcf3TlSCyoKZV8WKXlQUjE2UOK39PbSk4SvBWWz/Meh1uWjY1mQYTEva3dOH4d1YAABSN4Qv7OvGvm/JxW5Ud3oCEL//6HPxR1du9rbYolR+nu3VphkDeFhV1qZ7nxiQVX+/fXnMole6Cjl8SiZ20GAQ+lJAgqioSioqusRA2VuTPacNTgu23OLHvIx8DgPGoDFllMOtpYcWulhdm6he8cDzLqMOZ1UXZSx+qX4qblzqzeYoDRe7W9VeESOmu1l/YDELxo+ursCzPjr1HO/CHjkFU5tg+c2bfUGbFYEAk33vbg4df6cVP7ynF6w8ugSDQ784MkIsad5YtstIbluTDactAbeEibKzII3od2XPZRCpe7LJSSrbcVV1MOEqwoSwP919fgY6RIHonwjjhGUcoLs2y0xhwejiGH75/HmFRw8nzUbz/7eWoLzGjwmHA1vW5xGYgv58RWI3TnpFU+vkWI3iOS1mG8/pLqgVCL1Xk2kmu5W/DWrbZAAKGu6pL0Dbkx4e9I5AVDSaBR1iUseknZ+CLyHDaBNxWZcfztxdif9sksxm46SAfXJ2Nvcd8Bc6G5huHdta996mYzGw7hBAwaCn39SwiTvcnX+Y4PtOz3fXzmc90OmFzfXFukmwkFIfdpEdxlhnFWWYAgKioGA7F8c7pfrz8QAXyrTrwdMp/IK7CfWgwKRgDT/GPdVl46ePx7wN4DwvArNLiCX2UqeyxmfLiXSdLVVU1OO2mJPlYJIF8S/I4rec5ZJoE6DiCQrswTQIA7EYOOo7CH0se8m5eaodJ4NZdLPNHRPSNh6d/w8HYnERmZWSgcdVNqRQ1KJtzrBZGZyQ8LiuwGIRUJnPCZuRYIK6Si6fEWqcJUVGZvismGv7YMTpZ2zGaPEOpTPswLSJzgWhYYtbrkFDUJHlYVGDg6Sy5qKjQGBBMJMsBIMPAYSgoYVFGsnsdR1D1oyP5Hf+ycbi30eUG4E47vnQVi3a0Nuk41Bl0yZcLoqKBAtDxyVXKGENcUuEwz35XgbgKk0AhcMnux6MK4pqyeaTx+oPpxnUBaWeEQjlc7shy3esqTfJ+pGcEHCFYV5Z8CMzs7Bfjjl+eY7tvKyDVi5P3W+XzrZAz5AWNDGn3EY1yPYGEOEtu5DnEpJRfGuZEMC4TuzE5s3FZA2PA4BNr/fNa7FOknREtI75/Ikx+JqsadBd1cKPAYzAQZZhHmQZiKjJnXAd9PBCBUc9NzwfOhuYbMwz8G5Ki2CnlGE9xNsAsm3zPlI2mWjPtjAw+sdbPczTU70++S861GDEcik2TEBUV3skIunwhRCUNhzoC6BiNQ1anhrHhkAwDT2DRJ2fkUGcQobhy6AIJo547fHOl0/7kjdV4bGMVqc7PqrQh7Fm170RyPX6KeXV2UZVfOtoz8viSbOt04A6zARFRwccDY2gbmkAgJsFhMTITT4moMPzn6UnWN54g50MyVhWZWXWegVQ7zUkZ9McU/Hf7JKOU2wkAGXr+jZuqFpNaZxaAqb506/IChBKS0OUL/gbAvZdFpF+te1KItH27fyLMlyyyAADahyehqAwdw5P4QlUBCuxmEAISTEh4takL++6dOhxisob3u4Jk+wEvBJ6STl8clTlTjfTFoyMgQGvPMzWnAUBWVfvy3Nlf21YuzsJAILIpVWzz+/frJoqksafebBtgIVHGGy19ON47ihuW5MKo51lhphlzjeUmHcWty+xgjOCxDXm4/+VuvHxiDMf6wnitxa+FI3T6LRNKmaTO/nglqio0hSUunwiA/u21P5JV9c97jpwBpcDDa5fhcyU58PgjJPoZp9c7ZwJwFZjwtVUOvPPNSuw77sOW3/cgKmlbvM9e13NBjwfpPNaTvKdVjeFY7yiTNO1nV4QIAHQ/XXOTxtiwL5xgMUkBTynqChw40O6Z0yYqqdh1eAj/vG6q3/RPiAgmVKYo7KCn0fWbi3WDerapbdgvvtbUi46RAFqHJrD3WAeLy8q5gca6XVeESJG7ub14R9OW3u2uxYGo+PbeDztZy9AE1pfn4XwwjjMjgZR22w54saHUjGU5Ruz4wyC2vNLLgqL6cG9j3eaZuqP/VuPrkvz27rHg6/9z2jP2bseQdyIiNpz9fk3lXHHN+4qcEDKqgRsDgJ4G152FO1rufK/z/G+Pdo9YKnNt5J3TXsQlBWU5U4dBMKGi4aAHbefjqC+2YO2L7YxpaI8p+JK3oa5vTkfuzyf6gPvSjmu+ROZCYUPrQ3od2QrCamRVoxdWJ4yAEAaLkRfDCfWwzBLPXo2by6ty9Q/3nwwFnHUzx/h7dEj8pLvhcx9dFT/XcA1XH38FSPcnaGfJ7/sAAAAASUVORK5CYII="/>
                          </div>

                          <div class="addedDetailContent">
                            <p>{this.props.search.result.educations[0].degreeName}</p>
                            <span>{this.props.search.result.educations[0].institute}, {this.props.search.result.educations[0].marksType} {this.props.search.result.educations[0].marks}, {this.props.search.result.educations[0].yearOfPassing}</span>
                          </div>



                        </div> : null
                      }
                      <div id='ViewAllEducationalDetails' className='collapse' aria-expanded="false">
                        {this.props.search.result.educations.length > 1 && this.props.search.result.educations.slice(1).map((val, id) => (
                          <div class="addedDetail paddX15 disF">
                            <div class="addedDetailImg">
                              <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAABmJLR0QA/wD/AP+gvaeTAAAMHUlEQVRoge1ZaXBc1ZX+7n2vXy/qTXJrc2uXbEu2kdRy4cgrTrEFEwIDgUkFkgKHCWSCSYZhqARsqW1jqEpmSIpKxXY2klCVsJlhhsF2cCDBxmYI1mLZsmRr75YsqSW1eu9+680PYcUttUxLtjPzw19V/znvnHvO98659/S5D7iGa/g/BGO0fPfJlfPRdzY03wj3nwyX43aFu91cuO1UeTq6JB2lQnfzNkLJTo9WK8BNlFQ6i90nHBy47xp5+lUNrETQcRBlleh4zh+TpAM6Svf0bHMdnw+Rwh0t7xJgtafRZf8sXT6dBb3Lu39Q0FlydCaJ8t0nV8qqttWko3fLCnPkWI1sRX4mqcy1wWYQIKkahiajWR2jgQc6fcH7lzzXxojGBkRVe0vPxf/j3La1Q5fyK5noA4Y4c6YTY1oZmYab8UW0+Ss8pd/iKb2eMejKHVZU5dlRkW2FwNFLmk/GRPRNhNHpCzKPP0x4SkVZ044omvZbz/a634EQbV7xzIdI7g9P5ghReWuGTv81halFFoOA5Xk2sizXjjyraZ5v4m/QGMNoOI6zowGcGQmykCiBp2QiLiuHCLgXBhpqWy6bSMHOv9zAM/4RvcBvlhXVVpBpZsvz7WRZtg1mvW6BoV8aUUnBgD+CrrEQOzcaIIwwlQFtkqS+YoHhp+3uFZF5ESnZ0fyRBtTrOIp1ZblYU5oDnl66ZK40GANODfvxQfcIAjERAMBloKTvybqBuWxmbfb+qu4bijrL72PAg594xtYc6xkx5dpMbFm2jZQ6LJdVTpfCcCiG4VAMveMR1u8PQ9XAOAIPCHmXMfYNUeKNl7JPa4/oo8qDBr3wVVVVqykhpCQrAxU5NlQ4bLAaFlZqkzERvf4w+icizOsPk6ikgid0UGLaASbjF95drk8u6BbuaJZVHX/d+aerOxdM5GKs/MHJjsc35FcutupwqDPEPugOEhCK0iwLyrOtKHdYoOe5OQPv84fRPx6GZzICgafYUGrGLZU2rCkx49Z9nWwkElkz2LDm45m2SUTcjE/Vy1L2kSJ301PQKOfZ6Xp+5jOzQHH7cjtuX24nDMBZXxxHesI42DnODrQPkEVmAyt3WEmR3YyxSAK94yF4g1FkGXlsLLfg8Q2ZWF1ciCxTWi1sdmyk9Szb0TLmbXTVfyYRQuhGjdN4ALOIJOkBqMwxojLHiG+uySGyytA0GCV/7g7jw/4Rdl2ugdz9+UVYXVyMTOPCAp8JBjwBjpucKU+5+kCj64sLcaLjCOqLzagvNgPzbbZpwtvo+q9U8r/vuXoVcY3I/zek3CNlu0/uFwjZzMD8USV2d6oj8WqiyN30FAge8jSuqkrXZhaRsp2te7KMwj98cUUh8UxGFh/tGTkKQLjw/C+eCBjYnAuKiobxqIKuMRF9/gSq800oyhSQlcGDI3Pv/7ikTj/UOHQTRs6kSyIlEYGnd6wvyyVOewac9gwc6/PxTnfr0iF37bmEgpc/6Ao/8kFXONmIgsRlLUvRmElRGWFgoIRAYww94yIYGAgIBA4qR2lYT2lopl8CEjPYHR0AMLh91ZsA3rwsIpKmNZ8673cuzbHBG4hC1RgbaqzphhvofrrmOQDPXaxfsqvpxzzht5Y6LCTfYiSfeMZxX10pLAYd9rd04dh3VuBX/+vD3o98eHRdLve7pnGbLyzzUVnb0rfN9fp8gp0Xkcxs5Sveidipf3+vrYznqSyK7JFUA8+qfSdM4XFdu9UoFN+xsohYDTrsOdqB+6+vwGKbCcGEBGCqmXyjPgcRScXBMwEc/lYVeeuU3+w+NPhqxe6WW7qfcf3TlSCyoKZV8WKXlQUjE2UOK39PbSk4SvBWWz/Meh1uWjY1mQYTEva3dOH4d1YAABSN4Qv7OvGvm/JxW5Ud3oCEL//6HPxR1du9rbYolR+nu3VphkDeFhV1qZ7nxiQVX+/fXnMole6Cjl8SiZ20GAQ+lJAgqioSioqusRA2VuTPacNTgu23OLHvIx8DgPGoDFllMOtpYcWulhdm6he8cDzLqMOZ1UXZSx+qX4qblzqzeYoDRe7W9VeESOmu1l/YDELxo+ursCzPjr1HO/CHjkFU5tg+c2bfUGbFYEAk33vbg4df6cVP7ynF6w8ugSDQ784MkIsad5YtstIbluTDactAbeEibKzII3od2XPZRCpe7LJSSrbcVV1MOEqwoSwP919fgY6RIHonwjjhGUcoLs2y0xhwejiGH75/HmFRw8nzUbz/7eWoLzGjwmHA1vW5xGYgv58RWI3TnpFU+vkWI3iOS1mG8/pLqgVCL1Xk2kmu5W/DWrbZAAKGu6pL0Dbkx4e9I5AVDSaBR1iUseknZ+CLyHDaBNxWZcfztxdif9sksxm46SAfXJ2Nvcd8Bc6G5huHdta996mYzGw7hBAwaCn39SwiTvcnX+Y4PtOz3fXzmc90OmFzfXFukmwkFIfdpEdxlhnFWWYAgKioGA7F8c7pfrz8QAXyrTrwdMp/IK7CfWgwKRgDT/GPdVl46ePx7wN4DwvArNLiCX2UqeyxmfLiXSdLVVU1OO2mJPlYJIF8S/I4rec5ZJoE6DiCQrswTQIA7EYOOo7CH0se8m5eaodJ4NZdLPNHRPSNh6d/w8HYnERmZWSgcdVNqRQ1KJtzrBZGZyQ8LiuwGIRUJnPCZuRYIK6Si6fEWqcJUVGZvismGv7YMTpZ2zGaPEOpTPswLSJzgWhYYtbrkFDUJHlYVGDg6Sy5qKjQGBBMJMsBIMPAYSgoYVFGsnsdR1D1oyP5Hf+ycbi30eUG4E47vnQVi3a0Nuk41Bl0yZcLoqKBAtDxyVXKGENcUuEwz35XgbgKk0AhcMnux6MK4pqyeaTx+oPpxnUBaWeEQjlc7shy3esqTfJ+pGcEHCFYV5Z8CMzs7Bfjjl+eY7tvKyDVi5P3W+XzrZAz5AWNDGn3EY1yPYGEOEtu5DnEpJRfGuZEMC4TuzE5s3FZA2PA4BNr/fNa7FOknREtI75/Ikx+JqsadBd1cKPAYzAQZZhHmQZiKjJnXAd9PBCBUc9NzwfOhuYbMwz8G5Ki2CnlGE9xNsAsm3zPlI2mWjPtjAw+sdbPczTU70++S861GDEcik2TEBUV3skIunwhRCUNhzoC6BiNQ1anhrHhkAwDT2DRJ2fkUGcQobhy6AIJo547fHOl0/7kjdV4bGMVqc7PqrQh7Fm170RyPX6KeXV2UZVfOtoz8viSbOt04A6zARFRwccDY2gbmkAgJsFhMTITT4moMPzn6UnWN54g50MyVhWZWXWegVQ7zUkZ9McU/Hf7JKOU2wkAGXr+jZuqFpNaZxaAqb506/IChBKS0OUL/gbAvZdFpF+te1KItH27fyLMlyyyAADahyehqAwdw5P4QlUBCuxmEAISTEh4takL++6dOhxisob3u4Jk+wEvBJ6STl8clTlTjfTFoyMgQGvPMzWnAUBWVfvy3Nlf21YuzsJAILIpVWzz+/frJoqksafebBtgIVHGGy19ON47ihuW5MKo51lhphlzjeUmHcWty+xgjOCxDXm4/+VuvHxiDMf6wnitxa+FI3T6LRNKmaTO/nglqio0hSUunwiA/u21P5JV9c97jpwBpcDDa5fhcyU58PgjJPoZp9c7ZwJwFZjwtVUOvPPNSuw77sOW3/cgKmlbvM9e13NBjwfpPNaTvKdVjeFY7yiTNO1nV4QIAHQ/XXOTxtiwL5xgMUkBTynqChw40O6Z0yYqqdh1eAj/vG6q3/RPiAgmVKYo7KCn0fWbi3WDerapbdgvvtbUi46RAFqHJrD3WAeLy8q5gca6XVeESJG7ub14R9OW3u2uxYGo+PbeDztZy9AE1pfn4XwwjjMjgZR22w54saHUjGU5Ruz4wyC2vNLLgqL6cG9j3eaZuqP/VuPrkvz27rHg6/9z2jP2bseQdyIiNpz9fk3lXHHN+4qcEDKqgRsDgJ4G152FO1rufK/z/G+Pdo9YKnNt5J3TXsQlBWU5U4dBMKGi4aAHbefjqC+2YO2L7YxpaI8p+JK3oa5vTkfuzyf6gPvSjmu+ROZCYUPrQ3od2QrCamRVoxdWJ4yAEAaLkRfDCfWwzBLPXo2by6ty9Q/3nwwFnHUzx/h7dEj8pLvhcx9dFT/XcA1XH38FSPcnaGfJ7/sAAAAASUVORK5CYII="/>
                            </div>

                            <div class="addedDetailContent">
                              <p>{val.degreeName}</p>
                              <span>{val.institute}, {val.marksType} {val.marks}, {val.yearOfPassing}</span>
                            </div>



                          </div>
                        ))}

                      </div>
                    </div>
                  </div>

                  <div class="ProfileBasicDetail">
                    <div class="ViewProfileDetail VieweditableDetails">
                      <div class="ViewProfileDetailHead disF">
                        <h2>Certifications</h2>

                        <div class="expand ViewHistory">
                          {this.props.search.result.certificates.length > 1 && <a style={{ textDecoration: 'none' }} role="button" class="collapsed" data-toggle="collapse" href="#ViewAllCertifications" aria-expanded="false" aria-controls="ViewAllCertifications">View All {'>'}</a>}
                        </div>

                      </div>

                      {this.props.search.result.certificates.length > 0 ?
                        <div class="addedDetail paddX15 disF">
                          <div class="workIconImg">
                            {/* <img src={certification} /> */}
                            <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAABmJLR0QA/wD/AP+gvaeTAAAInklEQVRoge2Ye4xcdRXHP+d37507z85Ou93tW0JpoS1CfCWopVhQSXwQESUhKoWEGGOQgJLoHxgrkWiipBIV4yPxjfEFBgJGBS2WxFcQ2tIHYCmwpfve7c7Mzty5j9/xjzuzu6W0bHfX//ab3Oz+7u/cc77fc87vkYFFLGIRi1jEIhaxiEX8vyCdf/78eG1Tb8Xf1tvrNYEBVfpFGOrtlcGzcagP3LGebParGCm+toGtEfIFueqLR8/G7+Cg9qrSI8JKYMUrQ638RC3cvf0dSw6fJOTRPfWnjLBp83mFqgjLZ/iIgCFgQIR+YFCV4yIMqXIcGAQGkoT+lU98OaZSrOK4/hlZJVHA+GS5f+uXXMdJiQG9Iqxqk10F9KpOzfUA3lQulMGD/52sqLL/ikuLb50S8pc99YsVno7jGJf6sxduXn3+2WSrA7/e91z5pfs3TgU0Lo1Vb6Nw7O9YL0/9nMvIv/Jv3MYIE2/48HOt4tqNZ/J3Ojxz6PjhmOIFruOgysXv3lbcZ9KIXA4wWa/TP1RdNhfnAGqTNGthA4Iq0hjD738agioENcLyOdTXbQXVads5oH9gonuyXgNATMrdTUdsAWg2G9TrzfJcA2CA6gBEzalX7uRoe2qUyp6voW4emiOp7RxRqze6xHiUy12guhnaQhS5AJQwjGi1Wp61tmWMOXOfvwbiTHdrpohXw2mMAWPTtnNAHNsgDMNsFIYACGYTTOVFl6VGEQBhmNTnEgTHsyeNrcCkSR8rZ7adJZpBqw4QxTEAii6FTmtBCcDa1HcYxa1sds4tnJLu82DYBW0LEIWeGNaE82qrVisJARKbpG7b3DtCCjBDSBiHZxsgtooODa/gmRyEAklbgGs1NTDCoAcjLmSU1obhFdK7GtfIGbyeClUbAWibq54kREmY4U9EzpgzBawqVsEqTNRimoHFDzLF1U0DjirL4yaXD3pUgrS047mIx3oixp0cTSOjQabYGgrJZQ3lkosR2o/wOtJMm2NnHE8LMdRQlhmT8jcZpxjEFqtt0laxM8jPRCuwjA2na8vPjyrX9SV0xQ6uybMkD34hNVwWefS84hFbOOEm2FGtVQvUqhCr4mdPzl1HlAGMScUZAStSAuhwFahNC2kPHMcBILJSsdHs1mK9mvbq0mJfeFH5Tp9lRfBL0wYd4b4Hy9s7eyVyLhq709krO8PRyTWZejU5RUhabZ0etJEYtzxTiEIV2mVqXzVwPQ8RwbjOrBpXFcLQIgJbSt/w6F4Cfiad7DOwx4WHvfTZ46bvALIedC9hU/nrnkjqQ/X0cWYik3FFRPC8NI5AP7QrIsJBlCt936dQyAcikp2NU9vuPd8PrJcJDJlyWoGnHDhhWkfG3v7ygX1XrgLYctEfjq+P/7mOYfF5UwIZl4w3KX6mZYOWb6xVnFnkT4xQKOSDjJ/JAigcmBKCZR8C+XwB36MJzEpIbSJtq7w3HOOaNEUvGHTCNB/8610aNAobOmk7+Px1Gw48fdXkB6+4w8pRzXGuBdeQ94bioLU2U5tI6FrqnjbWTFS6Ck3HK6RChP3QaS3fPgSEhUKB1euWzWqXTxIlaCQ4Dmwp7XIp5dNqHHF48sg1A0GjkO/YLlkLl3weet9ZKOw9+qF+jjipbSnPltIu13EgaCQkyez6a/W6blMoFABCJ7IPt3OV4rEn6j/L+XJF1/LMShHUEQmBJnDCGBkXtF+QY5rwovH0UNB0jmxaL/t1YMcthNE99HTBpKBPePa+P+4yrgHHgyQEETj/IxAHcPhB+Nh7b7VcGhkKwNAJyHo3S89PvnPoiL4xm0vW20g2icM5iq5RZKW1WgG6gFyimlFFToxE/UHD/unybcUbYHrXwkSFG0vd4eaS4x5bu1bGZpUaANUKnT3dgoLWImX5EmHDVXDo1+mmcPg3qUktVKwYNVYATVXGVAA2rZf9wH7g92cK2denS+OCXd2q5Q513k0J2b5dYmDfrAV0YHiAMN6JVciBseq4Xs2OVEsm/JXQObgThVqkuJmqNYl1yLdP0zCGPPefTch2ok9K9jxuPSmk96f7yHp7qTXTtKywvH/rj2vNBIYCZXjG00zgA1t/VGOVBQeoNSHr7ZWVvzg4Xx7zFgKAZ3YRpqc7mxNWLnmhfPW2H064TotYIVZwnRZXb/vBxIry0TKb24dtGKXfLgDO7sZ2Gujw9Ruptp5lRQUcAwGwz0FHjTa03ASlINUcy6xwsQVfIbEwMA4ZZ6Osve/5+XJYECEA2vfxAwibWVpiamE0BCbai7oM5GdcOcZqYDko636+ZSHiz+4Emg0e6elh63CdcLxI3gfXgYIP+bYoVai3IE6g0YLAq7Onp2ehwi9Ma/3u1jcz4jxJw0B3GNEd1OkNDZWgjN/OVSuGsewEQxnLSLbISMYjb6E7eYtc883/zJfDwlTE6m10xZB4MO557C9UAFjdiji/PoECh/Nd9OfS6++aEHwLXTFYbgM+MV8K866I/vLmVbjOUSC9aw27UHcgkPTv0mQ3AGPOuyjZVEAxgeVxx0WEsefKNd86Nh8e899+PeczdEREAi0DnkLJwqo4RuVGyO1gRRRTTNK5lklt2x6w8un50phXRfShT+YJci8D6Y96oy40Z+QmZ3fLp+7eDqDfu303Dblsai5vYelUVcbRaJ1ce+/cfr1hvhUJcjfQEZGYl04SYVBcbpgaO7oDw/T1tmHSb1JUEO/6+VCZsxDdudMAt6CyF7gWc2w9rgZTBr59XG66u0MUuenul8jq36bmXW1xuHwexrwH9F/ArW2fc8Lcd60Lx9YDn5WP3vNI55V+/3PfJZbbMKr43o2nfuRcj0lexIrg672yc2cMPAo8qr+95X1tn3M65RfsZAdQxfDt2yfx9B+dtXGKzfdu303IJYwWi20hC4KFuTS2IYLFS+46aW28Go7uwNevLKSIRSxiEa+P/wEpiqiq6SMyUAAAAABJRU5ErkJggg=="/>
                          </div>

                          <div class="addedDetailContent">
                            <p>{this.props.search.result.certificates[0].certificateName}</p>
                            <span>{this.props.search.result.certificates[0].issuer}</span>
                          </div>



                        </div> : null}


                      {/* <div class="panel-body">
                          
                          {this.props.certificationFields.map((val,id) => id!==0&&(<CollapsedCertifications val = {val} key={id} id={val.id} modal="certifications"/>))}

                          </div> */}
                      <div id='ViewAllCertifications' className='collapse' aria-expanded="false">
                        {this.props.search.result.certificates.length > 1 && this.props.search.result.certificates.slice(1).map((val, id) => (
                          <div class="addedDetail paddX15 disF">
                            <div class="workIconImg">
                              {/* <img src={certification} /> */}
                              <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAABmJLR0QA/wD/AP+gvaeTAAAInklEQVRoge2Ye4xcdRXHP+d37507z85Ou93tW0JpoS1CfCWopVhQSXwQESUhKoWEGGOQgJLoHxgrkWiipBIV4yPxjfEFBgJGBS2WxFcQ2tIHYCmwpfve7c7Mzty5j9/xjzuzu6W0bHfX//ab3Oz+7u/cc77fc87vkYFFLGIRi1jEIhaxiEX8vyCdf/78eG1Tb8Xf1tvrNYEBVfpFGOrtlcGzcagP3LGebParGCm+toGtEfIFueqLR8/G7+Cg9qrSI8JKYMUrQ638RC3cvf0dSw6fJOTRPfWnjLBp83mFqgjLZ/iIgCFgQIR+YFCV4yIMqXIcGAQGkoT+lU98OaZSrOK4/hlZJVHA+GS5f+uXXMdJiQG9Iqxqk10F9KpOzfUA3lQulMGD/52sqLL/ikuLb50S8pc99YsVno7jGJf6sxduXn3+2WSrA7/e91z5pfs3TgU0Lo1Vb6Nw7O9YL0/9nMvIv/Jv3MYIE2/48HOt4tqNZ/J3Ojxz6PjhmOIFruOgysXv3lbcZ9KIXA4wWa/TP1RdNhfnAGqTNGthA4Iq0hjD738agioENcLyOdTXbQXVads5oH9gonuyXgNATMrdTUdsAWg2G9TrzfJcA2CA6gBEzalX7uRoe2qUyp6voW4emiOp7RxRqze6xHiUy12guhnaQhS5AJQwjGi1Wp61tmWMOXOfvwbiTHdrpohXw2mMAWPTtnNAHNsgDMNsFIYACGYTTOVFl6VGEQBhmNTnEgTHsyeNrcCkSR8rZ7adJZpBqw4QxTEAii6FTmtBCcDa1HcYxa1sds4tnJLu82DYBW0LEIWeGNaE82qrVisJARKbpG7b3DtCCjBDSBiHZxsgtooODa/gmRyEAklbgGs1NTDCoAcjLmSU1obhFdK7GtfIGbyeClUbAWibq54kREmY4U9EzpgzBawqVsEqTNRimoHFDzLF1U0DjirL4yaXD3pUgrS047mIx3oixp0cTSOjQabYGgrJZQ3lkosR2o/wOtJMm2NnHE8LMdRQlhmT8jcZpxjEFqtt0laxM8jPRCuwjA2na8vPjyrX9SV0xQ6uybMkD34hNVwWefS84hFbOOEm2FGtVQvUqhCr4mdPzl1HlAGMScUZAStSAuhwFahNC2kPHMcBILJSsdHs1mK9mvbq0mJfeFH5Tp9lRfBL0wYd4b4Hy9s7eyVyLhq709krO8PRyTWZejU5RUhabZ0etJEYtzxTiEIV2mVqXzVwPQ8RwbjOrBpXFcLQIgJbSt/w6F4Cfiad7DOwx4WHvfTZ46bvALIedC9hU/nrnkjqQ/X0cWYik3FFRPC8NI5AP7QrIsJBlCt936dQyAcikp2NU9vuPd8PrJcJDJlyWoGnHDhhWkfG3v7ygX1XrgLYctEfjq+P/7mOYfF5UwIZl4w3KX6mZYOWb6xVnFnkT4xQKOSDjJ/JAigcmBKCZR8C+XwB36MJzEpIbSJtq7w3HOOaNEUvGHTCNB/8610aNAobOmk7+Px1Gw48fdXkB6+4w8pRzXGuBdeQ94bioLU2U5tI6FrqnjbWTFS6Ck3HK6RChP3QaS3fPgSEhUKB1euWzWqXTxIlaCQ4Dmwp7XIp5dNqHHF48sg1A0GjkO/YLlkLl3weet9ZKOw9+qF+jjipbSnPltIu13EgaCQkyez6a/W6blMoFABCJ7IPt3OV4rEn6j/L+XJF1/LMShHUEQmBJnDCGBkXtF+QY5rwovH0UNB0jmxaL/t1YMcthNE99HTBpKBPePa+P+4yrgHHgyQEETj/IxAHcPhB+Nh7b7VcGhkKwNAJyHo3S89PvnPoiL4xm0vW20g2icM5iq5RZKW1WgG6gFyimlFFToxE/UHD/unybcUbYHrXwkSFG0vd4eaS4x5bu1bGZpUaANUKnT3dgoLWImX5EmHDVXDo1+mmcPg3qUktVKwYNVYATVXGVAA2rZf9wH7g92cK2denS+OCXd2q5Q513k0J2b5dYmDfrAV0YHiAMN6JVciBseq4Xs2OVEsm/JXQObgThVqkuJmqNYl1yLdP0zCGPPefTch2ok9K9jxuPSmk96f7yHp7qTXTtKywvH/rj2vNBIYCZXjG00zgA1t/VGOVBQeoNSHr7ZWVvzg4Xx7zFgKAZ3YRpqc7mxNWLnmhfPW2H064TotYIVZwnRZXb/vBxIry0TKb24dtGKXfLgDO7sZ2Gujw9Ruptp5lRQUcAwGwz0FHjTa03ASlINUcy6xwsQVfIbEwMA4ZZ6Osve/5+XJYECEA2vfxAwibWVpiamE0BCbai7oM5GdcOcZqYDko636+ZSHiz+4Emg0e6elh63CdcLxI3gfXgYIP+bYoVai3IE6g0YLAq7Onp2ehwi9Ma/3u1jcz4jxJw0B3GNEd1OkNDZWgjN/OVSuGsewEQxnLSLbISMYjb6E7eYtc883/zJfDwlTE6m10xZB4MO557C9UAFjdiji/PoECh/Nd9OfS6++aEHwLXTFYbgM+MV8K866I/vLmVbjOUSC9aw27UHcgkPTv0mQ3AGPOuyjZVEAxgeVxx0WEsefKNd86Nh8e899+PeczdEREAi0DnkLJwqo4RuVGyO1gRRRTTNK5lklt2x6w8un50phXRfShT+YJci8D6Y96oy40Z+QmZ3fLp+7eDqDfu303Dblsai5vYelUVcbRaJ1ce+/cfr1hvhUJcjfQEZGYl04SYVBcbpgaO7oDw/T1tmHSb1JUEO/6+VCZsxDdudMAt6CyF7gWc2w9rgZTBr59XG66u0MUuenul8jq36bmXW1xuHwexrwH9F/ArW2fc8Lcd60Lx9YDn5WP3vNI55V+/3PfJZbbMKr43o2nfuRcj0lexIrg672yc2cMPAo8qr+95X1tn3M65RfsZAdQxfDt2yfx9B+dtXGKzfdu303IJYwWi20hC4KFuTS2IYLFS+46aW28Go7uwNevLKSIRSxiEa+P/wEpiqiq6SMyUAAAAABJRU5ErkJggg=="/>
                            </div>

                            <div class="addedDetailContent">
                              <p>{val.certificateName}</p>
                              <span>{val.issuer}</span>
                            </div>



                          </div>))}
                      </div>

                    </div>


                    <div class="ProfileBasicDetail">
                      <div class="ViewProfileDetail">
                        <div class="EmploynmentConditionHead">
                          <h2>Skills</h2>


                        </div>
                        <div class="skills">
                          {this.props.search.result.skills.map((val) => (
                            <span>{val.skillName}</span>
                          ))}

                        </div>

                      </div>
                    </div>

                    {localStorage.getItem("token")!==null?
                    (<div class="ProfileBasicDetail">
                        <div class="ViewProfileDetail">
                          <div class="EmploynmentConditionHead">
                            <h2>Documents</h2>
    
                            
                          </div>
                          {this.state.resume!==''?
                          <div class="document">
                           
                          <div class="addedDetailImg"><img src={this.state.resumeFileType==="pdf"?PdfIcon:DocIcon}></img></div>
                           <div class="addedDetailContent"><a target='_blank' href={this.state.resume}>Resume</a><p id='resumeContentType'>{this.state.resumeFileType==="pdf"?'Pdf':'Doc'}</p></div>
                          </div>:null}
                        
                        </div>
                      </div>):null
                    }


                  </div>
                </div>
              </div>

              <div class="col-md-3">
                <div class="EmploynmentCondition mb-4">
                  <div class="EmploynmentConditionHead">
                    <h4>Employment Conditions</h4>


                  </div>


                  {/* <div class="editEmploynmentConditionInner" class="collapse" id="ConditionShow" aria-expanded="false"> */}
                  <div class="editEmploynmentConditionInner">
                    <div class="Conditions disF">
                      <svg xmlns="http://www.w3.org/2000/svg" enable-background="new 0 0 24 24" height="24px" viewBox="0 0 24 24" width="24px" fill="#FA852B"><rect fill="none" height="24" width="24" />
                        <path d="M7.76,16.24C6.67,15.16,6,13.66,6,12s0.67-3.16,1.76-4.24l1.42,1.42C8.45,9.9,8,10.9,8,12c0,1.1,0.45,2.1,1.17,2.83 L7.76,16.24z M16.24,16.24C17.33,15.16,18,13.66,18,12s-0.67-3.16-1.76-4.24l-1.42,1.42C15.55,9.9,16,10.9,16,12 c0,1.1-0.45,2.1-1.17,2.83L16.24,16.24z M12,10c-1.1,0-2,0.9-2,2s0.9,2,2,2s2-0.9,2-2S13.1,10,12,10z M20,12 c0,2.21-0.9,4.21-2.35,5.65l1.42,1.42C20.88,17.26,22,14.76,22,12s-1.12-5.26-2.93-7.07l-1.42,1.42C19.1,7.79,20,9.79,20,12z M6.35,6.35L4.93,4.93C3.12,6.74,2,9.24,2,12s1.12,5.26,2.93,7.07l1.42-1.42C4.9,16.21,4,14.21,4,12S4.9,7.79,6.35,6.35z" />
                      </svg>

                      <div class="employenmentsec">
                        <h6>Availability</h6>
                        <p>{this.props.search.result.jobPreferences[0]?this.props.search.result.jobPreferences[0].availableFrom:"N/A"} to {this.props.search.result.jobPreferences[0]?this.props.search.result.jobPreferences[0].availableTo:"N/A"}</p>
                      </div>
                    </div>

                    <div class="Conditions disF">
                      <svg xmlns="http://www.w3.org/2000/svg" height="21px" viewBox="0 0 24 24" width="21px" fill="#FA852B">
                        <path d="M0 0h24v24H0z" fill="none" /><path d="M20 6h-4V4c0-1.11-.89-2-2-2h-4c-1.11 0-2 .89-2 2v2H4c-1.11 0-1.99.89-1.99 2L2 19c0 1.11.89 2 2 2h16c1.11 0 2-.89 2-2V8c0-1.11-.89-2-2-2zm-6 0h-4V4h4v2z" />
                      </svg>

                      <div class="employenmentsec">
                        <h6>Work Type</h6>
                        <p>{this.props.search.result.jobPreferences[0]?this.props.search.result.jobPreferences[0].engagementType:"N/A"}</p>
                      </div>
                    </div>

                    <div class="Conditions disF">
                      <svg xmlns="http://www.w3.org/2000/svg" height="20px" viewBox="0 0 24 24" width="20px" fill="#FA852B">
                        <path d="M0 0h24v24H0z" fill="none" /><path d="M18 16.08c-.76 0-1.44.3-1.96.77L8.91 12.7c.05-.23.09-.46.09-.7s-.04-.47-.09-.7l7.05-4.11c.54.5 1.25.81 2.04.81 1.66 0 3-1.34 3-3s-1.34-3-3-3-3 1.34-3 3c0 .24.04.47.09.7L8.04 9.81C7.5 9.31 6.79 9 6 9c-1.66 0-3 1.34-3 3s1.34 3 3 3c.79 0 1.5-.31 2.04-.81l7.12 4.16c-.05.21-.08.43-.08.65 0 1.61 1.31 2.92 2.92 2.92 1.61 0 2.92-1.31 2.92-2.92s-1.31-2.92-2.92-2.92z" />
                      </svg>

                      <div class="employenmentsec">
                        <h6>Employment Type</h6>
                        <p>{this.props.search.result.jobPreferences[0]?this.props.search.result.jobPreferences[0].employmentType:"N/A"}</p>
                      </div>
                    </div>

                    <div class="Conditions disF">
                      <svg xmlns="http://www.w3.org/2000/svg" height="24px" viewBox="0 0 24 24" width="24px" fill="#FA852B">
                        <path d="M0 0h24v24H0z" fill="none" /><path d="M11.8 10.9c-2.27-.59-3-1.2-3-2.15 0-1.09 1.01-1.85 2.7-1.85 1.78 0 2.44.85 2.5 2.1h2.21c-.07-1.72-1.12-3.3-3.21-3.81V3h-3v2.16c-1.94.42-3.5 1.68-3.5 3.61 0 2.31 1.91 3.46 4.7 4.13 2.5.6 3 1.48 3 2.41 0 .69-.49 1.79-2.7 1.79-2.06 0-2.87-.92-2.98-2.1h-2.2c.12 2.19 1.76 3.42 3.68 3.83V21h3v-2.15c1.95-.37 3.5-1.5 3.5-3.55 0-2.84-2.43-3.81-4.7-4.4z" />
                      </svg>

                      <div class="employenmentsec">
                        <h6>Pay Rate</h6>
                        <p>{this.props.search.result.jobPreferences[0]?this.rate(this.props.search.result.jobPreferences[0]):"N/A"}</p>
                      </div>
                    </div>

                    <div class="collapse" id="candidateCollapse">
                      <div class="Conditions disF">
                        <svg xmlns="http://www.w3.org/2000/svg" enable-background="new 0 0 24 24" height="22px" viewBox="0 0 24 24" width="22px" fill="#FA852B"><g>
                          <path d="M0,0h24v24H0V0z" fill="none" /></g><g><path d="M12,2c-4.2,0-8,3.22-8,8.2c0,3.32,2.67,7.25,8,11.8c5.33-4.55,8-8.48,8-11.8C20,5.22,16.2,2,12,2z M12,12c-1.1,0-2-0.9-2-2 c0-1.1,0.9-2,2-2c1.1,0,2,0.9,2,2C14,11.1,13.1,12,12,12z" /></g>
                        </svg>

                        <div class="employenmentsec">
                          <h6>Work Location</h6>
                          <p>{this.props.search.result.jobPreferences[0] && this.props.search.result.jobPreferences[0].locationType ?( this.props.search.result.jobPreferences[0].locationType==="Both"?"Home and office":this.props.search.result.jobPreferences[0].locationType):"N/A"} </p>
                        </div>
                      </div>


                      <div class="Conditions disF">
                        <svg xmlns="http://www.w3.org/2000/svg" enable-background="new 0 0 24 24" height="22px" viewBox="0 0 24 24" width="22px" fill="#FA852B"><g>
                          <path d="M0,0h24v24H0V0z" fill="none" /></g><g><path d="M12,2c-4.2,0-8,3.22-8,8.2c0,3.32,2.67,7.25,8,11.8c5.33-4.55,8-8.48,8-11.8C20,5.22,16.2,2,12,2z M12,12c-1.1,0-2-0.9-2-2 c0-1.1,0.9-2,2-2c1.1,0,2,0.9,2,2C14,11.1,13.1,12,12,12z" /></g>
                        </svg>

                        <div class="employenmentsec">
                          <h6>Location Preference</h6>
                          <p>{(this.props.search.result.jobPreferences[0] && this.props.search.result.jobPreferences[0].locationPrefrences[0] &&this.props.search.result.jobPreferences[0].locationPrefrences[0].location)  ?this.props.search.result.jobPreferences[0].locationPrefrences[0].location.city:"N/A"}</p>
                        </div>
                      </div>
                    </div>
                    
                  </div>

                  <div className="expand Toggle">
                    
                    <a className="trigger-more collapsed" role="button" data-toggle="collapse" href="#candidateCollapse" aria-expanded="false" aria-controls="candidateCollapse">See More</a>
                  </div>

                </div>
                
                <div className="EmploynmentCondition">
                    <div className="EmploynmentConditionHead">
                      <h4>Similar Candidates</h4>
                    </div>
                    <div class="employenmentsec">
                        <h6 className="my-2">Lyla - Software Developer</h6>
                        <h6 className="my-2">Jack - Product Manager</h6>
                        <h6>John - Software Intern</h6>
                    </div>
                </div>

              </div>
            </div>
          </div>

        </section>

        <div class="modal fade" id="enterDetails" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered multistepModal CreateProfileModal" role="document">
            {/* <Modal subCategoryID={this.SubCategoryID} /> */}
          </div>
        </div>
      </div>

    )
  }
  componentWillUnmount()
  {
    if(this.props.viewDetails)
    {
      this.props.setViewDetails(false)
    }
  }
}
const mapStateToProps = state => {
  return {
    search: state.search,
    viewDetails : state.viewDetails
  }
}

const mapDispatchToProps = dispatch => {
  return {
    searchFilters: (data) => dispatch({ type: "SEARCH_FILTERS", data: data }),
    searchResult: (data) => dispatch({ type: "SEARCH_RESULT", item: data }),
    setViewDetails: (data) => dispatch({type: "VIEW_DETAILS",data:data})
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ViewCandidateProfile);