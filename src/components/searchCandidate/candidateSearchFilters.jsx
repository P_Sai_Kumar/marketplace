import React from 'react';
import { connect } from 'react-redux';
import $ from "jquery";
import axios from 'axios';

import Input from "../input/input"
import Location from '../input/location'
import { withRouter } from 'react-router';

class CandidateSearchFilter extends React.Component {

    state = {
        sub_options: []
    }
    subCategoryIDs = []
    CategoryOptions = []
    CategoryIDs = []

    handleValidation(value, rules) {
        let isValid = true;
        if (rules.required) {
            isValid = value.trim() !== "" && isValid;
        }

        if (rules.name) {
            isValid = /^[A-Za-z\s]+$/.test(value) && isValid;
        }


        return isValid
    }

    componentWillMount = async () => {
        try {
            $('.modal-backdrop').hide()
            // $('#signUp').modal('hide');
        } catch (err) {

        }
        axios.get('api/allcategories', { headers: this.headers }).then(async (res) => {
            this.CategoryOptions = res.data.map((item, id) => { return item.name })
            this.CategoryIDs = res.data.map((item, id) => { return item.id })
            this.forceUpdate()
            for (let x of res.data) {
                if (x.name === this.props.fields.Category) {
                    await axios.get('api/allsubcategories/' + x.id, { headers: this.headers }).then((res) => {
                        this.setState({ sub_options: res.data.map((item, id) => { return item.name }) })
                        this.subCategoryIDs = res.data.map((item) => { return item })
                    })
                    this.forceUpdate()
                    break
                }
            }
        })
    }


    handleSubCategory = async (id, event) => {
        await axios.get('api/allsubcategories/' + id, { headers: this.headers }).then((res) => {
            this.setState({ sub_options: res.data.map((item, id) => { return item.name }) })
            this.subCategoryIDs = res.data.map((item) => { return item })
            this.forceUpdate()
        })
    }



    handleSubmit = async () => {
        console.log(this.props.formValid)

        if (this.props.formValid) {
            var data = {
                Category: this.props.fields.Category,
                SubCategory: this.props.fields.SubCategory.SubCategory,
                Skill: this.props.fields.Skill.Skill,
                Designation: this.props.fields.Designation.Designation,
                Location: this.props.fields.Location.Location,

            }


            
            await this.props.mapDatabaseToLocal("candidateSearchFilters", data)
            if (data.Location || data.Designation || data.Category || data.SubCategory || data.Skill){
                var param=   { }
                if(data.Designation)
                param={...param,designation:data.Designation}
                if(data.Location)
                param={...param,location:data.Location}
                if(data.Skill)
                param={...param,skill:data.Skill}
                if(data.Category)
                param={...param,category:data.Category}
                if(data.SubCategory)
                param={...param,subCategory: data.SubCategory}
                     axios.get("/api/search/candidate-filters"
                      ,{
                        params:param
                      },{"headers": {
                        "content-type": "application/json",  
                        }
                      }
                  ).then((res) => {
                    this.props.candidateSearchFilters(res.data)
                  })}
                  else if (sessionStorage.getItem("candidateSearchInput")){
    
                    axios.get("api/search/workerSearchQuery/"+sessionStorage.getItem("candidateSearchInput"),
                   
                    ).then((res) => {
                        this.props.candidateSearchFilters(res.data)
                    })
                  }
                  else{
                    axios.get("api/search/allworkers"
                    ).then((res)=>{
                      this.props.candidateSearchFilters(res.data.content)
                    })      
                  }





            $('#enterDetails').click();


        }
        else
            this.forceUpdate()

    }
    resetAll(){
        // axios.get("api/search/allworkers"
        // ).then((res)=>{
        //   this.props.candidateSearchFilters(res.data.content)
        // }) 
       
    }



    handleChange(field, rules, event) {
        let k = this.handleValidation(event.target.value, rules)
        if (k) {
            this.props.changeErrorState(field, true)
        } else {
            this.props.changeErrorState(field, false)
        }
        let obj = {}
        if (event.target.type === 'checkbox') {
            obj[field] = event.target.checked
        }
        else {
            obj[field] = event.target.value
        }
        obj['inValid'] = !k
        this.props.changeState(field, obj)
    }
    render() {
        return (
            <div class="modal-content">
                <div class="modal-header">
                <div class="SearchFilterHead">
                <h6>Search Filter</h6> 
              </div>
                    
                </div>
                <div class="modal-body ">

                    <div class="form-row">

                        <Input
                            divClass="form-group col-md-6" label="Category"
                            config={{ className: "form-control form-select", }}
                            elementType="select"
                            options={["Select"].concat(this.CategoryOptions)}
                            value={this.props.fields.Category}
                            change={(event) => { this.props.setCategory(event); this.handleSubCategory(this.CategoryIDs[this.CategoryOptions.indexOf(event.target.value)]) }} />

                        <Input
                            divClass="form-group col-md-6" label="Sub-Category"
                            config={{ className: "form-control form-select", }}
                            elementType="select"
                            options={["Select"].concat(this.state.sub_options)}
                            value={this.props.fields.SubCategory.SubCategory}
                            change={(event) => { this.handleChange("SubCategory", {}, event); localStorage.setItem("subCat", JSON.stringify(this.subCategoryIDs[this.state.sub_options.indexOf(event.target.value)])) }} />

                        <Input
                            divClass="form-group col-md-6" label="Skill"
                            config={{
                                className: "form-control",
                                placeholder: "Enter Skill",
                                type: "text"
                            }}
                            value={this.props.fields.Skill.Skill}
                            change={this.handleChange.bind(this, "Skill", { required: false })}

                            elementType="input"
                        />

                        <Input
                            divClass="form-group col-md-6" label="Designation"
                            config={{
                                className: "form-control",
                                placeholder: "Enter Designation",
                                type: "text"
                            }}
                            value={this.props.fields.Designation.Designation}
                            change={this.handleChange.bind(this, "Designation", { required: false })}
                            
                            
                            elementType="input"
                        />




                        <Location
                            divClass="form-group col-md-12" label="Location"
                            config={{ className: "form-control form-select", }}
                            value={this.props.fields.Location.Location}
                            
                            change={this.handleChange.bind(this, "Location", { required: false })} />

                    </div>
                </div>
                <div class="modal-footer">
                    <div class="btn-group NextFormButtons ModalNextFormButtons ">
                        <button class="common-btn commonOutlineBtn" onClick={this.props.resetForm}>Reset</button>
                        <button class="common-btn commonBlueBtn"
                            onClick={() => { this.props.checkFormIsValid(); setTimeout(() => this.handleSubmit(this.props.editDetailsID), 5) }}>Search</button>
                    </div>
                </div>
            </div>

        )
    }
}

const mapStateToProps = state => {
    return {
        fields: state.candidateSearchFilters.fields,
        
        formValid: state.candidateSearchFilters.formValid,

    }
}

const mapDispatchToProps = dispatch => {
    return {
        setCategory : (event) => dispatch({type : "CHANGE_CATEGORY", item : event.target.value,name:"searchFilter"}),
        candidateSearchFilters: (data) => dispatch({ type: "CANDIDATE_SEARCH_RESULT", data: data }),
        changeState: (name, val) => dispatch({ type: "CHANGE_FIELD", name: name, val: val, data: 'candidateSearchFilters' }),
        changeErrorState: (field, val) => dispatch({ type: "CHANGE_ERROR_STATE", field: field, val: val, data: 'candidateSearchFilters' }),
        checkFormIsValid: () => dispatch({ type: "IS_FORM_VALID", data: 'candidateSearchFilters' }),
        addDetails: (val) => dispatch({ type: "ADD_DETAILS", data: 'candidateSearchFilters', val: val }),
        resetForm: () => dispatch({ type: "RESET_FORM", data: 'candidateSearchFilters' }),
        editAction: () => dispatch({ type: "EDIT_ACTION", data: "candidateSearchFilters" }),
        mapDatabaseToLocal: (res) => dispatch({ type: "MAP_DATABASE_TO_LOCAl", name: 'candidateSearchFilters', res: res })
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(withRouter(CandidateSearchFilter))