import { useReactMediaRecorder } from "react-media-recorder";
import React, { useEffect, useState, useCallback } from "react";
import axios from "axios";

const UseRecorderV2 = (props) => {
//   const [second, setSecond] = useState("00");
//   const [minute, setMinute] = useState("00");
  const [isDeleted, setIsDeleted] = useState(false);
  const [isActive, setIsActive] = useState(false);
  const [intervalId, setIntervalDisp] = useState(null);
//   const [, forceUpdate] = useState(0);
//   const [mediaBlobUrl, setmediaBlobUrl] = useState(null);
  const [render, setRender] = useState(false);
//   const [mediaBlobUrl, setMediaBlobUrl] = useState(null);
  var clear;
//   const [counter, setCounter] = useState(0);

const forceUpdate = useState()[1].bind(null, {})


  useEffect(() => {
    let intervalId;
    

    // if (isActive) {
    //   intervalId = setInterval(() => {
    //     const secondCounter = counter % 60;
    //     const minuteCounter = Math.floor(counter / 60);

    //     let computedSecond =
    //       String(secondCounter).length === 1
    //         ? `0${secondCounter}`
    //         : secondCounter;
    //     let computedMinute =
    //       String(minuteCounter).length === 1
    //         ? `0${minuteCounter}`
    //         : minuteCounter;

    //     setSecond(computedSecond);
    //     setMinute(computedMinute);

    //     setCounter((counter) => counter + 1);
    //   }, 650);
    // }

    // return () => clearInterval(intervalId);
    
    
  }, [isActive]);

  function stopTimer() {
    // setIsActive(false);
    // setCounter(0);
    // setSecond("00");
    // setMinute("00");
  }

  const startTimer = (duration, display) => {
    var timer = duration, minutes, seconds;
        setIntervalDisp(setInterval(() => {
        minutes = parseInt(timer / 60, 10);
        seconds = parseInt(timer % 60, 10);

        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;

        display.textContent = minutes + ":" + seconds + " minutes remaining";

        if (--timer < 0) {
            clearInterval(intervalId)
        }
},1000))}

  const {status, startRecording, stopRecording, mediaBlobUrl,} = useReactMediaRecorder({
    video: false,
    audio: true,
    echoCancellation: true
  });
//   console.log("deed", mediaBlobUrl);



    const onDelete = async () => {
    
    if(localStorage.getItem('audioFileID')!==null)
        {
            await axios.delete('api/files/'+localStorage.getItem('audioFileID'))
            .then((res)=>{ console.log(res)
            })
            localStorage.removeItem('audioFileID')
            localStorage.removeItem('audioURL')
        }
        setIsDeleted(true)
        forceUpdate()
        setRender(false)
    // audioURlnull()
    // clearBlobUrl()
    }

    // const forceUpdate: () => useState()[1].bind(null, {})
    


    var audio = null;
    var deleteAudio = <a class="removeDetail ml-2" onClick={onDelete} style={{position: "relative", top: "-24px"}}>
                        <svg xmlns="http://www.w3.org/2000/svg" height="20px" viewBox="0 0 24 24" width="20px" fill="#f67980">
                            <path d="M0 0h24v24H0z" fill="none"/><path d="M6 19c0 1.1.9 2 2 2h8c1.1 0 2-.9 2-2V7H6v12zM19 4h-3.5l-1-1h-5l-1 1H5v2h14V4z"/>
                        </svg>
                    </a>    
    const audioFunc = async () => {

        if(localStorage.getItem("audioURL")){
            audio = <div className="ml-2 mt-2 audio-tag-style"><audio src={localStorage.getItem("audioURL")} id="audio" controls >
                        {/* <source  type="audio/mp3"/> */}
            </audio>{deleteAudio}</div>
        } else if(mediaBlobUrl){
            audio = <div className="ml-2 mt-2 audio-tag-style"><audio src={mediaBlobUrl} id="audio" controls >
                        {/* <source  type="audio/mp3"/> */}
                    </audio> {deleteAudio}</div>      
            // await setTimeout(async() => {setRender(!render)},1000)       
            // if(!localStorage.getItem("audioURL")){
            var reader = new FileReader();
            // let response = await fetch(mediaBlobUrl).then(r => r.blob());
            let response = await fetch(mediaBlobUrl);
            let data = await response.blob();
            let metadata = {
                type: 'audio/mp3'
            };
            let blob = new Blob([data], metadata)
            console.log(blob)
            reader.readAsDataURL(blob); 
            reader.onloadend = async function() {
            var base64data = reader.result; 
            localStorage.setItem("audioURL", base64data);               
            console.log(base64data);
            if(!localStorage.getItem("audioFileID")){
                await axios.post("api/files", {"audioresume": base64data.split(",")[1], "filenameContentType": base64data.split(",")[0], "worker" : {"id": localStorage.getItem("WorkerID")}}).then((res) => {localStorage.setItem('audioFileID',res.data.id)})}
                console.log("uploaded")
                setRender(false)
            }
            // await axios.post("api/files", {"audioresume": base64data.split(",")[1], "filenameContentType": base64data.split(",")[0], "worker" : {"id": localStorage.getItem("WorkerID")}}).then((res) => {localStorage.setItem('audioFileID',res.data.id)})}
            clearInterval(intervalId)
            document.querySelector("#time").textContent = ""  
            // } 
                    
        }

        
        if(isDeleted||isActive){
            audio = null
        }
    }

    audioFunc()
    
    return(
        <div>
            <div class="flex-1 flex-center flex-mobile-top">
                    <div class={isActive ? "btn-record active":"btn-record"} onClick={async() => {
                                                                        if (!isActive) {
                                                                            startRecording();
                                                                            setIsDeleted(false);
                                                                            // clear = setTimeout(async () => { stopRecording(); var reader = new FileReader();
                                                                            //     let blob = await fetch(mediaBlobUrl).then(r => r.blob());
                                                                            //     reader.readAsDataURL(blob); 
                                                                            //     reader.onloadend = async function() {
                                                                            //     var base64data = reader.result; 
                                                                            //     localStorage.setItem("audioURL", base64data);               
                                                                            //     console.log(base64data);
                                                                            //     await axios.post("api/files", {"audioresume": base64data.split(",")[1], "filenameContentType": base64data.split(",")[0], "worker" : {"id": localStorage.getItem("WorkerID")}}).then((res) => {localStorage.setItem('audioFileID',res.data.id)})}
                                                                            //     clearInterval(intervalId)
                                                                            //     document.querySelector("#time").textContent = ""}, 1000);
                                                                            startTimer(119,document.querySelector('#time'))
                                                                        } else {
                                                                            stopRecording()
                                                                            clearInterval(intervalId)
                                                                            document.querySelector("#time").textContent = ""  
                                                                            setRender(true)
                                                                            
                                                                            
                                                                        }

                                                                        setIsActive(!isActive);
                                                                    }}>
                        <i class="icn-record"> <i class="icn-record-inner"></i> 
                        <svg width="14px" height="19px" viewBox="0 0 14 19" version="1.1" xmlns="http://www.w3.org/2000/svg"> <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"> 
                        <path d="M7,12 C8.66,12 9.99,10.66 9.99,9 L10,3 C10,1.34 8.66,0 7,0 C5.34,0 4,1.34 4,3 L4,9 C4,10.66 5.34,12 7,12 Z M12.3,9 C12.3,12 9.76,14.1 7,14.1 C4.24,14.1 1.7,12 1.7,9 L0,9 C0,12.41 2.72,15.23 6,15.72 L6,19 L8,19 L8,15.72 C11.28,15.24 14,12.42 14,9 L12.3,9 Z" id="mic" fill="#FFFFFF" fill-rule="nonzero"></path> </g> 
                        </svg> </i>
                    </div>
                    {/* {audioURL} */}
                    <div className="error otp_timer ml-2 my-auto"><span id="time"></span></div>
                    {audio}
            </div>
            
        </div>
        
    )

//   return (
     
    // <div
    //   style={{
    //     border: "1px solid black",
    //     backgroundColor: "black",
    //     width: "700px",
    //     height: "350px"
    //   }}
    // >
    //   <div
    //     style={{
    //       border: "1px solid #bd9f61",
    //       height: "70px",
    //       backgroundColor: "#bd9f61",
    //       display: "flex"
    //     }}
    //   >
    //     <h4
    //       style={{
    //         marginLeft: "10px",
    //         textTransform: "capitalize",
    //         fontFamily: "sans-serif",
    //         fontSize: "18px",
    //         color: "white"
    //       }}
    //     >
    //       {status}
    //     </h4>
    //   </div>
    //   <div style={{ height: "38px" }}>
    //     {" "}
    //     <video src={mediaBlobUrl} controls loop />
    //   </div>

    //   <div
    //     className="col-md-6 col-md-offset-3"
    //     style={{
    //       backgroundColor: "black",
    //       color: "white",
    //       marginLeft: "357px"
    //     }}
    //   >
    //     <button
    //       style={{
    //         backgroundColor: "black",
    //         borderRadius: "8px",
    //         color: "white"
    //       }}
    //       onClick={stopTimer}
    //     >
    //       Clear
    //     </button>
    //     <div style={{ marginLeft: "70px", fontSize: "54px" }}>
    //       <span className="minute">{minute}</span>
    //       <span>:</span>
    //       <span className="second">{second}</span>
    //     </div>

    //     <div style={{ marginLeft: "20px", display: "flex" }}>
    //       <label
    //         style={{
    //           fontSize: "15px",
    //           fontWeight: "Normal"
    //           // marginTop: "20px"
    //         }}
    //         htmlFor="icon-button-file"
    //       >
    //         <h3 style={{ marginLeft: "15px", fontWeight: "normal" }}>
    //           Press the Start to record
    //         </h3>

    //         <div>
    //           <button
    //             style={{
    //               padding: "0.8rem 2rem",
    //               border: "none",
    //               marginLeft: "15px",
    //               fontSize: "1rem",
    //               cursor: "pointer",
    //               borderRadius: "5px",
    //               fontWeight: "bold",
    //               backgroundColor: "#42b72a",
    //               color: "white",
    //               transition: "all 300ms ease-in-out",
    //               transform: "translateY(0)"
    //             }}
    //             onClick={() => {
    //               if (!isActive) {
    //                 startRecording();
    //               } else {
    //                 pauseRecording();
    //               }

    //               setIsActive(!isActive);
    //             }}
    //           >
    //             {isActive ? "Pause" : "Start"}
    //           </button>
    //           <button
    //             style={{
    //               padding: "0.8rem 2rem",
    //               border: "none",
    //               backgroundColor: "#df3636",
    //               marginLeft: "15px",
    //               fontSize: "1rem",
    //               cursor: "pointer",
    //               color: "white",
    //               borderRadius: "5px",
    //               fontWeight: "bold",
    //               transition: "all 300ms ease-in-out",
    //               transform: "translateY(0)"
    //             }}
    //             onClick={() => {
    //               pauseRecording();
    //               stopRecording();
    //               setIsActive(!isActive);
    //             }}
    //           >
    //             Stop
    //           </button>
    //         </div>
    //       </label>
    //     </div>
    //     <b></b>
    //   </div>
    // </div>
//   );
};
export default UseRecorderV2;