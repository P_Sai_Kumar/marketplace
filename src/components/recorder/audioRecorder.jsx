import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import useRecorder from "./useRecorder";
import { Row } from 'react-bootstrap';

const AudioRecorder=(props)=>{
     
    let [audioURL, isRecording, startRecording, stopRecording, audioURlnull]=useRecorder()

    useEffect(()=>
    {
        axios.get('api/files/worker/'+localStorage.getItem("WorkerID"))
                .then(async(res)=>{
                console.log(res.data);
                if(res.data.length!==0)
                {
                  for(var i=0; i< res.data.length; i++){
                    if(res.data[i].filenameContentType.includes("audio")){
                    localStorage.setItem("audioURL",res.data[i].filenameContentType+', '+res.data[i].audioresume)
                    localStorage.setItem('audioFileID',res.data[i].id)
                    } 
                  }
                        
                }
                })
    },[])

    const onDelete = async () => {
        
        if(localStorage.getItem('audioFileID')!==null)
            {
                await axios.delete('api/files/'+localStorage.getItem('audioFileID'))
                .then((res)=>{ console.log(res)
                })
                localStorage.removeItem('audioFileID')
                localStorage.removeItem('audioURL')
            }
        audioURlnull()
    }


        var audio = null;
        var deleteAudio = <a class="removeDetail ml-2" onClick={onDelete} style={{position: "relative", top: "-24px"}}>
                            <svg xmlns="http://www.w3.org/2000/svg" height="20px" viewBox="0 0 24 24" width="20px" fill="#f67980">
                                <path d="M0 0h24v24H0z" fill="none"/><path d="M6 19c0 1.1.9 2 2 2h8c1.1 0 2-.9 2-2V7H6v12zM19 4h-3.5l-1-1h-5l-1 1H5v2h14V4z"/>
                            </svg>
                        </a>    
        if(audioURL){
            audio = <div className="ml-2 mt-2 audio-tag-style"><audio src={audioURL} id="audio" controls >
                        {/* <source  type="audio/mp3"/> */}
                    </audio> {deleteAudio}</div>
        } else if(localStorage.getItem("audioURL")){
            audio = <div className="ml-2 mt-2 audio-tag-style"><audio src={localStorage.getItem("audioURL")} id="audio" controls >
                        {/* <source  type="audio/mp3"/> */}
            </audio>{deleteAudio}</div>
        }
        return(
            <div>
                <div class="flex-1 flex-center flex-mobile-top">
                        <div class={isRecording ? "btn-record active":"btn-record"} onClick={isRecording ? stopRecording:startRecording}>
                            <i class="icn-record"> <i class="icn-record-inner"></i> 
                            <svg width="14px" height="19px" viewBox="0 0 14 19" version="1.1" xmlns="http://www.w3.org/2000/svg"> <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"> 
                            <path d="M7,12 C8.66,12 9.99,10.66 9.99,9 L10,3 C10,1.34 8.66,0 7,0 C5.34,0 4,1.34 4,3 L4,9 C4,10.66 5.34,12 7,12 Z M12.3,9 C12.3,12 9.76,14.1 7,14.1 C4.24,14.1 1.7,12 1.7,9 L0,9 C0,12.41 2.72,15.23 6,15.72 L6,19 L8,19 L8,15.72 C11.28,15.24 14,12.42 14,9 L12.3,9 Z" id="mic" fill="#FFFFFF" fill-rule="nonzero"></path> </g> 
                            </svg> </i>
                        </div>
                        {/* {audioURL} */}
                        <div className="error otp_timer ml-2 my-auto"><span id="time"></span></div>
                        {audio}
                </div>
                
            </div>
            
        )
}
const mapStateToProps = state => {
    return {
        fields: state.fields.fields,
    }
  }
const mapDispatchToProps = dispatch => {
    return {
        changeState : (name,val)=> dispatch({type:"CHANGE_FIELD",name:name,val:val,data : 'fields'}),
        mapDatabaseToLocal : (name,res) => dispatch({type : "MAP_DATABASE_TO_LOCAl", name:name, res: res})
    }
}
export default connect(mapStateToProps,mapDispatchToProps)(AudioRecorder)