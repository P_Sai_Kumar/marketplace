import axios from "axios";
import { useEffect, useState } from "react";

const useRecorder = () => {
  const [audioURL, setAudioURL] = useState("");
  const [isRecording, setIsRecording] = useState(false);
  const [recorder, setRecorder] = useState(null);
  const [stream, setStream] = useState(null);
  const [intervalDisp, setIntervalDisp] = useState(null);
  var clear;

  useEffect( () => {
    // Lazily obtain recorder first time we're recording.
    if (recorder === null) {
      if (isRecording) {
        requestRecorder().then((obj) => {setStream(obj[0]); setRecorder(obj[1]);
          // clear = setTimeout(function(){ setIsRecording(false);}, 121000);
          startTimer(119,document.querySelector('#time'))}).catch((e) => console.log(e))
      }
      return;
    }

    console.log(recorder);
    // Manage recorder state.
    if (isRecording) {
      recorder.start();
    } else {
      recorder.stop();
      stream.getTracks() // get all tracks from the MediaStream
      .forEach( track => track.stop() ); // stop each of them
      setRecorder(null)
    }

    // Obtain the audio when ready.
    const handleData = (e) => {
      setAudioURL(URL.createObjectURL(e.data));
      // document.getElementById("audio").load();
      var reader = new FileReader();
      reader.readAsDataURL(e.data); 
      reader.onloadend = async function() {
        var base64data = reader.result; 
        localStorage.setItem("audioURL", base64data);               
        console.log(base64data);
        await axios.post("api/files", {"audioresume": base64data.split(",")[1], "filenameContentType": base64data.split(",")[0], "worker" : {"id": localStorage.getItem("WorkerID")}}).then((res) => {localStorage.setItem('audioFileID',res.data.id)})
      }
      
    };

    recorder.addEventListener("dataavailable", handleData);
    // clearInterval(intervalDisp);
    return () => {recorder.removeEventListener("dataavailable", handleData); setIsRecording(false);  stream.getTracks().forEach( track => track.stop() );
    setRecorder(null)};
  }, [recorder, isRecording]);

  const startTimer = (duration, display) => {
    var timer = duration, minutes, seconds;
        setIntervalDisp(setInterval(() => {
        minutes = parseInt(timer / 60, 10);
        seconds = parseInt(timer % 60, 10);

        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;

        display.textContent = minutes + ":" + seconds + " minutes remaining";

        if (--timer < 0) {
            clearInterval(intervalDisp)
        }
    },1000))
}

  const startRecording = async () => {
    if(localStorage.getItem('audioFileID')!==null)
    {
        await axios.delete('api/files/'+localStorage.getItem('audioFileID'))
        .then((res)=>{ console.log(res)
        })
        localStorage.removeItem('audioFileID')
        localStorage.removeItem('audioURL')
    }
    audioURlnull()
    setIsRecording(true);
  };

  const stopRecording = async () => {
    setIsRecording(false);
    // clearTimeout(clear);
    clearInterval(intervalDisp)
    document.querySelector("#time").textContent = ""
  };

  const audioURlnull = async () => {
    setAudioURL(null)
  }

  return [audioURL, isRecording, startRecording, stopRecording, audioURlnull];
};

async function requestRecorder() {
  const stream = await navigator.mediaDevices.getUserMedia({ audio: true });
  // const audio = document.createElement('video');
  return [stream, new MediaRecorder(stream)];
}
export default useRecorder;
