import React from 'react';
import { connect } from 'react-redux';

class SaveForCandidates extends React.Component {

    render(){
        return(
            <div className="modal-content">
                <div className="modal-header border-bottom-0 pb-0">
                    <h5 className="modal-title" id="exampleModalLongTitle">Select Candidate</h5>
                </div>
                <div className="modal-body candidate-signup">
                    <div className="form-group">
                        <div class="input-group">
                            <input type="text" class="form-control border-right-0 py-4" placeholder="Search" />
                            <div class="input-group-append">
                                <span class="input-group-text bg-white">
                                    <svg xmlns="http://www.w3.org/2000/svg" height="18px" viewBox="0 0 24 24" width="18px" fill="#000000">
                                        <path d="M0 0h24v24H0z" fill="none" /><path d="M15.5 14h-.79l-.28-.27C15.41 12.59 16 11.11 16 9.5 16 5.91 13.09 3 9.5 3S3 5.91 3 9.5 5.91 16 9.5 16c1.61 0 3.09-.59 4.23-1.57l.27.28v.79l5 4.99L20.49 19l-4.99-5zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5 14 7.01 14 9.5 11.99 14 9.5 14z" />
                                    </svg>
                                </span>
                            </div>
                        </div>
                    </div>


                    <div class="table-responsive">
                        <table class="table table-borderless mb-0">
                            <thead>
                                <tr>
                                    <th style={{width: "2%"}}>
                                        <div class="form-check pl-0 mt-auto">
                                            <label class="checkBoxContainer check-black-style">
                                                <input type="checkbox" />
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                    </th>
                                    <th>Candidate Name</th>
                                    <th>Designation</th>
                                    <th>Location</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <div class="form-check pl-0 mt-auto">
                                            <label class="checkBoxContainer check-black-style">
                                                <input type="checkbox" />
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td>Christopher Nolan</td>
                                    <td>UI &amp; UX Designer</td>
                                    <td>Hyderabad</td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="form-check pl-0 mt-auto">
                                            <label class="checkBoxContainer check-black-style">
                                                <input type="checkbox" />
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td>Christopher Nolan</td>
                                    <td>UI &amp; UX Designer</td>
                                    <td>Hyderabad</td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="form-check pl-0 mt-auto">
                                            <label class="checkBoxContainer check-black-style">
                                                <input type="checkbox" />
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td>Christopher Nolan</td>
                                    <td>UI &amp; UX Designer</td>
                                    <td>Hyderabad</td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="form-check pl-0 mt-auto">
                                            <label class="checkBoxContainer check-black-style">
                                                <input type="checkbox" />
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td>Christopher Nolan</td>
                                    <td>UI &amp; UX Designer</td>
                                    <td>Hyderabad</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    {/* <div className="row">
                        <input type="checkbox" className="form-check-input" value=""/>
                        <div className="col-md-4">
                            <p className="text-dark">Candidate Name</p>
                        </div>
                        <div className="col-md-4">
                            <p className="text-dark">Designation</p>
                        </div>
                        <div className="col-md-4">
                            <p className="text-dark">Location</p>
                        </div>
                    </div> */}
                </div>
                <div className="modal-footer bg-white mx-4 p-0">
                <div className="btn-group NextFormButtons ModalNextFormButtons ">
                    <button className="common-btn orange-btn-style">Reset</button>
                    <button className="common-btn commonBlueBtn">Save Job</button>
                </div>
                </div>
            </div>
        )
    }
}

export default SaveForCandidates;