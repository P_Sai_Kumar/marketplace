import React from 'react';
import {connect} from 'react-redux';
import $ from "jquery";
import axios from 'axios';

import Input from "../input/input"
import Location from '../input/location'

class JobSearchFilter extends React.Component {
    

    handleValidation(value, rules){
        let isValid = true;
        if (rules.required) {
            isValid = value.trim() !== "" && isValid;
        }

        if (rules.name) {
            isValid = /^[A-Za-z\s]+$/.test(value) && isValid;
        }

        
        return isValid
    }
    handleSubmit = async() => {
        console.log(this.props.formValid)
        if (this.props.formValid){
            
            var data = {
                
                Skill: this.props.fields.Skill.Skill,
                Designation: this.props.fields.Designation.Designation,
                Location: this.props.fields.Location.Location,
               
            }
            await this.props.mapDatabaseToLocal(data)
            if (data.Designation && data.Location) {
                axios.get("api/jobs?q=" + data.Designation + "&location=" + data.Location
                ).then((res) => {
                    this.props.jobSearchFilters(res.data.jobs)
                  
                })
              }
              else if (data.Designation) {
                axios.get("api/jobs?q=" + data.Designation
                ).then((res) => {
                    this.props.jobSearchFilters(res.data.jobs)
                })
              }
              else if (data.Location) {
                axios.get("api/jobs?location=" + data.Location
                ).then((res) => {
          
                    this.props.jobSearchFilters(res.data.jobs)
                })
              }
              else
                axios.get("api/jobs"
                ).then((res) => {
          
                    this.props.jobSearchFilters(res.data.jobs)
                    
                })

            $('#enterDetails').click();
            // window.location.reload(true);
        } else {
            this.forceUpdate()
        }
    }
    handleChange(field, rules, event)
    {
        let k = this.handleValidation(event.target.value, rules)
        if (k){
            this.props.changeErrorState(field, true)
        } else {
            this.props.changeErrorState(field, false)
        }
        let obj={}
        if(event.target.type==='checkbox')
        {
            obj[field]=event.target.checked
        }
        else{
            obj[field]=event.target.value
        }
        obj['inValid']=!k
        this.props.changeState(field, obj)
    }
    render(){
        
        return(    
            <div class="modal-content">
                <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Search Filters</h5>
                </div>
                <div class="modal-body candidate-signup ">

                <div class="form-row">
                    

                        <Input 
                        divClass="form-group col-md-6" label="Skill" 
                        config = {{className :"form-control" ,
                                placeholder : "Enter Skill", 
                                type:"text"}}
                        value={this.props.fields.Skill.Skill}
                        change={this.handleChange.bind(this,"Skill" ,{required : false})}
                        
                        elementType="input" 
                        />

                    <Input 
                        divClass="form-group col-md-6" label="Designation" 
                        config = {{className :"form-control" ,
                                placeholder : "Enter Designation", 
                                type:"text"}}
                        value={this.props.fields.Designation.Designation}
                        change={this.handleChange.bind(this,"Designation" ,{required : false})}
                        inValid = {this.props.fields.Designation.inValid}
                        error = {this.props.errors.Designation}
                        elementType="input" 
                        />
                

                

                     <Location 
                        divClass="form-group col-md-12" label="Location" 
                        config = {{className :"form-control form-select" ,}}
                        value={this.props.fields.Location.Location}
                        inValid = {this.props.fields.Location.inValid}
                        change={this.handleChange.bind(this,"Location", {required : false})}/>
                
                </div>
                </div>
                <div class="modal-footer">
                <div class="btn-group NextFormButtons ModalNextFormButtons ">
                    <button class="common-btn commonOutlineBtn" onClick={this.props.resetForm}>Reset</button>
                    <button class="common-btn commonBlueBtn" 
                    onClick = {() => {this.props.checkFormIsValid(); setTimeout(() => this.handleSubmit(this.props.editDetailsID),5)}}>Search</button>
                </div>
                </div>
            </div>
     
        )
    }
}

const mapStateToProps = state => {
    return {
        fields:state.jobSearchFilters.fields,
        errors : state.jobSearchFilters.errors,
        formValid : state.jobSearchFilters.formValid,
        
    }
}

const mapDispatchToProps = dispatch => {
    return {
        jobSearchFilters: (data) => dispatch({ type: "JOB_SEARCH_RESULT", data: data }),
        changeState : (name,val)=> dispatch({type:"CHANGE_FIELD",name:name,val:val, data:'jobSearchFilters'}),
        changeErrorState : (field, val) => dispatch({type : "CHANGE_ERROR_STATE", field : field, val : val, data : 'jobSearchFilters'}),
        checkFormIsValid : () => dispatch({type: "IS_FORM_VALID", data : 'jobSearchFilters'}), 
        addDetails : (val) => dispatch({type: "ADD_DETAILS", data : 'jobSearchFilters', val:val}),
        resetForm : () => dispatch({type:"RESET_FORM", data : 'jobSearchFilters'}),
        editAction : () => dispatch({type : "EDIT_ACTION", data : "jobSearchFilters"}),
        mapDatabaseToLocal : (res) => dispatch({type : "MAP_DATABASE_TO_LOCAl", name:'jobSearchFilters', res: res})
    }
}
export default connect(mapStateToProps,mapDispatchToProps)(JobSearchFilter)