import Header from "../header/Header";
import React from "react";
import axios from "axios";
import { connect } from "react-redux";
import Modal from '../user_management/modal';
import checkCircle from "../../imgaes/icons/check_circle_black_48dp.svg"
import './viewJobDetails.css'
import $ from 'jquery'; 

class ViewJobDetails extends React.Component{

  
  jobID = null
  constructor(props) {
    super(props)
    var today = new Date(),

    date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();

    this.jobID = this.props.match.params.id;
    this.state={
      applied:null,
      savedJob:null,
      appliedDate:null,
      savedDate:null,
      currentDate:date
    }
  }

  componentWillMount = async () => {

    await axios.get('api/jobs/' + this.jobID)
    .then(async (res) => {
      await this.props.jobDetails(res.data.job)
      if(localStorage.getItem("WorkerID")!==null){
       await axios.get('api/workers/'+localStorage.getItem("WorkerID"),
        {headers:{
          'Content-Type':'application/json',
          'Authorization':'Bearer '+localStorage.getItem("token")
        }}).then((response)=>{
          console.log(this.props.jobDetail.id)
          
          response.data.vmsjobsubmits.map((result)=>{
            if(result.vmsjobsubmitName===this.props.jobDetail.id)
            {
              console.log(result)
              this.setState({appliedDate:result.submitdate,applied:true})
              console.log(this.state.appliedDate)
              return
            }
          })
        })
        await axios.get('api/workers/'+localStorage.getItem("WorkerID"),
        {headers:{
          'Content-Type':'application/json',
          'Authorization':'Bearer '+localStorage.getItem("token")
        }}).then((response)=>{
          response.data.vmsjobsaves.map((result)=>{
            if(result.vmsjobsaveName===this.props.jobDetail.id)
            {
              this.setState({savedJob:true,savedDate:result.savedate})
              return
            }
          })
        })
      }

    })
    
  }

    saveJob=()=>{

      if(localStorage.getItem("VendorID"))
      return
      
      axios.post('api/workerJobSave/'+localStorage.getItem("WorkerID")+'/'+this.props.jobDetail.id,
      {headers:{
        'Content-Type':'application/json',
        'Authorization':'Bearer '+localStorage.getItem("token")
      }}).then((response)=>{
        response.data.vmsjobsaves.map((result)=>{
          if(result.vmsjobsaveName===this.props.jobDetail.id)
          {
          this.setState({savedJob:true,savedDate:this.state.currentDate})
          
        }
        })
        console.log(this.state.savedJob)
      })

    }

    

    unsaveJob=()=>{
      axios.post('api/workerJobSave/'+localStorage.getItem("WorkerID")+'/'+this.props.jobDetail.id,
      {headers:{
        'Content-Type':'application/json',
        'Authorization':'Bearer '+localStorage.getItem("token")
      }}).then((response)=>{
        response.data.vmsjobsaves.map((result)=>{
          if(result.vmsjobsaveName===this.props.jobDetail.id)
          return
          
        })
        this.setState({savedJob:false})
        console.log(this.state.savedJob)
      })
    }

      
      applyJob=()=>{

        console.log(this.props.jobDetail.id)

        console.log(localStorage.getItem("WorkerID"))
  
        console.log(this.props.jobDetail.id)
        if(localStorage.getItem("VendorID"))
        return 
        axios.post('api/cadidateSubmission/'+localStorage.getItem("WorkerID")+'/'+this.props.jobDetail.id,   
        )
      .then((res)=>{
        try{
        if(res.data.id)
        {
          console.log(res)
          this.setState({applied:true,appliedDate:this.state.currentDate})
          axios.post("api/jobApplyEmail/"+this.props.jobDetail.id+"/Google")
        }
      }
      catch(er){}

      })

    }
    
    render(){
      console.log(this.props.jobDetail)
      
      try{
        $('.modal-backdrop').hide();
        } 
      catch(err){}

        return(
           <div>
               <Header hideEmployer={true}/>
               <section class="mainbgColor view-profile-section">
        <div class="container mobPadd30">
            {/* <!-- <div class="row mobMar0"> --> */}
                {/* <!-- <div class="col-md-12 mobPadd0"> --> */}
                <div className="row">
                  <div className="col-md-8">
                  <div class="FilterList">
                   {/* <!-- <div class="SearchedList"> --> */}
                   <div class="SearchResult">
                     <div class="FilteredOptions">
                        <div class="row">
                        <div class="col-md-5">
                          <div class="disFMob">
                            <div class="jobHead">
                                <h6>{this.props.jobDetail.template_name}</h6>
                                {this.props.jobDetail.modified_on ?<p class="updateStatus">Updated on {this.props.jobDetail.modified_on.split("T")[0]}</p>:null}
                            </div>
                            <div class="SavedJobMain disNoneDesk disBlockMob">
                              
                              {/* <a  class="SavedJobIcon">
                                  <svg xmlns="http://www.w3.org/2000/svg" height="20px" viewBox="0 0 24 24" width="20px" fill="#FA852B">
                                      <path d="M0 0h24v24H0z" fill="none"/><path d="M17 3H7c-1.1 0-1.99.9-1.99 2L5 21l7-3 7 3V5c0-1.1-.9-2-2-2z"/>
                                  </svg>
                              </a>
                              <a  class="SavedJobIcon">
                                <svg xmlns="http://www.w3.org/2000/svg" height="20px" viewBox="0 0 24 24" width="20px" fill="#929292">
                                    <path d="M0 0h24v24H0z" fill="none"/><path d="M17 3H7c-1.1 0-1.99.9-1.99 2L5 21l7-3 7 3V5c0-1.1-.9-2-2-2zm0 15l-5-2.18L7 18V5h10v13z"/>
                                </svg>
                              </a> */}
                              {
                                   this.state.savedJob && localStorage.getItem("token")!==null?
                                   <a  class="SavedJobIcon " onClick={this.unsaveJob} style={{cursor:'pointer'}}>
                                    <svg xmlns="http://www.w3.org/2000/svg" height="20px" viewBox="0 0 24 24" width="20px" fill="#FA852B">
                                          <path d="M0 0h24v24H0z" fill="none"/><path d="M17 3H7c-1.1 0-1.99.9-1.99 2L5 21l7-3 7 3V5c0-1.1-.9-2-2-2z"/>
                                      </svg>
                                  </a>:(localStorage.getItem("token")!==null?
                                  <a  class="SavedJobIcon" onClick={this.saveJob} style={{cursor:'pointer'}}>
                                      <svg xmlns="http://www.w3.org/2000/svg" height="20px" viewBox="0 0 24 24" width="20px" fill="#929292">
                                        <path d="M0 0h24v24H0z" fill="none"/><path d="M17 3H7c-1.1 0-1.99.9-1.99 2L5 21l7-3 7 3V5c0-1.1-.9-2-2-2zm0 15l-5-2.18L7 18V5h10v13z"/>
                                    </svg>
                                  </a>:
                                  <div>
                                  <a  class="SavedJobIcon" data-toggle="modal" data-target="#hide" style={{cursor:'pointer'}}>
                                      <svg xmlns="http://www.w3.org/2000/svg" height="20px" viewBox="0 0 24 24" width="20px" fill="#929292">
                                        <path d="M0 0h24v24H0z" fill="none"/><path d="M17 3H7c-1.1 0-1.99.9-1.99 2L5 21l7-3 7 3V5c0-1.1-.9-2-2-2zm0 15l-5-2.18L7 18V5h10v13z"/>
                                    </svg>
                                  </a>
                                </div>)
                                  }
                            </div>
                          </div>
                            <ul class="searchDetails disF ">
                                <li>
                                  <svg xmlns="http://www.w3.org/2000/svg" height="16px" viewBox="0 0 24 24" width="16px" fill="#FA852B">
                                      <path d="M0 0h24v24H0V0z" fill="none"/><path d="M12 7V5c0-1.1-.9-2-2-2H4c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h16c1.1 0 2-.9 2-2V9c0-1.1-.9-2-2-2h-8zM6 19H4v-2h2v2zm0-4H4v-2h2v2zm0-4H4V9h2v2zm0-4H4V5h2v2zm4 12H8v-2h2v2zm0-4H8v-2h2v2zm0-4H8V9h2v2zm0-4H8V5h2v2zm9 12h-7v-2h2v-2h-2v-2h2v-2h-2V9h7c.55 0 1 .45 1 1v8c0 .55-.45 1-1 1zm-1-8h-2v2h2v-2zm0 4h-2v2h2v-2z"/>
                                  </svg>
                                  <span>{this.props.jobDetail.custom_fields && this.props.jobDetail.custom_fields.company_name?this.props.jobDetail.custom_fields.company_name:"SimplifyVMS"}</span>
                                </li>
                                {/* <!-- <li>
                                  <svg xmlns="http://www.w3.org/2000/svg" height="16px" viewBox="0 0 24 24" width="16px" fill="#FA852B">
                                    <path d="M0 0h24v24H0z" fill="none"/><path d="M20 4H4c-1.11 0-1.99.89-1.99 2L2 18c0 1.11.89 2 2 2h16c1.11 0 2-.89 2-2V6c0-1.11-.89-2-2-2zm0 14H4v-6h16v6zm0-10H4V6h16v2z"/>
                                  </svg>
                                  <span>$12/Hour</span>
                                </li> --> */}
                                <li>
                                  <svg xmlns="http://www.w3.org/2000/svg" enable-background="new 0 0 24 24" height="15px" viewBox="0 0 24 24" width="15px" fill="#FA852B"><g>
                                    <path d="M0,0h24v24H0V0z" fill="none"/></g><g><path d="M12,2c-4.2,0-8,3.22-8,8.2c0,3.32,2.67,7.25,8,11.8c5.33-4.55,8-8.48,8-11.8C20,5.22,16.2,2,12,2z M12,12c-1.1,0-2-0.9-2-2 c0-1.1,0.9-2,2-2c1.1,0,2,0.9,2,2C14,11.1,13.1,12,12,12z"/></g>
                                  </svg>
                                  <span>{this.props.jobDetail.location.city.name?this.props.jobDetail.location.city.name:"N/A"}</span>
                                </li>
                              </ul> 
                        </div>
                        {(!localStorage.getItem('EmployerID')) &&
                        <div class="col-md-7">
                            <div class="AppliedDiv disF">
                              {
                              this.state.applied && localStorage.getItem("token")!==null?
                              <div class="SavedJobMain">
                              <p class="SavedJob mr-2" >Applied on {this.state.appliedDate} </p>
                              
                              </div>
                              :(this.state.savedJob && localStorage.getItem("token")!==null?
                                 <div class="SavedJobMain">
                                 <p class="SavedJob" >Saved on {this.state.savedDate}</p>
                                 </div>:null)
                               }
                                <div class="SavedJobMain DisNoneMob">
                                  {this.state.applied && localStorage.getItem("token")!==null?null:
                                   (this.state.savedJob && localStorage.getItem("token")!==null?
                                   <a  class="SavedJobIcon " onClick={this.unsaveJob} style={{cursor:'pointer'}}>
                                    <svg xmlns="http://www.w3.org/2000/svg" height="20px" viewBox="0 0 24 24" width="20px" fill="#FA852B">
                                          <path d="M0 0h24v24H0z" fill="none"/><path d="M17 3H7c-1.1 0-1.99.9-1.99 2L5 21l7-3 7 3V5c0-1.1-.9-2-2-2z"/>
                                      </svg>
                                  </a>:(localStorage.getItem("token")!==null?
                                  <a  class="SavedJobIcon" onClick={this.saveJob} style={{cursor:'pointer'}}>
                                      <svg xmlns="http://www.w3.org/2000/svg" height="20px" viewBox="0 0 24 24" width="20px" fill="#929292">
                                        <path d="M0 0h24v24H0z" fill="none"/><path d="M17 3H7c-1.1 0-1.99.9-1.99 2L5 21l7-3 7 3V5c0-1.1-.9-2-2-2zm0 15l-5-2.18L7 18V5h10v13z"/>
                                    </svg>
                                  </a>:
                                  <div>
                                  <a  class="SavedJobIcon" data-toggle="modal" data-target="#hide" style={{cursor:'pointer'}}>
                                      <svg xmlns="http://www.w3.org/2000/svg" height="20px" viewBox="0 0 24 24" width="20px" fill="#929292">
                                        <path d="M0 0h24v24H0z" fill="none"/><path d="M17 3H7c-1.1 0-1.99.9-1.99 2L5 21l7-3 7 3V5c0-1.1-.9-2-2-2zm0 15l-5-2.18L7 18V5h10v13z"/>
                                    </svg>
                                  </a>
                                </div>))
                                  }
                                </div>
                                {this.state.applied && localStorage.getItem("token")!==null?
                                <button type="button" class="btn appliedBtn  ">
                                  
                                  <img className="img-fluid mr-1 invert-color svg-sm" src={checkCircle} alt="check circle" />
                                  Applied  
                                </button>
                                :
                                (localStorage.getItem("token")!==null?
                                <button type="button" class="btn common-lightblue-button ApplyJobBtn" onClick={this.applyJob}>
                                  <span>Apply</span>
                                </button>
                                :
                                <div>
                                <button type="button" class="btn common-lightblue-button ApplyJobBtn applyJobMobButton" data-toggle="modal" data-target="#hide">
                                  <span>Apply</span>
                                </button>
                              </div>)
                                }                           
                            </div>
                        </div> }
                        
                    </div>

                     </div>

                     <div class="row mt-3 mt-md-4">
                         <div class="col-md-6">
                             <div class="jobView card Description">
                                <h6>
                                  <svg xmlns="http://www.w3.org/2000/svg" height="18px" viewBox="0 0 24 24" width="18px" fill="#FA852B">
                                    <path d="M0 0h24v24H0z" fill="none"/><path d="M14 2H6c-1.1 0-1.99.9-1.99 2L4 20c0 1.1.89 2 1.99 2H18c1.1 0 2-.9 2-2V8l-6-6zm2 16H8v-2h8v2zm0-4H8v-2h8v2zm-3-5V3.5L18.5 9H13z"/>
                                  </svg>
                                  Description
                                </h6>

                                <p>{this.props.jobDetail.category.description}</p>
                             </div>
                         </div>

                         <div class="col-md-6">
                          <div class="jobView card ">
                             <h6>
                              <svg xmlns="http://www.w3.org/2000/svg" height="20px" viewBox="0 0 24 24" width="20px" fill="#FA852B">
                                <path d="M0 0h24v24H0V0z" fill="none"/><path d="M11 7h2v2h-2zm0 4h2v6h-2zm1-9C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm0 18c-4.41 0-8-3.59-8-8s3.59-8 8-8 8 3.59 8 8-3.59 8-8 8z"/>
                              </svg>
                               Basic Details
                             </h6>
                             
                            <div class="basicDetails">
                              <div class="row">
                                <div class="col-md-6">
                                    <label>Estimated Start Date</label>
                                    <p>{this.props.jobDetail.start_date?this.props.jobDetail.start_date.split("T")[0]:"N/A"}</p>
                                </div>
                                <div class="col-md-6">
                                    <label>Estimated End Date</label>
                                    <p>{this.props.jobDetail.end_date?this.props.jobDetail.end_date.split("T")[0]:"N/A"}</p>
                                </div>
                                <div class="col-md-6">
                                  <label>Duration</label>
                                  <p>2 Years</p>
                              </div>
                              <div class="col-md-6">
                                  <label>Allowed Expense</label>
                                  <p>YES</p>
                              </div>
                              <div class="col-md-6">
                                <label>Overtime Excempt</label>
                                <p>{this.props.jobDetail.is_ot_exempt?"Allowed":"Not Allowed"}</p>
                            </div>
                            <div class="col-md-6">
                                <label>No.of Openings</label>
                                <p>{this.props.jobDetail.positions}</p>
                            </div>
                              </div>
                            </div>
                             
                          </div>

                          <div class="jobView card ">
                            <h6>
                             <svg xmlns="http://www.w3.org/2000/svg" height="20px" viewBox="0 0 24 24" width="20px" fill="#FA852B">
                               <path d="M0 0h24v24H0V0z" fill="none"/><path d="M11 7h2v2h-2zm0 4h2v6h-2zm1-9C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm0 18c-4.41 0-8-3.59-8-8s3.59-8 8-8 8 3.59 8 8-3.59 8-8 8z"/>
                             </svg>
                             Financial Info
                            </h6>
                            
                           <div class="basicDetails">
                             <div class="row">
                               <div class="col-md-6">
                                   <label>Minimum Bill Rate</label>
                                   <p>$ {this.props.jobDetail.min_bill_rate?this.props.jobDetail.min_bill_rate:"N/A"}</p>
                               </div>
                               <div class="col-md-6">
                                   <label>Maximum Bill Rate</label>
                                   <p>$ {this.props.jobDetail.max_bill_rate?this.props.jobDetail.max_bill_rate:"N/A"}</p>
                               </div>
                             
                             </div>
                           </div>
                         </div>
                      </div>
                     </div>
                   </div>
                   

                  </div>
                  </div>
                  <div className="col-md-4 mb-3">
                    <div className="card">
                      <h6>About the Company</h6>
                      <p className="mt-3 mb-0">{this.props.jobDetail.custom_fields && this.props.jobDetail.custom_fields.company_name?this.props.jobDetail.custom_fields.company_name:"SimplifyVMS"} </p>
                      <a href="https://www.simplifyvms.com/" target="_blank">https://{this.props.jobDetail.custom_fields && this.props.jobDetail.custom_fields.company_name?this.props.jobDetail.custom_fields.company_name:"simplifyvms"}.com/</a>
                      <p className="mt-3 mb-0">Total Posted Jobs - 100</p>
                      <p>Total Active Jobs - 34</p>
                      <p className="mb-0">Active Since - 01/Mar/2021</p>
                    </div>
                    <div className="card FilterList">
                      <h6>Similar Jobs</h6>
                      <div className="jobView mt-3">
                        <p className="mb-1">Software Architect</p>
                        <div className="row">
                          <div className="col-auto col-md-6">
                            <svg xmlns="http://www.w3.org/2000/svg" height="16px" viewBox="0 0 24 24" width="16px" fill="#FA852B">
                              <path d="M0 0h24v24H0z" fill="none" /><path d="M20 4H4c-1.11 0-1.99.89-1.99 2L2 18c0 1.11.89 2 2 2h16c1.11 0 2-.89 2-2V6c0-1.11-.89-2-2-2zm0 14H4v-6h16v6zm0-10H4V6h16v2z" />
                            </svg>
                            <span className="ml-2">$12/Hour</span>
                          </div>
                          <div className="col-auto col-md-6">
                            <svg xmlns="http://www.w3.org/2000/svg" enable-background="new 0 0 24 24" height="15px" viewBox="0 0 24 24" width="15px" fill="#FA852B"><g>
                              <path d="M0,0h24v24H0V0z" fill="none" /></g><g><path d="M12,2c-4.2,0-8,3.22-8,8.2c0,3.32,2.67,7.25,8,11.8c5.33-4.55,8-8.48,8-11.8C20,5.22,16.2,2,12,2z M12,12c-1.1,0-2-0.9-2-2 c0-1.1,0.9-2,2-2c1.1,0,2,0.9,2,2C14,11.1,13.1,12,12,12z" /></g>
                            </svg>
                            <span className="ml-2">Raino</span>
                          </div>
                        </div>
                      </div>
                      <div className="jobView my-4">
                        <p className="mb-1">Php Developer</p>
                        <div className="row">
                          <div className="col-auto col-md-6">
                            <svg xmlns="http://www.w3.org/2000/svg" height="16px" viewBox="0 0 24 24" width="16px" fill="#FA852B">
                              <path d="M0 0h24v24H0z" fill="none" /><path d="M20 4H4c-1.11 0-1.99.89-1.99 2L2 18c0 1.11.89 2 2 2h16c1.11 0 2-.89 2-2V6c0-1.11-.89-2-2-2zm0 14H4v-6h16v6zm0-10H4V6h16v2z" />
                            </svg>
                            <span className="ml-2">$20/Hour</span>
                          </div>
                          <div className="col-auto col-md-6">
                            <svg xmlns="http://www.w3.org/2000/svg" enable-background="new 0 0 24 24" height="15px" viewBox="0 0 24 24" width="15px" fill="#FA852B"><g>
                              <path d="M0,0h24v24H0V0z" fill="none" /></g><g><path d="M12,2c-4.2,0-8,3.22-8,8.2c0,3.32,2.67,7.25,8,11.8c5.33-4.55,8-8.48,8-11.8C20,5.22,16.2,2,12,2z M12,12c-1.1,0-2-0.9-2-2 c0-1.1,0.9-2,2-2c1.1,0,2,0.9,2,2C14,11.1,13.1,12,12,12z" /></g>
                            </svg>
                            <span className="ml-2">Bayramören</span>
                          </div>
                        </div>
                      </div>
                      <div className="jobView">
                        <p className="mb-1">Doctor</p>
                        <div className="row">
                          <div className="col-auto col-md-6">
                            <svg xmlns="http://www.w3.org/2000/svg" height="16px" viewBox="0 0 24 24" width="16px" fill="#FA852B">
                              <path d="M0 0h24v24H0z" fill="none" /><path d="M20 4H4c-1.11 0-1.99.89-1.99 2L2 18c0 1.11.89 2 2 2h16c1.11 0 2-.89 2-2V6c0-1.11-.89-2-2-2zm0 14H4v-6h16v6zm0-10H4V6h16v2z" />
                            </svg>
                            <span className="ml-2">$18/Hour</span>
                          </div>
                          <div className="col-auto col-md-6">
                            <svg xmlns="http://www.w3.org/2000/svg" enable-background="new 0 0 24 24" height="15px" viewBox="0 0 24 24" width="15px" fill="#FA852B"><g>
                              <path d="M0,0h24v24H0V0z" fill="none" /></g><g><path d="M12,2c-4.2,0-8,3.22-8,8.2c0,3.32,2.67,7.25,8,11.8c5.33-4.55,8-8.48,8-11.8C20,5.22,16.2,2,12,2z M12,12c-1.1,0-2-0.9-2-2 c0-1.1,0.9-2,2-2c1.1,0,2,0.9,2,2C14,11.1,13.1,12,12,12z" /></g>
                            </svg>
                            <span className="ml-2">Al-Bab District</span>
                          </div>
                        </div>
                      </div>
                    </div>

                  </div>
                </div>
                  

              
        </div>
    </section>

           </div>
        );
    };
}

const mapStateToProps = state => {
  return {
    jobDetail: state.jobDetails,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    jobDetails: (data) => dispatch({ type: "JOB_DETAILS", item: data }),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ViewJobDetails);