import React, { Component } from "react";
import "./loader.css";

export default class ButtonLoader extends Component {

  fetchData = () => {
    // this.setState({ loading: true });

    //Faking API call here
    // setTimeout(() => {
    //   this.setState({ loading: false });
    // }, 2000);
  };

  render() {
    const loading = this.props.loading;

    return (
        <button type="button" className={this.props.class} onClick={() => {this.props.click(); this.props.setLoading(true)}} disabled={loading}>
          {loading && (
            <div
              className="loader"
            //   style={{ marginRight: "5px" }}
            />
          )}
          {!loading && this.props.label}
        </button>
    );
  }
}
