import React from 'react';

import SignupAs from './signup/SignupAs';
import Signup from './signup/Signup';
import Signup2 from './signup/Signup2';
import OTP from './otp/otp';
import Success from './signup/Success';
import Login from './login/Login';
import Employer from "./signup/employer";

class Modal extends React.Component{
    constructor(props){
        super(props)
        console.log(this.props)
    }


    state = {
        candidate : false,
        employer : false,
        vendor : false,
        loading : false,
        resumeLoading : false,
        employer_fields : {
            // id : "",
            email : "",
            // password : "",
            errors : {

            }
        },
        vendor_fields : {
            email : "",
            errors : {

            }
        },
        mobile : false,
        otp : false,
        success : false,
        mobNum : '',
        mobNum_errors : {

        },
        editable : false,
        login : false,
        signup: false,
        active: "Sign Up",
        viewProfile:true
    }
    
    
    candidateSignup = () => {
        this.setState({ candidate: true})
    }

    employerSignup = () => {
        this.setState({employer: true})
    }

    vendorSignup = () => {
        this.setState({vendor: true})
    }

    // empFieldChange = (field, event) => {
    //     var new_employer_field = {...this.state.employer_fields}
    //     new_employer_field[field] = event.target.value
    //     new_employer_field.errors[field] = ""
    //     this.setState({employer_fields:new_employer_field })
    // }
    empFieldChange = (event) => {
        var new_employer_field = {...this.state.employer_fields}
        new_employer_field["email"] = event.target.value
        new_employer_field.errors["email"] = ""
        this.setState({employer_fields:new_employer_field })

    }

    vendorFieldChange = (event) => {
        var new_vendor_field = {...this.state.vendor_fields}
        new_vendor_field["email"] = event.target.value
        new_vendor_field.errors["email"] = ""
        this.setState({vendor_fields: new_vendor_field})
    }

    setErrorsEmp = (errors) => {
        var new_employer_field = {...this.state.employer_fields}
        new_employer_field["errors"] = errors
        this.setState({employer_fields : new_employer_field})
    }

    setErrorsVendor = (errors) => {
        var new_vendor_field = {...this.state.vendor_fields}
        new_vendor_field["errors"] = errors
        this.setState({vendor_fields : new_vendor_field})
    }

    setErrorsCandidate = (errors) => {
        var new_candidate_field = {...this.state.mobNum_errors}
        new_candidate_field = errors 
        this.setState({mobNum_errors : new_candidate_field})
    }

    signupMobile = () => {
        // const s = [...this.state.signup]
        // s[0].mobile = true;
        // const s = this.state.signup[0].mobile
        this.setState({ mobile : true })
    }

    OtpVerfication = async (mobNum1) => {
        await this.setState({ otp : true, mobNum: mobNum1 }, () => {})
        console.log(this.state.mobNum)
    }

    success = () => {
        this.setState({ success :  true})
    }

    editableTextbox = (val) => {
        const obj  = { }
        obj['editable'] = val
        this.setState(obj) 
    }

    back = (val) => {
        const obj  = { }
        obj[val] = false
        this.setState(obj)
    }

    goToSignup = async () => {
        await this.setState({signup : true, login : false})
    }

    setActive = (val) => {
        this.setState({active : val})
    }

    setLoading = (val) => {
        this.setState({loading : val})
    }

    setResumeLoading = (val) => {
        this.setState({resumeLoading : val})
    }

    render() {
        let variable = null

        if(this.props.signUp === "true"){
            variable = <SignupAs candidate={this.candidateSignup} employer={this.employerSignup} vendor={this.vendorSignup} hideCandidate={this.props.hideCandidate} hideEmployer={this.props.hideEmployer}/>
            
        } else if(this.props.login === "true"){
            variable = <Login click={this.goToSignup} OtpVerfication={this.OtpVerfication} setActive={this.setActive}/>
        }

        if(this.state.signup === true){
            variable = <SignupAs candidate={this.candidateSignup} employer={this.employerSignup} vendor={this.vendorSignup} hideCandidate={this.props.hideCandidate} hideEmployer={this.props.hideEmployer}/>
        }

        if (this.state.candidate === true) {
            variable = <Signup signupMobile = { this.signupMobile } back = { this.back } success = { this.success } linkedin="true" worker={true}/>
        }

        if (this.state.employer === true ) {
            // variable = <Employer back = { this.back } fields={this.state.employer_fields} empFieldChange={this.empFieldChange} setErrors={this.setErrors}/>
            variable = <Signup back = { this.back } label="Email Address" signupMobile = { this.signupMobile } success = { this.success } employer={true}/>
        }

        if (this.state.vendor === true) {
            variable = <Signup back = { this.back } label="Work Email" signupMobile = { this.signupMobile } success = { this.success } vendor={true}/>
        }

        if (this.state.mobile === true) {
            // variable = <Signup2 back = { this.back } OtpVerfication = { this.OtpVerfication } mobNum = { this.setMobNum } />
            if (this.state.vendor === true){
                variable = <Signup2 back = { this.back } OtpVerfication = { this.OtpVerfication } setActive={this.setActive} label="Work Email" fields={this.state.vendor_fields} vendorFieldChange={this.vendorFieldChange} setErrors={this.setErrorsVendor} vendor={true}/>

            }else if (this.state.employer === true){
                variable = <Signup2 back = { this.back } OtpVerfication = { this.OtpVerfication } setActive={this.setActive} label="Email Address" fields={this.state.employer_fields} empFieldChange={this.empFieldChange} setErrors={this.setErrorsEmp} employer={true}/>
            }else {
                variable = <Signup2 back = { this.back } OtpVerfication = { this.OtpVerfication } setActive={this.setActive} setErrors={this.setErrorsEmp} worker={true}/>
            }
            
        }
        if (this.state.otp === true){
            variable = <OTP back = { this.back } mobNum={this.state.mobNum} editableTextbox = {this.editableTextbox} editable = { this.state.editable } success = { this.success } active={this.state.active} viewProfile={this.props.viewProfile} loading={this.state.loading} isVendor={this.state.vendor} isEmployer={this.state.employer} isWorker={this.state.candidate} setLoading={(val) => this.setLoading(val)} resendActive={this.state.editable}/>
        }
        if (this.state.success === true || this.props.success === true) {
            variable = <Success url={this.state.vendor ? "../../../vendor/createProfile/basicDetails/": this.state.employer?"../../../employer/createProfile/basicDetails":"../../../createProfile/basicDetails"} resumeLoading={this.state.resumeLoading} setResumeLoading={(val) => this.setResumeLoading(val)} isVendor={this.state.vendor} isEmployer={this.state.employer}/>
        }

        return (
            <div className="wid100">
                { variable }
            </div>
        )
    }
}

export default Modal;