import React from 'react';
import './signup.css';
import {Back} from '../../back_btn/Back'

class Employer extends React.Component{
    constructor(props)
    {
        super(props);
    }

    handleChange = (field, event) => {
        this.props.empFieldChange(field, event)
    }

    handleSubmit = async () => {
        let errors = {}
        let fields = this.props.fields
        let formIsValid = true

        if (!fields["id"]) {
            formIsValid = false;
            errors["id"] = "Program ID Cannot be empty";
        }

        if (!fields["email"]) {
            formIsValid = false;
            errors["email"] = "Email Cannot be empty";
        }

        if (!fields["password"]) {
            formIsValid = false;
            errors["password"] = "Password Cannot be empty";
        }
      
        if (typeof fields["id"] !== "undefined") {
        if (!/^[0-9]*$/.test(fields["id"])) {
            formIsValid = false;
            errors["id"] = "Enter valid Program ID";
        }
        }

        if (typeof fields["email"] !== "undefined" && fields["email"]) {
            if (!/^[^@\s]+@[^@\s]+\.[^@\s]+$/.test(fields["email"])) {
                formIsValid = false;
                errors["email"] = "Enter valid email";
            }
            }

        if (!formIsValid){
            this.props.setErrors(errors)
        }
        if (formIsValid){
            console.log(this.props.fields)
        }
    }

    render(){
    return (
        // <div class="modal fade" id="WelcometoMarketplace" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        //     <div class="modal-dialog modal-dialog-centered multistepModal" role="document">
            <div class="modal-content">
                <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">
                    <svg xmlns="http://www.w3.org/2000/svg" height="24px" viewBox="0 0 24 24" width="24px" fill="#000000" onClick = { () => this.props.back('employer') }>
                        <path d="M0 0h24v24H0z" fill="none"/><path d="M20 11H7.83l5.59-5.59L12 4l-8 8 8 8 1.41-1.41L7.83 13H20v-2z"/>
                    </svg>
                    Welcome To Simplify Marketplace</h5>
                
                <p>Login to your Simplify Account to Continue</p>
                </div>
                <div class="modal-body welcome-to-marketpalce">
                    <div class="form-group">
                        <div class="InputParent">
                        <input type="text" className={this.props.fields.errors["id"] ? "form-control Invalid" : "form-control"} id="programId" placeholder="Enter Program ID" required value={this.props.fields.id} onChange={(event) => this.handleChange("id", event)}/>
                        <svg xmlns="http://www.w3.org/2000/svg" height="20px" viewBox="0 0 24 24" width="20px" fill="#007BFF">
                            <path d="M0 0h24v24H0z" fill="none"/><path d="M10 20v-6h4v6h5v-8h3L12 3 2 12h3v8z"/>
                        </svg>
                        </div>
                        {/* <div class="invalid-feedback">
                        Please provide a program ID.
                        </div> */}
                        {<p className="error">{this.props.fields.errors["id"]}</p>}
                    </div>
                    <div class="form-group">
                        <div class="InputParent">
                        <input type="text" className={this.props.fields.errors["email"] ? "form-control Invalid" : "form-control"} id="programId" placeholder="Enter Email Address" required value={this.props.fields.email} onChange={(event) => this.handleChange("email", event)}/>
                        <svg xmlns="http://www.w3.org/2000/svg" height="16px" viewBox="0 0 24 24" width="16px" fill="#007BFF">
                            <path d="M0 0h24v24H0z" fill="none"/><path d="M20 4H4c-1.1 0-1.99.9-1.99 2L2 18c0 1.1.9 2 2 2h16c1.1 0 2-.9 2-2V6c0-1.1-.9-2-2-2zm0 4l-8 5-8-5V6l8 5 8-5v2z"/>
                        </svg>
                        </div>
                        {/* <div class="invalid-feedback">
                        Please provide email address.
                        </div> */}
                        {<p className="error">{this.props.fields.errors["email"]}</p>}
                    </div>
                    <div class="form-group">
                        <div class="InputParent">
                        <input type="password" className={this.props.fields.errors["password"] ? "form-control Invalid" : "form-control"} id="programId" placeholder="Enter Your Password" required value={this.props.fields.password} onChange={(event) => this.handleChange("password", event)}/>
                        <svg xmlns="http://www.w3.org/2000/svg" height="18px" viewBox="0 0 24 24" width="18px" fill="#007BFF">
                            <path d="M0 0h24v24H0z" fill="none"/><path d="M12.65 10C11.83 7.67 9.61 6 7 6c-3.31 0-6 2.69-6 6s2.69 6 6 6c2.61 0 4.83-1.67 5.65-4H17v4h4v-4h2v-4H12.65zM7 14c-1.1 0-2-.9-2-2s.9-2 2-2 2 .9 2 2-.9 2-2 2z"/>
                        </svg>
                        </div>
                        {/* <div class="invalid-feedback">
                        Please provide a Password.
                        </div> */}
                        {<p className="error">{this.props.fields.errors["password"]}</p>}
                    </div>
                </div>
                <div class="modal-footer welcome-to-marketpalce-footer">
                <p>Forgot <a href="#">Username</a> Or <a href="#">Password</a></p>
                <button onClick={this.handleSubmit} type="button" class="common-lightblue-button welcomeSignIn">Sign in</button>
                </div>
            </div>
    )
    }
}
 
export default Employer;