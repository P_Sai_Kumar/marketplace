import React from 'react';
import {Header} from '../../header/Header'
import {Link,withRouter,Redirect} from 'react-router-dom'
import './signup.css';
import { Home } from '../../home/home';
import {Back} from '../../back_btn/Back'
import { GoogleLogin } from "react-google-login";
import LinkedIn from "./linkedin-login/LinkedIn";
import $ from 'jquery'

import linkedin from 'react-linkedin-login-oauth2/assets/linkedin.png'
import axios from 'axios';

class Signup extends React.Component{
    constructor(props)
    {
        super(props);
    }
    componentWillMount() {
      window.addEventListener('message', this.handlePostMessage);
    }
    handlePostMessage = event => {
      if (event.data.type === 'code') {
        const { code } = event.data;
        console.log(code)
        axios.get(`api/linkedin/${code}`)
        .then((response)=>{
          console.log(response)
          // response.data[1].localizedFirstName
          // response.data[1].localizedLastName
          // response.data[1].profilePicture.displayImage
                  localStorage.setItem("email",response.data.emailJson.elements[0]["handle~"].emailAddress);
                  const data = {         
                      "email": response.data.emailJson.elements[0]["handle~"].emailAddress,
                      "login": response.data.emailJson.elements[0]["handle~"].emailAddress,
                      "userType":(this.props.worker?"worker":(this.props.employer?"employer":"vendor")),
                      "activated": true,
                    }
                    axios.post("api/register/thirdparty", data, 
                    {headers : {'Content-Type': 'application/json'}}).then(async(res) => {
                        console.log(res)
                        await axios.post("api/authenticate", {"username" : response.data.emailJson.elements[0]["handle~"].emailAddress, "password": "1234"}).then(
                          (res) => {console.log(res) ; localStorage.setItem("token", res.data.id_token);axios.defaults.headers.common["Authorization"] = "Bearer " +res.data.id_token; console.log(localStorage.getItem("token"))}
                      ).catch(err => console.log(err))
                      localStorage.setItem("user",JSON.stringify(res.data.user))
                      localStorage.setItem("userID", res.data.user.id)
                      axios.get("api/workers/get/" + localStorage.getItem('userID'),{headers : {'Content-Type': 'application/json','Authorization':'Bearer '+localStorage.getItem("token")}}).then(
                        (res1) => {
                            console.log(res1) ;
                            localStorage.setItem("WorkerID", res1.data.id);
                            this.props.history.push('/viewProfile')
                          })
                    .catch(async(err) =>{
                      this.props.success()
                      await axios.post('api/workers', {firstName:response.data.localDetailsJson.localizedFirstName,lastName:response.data.localDetailsJson.localizedLastName,email:response.data.emailJson.elements[0]["handle~"].emailAddress})
                      .then((response) => {localStorage.setItem("Worker", JSON.stringify(response.data));localStorage.setItem("WorkerID", response.data.id)}).catch((e) => console.log(e))
                      })
              })
        })
      }
    };

    // GMail Login
    OnSuccess=(response)=> {
      console.log(response)
        console.log(response,response.profileObj.email);
        localStorage.setItem("email",response.profileObj.email);
        const data = {         
            "email": response.profileObj.email,
            "login": response.profileObj.email,
            "userType":(this.props.worker?"worker":(this.props.employer?"employer":"vendor")),
            "activated": true,
          }
          console.log(data)
          
          axios.post("api/register/thirdparty", data, 
          {headers : {'Content-Type': 'application/json'}}).then(async(res) => {
              console.log(res)
              await axios.post("api/authenticate", {"username" : response.profileObj.email, "password": "1234"}).then(
                (res) => {console.log(res) ; localStorage.setItem("token", res.data.id_token); axios.defaults.headers.common["Authorization"] = "Bearer " +res.data.id_token;console.log(localStorage.getItem("token"))}
            ).catch(err => console.log(err))
            localStorage.setItem("user",JSON.stringify(res.data.user))
            localStorage.setItem("userID", res.data.user.id)
            await axios.get("api/workers/get/" + res.data.user.id,).then(
              (res1) => {
                  console.log(res1) ;
                  localStorage.setItem("WorkerID", res1.data.id);
                  this.props.history.push('/viewProfile')
                })
          .catch(err => this.props.success())
              // if (res.data.check){
              //   try{
              //       $('.modal-backdrop').hide();
              //     } catch(err){}
              //     localStorage.setItem("userID", res.data.user.id)
              //     await axios.get("api/workers/get/" + res.data.user.id,).then(
              //       (res1) => {
              //           console.log(res1) ;
              //           localStorage.setItem("WorkerID", res1.data.id);
              //           this.props.history.push('/viewProfile')
              //         })
              //   .catch(err => this.props.history.push('/createProfile/basicDetails'))
              //   // await axios.get('api/workers/'+localStorage.getItem("WorkerID"), {headers : this.headers}).then((res) => 
              //   // {
              //   //     if(res.data.page2===null||res.data.page2===false)
              //   //     {
              //   //         page='/createProfile/additionalDetails'
              //   //     }
              //   //     else if(res.data.page3===null||res.data.page3===false)
              //   //     {
              //   //         page='/createProfile/employmentDetails'
              //   //     }
              //   // })
              // }
              // else{
              //   this.props.success()
              // }
            }).catch((error) => {
                console.log(error)
            })
         
      };
      
    OnFailure(response){
        console.log('logout done');
        console.log(response);
      };
    render(){
    return (
        <div>
            <div class="modal-content">
                <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">
                    <svg xmlns="http://www.w3.org/2000/svg" height="24px" viewBox="0 0 24 24" width="24px" fill="#000000" onClick = { () => {this.props.back('candidate'); this.props.back('vendor');this.props.back('employer')} }>
                        <path d="M0 0h24v24H0z" fill="none"/><path d="M20 11H7.83l5.59-5.59L12 4l-8 8 8 8 1.41-1.41L7.83 13H20v-2z"/>
                    </svg>
                    SignIn / Register
                </h5>
                <p>Enter your email id and continue</p>
                </div>
                <div class="modal-body candidate-signup">
                    {/* <div class="form-group">
                        <label for="MobileNumber" style={{ marginBottom : '8px' }}>Mobile Number</label>
                        <input onClick = {this.props.signupMobile} type="text" class="form-control" id="MobileNumber" placeholder="Enter your mobile number" required />
                        <div class="invalid-feedback">
                        Please provide a number.
                        </div>
                    </div> */}
                     <div class="form-group">
                        <label for="MobileNumber" style={{ marginBottom : '8px' }}>{this.props.label ? this.props.label:"Email ID"}</label>
                        <input onClick = {this.props.signupMobile} type="text" class="form-control" id="MobileNumber" placeholder={"Enter your "+ (this.props.label ? this.props.label:"email id")} required />
                        <div class="invalid-feedback">
                        Please provide a email id.
                        </div>
                    </div>
                    <p style={{marginBottom : 0, marginTop: '14px'}}>Or</p>

                    <div class="btn-group">
                        {/* <a href="https://www.linkedin.com/login?session_redirect=%2Foauth%2Fv2%2Flogin-success%3Fapp_id%3D113778134%26auth_type%3DAC%26flow%3D%257B%2522authFlowName%2522%253A%2522generic-permission-list%2522%252C%2522currentSubStage%2522%253A0%252C%2522authorizationType%2522%253A%2522OAUTH2_AUTHORIZATION_CODE%2522%252C%2522redirectUri%2522%253A%2522http%253A%252F%252F172.105.34.32%2522%252C%2522currentStage%2522%253A%2522LOGIN_SUCCESS%2522%252C%2522appId%2522%253A113778134%252C%2522creationTime%2522%253A1631788725757%252C%2522state%2522%253A%2522235871871647%2522%252C%2522scope%2522%253A%2522r_liteprofile%252Cr_emailaddress%2522%257D&fromSignIn=1&trk=oauth&cancel_redirect=%2Foauth%2Fv2%2Flogin-cancel%3Fapp_id%3D113778134%26auth_type%3DAC%26flow%3D%257B%2522authFlowName%2522%253A%2522generic-permission-list%2522%252C%2522currentSubStage%2522%253A0%252C%2522authorizationType%2522%253A%2522OAUTH2_AUTHORIZATION_CODE%2522%252C%2522redirectUri%2522%253A%2522http%253A%252F%252F172.105.34.32%2522%252C%2522currentStage%2522%253A%2522LOGIN_SUCCESS%2522%252C%2522appId%2522%253A113778134%252C%2522creationTime%2522%253A1631788725757%252C%2522state%2522%253A%2522235871871647%2522%252C%2522scope%2522%253A%2522r_liteprofile%252Cr_emailaddress%2522%257D" target="_blank"><button type="button" class="Social-login-btn fb-button">Login with Facebook</button></a> */}
                        {/* <button type="button" class="Social-login-btn Google-button">Login with Google</button> */}
                        <GoogleLogin
                            clientId="797214205429-21iqqd5n4h8seko2r9qsd8rcinlue4gk.apps.googleusercontent.com"
                            render={renderProps => (
                                <button class={"Social-login-btn Google-button" + (this.props.linkedin ?  "": " w-100")} onClick={renderProps.onClick}>Continue with Gmail</button>
                              )}
                            buttonText="Login"
                            onSuccess={this.OnSuccess}
                            onFailure={this.OnFailure}
                            // redirectUri="http://localhost:3000"
                        />{
                            this.props.linkedin ? <LinkedIn 
                            clientId="78ym1vbrtzk7yb"
                            scope={["r_liteprofile","r_emailaddress"]}
                            text="Continue With LinkedIn"
                            className="Social-login-btn LinkedIn-button"
                            /> : ""
                        }
                         
                             
                    </div>
                </div>
                {/* <div class="modal-footer">
                <p>I have already an Account <a href="#">Sign In</a></p>
                </div> */}
        </div>
        </div>
    )
    }
}
 
export default withRouter(Signup);