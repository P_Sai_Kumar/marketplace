import React, { Component } from "react";
import getURL from "./getURL";
import PropTypes from "prop-types";

class LinkedIn extends Component {

  start = () => {
    const { clientId, scope } = this.props;
    const state = Math.random()
      .toString(36)
      .substring(7);
    localStorage.linkedInReactState = state;
    localStorage.linkedInReactRedirectUri = window.location.href;
    //window.location.href = getURL(clientId, state, scope); // build url out of clientid, scope and state
    const oauthUrl =  getURL(clientId, state, scope);
    const width = 450,
      height = 730,
      left = window.screen.width / 2 - width / 2,
      top = window.screen.height / 2 - height / 2;
    window.open(
      oauthUrl,
      'Linkedin',
      'menubar=no,location=no,resizable=no,scrollbars=no,status=no, width=' +
      width +
      ', height=' +
      height +
      ', top=' +
      top +
      ', left=' +
      left
    );
  };

  render() {
    return (
      <button className={this.props.className} onClick={this.start}>
        {this.props.text}
      </button>
    );
  }
}

LinkedIn.propTypes = {
  clientId: PropTypes.string,
  callback: PropTypes.func.isRequired,
  className: PropTypes.string,
  text: PropTypes.node,
  scope: PropTypes.arrayOf(PropTypes.string)
};

export default LinkedIn;
