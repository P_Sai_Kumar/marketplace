import axios from "axios";
import React from "react";
import Header from "../header/Header";
import { useState, useEffect } from "react";
import { Link, useHistory, useLocation } from 'react-router-dom';
import Modal from "../createProfile/additionalDetails/modal";
import { connect } from "react-redux";
import $, { event } from 'jquery'

function AppliedJobs(props) {

  const history = useHistory();
  const [appliedJobs, setAppliedJobs] = useState([])
  const [loading, setLoading] = useState(true)
  const [designation, setDesignation] = useState("")
  const [location, setLocation] = useState("")
  const [skills,setSkills]=useState("")
  const [savedJobs, setSavedJobs] = useState({})

  useEffect(() => {
    if(props.searchResult)
    setAppliedJobs(props.searchResult)
   
  }, [props.searchResult]);

  useEffect(() => {
    try {
      $('.modal-backdrop').hide();
      // $('#signUp').click();
      // window.location.reload(false);
    } catch (err) {

    }

    axios.get("api/appliedJobs/" + localStorage.getItem("WorkerID"), {
      "headers": {
        "Authorization": "Bearer " + localStorage.getItem("token"),
        "content-type": "application/json",

      }
    }).then(res => {
      console.log(res)
      setAppliedJobs(res.data)
      setLoading(false)
    })

    if (localStorage.getItem("WorkerID") !== null) {
      axios.get('api/workers/' + localStorage.getItem("WorkerID"),
        {
          headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem("token")
          }
        }).then((response) => {
          response.data.vmsjobsaves.map((result) => {
            savedJobs[result.vmsjobsaveName] = true
            setSavedJobs({ ...savedJobs })

          })
          console.log(savedJobs)
        })
    }


  }, []);

  const saveJob = (e, res) => {

    e.stopPropagation();
    e.nativeEvent.stopImmediatePropagation();
    axios.post('api/workerJobSave/' + localStorage.getItem("WorkerID") + '/' + res.id,
      {
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + localStorage.getItem("token")
        }
      }).then((response) => {
        response.data.vmsjobsaves.map((result) => {
          if (result.vmsjobsaveName === res.id)
            savedJobs[res.id] = true
          setSavedJobs({ ...savedJobs })

        })
        console.log(savedJobs)
      })
  }

  const unsaveJob = (e, res) => {

    e.stopPropagation()
    e.nativeEvent.stopImmediatePropagation();

    axios.post('api/workerJobSave/' + localStorage.getItem("WorkerID") + '/' + res.id,
      {
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + localStorage.getItem("token")
        }
      }).then((response) => {
        response.data.vmsjobsaves.map((result) => {
          if (result.vmsjobsaveName === res.id)
            return
        })
        savedJobs[res.id] = false
        setSavedJobs({ ...savedJobs })
      })
  }



  const reset = () => {

    setLocation("")
    setDesignation("")
    setSkills("")
    setLoading(true)
    axios.get("api/appliedJobs/" + localStorage.getItem("WorkerID"), {
      "headers": {
        "Authorization": "Bearer " + localStorage.getItem("token"),
        "content-type": "application/json",

      }
    }).then(res => {
      console.log(res)
      setAppliedJobs(res.data)
      setLoading(false)
    })


  }

  const skillsChange = () => {
    setSkills(event.target.value)

  }

  const designationChange = (event) => {
    setDesignation(event.target.value)

  }
  const locationChange = (event) => {
    setLocation(event.target.value)

  }
  const handleSubmit = () => {
    setLoading(true)
    let params = {}
    if (designation && location)
      params = { q: designation, location: location }
    else if (designation)
      params = { q: designation }
    else if (location)
      params = { location: location }
    axios.get("api/vmsjob-submits/appliedJobs/" + localStorage.getItem("WorkerID"), {
      params: params
    }, {
      "headers": {
        "Authorization": "Bearer " + localStorage.getItem("token"),
        "content-type": "application/json",

      }
    }).then(res => {
      console.log(res.data)
      setAppliedJobs(res.data)
      setLoading(false)


    })

  }
  const handleView = (res) => {

    history.push('/jobDetails/' + res.id)

  }
  const sortBy=(event)=>{
    if(event.target.value==='a-z')
    setAppliedJobs([...appliedJobs.sort((a, b) =>(a.template_name?a.template_name:'').toLowerCase().trim() > (b.template_name?b.template_name:'').toLowerCase().trim() ? 1:-1)])
    else if(event.target.value==='z-a')
    setAppliedJobs([...appliedJobs.sort((a, b) => (a.template_name?a.template_name:'').toLowerCase().trim() < (b.template_name?b.template_name:'').toLowerCase().trim() ? 1:-1)])
    else if(event.target.value==="recent")
    handleSubmit()
  }

  

  return (
    <div>
      <Header />
      <section class="mainbgColor view-profile-section">
        <div class="container mobPadd30">
          <div class="row mobMar0">

            <div class="col-md-3 DisNoneMob">
              <div class="SearchFilterHead">
                <h6>Search Filter</h6> <a style={{color:"#007BFF",cursor:"pointer"}} onClick={reset}>Reset All</a>
              </div>
              <div class="searchFilter">

                <div class="SkillsFilter">
                  <div class="form-group">
                    <label for="inputCategory">Skills</label>
                    <input type="name" class="form-control" id="inputSkill" placeholder="Enter Skill." value={skills} onChange={skillsChange} />
                  </div>
                  <div class="form-group">
                    <div class="form-group">
                      <label for="inputDesignation">Designation</label>
                      <input type="name" class="form-control" id="inputDesignation" value={designation} placeholder="Enter Designation." onChange={designationChange} />
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="inputLocation">Location</label>
                    <select id="inputLocation" class="form-control" onChange={locationChange} value={location}>
                      <option value="" hidden></option>
                      <option value="1d2f468e-4d15-4e65-8c6f-86e9603a2cbe">Botswana</option>
                      <option value="377858c0-5b17-453b-b75b-b986771acb6b">Greenland</option>
                      <option value="9ee005bf-7527-4ef7-93a6-9ccc9e9dde62">Poland</option>
                    </select>
                    {/* <svg xmlns="http://www.w3.org/2000/svg" height="20px" viewBox="0 0 24 24" width="20px" fill="#000000"><path d="M0 0h24v24H0z" fill="none" /><path d="M16.59 8.59L12 13.17 7.41 8.59 6 10l6 6 6-6z" /></svg> */}
                  </div>
                {localStorage.getItem("VendorID")!==null?
                <div>
                  

                  <div class="form-group">
                    <label for="searchCand">Search Candidate</label>
                    <select id="searchCand" class="form-control">
                      <option value="" hidden>Select Candidate</option>
                    </select>
                  </div>
                  </div>
                :null}

                </div>
              </div>


              <button type="button" class="SearchFilterButton" onClick={handleSubmit}>Search</button>

            </div>


            <div class="col-md-9 mobPadd0">
              <div class="FilterList">
                {/* <h5>Applied Jobs</h5> */}
                <div class="topLine disF">
                  <div class="FilterMenu">
                    <a class="disNoneDesk disBlockMob" data-toggle="modal" data-target="#enterDetails" onClick={() => { props.changeModal('jobSearchFilters') }}>
                      <h6 class="disF">
                        <svg xmlns="http://www.w3.org/2000/svg" enable-background="new 0 0 24 24" height="15px" viewBox="0 0 24 24" width="15px" fill="#929292"><g>
                          <path d="M0,0h24 M24,24H0" fill="none" /><path d="M7,6h10l-5.01,6.3L7,6z M4.25,5.61C6.27,8.2,10,13,10,13v6c0,0.55,0.45,1,1,1h2c0.55,0,1-0.45,1-1v-6 c0,0,3.72-4.8,5.74-7.39C20.25,4.95,19.78,4,18.95,4H5.04C4.21,4,3.74,4.95,4.25,5.61z" /><path d="M0,0h24v24H0V0z" fill="none" /></g>
                        </svg>
                        Filter
                      </h6>
                    </a>
                    
                  </div>
                  <span class="disF">
                  <svg xmlns="http://www.w3.org/2000/svg" height="24px" viewBox="0 0 24 24" width="24px" fill="#929292">
                      <path d="M0 0h24v24H0z" fill="none" /><path d="M3 18h6v-2H3v2zM3 6v2h18V6H3zm0 7h12v-2H3v2z" />
                    </svg>
                    <select className="form-control h-auto py-0" name="sort-by" id="sort-by" onChange={sortBy}>
                      <option value="" hidden>Sort By</option>
                      <option value="recent">Recent</option>
                      <option value="a-z">Name (A-Z)</option>
                      <option value="z-a">Name (Z-A)</option>
                    </select>
                  </span>
                </div>

                  <div className="filter-count-text">
                    <span>{appliedJobs && appliedJobs.length > 0 ? "1 -" : null} {appliedJobs && appliedJobs.length > 20 ? 20 : appliedJobs.length} of {appliedJobs.length} Results Showing for Applied Jobs</span>
                  </div>


                {loading ?
                  <div class="cssload-container" style={{ alignContent: 'center' }}>
                    <div class="cssload-speeding-wheel" style={{ alignContent: 'center' }}></div>
                  </div> :

                  <div class="SearchedList">
                    {
                      appliedJobs && appliedJobs.map(res => (
                        // <Link to={{ pathname: '/jobDetails/' + res.id }} style={{ textDecoration: 'none', color: 'inherit' }}>
                        <div class="SearchResult" >
                          <div class="FilteredOptions FilterdisF">
                            <div class="jobDetails" onClick={() => handleView(res)} style={{ cursor: 'pointer' }}>
                              <div class="jobHead">
                                <h6>{res.template_name}</h6>

                              </div>

                              {/* <ul class="searchDetails disF ">
                                <li>
                                  <svg xmlns="http://www.w3.org/2000/svg" height="16px" viewBox="0 0 24 24" width="16px" fill="#FA852B">
                                    <path d="M0 0h24v24H0V0z" fill="none" /><path d="M12 7V5c0-1.1-.9-2-2-2H4c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h16c1.1 0 2-.9 2-2V9c0-1.1-.9-2-2-2h-8zM6 19H4v-2h2v2zm0-4H4v-2h2v2zm0-4H4V9h2v2zm0-4H4V5h2v2zm4 12H8v-2h2v2zm0-4H8v-2h2v2zm0-4H8V9h2v2zm0-4H8V5h2v2zm9 12h-7v-2h2v-2h-2v-2h2v-2h-2V9h7c.55 0 1 .45 1 1v8c0 .55-.45 1-1 1zm-1-8h-2v2h2v-2zm0 4h-2v2h2v-2z" />
                                  </svg>
                                  <span>simplifyVMS</span>
                                </li>
                                <li>
                                  <svg xmlns="http://www.w3.org/2000/svg" height="16px" viewBox="0 0 24 24" width="16px" fill="#FA852B">
                                    <path d="M0 0h24v24H0z" fill="none" /><path d="M20 4H4c-1.11 0-1.99.89-1.99 2L2 18c0 1.11.89 2 2 2h16c1.11 0 2-.89 2-2V6c0-1.11-.89-2-2-2zm0 14H4v-6h16v6zm0-10H4V6h16v2z" />
                                  </svg>
                                  <span>$12/Hour</span>
                                </li>
                                <li>
                                  <svg xmlns="http://www.w3.org/2000/svg" enable-background="new 0 0 24 24" height="15px" viewBox="0 0 24 24" width="15px" fill="#FA852B"><g>
                                    <path d="M0,0h24v24H0V0z" fill="none" /></g><g><path d="M12,2c-4.2,0-8,3.22-8,8.2c0,3.32,2.67,7.25,8,11.8c5.33-4.55,8-8.48,8-11.8C20,5.22,16.2,2,12,2z M12,12c-1.1,0-2-0.9-2-2 c0-1.1,0.9-2,2-2c1.1,0,2,0.9,2,2C14,11.1,13.1,12,12,12z" /></g>
                                  </svg>
                                  <span>{res.location.city.name}</span>
                                </li>
                              </ul> */}

                              <div className="row my-2">
                                <div className="col-auto col-md-4">
                                  <svg xmlns="http://www.w3.org/2000/svg" height="16px" viewBox="0 0 24 24" width="16px" fill="#FA852B">
                                    <path d="M0 0h24v24H0V0z" fill="none" /><path d="M12 7V5c0-1.1-.9-2-2-2H4c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h16c1.1 0 2-.9 2-2V9c0-1.1-.9-2-2-2h-8zM6 19H4v-2h2v2zm0-4H4v-2h2v2zm0-4H4V9h2v2zm0-4H4V5h2v2zm4 12H8v-2h2v2zm0-4H8v-2h2v2zm0-4H8V9h2v2zm0-4H8V5h2v2zm9 12h-7v-2h2v-2h-2v-2h2v-2h-2V9h7c.55 0 1 .45 1 1v8c0 .55-.45 1-1 1zm-1-8h-2v2h2v-2zm0 4h-2v2h2v-2z" />
                                  </svg>
                                  <span className="ml-2">simplifyVMS</span>
                                </div>
                                <div className="col-auto col-md-4">
                                  <svg xmlns="http://www.w3.org/2000/svg" height="16px" viewBox="0 0 24 24" width="16px" fill="#FA852B">
                                    <path d="M0 0h24v24H0z" fill="none" /><path d="M20 4H4c-1.11 0-1.99.89-1.99 2L2 18c0 1.11.89 2 2 2h16c1.11 0 2-.89 2-2V6c0-1.11-.89-2-2-2zm0 14H4v-6h16v6zm0-10H4V6h16v2z" />
                                  </svg>
                                  <span className="ml-2">$12/Hour</span>
                                </div>
                                <div className="col-auto col-md-4">
                                  <svg xmlns="http://www.w3.org/2000/svg" enable-background="new 0 0 24 24" height="15px" viewBox="0 0 24 24" width="15px" fill="#FA852B"><g>
                                    <path d="M0,0h24v24H0V0z" fill="none" /></g><g><path d="M12,2c-4.2,0-8,3.22-8,8.2c0,3.32,2.67,7.25,8,11.8c5.33-4.55,8-8.48,8-11.8C20,5.22,16.2,2,12,2z M12,12c-1.1,0-2-0.9-2-2 c0-1.1,0.9-2,2-2c1.1,0,2,0.9,2,2C14,11.1,13.1,12,12,12z" /></g>
                                  </svg>
                                  <span className="ml-2">{res.location.city.name}</span>
                                </div>
                              </div>

                              <p class="updateStatus">{res.modifiedOn ? "Updated on " + res.modifiedOn : null}</p>

                            </div>
                            <div class="saveIcon" >

                              {savedJobs[res.id] && localStorage.getItem("token") !== null ?
                                <a onClick={(e) => unsaveJob(e, res)} style={{ cursor: 'pointer' }}>
                                  <svg xmlns="http://www.w3.org/2000/svg" height="20px" viewBox="0 0 24 24" width="20px" fill="#FA852B">
                                    <path d="M0 0h24v24H0z" fill="none" /><path d="M17 3H7c-1.1 0-1.99.9-1.99 2L5 21l7-3 7 3V5c0-1.1-.9-2-2-2z" />
                                  </svg>
                                </a> :
                                <a onClick={(e) => saveJob(e, res)} style={{ cursor: 'pointer' }}>
                                  <svg xmlns="http://www.w3.org/2000/svg" height="20px" viewBox="0 0 24 24" width="20px" fill="#929292">
                                    <path d="M0 0h24v24H0z" fill="none" /><path d="M17 3H7c-1.1 0-1.99.9-1.99 2L5 21l7-3 7 3V5c0-1.1-.9-2-2-2zm0 15l-5-2.18L7 18V5h10v13z" />
                                  </svg>
                                </a>
                              }




                            </div>
                          </div>


                        </div>


                      ))}
                  </div>

                }
                <div className="modal fade" id="enterDetails" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">

                  <div className="modal-dialog modal-dialog-centered multistepModal CreateProfileModal" role="document">

                    <Modal />

                  </div>

                </div>
              </div>
            </div>


          </div>
        </div>
      </section>


    </div>

  )

}

const mapStateToProps = state => {
  return {
    searchResult:state.jobSearchResult,
    fields: state.jobSearchFilters.fields,
    modalSelected: state.modalSelected,
  }
}

const mapDispatchToProps = dispatch => {
  return {

    changeModal : (modal)=> dispatch({type:"CHANGE_MODAL", modal : modal}),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AppliedJobs);

