import React from "react";
import { connect } from "react-redux";
import axios from 'axios';
import $ from "jquery";

import Input from "./input/input";
import UploadImage from "./uploadImage/uploadImage";
import Location from './input/location'
import { compose } from "redux";
import { withRouter } from 'react-router-dom';

class BasicDetails extends React.Component{
    subCategoryIDs = []
    state = {
        sub_options : []
    }
    CategoryOptions = []
    CategoryIDs =[]
    WorkerID=localStorage.getItem("VendorID")?sessionStorage.getItem("WorkerID"):localStorage.getItem("WorkerID")
    handleValidation(value, rules){
        let isValid = true;
        if (rules.required) {
            isValid = value.trim() !== "" && isValid;
        }

        if (rules.name) {
            isValid = /^[A-Za-z\s]+$/.test(value) && isValid;
        }

        if (rules.email) {
            isValid = /^[^@\s]+@[^@\s]+\.[^@\s]+$/.test(value) && isValid;
        }

        if (rules.mobile) {
            isValid = /^([0-9]{10})$/.test(value) && isValid;
        }

        if (rules.select) {
            isValid = value !== "Select" && isValid;
        }
        return isValid
    }

    handleChange(field, rules, event)
    {
        if(event===null)
        {
            this.props.changeState(field,'')
            return
        }
        let k = this.handleValidation(event.target.value, rules)
        if (k){
            this.props.changeErrorState(field, true)
        } else {
            this.props.changeErrorState(field, false)
        }
        
        if(field === 'profilePic')
        {
            var reader = new FileReader();
            reader.readAsDataURL(event.target.files[0]);
            reader.onloadend =  async()=> {
            var base64String = reader.result;
            console.log(base64String.split(",")[1]);
            this.props.changeState(field,base64String)
            }
        }
        else{
            var obj = {}
            obj[field] = event.target.value 
            obj['inValid'] = !k
            this.props.changeState(field, obj)
        }
    }

    componentWillMount=async()=>{
        if (this.WorkerID !== null){
                await axios.get('api/workers/profile/'+this.WorkerID, {headers : this.headers}).then((res) => 
                {
                console.log(res)
                this.props.mapDatabaseToLocal("fields",res.data.worker,res.data.category[res.data.category.length-1])
                })
                axios.get('api/photos/'+(localStorage.getItem("VendorID")?sessionStorage.getItem("photoID"):localStorage.getItem("photoID")))
                .then((res)=>{
                    console.log(res.data.pic)
                    //this.props.setProfilePic(JSON.parse(res.data.name))
                    this.props.changeState('profilePic',res.data.picContentType+', '+res.data.pic)
                })
        } else {
            this.props.getEmail()
        }

        await axios.get('api/allcategories', {headers : this.headers}).then(async(res) => 
        {
            this.CategoryOptions = res.data.map((item, id) => {return item.name})
            this.CategoryIDs = res.data.map((item, id) => {return item.id})
            this.forceUpdate()
            for(let x of res.data)
            {
                if(x.name===this.props.fields.Category)
                {
                    await axios.get('api/allsubcategories/'+x.id, {headers : this.headers}).then((res) => 
                    {
                        this.setState({sub_options : res.data.map((item, id) => {return item.name})})
                        this.subCategoryIDs = res.data.map((item) => {return item})
                    })
                    this.forceUpdate()
                    break
                }
            }
        })

    }

    handleSubmit = async () => {
        console.log(this.props.formValid)

        const headers = {
            'Content-Type': 'application/json',
            // 'Authorization': 'Bearer '+ sessionStorage.getItem("token")
          }

        console.log(headers)

        if (this.props.formValid){
            var data = {
                firstName: this.props.fields.Name.Name,
                lastName: this.props.fields.LastName.LastName,
                email: this.props.fields.Email.Email,
                primaryPhone: this.props.fields.PhoneNumber.PhoneNumber,
                gender: this.props.fields.Gender.Gender,
                dateOfBirth: this.props.fields.DOB.DOB,
                idProof: this.props.fields.ID_Proof.ID_Proof,
                idCode: this.props.fields.ID_Code.ID_Code,
                // status: this.props.fields.Status.Status,
                language: this.props.fields.Language.Language,
                workerLocation: this.props.fields.CurrentLocation.CurrentLocation,
            }
            if(!localStorage.getItem('VendorID'))
            {
                data['user']=JSON.parse(localStorage.getItem("user"))
            }
            if(localStorage.getItem('VendorID'))
            {
                data['vendor']={id:localStorage.getItem("VendorID")}
            }
            if(this.props.fields.profilePic!=='')
            {
                let photoID=localStorage.getItem("VendorID")?sessionStorage.getItem('photoID'):localStorage.getItem('photoID')
                if(photoID==null)
                {
                    axios.post('api/photos',{worker:{id:Number(this.WorkerID)},pic:this.props.fields.profilePic.split(",")[1],picContentType:this.props.fields.profilePic.split(",")[0]}) 
                        .then((res)=>{
                            console.log(res)
                            localStorage.getItem("VendorID")?sessionStorage.setItem('photoID',res.data.id):localStorage.setItem('photoID',res.data.id)
                        })
                }
                else{
                    axios.patch('api/photos/'+photoID,{id:Number(photoID),worker:{id:Number(this.WorkerID)},pic:this.props.fields.profilePic.split(",")[1],picContentType:this.props.fields.profilePic.split(",")[0]},{headers : {'Content-Type': 'application/merge-patch+json'}}) 
                    .then((res)=>{
                        console.log(res)
                    }) 
                }
            }
            console.log(data)
            data["id"] = this.WorkerID
            await axios.patch('api/workers/'+this.WorkerID, data, {headers : {'Content-Type': 'application/merge-patch+json'}})
            .then((response) => {console.log(response);}).catch((e) => console.log(e))

            axios.get('api/workers/profile/'+this.WorkerID,).then((res) => 
            {
                console.log(res.data.jobPreference.length,res.data.jobPreference[0])
                if (res.data.jobPreference.length !== 0){
                    var d=res.data.jobPreference[0]
                    console.log(d)
                    var d1={
                        jobSearchStatus:d.jobSearchStatus,
                        availabilityStatus:d.availabilityStatus,
                        availableFrom: d.availableFrom,
                        availableTo: d.availableTo,
                        engagementType: d.engagementType,
                        employmentType: d.employmentType,
                        locationType: d.locationType,
                        hourPerDay: Number(d.hourPerDay),
                        currencyType:d.currencyType,
                        monthlyRate : d.monthlyRate,
                        dailyRate : d.dailyRate,
                        hourlyRate : d.hourlyRate,
                        yearlyRate:d.yearlyRate,
                        worker : {
                            id : this.WorkerID,
                        },
                        subCategory : JSON.parse(localStorage.getItem("VendorID")?sessionStorage.getItem("subCat"):localStorage.getItem("subCat"))
                    }
                    d1['id']=res.data.jobPreference[0].id
                    axios.put('api/job-preferences/'+res.data.jobPreference[0].id,  d1, {headers : headers})
                .then((response) => {
                    console.log(response)
                }).catch((e) => console.log(e))
                }
                else{
                    axios.post('api/job-preferences/', {worker:{id:this.WorkerID},subCategory: JSON.parse(localStorage.getItem('VendorID')?sessionStorage.getItem("subCat"):localStorage.getItem("subCat"))})
                    .then(async(response) => {
                        console.log(response)
                        await axios.post('api/locations', 
                    {city : ''}, {headers : headers})
                    .then(async(res) => {
                        console.log(res);  await axios.post('api/location-prefrences', {worker : response.data, location : res.data, prefrenceOrder : 1})})
                        .catch((e) => console.log(e))
                    }).catch((e) => console.log(e))
                }
            })

        
            
                // await axios.post('api/categories', {isParent : true, name : this.props.fields.Category.Category},
                //     {headers : headers})
                // .then((response) => 
                    
                //     {
                //     console.log(response)
                //     axios.post('api/categories', {isParent : false, name : this.props.fields.Sub_Category.Sub_Category, parent : response.data},
                //         {headers : headers})
                //     .then((response) => {console.log(response)}).catch((e) => console.log(e))  
                // }).catch((e) => console.log(e))  
            // } else {
            //     data["id"] = this.WorkerID
            //     await axios.put('api/workers/'+this.WorkerID, data, {headers : headers})
            // .then((response) => {console.log(response);}).catch((e) => console.log(e))
            // }            
            $('#enterDetails').click();


        } else {
            this.forceUpdate()
        }
    }


    handleSubCategory = async (id , event) => {
        await axios.get('api/allsubcategories/'+id, {headers : this.headers}).then((res) => 
    {
        console.log(res)
        this.setState({sub_options : res.data.map((item, id) => {return item.name})})
        this.subCategoryIDs = res.data.map((item) => {return item})
        this.forceUpdate()
    })
    }
    
    render(){

    return (
        <div className="modal-content">
            <div className="FormSec basicDetails">
                                <form>
                                    <div className="form-row">
                                        <div className="col-md-9">
                                            <div className="form-row">
                                                <Input 
                                                divClass="form-group col-md-6" label="First Name" 
                                                config = {{className :"form-control" ,
                                                        placeholder : "Enter your First Name", 
                                                        type:"name"}}
                                                value = {this.props.fields.Name.Name}
                                                change={this.handleChange.bind(this,"Name" ,{required : true, name : true})}
                                                inValid = {this.props.fields.Name.inValid}
                                                error = {this.props.errors.Name}
                                                elementType="input" 
                                                />

                                                <Input 
                                                divClass="form-group col-md-6" label="Last Name" 
                                                config = {{className :"form-control" ,
                                                        placeholder : "Enter your Last Name", 
                                                        type:"name"}}
                                                value = {this.props.fields.LastName.LastName}
                                                change={this.handleChange.bind(this,"LastName" ,{required : true, name : true})}
                                                inValid = {this.props.fields.LastName.inValid}
                                                error = {this.props.errors.LastName}
                                                elementType="input" 
                                                />
                                            </div>

                                            <div className="form-row">
                                                <Input 
                                                divClass="form-group col-md-6" label="Email Address" 
                                                config = {{className :"form-control" ,
                                                    placeholder : "user@gmail.com", 
                                                    type:"text"}}
                                                elementType="input" 
                                                value = {this.props.fields.Email.Email}
                                                change={this.handleChange.bind(this,"Email" ,
                                                {required : true, email : true})} 
                                                error = {this.props.errors.Email}
                                                inValid = {this.props.fields.Email.inValid}
                                                />

                                                <Input 
                                                divClass="form-group col-md-6" label="Phone Number" 
                                                config = {{className :"form-control" ,
                                                    placeholder : "+91 XXX-XXX-XXXX", 
                                                    type:"text"}}
                                                elementType="input" 
                                                value = {this.props.fields.PhoneNumber.PhoneNumber}
                                                inValid = {this.props.fields.PhoneNumber.inValid}
                                                error = {this.props.errors.PhoneNumber}
                                                change={this.handleChange.bind(this,"PhoneNumber",{required : true, mobile : true})}/>

                                            </div>
                                            <div className="form-row form-group">
                                                <label htmlFor="inputGender" style={{paddingLeft:'0.5rem'}}>Gender</label>
                                            <div className="RadioBtn ml-2 ml-md-4">
                                                    <div className="form-check form-check-inline">
                                                        <input className="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="Male" checked={this.props.fields.Gender.Gender=== "Male"} onChange={this.handleChange.bind(this,"Gender", {})}/>
                                                        <label className="form-check-label" htmlFor="inlineRadio1">Male</label>
                                                    </div>
                                                    <div className="form-check form-check-inline">
                                                        <input className="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="Female" checked={this.props.fields.Gender.Gender === "Female"} onChange={this.handleChange.bind(this,"Gender", {})}/>
                                                        <label className="form-check-label" htmlFor="inlineRadio2">Female</label>
                                                    </div>
                                                    <div className="form-check form-check-inline">
                                                        <input className="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="Other" checked={this.props.fields.Gender.Gender === "Other"} onChange={this.handleChange.bind(this,"Gender", {})}/>
                                                        <label className="form-check-label" htmlFor="inlineRadio2">Other</label>
                                                    </div>
                                            </div>
                                            </div>
                                        </div>
                                        <UploadImage type='pic' file={this.props.fields.profilePic} onChange={this.handleChange.bind(this,"profilePic", {})}/>
                                         {console.log(this.props.fields.profilePic)}
                                        </div>

                                    <div className="form-row">
                                        <Input 
                                        divClass="form-group col-md-4" label="Date of Birth" 
                                        config = {{className :"form-control" , 
                                                    type:"date"}}
                                        elementType="input" 
                                        value = {this.props.fields.DOB.DOB}
                                        change={this.handleChange.bind(this,"DOB",{required : true})}
                                        inValid = {this.props.fields.DOB.inValid}
                                        error = {this.props.errors.DOB}/>

                                        <Input 
                                        divClass="form-group col-md-4" label="Category" 
                                        config = {{className :"form-control form-select" ,}}
                                        elementType="select"
                                        options = {this.CategoryOptions}
                                        value = {this.props.fields.Category}
                                        change={(event) => {this.props.setCategory(event ); this.handleSubCategory(this.CategoryIDs[this.CategoryOptions.indexOf(event.target.value)])}}/>

                                        {this.props.fields.Category &&
                                        <Input 
                                        divClass="form-group col-md-4" label="Sub-Category" 
                                        config = {{className :"form-control form-select" ,}}
                                        elementType="select"
                                        options = {["select"].concat(this.state.sub_options)} 
                                        value={this.props.fields.Sub_Category.Sub_Category}
                                        change={(event) => {this.handleChange("Sub_Category", {}, event);localStorage.getItem('VendorID')?sessionStorage.setItem("subCat",JSON.stringify(this.subCategoryIDs[this.state.sub_options.indexOf(event.target.value)])):localStorage.setItem("subCat",JSON.stringify(this.subCategoryIDs[this.state.sub_options.indexOf(event.target.value)]))}}/>}

                                    </div>

                                    <div className="form-row">
                                            <Input 
                                            divClass="form-group col-6 col-md-4" label="ID Proof" 
                                            config = {{className :"form-control form-select" ,}}
                                            elementType="select"
                                            options = {["Select", "SSN", "Passport"] }
                                            value = {this.props.fields.ID_Proof.ID_Proof}
                                            inValid = {this.props.fields.ID_Proof.inValid}
                                            change={this.handleChange.bind(this,"ID_Proof",{select : true})} />

                                            <Input 
                                            divClass="form-group col-6 col-md-4" label="ID Code" 
                                            config = {{className :"form-control" , type: "text"}}
                                            elementType="input"
                                            value = {this.props.fields.ID_Code.ID_Code}
                                            inValid = {this.props.fields.ID_Code.inValid}
                                            change={this.handleChange.bind(this,"ID_Code",{required : true})}
                                             />
                                             
                                            <Input 
                                            divClass="form-group col-md-4" label="Language" 
                                            config = {{className :"form-control form-select" ,}}
                                            elementType="select" 
                                            options = {["Select", "English", "Hindi", "Telugu", 
                                            "Tamil", "Others"] }
                                            value = {this.props.fields.Language.Language}
                                            inValid = {this.props.fields.Language.inValid}
                                            change={this.handleChange.bind(this,"Language",{select : true})}/>
                                        
                                    </div>

                                    {/* <Input 
                                    divClass="form-group" label="Current Location" 
                                    config = {{className :"form-control form-select" ,}}
                                    elementType="select" name="Location"
                                    options = {["Select", "Delhi", "Mumbai", "Hyderabad", 
                                    "Noida", "Others"] }
                                    value={this.props.fields.CurrentLocation.CurrentLocation}
                                    inValid = {this.props.fields.CurrentLocation.inValid}
                                    change={this.handleChange.bind(this,"CurrentLocation",{select : true})}/> */}
                                    <Location 
                                    divClass="form-group" label="Current Location" 
                                    config = {{className :"form-control form-select" ,}}
                                    value={this.props.fields.CurrentLocation.CurrentLocation}
                                    inValid = {this.props.fields.CurrentLocation.inValid}
                                    change={this.handleChange.bind(this,"CurrentLocation",{select : true})}/>


                                </form>
                               
                            </div>
                            <div class="modal-footer">
                                    <div class="btn-group NextFormButtons ModalNextFormButtons ">
                                        <button class="common-btn commonBlueBtn"
                                        onClick = {() => {this.props.checkFormIsValid();setTimeout(() => this.handleSubmit(),5)}}>Save</button>
                                    </div>
                            </div>
        </div>
        
    )
    }
}

const mapStateToProps = state => {
    return {
        sel : state.CategorySelected,
        fields: state.fields.fields,
        errors : state.fields.errors,
        formValid : state.fields.formValid,
        token: state.token,
        isBasicDetailsFilled:state.isBasicDetailsFilled,
        isAdditionalDetailsFilled:state.isAdditionalDetailsFilled,
        isEmploymentDetailsFilled:state.isEmploymentDetailsFilled
    }
}

const mapDispatchToProps = dispatch => {
    return {
        setCategory : (event) => dispatch({type : "CHANGE_CATEGORY", item : event.target.value}),
        changeState : (name,val)=> dispatch({type:"CHANGE_FIELD",name:name,val:val,data : 'fields'}),
        goToAdditionalDetails : () => dispatch({type: "ADDITIONAL_DETAILS"}),
        changeErrorState : (field, val) => dispatch({type : "CHANGE_ERROR_STATE", field : field, val : val, data : 'fields'}),
        checkFormIsValid : () => dispatch({type: "IS_FORM_VALID", data : 'fields'}), 
        onFilled : () => dispatch({type: "ON_FILLED", data : 'basic'}),
        // workerId: (id) => dispatch({type : "SET_WORKER_ID", id : id})
        getEmail : () => dispatch({type: "GET_EMAIL"}),
        mapDatabaseToLocal : (name,res,res2) => dispatch({type : "MAP_DATABASE_TO_LOCAl", name:name, res: res, res2: res2})
    }
}


export default compose(withRouter, connect(mapStateToProps, mapDispatchToProps))(BasicDetails);