import Header from "../header/Header";
import { Link } from 'react-router-dom';
import axios from 'axios';
import { useHistory } from 'react-router';
import { useState, useEffect } from 'react';
import './home.css';



export const Home = (props) => {

  const [value, setvalue] = useState("")
  const [errors, setErrors] = useState({})


  const history = useHistory()

  useEffect(() => {
    sessionStorage.setItem("candidateSearchInput", "")
    sessionStorage.setItem("jobSearchInput","")
    
  }
    , [])

  const orangeBtn = {
    backgroundColor: '#FA852B',
    borderColor: '#FA852B',
    borderRadius: '4px',
    width: '120px',
    height: '45px',
  }



  const designationHandler = (event,props) => {

    setvalue(event.target.value)
    if(props==='candidate')
    sessionStorage.setItem("candidateSearchInput", event.target.value)
    if(props==='job')
    sessionStorage.setItem("jobSearchInput",event.target.value)
    // handleValidation()
    // if (event.target.value.trim())
    //   axios.get("search/designationSuggestions/" + event.target.value, {
    //     transformRequest: (data, headers) => {
    //       delete headers.common['Authorization'];

    //     }
    //   }).then((res) => {
    //     console.log(res.data)
    //     setsuggestion(res.data)

    //   })
    // else
    //   setsuggestion([])


  }


  
  const handleSubmit = (res) => {
    
      console.log("entered")
      if(res==='candidate')
      history.push('/searchCandidate')
      if(res==='job')
      history.push('/searchJob')
    
  }
  const handleValidation = () => {
    let errors = {}
    let formIsValid = true

    if (!sessionStorage.getItem("userInput").trim()) {
      formIsValid = false
      errors["designation"] = "Field is required"
    }


    setErrors(errors)
    return formIsValid;
  }


  return (
    <div>
      <Header />

      <div>
        <section className="section-top">
          <div className="container-fluid">
            <div className="row">
              <div className="col-md-8 disTable">
                <div className="top-section-left">
                  <h1>Join the <span>world’s</span> <br /> best <span>Marketplace</span></h1>
                  <p>Find great talent. Build your business.<br /> Take your career to the next level.</p>
                  <div className="bt-group">
                  <Link to="/searchJob" style={{ textDecoration: 'none', color: 'inherit' }}><button className="common-btn commonBlueBtn">I want a Job</button></Link>
                  <Link to="/searchCandidate" style={{ textDecoration: 'none', color: 'inherit' }}><button className="common-btn commonOutlineBtn">I want to Hire</button></Link>
                  </div>
                </div>
              </div>
              <div class="col-md-4 disTable marT60">
                <div class="top-section-right">
                  <ul class="nav nav-pills" id="pills-tab" role="tablist">
                    <li class="nav-item">
                      <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Find a Job</a>
                    </li>
                    <li class="nav-item">
                    <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Hire Someone</a>
                    </li>
                  </ul>

                  <div class="tab-content" id="pills-tabContent">
                    <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                      <div class="searchFilter">
                        
                        <div class="SkillsFilter">
                          

                          <div class="form-group">
                            <div class="form-group">
                              {/* <label for="inputDesignation">Designation</label> */}
                              <div className="landingpageInput">
                                <input type="name" class="form-control" id="inputDesignation"
                                  onChange={(e)=>designationHandler(e,"job")} value={value} autoComplete="off" placeholder="Enter Designation." />
                              

                                <svg xmlns="http://www.w3.org/2000/svg" height="16px" viewBox="0 0 24 24" width="16px" fill="#AFAFAF">
                                  <path d="M0 0h24v24H0z" fill="none" /><path d="M20 6h-4V4c0-1.11-.89-2-2-2h-4c-1.11 0-2 .89-2 2v2H4c-1.11 0-1.99.89-1.99 2L2 19c0 1.11.89 2 2 2h16c1.11 0 2-.89 2-2V8c0-1.11-.89-2-2-2zm-6 0h-4V4h4v2z" />
                                </svg>
                              </div>
                              
                            </div>
                          </div>

                          

                          <div class="form-group">
                            <button type="submit" class="btn btn-primary orange-btn" onClick={()=>handleSubmit("job")}>Explore Now</button>
                          </div>

                        </div>
                      </div>
                    </div>

                    <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                    <div class="searchFilter">
                        
                        <div class="SkillsFilter">
                          

                          <div class="form-group">
                            <div class="form-group">
                              {/* <label for="inputDesignation">Designation</label> */}
                              <div className="landingpageInput">
                                <input type="name" class="form-control" id="inputDesignation"
                                  onChange={(e)=>designationHandler(e,"candidate")} value={value} autoComplete="off" placeholder="Enter Designation." />
                              

                                <svg xmlns="http://www.w3.org/2000/svg" height="16px" viewBox="0 0 24 24" width="16px" fill="#AFAFAF">
                                  <path d="M0 0h24v24H0z" fill="none" /><path d="M20 6h-4V4c0-1.11-.89-2-2-2h-4c-1.11 0-2 .89-2 2v2H4c-1.11 0-1.99.89-1.99 2L2 19c0 1.11.89 2 2 2h16c1.11 0 2-.89 2-2V8c0-1.11-.89-2-2-2zm-6 0h-4V4h4v2z" />
                                </svg>
                              </div>
                              <span style={{ color: "red" }}>{errors["designation"]}</span>
                            </div>
                          </div>

                          

                          <div class="form-group">
                            <button type="submit" class="btn btn-primary orange-btn" onClick={()=>handleSubmit("candidate")}>Explore Now</button>
                          </div>

                        </div>
                      </div>



                    </div>
                  </div>


                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  )
}