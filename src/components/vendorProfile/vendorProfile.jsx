import axios from "axios";
import React from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import Header from '../header/Header.jsx'
import './vendorProfile.css';
import userPhotoDefault from "../../imgaes/userPhotoDefault.png";
import $ from 'jquery';
import Modal from "../createProfile/additionalDetails/modal.jsx";
import UploadImage from "../uploadImage/uploadImage.jsx";
import MicrosoftTeamsImage from '../../imgaes/MicrosoftTeams-image.png'


class vendorProfile extends React.Component {

    state = {
        skills: '',
        designation: '',
        location: ''
    }

    constructor(props) {
        super(props)

    }



    componentWillMount = async () => {
        try {
            $('.modal-backdrop').hide();
            // $('#signUp').click();
            // window.location.reload(false);
        } catch (err) {

        }

        await axios.get("api/vendors/" + localStorage.getItem("VendorID"), {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem("token")
            }
        }).then((res) => {

            this.props.mapDatabaseToLocal("vendorBasicDetails", res.data)
            this.props.mapDatabaseToLocal("skills", res.data.worker.skills)
        })


        await axios.get('api/workers/profile/' + localStorage.getItem("WorkerID")
        ).then(async (response) => {
            if (response.data.jobPreference.length !== 0) {

                this.props.mapDatabaseToLocal("vendorCandidateDetails", response.data)
            }
        })
        await axios.get("api/refereces/worker/" + localStorage.getItem("WorkerID"), {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem("token")
            }
        }).then((response) => {

            this.props.mapDatabaseToLocal("recommendations", response.data)
        })
        await axios.get("api/search/vendor-candidate/",{
            params:{
                vendorId:localStorage.getItem("VendorID")
                  }
        }).then(async(res)=>{
            console.log(res.data)
            let data=[]
            for(let x of res.data.candidates)
            {
              let d={...x}
              await axios.get('api/worker-photo/'+x.id).then((response)=>{
                        console.log(response.data)
                  d['profile_pic']=response.data
              })
              data.push(d)
            }
            this.props.mapDatabaseToLocal("vendorCandidates",data)
        })


    }
    cat = (res) => {
        var result = null;

        if (res.Category && res.Category.Category)
            result = res.Category.Category
        if (res.SubCategory && res.SubCategory.SubCategory) {
            result += "/" + res.SubCategory.SubCategory
        }

        return result;
    }

    onChangeHandler = (data, event) => {
        switch (data) {
            case "skills":
                this.setState({ skills: event.target.value })
                break;
            case "designation":
                this.setState({ designation: event.target.value })
                break;
            case "location":
                this.setState({ location: event.target.value })
                break;
            default:
                break;
        }
    }

    reset=()=>{
        this.setState({skills:'',designation:'',location:''})
        axios.get("api/search/vendor-candidate/", {
            params: {
                vendorId: localStorage.getItem("VendorID")
            }
        }).then(async(res) => {
            await this.props.mapDatabaseToLocal("vendorCandidates", res.data.candidates)   
            return 
        })
    }

    search=()=>{
        var input=document.getElementById("search").value;
        if(input)
        {
        var param={vendorId: localStorage.getItem("VendorID"),searchText:input}
        
        axios.get('api/search/vendor-candidate/filters',{
            params:param
        }).then(async(res) => {
            await this.props.mapDatabaseToLocal("vendorCandidates", res.data.candidates)   
            return
        })
        }
        else{
            axios.get("api/search/vendor-candidate/", {
                params: {
                    vendorId: localStorage.getItem("VendorID")
                }
            }).then(async(res) => {
                await this.props.mapDatabaseToLocal("vendorCandidates", res.data.candidates)   
                return 
            })
            }

    }

    handleSubmit=()=>{
        if(this.state.skills || this.state.designation || this.state.location  ){
        var param={vendorId: localStorage.getItem("VendorID")}
        if(this.state.designation)
        param={...param,designation:this.state.designation}
        if(this.state.location)
        param={...param,location:this.state.location}
        if(this.state.skills)
        param={...param,skill:this.state.skills}
        axios.get('api/search/vendor-candidate/filters',{
            params:param
        }).then(async(res) => {
            await this.props.mapDatabaseToLocal("vendorCandidates", res.data.candidates)   
            return
        })
        }
        else{
        axios.get("api/search/vendor-candidate/", {
            params: {
                vendorId: localStorage.getItem("VendorID")
            }
        }).then(async(res) => {
            await this.props.mapDatabaseToLocal("vendorCandidates", res.data.candidates)   
            return 
        })
        }


    }
    view=(id)=>{
        console.log(id)
        localStorage.setItem("VendorCandidateID",id);
    }



    render() {

        $('.trigger-more').click(function () {
            if ($(this).hasClass('collapsed')) {
                $(this).text('See Less');
            } else {
                $(this).text('See More');
            }
        });

        console.log(this.props.myCandidatesFields)

        return (

            <div>
                <Header />
                <section class="mainbgColor view-profile-section">
                    <div class="container-fluid">
                        <div class="row mobMar0">
                            <div class="col-md-9 mobPadd0">
                                <div class="ProfileDetail">
                                    <div class="ProfileBasicDetail">
                                        {/* <div class="coverPhoto">
                                            <div class="uploadCoverPhoto">
                                                <svg xmlns="http://www.w3.org/2000/svg" height="18px" viewBox="0 0 24 24" width="18px" fill="#000000">
                                                    <path d="M0 0h24v24H0z" fill="none" /><circle cx="12" cy="12" r="3.2" /><path d="M9 2L7.17 4H4c-1.1 0-2 .9-2 2v12c0 1.1.9 2 2 2h16c1.1 0 2-.9 2-2V6c0-1.1-.9-2-2-2h-3.17L15 2H9zm3 15c-2.76 0-5-2.24-5-5s2.24-5 5-5 5 2.24 5 5-2.24 5-5 5z" />
                                                </svg>
                                              <img src={this.props.vendorBasicDetailsFields.coverPhoto}   style={{backgroundImage:this.props.vendorBasicDetailsFields.coverPhoto,backgroundRepeat:'no-repeat'}}></img>  
                                            </div>
                                        </div> */}
                                        <UploadImage type='cover_pic' file={this.props.vendorBasicDetailsFields.coverPhoto} vendor='vendor' />

                                        <div class="ViewProfileDetail marT-30">
                                            <div class="ProfilePicMain">
                                                {/* {this.props.fields.profilePic ? <img src={this.props.fields.profilePic} class="profileImg" /> : <img src={userPhotoDefault} class="profileImg"/>} */}

                                                {this.props.vendorBasicDetailsFields.profilePic ? <img src={this.props.vendorBasicDetailsFields.profilePic} class="profileImg" /> : <img src={userPhotoDefault} class="profileImg" />}
                                            </div>
                                            <div class="profileActionButtons">

                                                {/*  <div class="status online"><span class="onlinedot"></span>Active</div> */}

                                                <button type="button" class="btn commonOutlineBtn EditViewedProfile" data-toggle="modal" data-target="#enterDetails" onClick={() => { this.props.changeModal("editVendorBasicDetails") }}>
                                                    <svg xmlns="http://www.w3.org/2000/svg" height="13px" viewBox="0 0 24 24" width="13px" fill="#007BFF">
                                                        <path d="M0 0h24v24H0z" fill="none" /><path d="M3 17.25V21h3.75L17.81 9.94l-3.75-3.75L3 17.25zM20.71 7.04c.39-.39.39-1.02 0-1.41l-2.34-2.34c-.39-.39-1.02-.39-1.41 0l-1.83 1.83 3.75 3.75 1.83-1.83z" />
                                                    </svg>
                                                    &nbsp;Edit Profile
                                                </button>

                                            </div>


                                            <h2 class="FontBold">{this.props.vendorBasicDetailsFields.Name.Name}</h2>

                                            <div class="ProfileBasic">
                                                <ul class="ProfileBasicInfo">
                                                    <li>
                                                        <span>
                                                            <svg xmlns="http://www.w3.org/2000/svg" height="15px" viewBox="0 0 24 24" width="15px" fill="#FA852B">
                                                                <path d="M0 0h24v24H0z" fill="none" /><path d="M20.01 15.38c-1.23 0-2.42-.2-3.53-.56-.35-.12-.74-.03-1.01.24l-1.57 1.97c-2.83-1.35-5.48-3.9-6.89-6.83l1.95-1.66c.27-.28.35-.67.24-1.02-.37-1.11-.56-2.3-.56-3.53 0-.54-.45-.99-.99-.99H4.19C3.65 3 3 3.24 3 3.99 3 13.28 10.73 21 20.01 21c.71 0 .99-.63.99-1.18v-3.45c0-.54-.45-.99-.99-.99z" />
                                                            </svg>
                                                            {this.props.vendorBasicDetailsFields.PhoneNumber.PhoneNumber}
                                                        </span>
                                                    </li>

                                                    <li>
                                                        <span>
                                                            <svg xmlns="http://www.w3.org/2000/svg" height="15px" viewBox="0 0 24 24" width="15px" fill="#FA852B">
                                                                <path d="M0 0h24v24H0z" fill="none" /><path d="M20 4H4c-1.1 0-1.99.9-1.99 2L2 18c0 1.1.9 2 2 2h16c1.1 0 2-.9 2-2V6c0-1.1-.9-2-2-2zm0 4l-8 5-8-5V6l8 5 8-5v2z" />
                                                            </svg>
                                                            {this.props.vendorBasicDetailsFields.Email.Email}
                                                        </span>
                                                    </li>
                                                </ul>

                                                <ul class="ProfileBasicInfo">
                                                    <li>
                                                        <span>
                                                            <svg xmlns="http://www.w3.org/2000/svg" enable-background="new 0 0 24 24" height="15px" viewBox="0 0 24 24" width="15px" fill="#FA852B"><rect fill="none" height="24" width="24" />
                                                                <path d="M12,7V3H2v18h20V7H12z M10,19H4v-2h6V19z M10,15H4v-2h6V15z M10,11H4V9h6V11z M10,7H4V5h6V7z M20,19h-8V9h8V19z M18,11h-4v2 h4V11z M18,15h-4v2h4V15z" />
                                                            </svg>
                                                            {this.props.vendorBasicDetailsFields.EmployerName.EmployerName}
                                                        </span>
                                                    </li>
                                                    <li>
                                                        <span>
                                                            <svg xmlns="http://www.w3.org/2000/svg" enable-background="new 0 0 24 24" height="15px" viewBox="0 0 24 24" width="15px" fill="#FA852B"><g><rect fill="none" height="24" width="24" y="0" /></g>
                                                                <g><path d="M20,7h-5V4c0-1.1-0.9-2-2-2h-2C9.9,2,9,2.9,9,4v3H4C2.9,7,2,7.9,2,9v11c0,1.1,0.9,2,2,2h16c1.1,0,2-0.9,2-2V9 C22,7.9,21.1,7,20,7z M9,12c0.83,0,1.5,0.67,1.5,1.5c0,0.83-0.67,1.5-1.5,1.5s-1.5-0.67-1.5-1.5C7.5,12.67,8.17,12,9,12z M12,18H6 v-0.43c0-0.6,0.36-1.15,0.92-1.39C7.56,15.9,8.26,15.75,9,15.75s1.44,0.15,2.08,0.43c0.55,0.24,0.92,0.78,0.92,1.39V18z M13,9h-2V4 h2V9z M17.25,16.5h-2.5c-0.41,0-0.75-0.34-0.75-0.75v0c0-0.41,0.34-0.75,0.75-0.75h2.5c0.41,0,0.75,0.34,0.75,0.75v0 C18,16.16,17.66,16.5,17.25,16.5z M17.25,13.5h-2.5c-0.41,0-0.75-0.34-0.75-0.75v0c0-0.41,0.34-0.75,0.75-0.75h2.5 c0.41,0,0.75,0.34,0.75,0.75v0C18,13.16,17.66,13.5,17.25,13.5z" /></g>
                                                            </svg>
                                                            {this.props.vendorBasicDetailsFields.EmployeeID.EmployeeID}
                                                        </span>
                                                    </li>
                                                    <li>
                                                        <span>
                                                            <svg xmlns="http://www.w3.org/2000/svg" height="15px" viewBox="0 0 24 24" width="15px" fill="#FA852B">
                                                                <path d="M0 0h24v24H0V0z" fill="none" /><path d="M11.99 2C6.47 2 2 6.48 2 12s4.47 10 9.99 10C17.52 22 22 17.52 22 12S17.52 2 11.99 2zm6.93 6h-2.95c-.32-1.25-.78-2.45-1.38-3.56 1.84.63 3.37 1.91 4.33 3.56zM12 4.04c.83 1.2 1.48 2.53 1.91 3.96h-3.82c.43-1.43 1.08-2.76 1.91-3.96zM4.26 14C4.1 13.36 4 12.69 4 12s.1-1.36.26-2h3.38c-.08.66-.14 1.32-.14 2s.06 1.34.14 2H4.26zm.82 2h2.95c.32 1.25.78 2.45 1.38 3.56-1.84-.63-3.37-1.9-4.33-3.56zm2.95-8H5.08c.96-1.66 2.49-2.93 4.33-3.56C8.81 5.55 8.35 6.75 8.03 8zM12 19.96c-.83-1.2-1.48-2.53-1.91-3.96h3.82c-.43 1.43-1.08 2.76-1.91 3.96zM14.34 14H9.66c-.09-.66-.16-1.32-.16-2s.07-1.35.16-2h4.68c.09.65.16 1.32.16 2s-.07 1.34-.16 2zm.25 5.56c.6-1.11 1.06-2.31 1.38-3.56h2.95c-.96 1.65-2.49 2.93-4.33 3.56zM16.36 14c.08-.66.14-1.32.14-2s-.06-1.34-.14-2h3.38c.16.64.26 1.31.26 2s-.1 1.36-.26 2h-3.38z" />
                                                            </svg>
                                                            {this.props.vendorBasicDetailsFields.EmployerWebsite.EmployerWebsite}
                                                        </span>
                                                    </li>
                                                </ul>

                                            </div>

                                        </div>
                                    </div>

                                    {/* ONLY FOR MOBILE */}

                                    <div class="disNoneDesk disBlockMob m-3 rounded">
                                        <div class="EmploynmentCondition">
                                            <div class="EmploynmentConditionHead">
                                                <h4 className="mb-0">Default Details</h4>

                                                <div class="editEmploynmentCondition">
                                                    <svg xmlns="http://www.w3.org/2000/svg" height="18px" viewBox="0 0 24 24" width="18px" fill="#007BFF">
                                                        <path d="M0 0h24v24H0z" fill="none" /><path d="M3 17.25V21h3.75L17.81 9.94l-3.75-3.75L3 17.25zM20.71 7.04c.39-.39.39-1.02 0-1.41l-2.34-2.34c-.39-.39-1.02-.39-1.41 0l-1.83 1.83 3.75 3.75 1.83-1.83z" />
                                                    </svg>
                                                </div>
                                                <a className="mob-see-more collapsed" role="button" data-toggle="collapse" href="#vendorMobCollapse" aria-expanded="false" aria-controls="vendorMobCollapse"></a>
                                            </div>


                                            {/* <div class="editEmploynmentConditionInner" class="collapse" id="ConditionShow" aria-expanded="false"> */}
                                            <div class="collapse" id="vendorMobCollapse" aria-expanded="false">
                                                <div className="scroll-items">
                                                    <div class="Conditions disF">
                                                        <svg xmlns="http://www.w3.org/2000/svg" height="24px" viewBox="0 0 24 24" width="24px" fill="#FA852B">
                                                            <path d="M0 0h24v24H0V0z" fill="none" /><path d="M4 13h6c.55 0 1-.45 1-1V4c0-.55-.45-1-1-1H4c-.55 0-1 .45-1 1v8c0 .55.45 1 1 1zm0 8h6c.55 0 1-.45 1-1v-4c0-.55-.45-1-1-1H4c-.55 0-1 .45-1 1v4c0 .55.45 1 1 1zm10 0h6c.55 0 1-.45 1-1v-8c0-.55-.45-1-1-1h-6c-.55 0-1 .45-1 1v8c0 .55.45 1 1 1zM13 4v4c0 .55.45 1 1 1h6c.55 0 1-.45 1-1V4c0-.55-.45-1-1-1h-6c-.55 0-1 .45-1 1z" />
                                                        </svg>

                                                        <div class="employenmentsec">
                                                            <h6>Category</h6>
                                                            <p>{this.props.vendorCandidateDetailsFields.Category ? this.props.vendorCandidateDetailsFields.Category : "N/A"}</p>
                                                        </div>
                                                    </div>

                                                    <div class="Conditions disF">
                                                        <svg xmlns="http://www.w3.org/2000/svg" height="24px" viewBox="0 0 24 24" width="24px" fill="#FA852B">
                                                            <path d="M0 0h24v24H0V0z" fill="none" /><path d="M4 13h6c.55 0 1-.45 1-1V4c0-.55-.45-1-1-1H4c-.55 0-1 .45-1 1v8c0 .55.45 1 1 1zm0 8h6c.55 0 1-.45 1-1v-4c0-.55-.45-1-1-1H4c-.55 0-1 .45-1 1v4c0 .55.45 1 1 1zm10 0h6c.55 0 1-.45 1-1v-8c0-.55-.45-1-1-1h-6c-.55 0-1 .45-1 1v8c0 .55.45 1 1 1zM13 4v4c0 .55.45 1 1 1h6c.55 0 1-.45 1-1V4c0-.55-.45-1-1-1h-6c-.55 0-1 .45-1 1z" />
                                                        </svg>

                                                        <div class="employenmentsec">
                                                            <h6>Sub-Category</h6>
                                                            <p>{this.props.vendorCandidateDetailsFields.Sub_Category.Sub_Category ? this.props.vendorCandidateDetailsFields.Sub_Category.Sub_Category : "N/A"}</p>
                                                        </div>
                                                    </div>

                                                    <div class="Conditions disF">
                                                        <svg xmlns="http://www.w3.org/2000/svg" enable-background="new 0 0 24 24" height="24px" viewBox="0 0 24 24" width="24px" fill="#FA852B">
                                                            <g><rect fill="none" height="24" width="24" /><path d="M19,3H5C3.89,3,3,3.9,3,5v14c0,1.1,0.89,2,2,2h14c1.1,0,2-0.9,2-2V5C21,3.9,20.11,3,19,3z M19,19H5V7h14V19z M13.5,13 c0,0.83-0.67,1.5-1.5,1.5s-1.5-0.67-1.5-1.5c0-0.83,0.67-1.5,1.5-1.5S13.5,12.17,13.5,13z M12,9c-2.73,0-5.06,1.66-6,4 c0.94,2.34,3.27,4,6,4s5.06-1.66,6-4C17.06,10.66,14.73,9,12,9z M12,15.5c-1.38,0-2.5-1.12-2.5-2.5c0-1.38,1.12-2.5,2.5-2.5 c1.38,0,2.5,1.12,2.5,2.5C14.5,14.38,13.38,15.5,12,15.5z" />
                                                            </g>
                                                        </svg>

                                                        <div class="employenmentsec">
                                                            <h6>ID Proof</h6>
                                                            <p>{this.props.vendorCandidateDetailsFields.ID_Proof.ID_Proof ? this.props.vendorCandidateDetailsFields.ID_Proof.ID_Proof : "N/A"}</p>
                                                        </div>
                                                    </div>

                                                    <div class="Conditions disF">
                                                        <svg xmlns="http://www.w3.org/2000/svg" height="24px" viewBox="0 0 24 24" width="24px" fill="#FA852B">
                                                            <path d="M0 0h24v24H0V0z" fill="none" /><path d="M20 5h-9.12L10 2H4c-1.1 0-2 .9-2 2v13c0 1.1.9 2 2 2h7l1 3h8c1.1 0 2-.9 2-2V7c0-1.1-.9-2-2-2zM7.17 14.59c-2.25 0-4.09-1.83-4.09-4.09s1.83-4.09 4.09-4.09c1.04 0 1.99.37 2.74 1.07l.07.06-1.23 1.18-.06-.05c-.29-.27-.78-.59-1.52-.59-1.31 0-2.38 1.09-2.38 2.42s1.07 2.42 2.38 2.42c1.37 0 1.96-.87 2.12-1.46H7.08V9.91h3.95l.01.07c.04.21.05.4.05.61 0 2.35-1.61 4-3.92 4zm6.03-1.71c.33.6.74 1.18 1.19 1.7l-.54.53-.65-2.23zm.77-.76h-.99l-.31-1.04h3.99s-.34 1.31-1.56 2.74c-.52-.62-.89-1.23-1.13-1.7zM21 20c0 .55-.45 1-1 1h-7l2-2-.81-2.77.92-.92L17.79 18l.73-.73-2.71-2.68c.9-1.03 1.6-2.25 1.92-3.51H19v-1.04h-3.64V9h-1.04v1.04h-1.96L11.18 6H20c.55 0 1 .45 1 1v13z" />
                                                        </svg>

                                                        <div class="employenmentsec">
                                                            <h6>Language</h6>
                                                            <p>{this.props.vendorCandidateDetailsFields.Language.Language ? this.props.vendorCandidateDetailsFields.Language.Language : "N/A"}</p>
                                                        </div>
                                                    </div>

                                                    <span id="dots"></span>
                                                    <span id="more">
                                                        <div class="Conditions disF">
                                                            <svg xmlns="http://www.w3.org/2000/svg" enable-background="new 0 0 24 24" height="22px" viewBox="0 0 24 24" width="22px" fill="#FA852B"><g>
                                                                <path d="M0,0h24v24H0V0z" fill="none" /></g><g><path d="M12,2c-4.2,0-8,3.22-8,8.2c0,3.32,2.67,7.25,8,11.8c5.33-4.55,8-8.48,8-11.8C20,5.22,16.2,2,12,2z M12,12c-1.1,0-2-0.9-2-2 c0-1.1,0.9-2,2-2c1.1,0,2,0.9,2,2C14,11.1,13.1,12,12,12z" /></g>
                                                            </svg>

                                                            <div class="employenmentsec">
                                                                <h6>Current Location</h6>
                                                                <p>{this.props.vendorCandidateDetailsFields.CurrentLocation.CurrentLocation ? this.props.vendorCandidateDetailsFields.CurrentLocation.CurrentLocation : 'N/A'}</p>
                                                            </div>
                                                        </div>


                                                        <div class="Conditions disF">
                                                            <svg xmlns="http://www.w3.org/2000/svg" height="24px" viewBox="0 0 24 24" width="24px" fill="#FA852B"><path d="M0 0h24v24H0z" fill="none" />
                                                                <path d="M11.99 2c-5.52 0-10 4.48-10 10s4.48 10 10 10 10-4.48 10-10-4.48-10-10-10zm3.61 6.34c1.07 0 1.93.86 1.93 1.93 0 1.07-.86 1.93-1.93 1.93-1.07 0-1.93-.86-1.93-1.93-.01-1.07.86-1.93 1.93-1.93zm-6-1.58c1.3 0 2.36 1.06 2.36 2.36 0 1.3-1.06 2.36-2.36 2.36s-2.36-1.06-2.36-2.36c0-1.31 1.05-2.36 2.36-2.36zm0 9.13v3.75c-2.4-.75-4.3-2.6-5.14-4.96 1.05-1.12 3.67-1.69 5.14-1.69.53 0 1.2.08 1.9.22-1.64.87-1.9 2.02-1.9 2.68zM11.99 20c-.27 0-.53-.01-.79-.04v-4.07c0-1.42 2.94-2.13 4.4-2.13 1.07 0 2.92.39 3.84 1.15-1.17 2.97-4.06 5.09-7.45 5.09z" />
                                                            </svg>

                                                            <div class="employenmentsec">
                                                                <h6>Recommendation</h6>
                                                                <p>{this.props.recommendationFields.length > 0 ? this.props.recommendationFields.map((val, index) => { return val.Name.Name + (index !== this.props.recommendationFields.length - 1 ? ", " : "") }) : "N/A"}</p>
                                                            </div>
                                                        </div>

                                                        <div class="Conditions disF">
                                                            <svg xmlns="http://www.w3.org/2000/svg" enable-background="new 0 0 24 24" height="24px" viewBox="0 0 24 24" width="24px" fill="#FA852B"><g>
                                                                <rect fill="none" height="24" width="24" /></g><g><g /><path d="M12,3c-0.46,0-0.93,0.04-1.4,0.14C7.84,3.67,5.64,5.9,5.12,8.66c-0.48,2.61,0.48,5.01,2.22,6.56C7.77,15.6,8,16.13,8,16.69 V19c0,1.1,0.9,2,2,2h0.28c0.35,0.6,0.98,1,1.72,1s1.38-0.4,1.72-1H14c1.1,0,2-0.9,2-2v-2.31c0-0.55,0.22-1.09,0.64-1.46 C18.09,13.95,19,12.08,19,10C19,6.13,15.87,3,12,3z M14,19h-4v-1h4V19z M14,17h-4v-1h4V17z M12.5,11.41V14h-1v-2.59L9.67,9.59 l0.71-0.71L12,10.5l1.62-1.62l0.71,0.71L12.5,11.41z" /></g>
                                                            </svg>

                                                            <div class="employenmentsec">
                                                                <h6>Skills</h6>
                                                                <p>{this.props.skills.length > 0 ? this.props.skills.map((skill, index) => { return skill.skillName + (index !== this.props.skills.length - 1 ? ", " : "") }) : "N/A"}</p>
                                                            </div>
                                                        </div>

                                                        <div class="Conditions disF">
                                                            <svg xmlns="http://www.w3.org/2000/svg" height="24px" viewBox="0 0 24 24" width="24px" fill="#FA852B">
                                                                <path d="M0 0h24v24H0z" fill="none" /><path d="M20 19.59V8l-6-6H6c-1.1 0-1.99.9-1.99 2L4 20c0 1.1.89 2 1.99 2H18c.45 0 .85-.15 1.19-.4l-4.43-4.43c-.8.52-1.74.83-2.76.83-2.76 0-5-2.24-5-5s2.24-5 5-5 5 2.24 5 5c0 1.02-.31 1.96-.83 2.75L20 19.59zM9 13c0 1.66 1.34 3 3 3s3-1.34 3-3-1.34-3-3-3-3 1.34-3 3z" />
                                                            </svg>

                                                            <div class="employenmentsec">
                                                                <h6>Job Search Status</h6>
                                                                <p>{this.props.vendorCandidateDetailsFields.JobStatus.JobStatus ? this.props.vendorCandidateDetailsFields.JobStatus.JobStatus : "N/A"}</p>
                                                            </div>
                                                        </div>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="ProfileBasicDetail">
                                        <div class="vendorHead Candidate-profile-card ViewProfileDetail disF">
                                            <h2 class="CandidateProfileHead">My Candidates</h2>
                                            <div class="rightIcons d-md-flex align-items-center">

                                                <div className="d-none d-md-block">
                                                    <div class="input-group">
                                                        <input type="text" id="search" class="form-control" placeholder="Search" />
                                                        <div class="input-group-append" style={{ cursor: "pointer" }} onClick={this.search}>
                                                            <p class="input-group-text px-2">
                                                                <svg xmlns="http://www.w3.org/2000/svg" height="18px" viewBox="0 0 24 24" width="18px" fill="#000000">
                                                                    <path d="M0 0h24v24H0z" fill="none" /><path d="M15.5 14h-.79l-.28-.27C15.41 12.59 16 11.11 16 9.5 16 5.91 13.09 3 9.5 3S3 5.91 3 9.5 5.91 16 9.5 16c1.61 0 3.09-.59 4.23-1.57l.27.28v.79l5 4.99L20.49 19l-4.99-5zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5 14 7.01 14 9.5 11.99 14 9.5 14z" />
                                                                </svg>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>

                                                {/* <div className="d-inline-block d-md-none">
                                                    <a href="#" title="Search">
                                                        <svg xmlns="http://www.w3.org/2000/svg" height="18px" viewBox="0 0 24 24" width="18px" fill="#000000">
                                                            <path d="M0 0h24v24H0z" fill="none" /><path d="M15.5 14h-.79l-.28-.27C15.41 12.59 16 11.11 16 9.5 16 5.91 13.09 3 9.5 3S3 5.91 3 9.5 5.91 16 9.5 16c1.61 0 3.09-.59 4.23-1.57l.27.28v.79l5 4.99L20.49 19l-4.99-5zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5 14 7.01 14 9.5 11.99 14 9.5 14z" />
                                                        </svg>
                                                    </a>
                                                </div> */}

                                                <a href="#" title="Filter" data-toggle="collapse" href="#filterCollapse" aria-expanded="false" aria-controls="filterCollapse">
                                                    <svg xmlns="http://www.w3.org/2000/svg" height="18px" viewBox="0 0 24 24" width="18px" fill="#000000">
                                                        <path d="M0 0h24v24H0V0z" fill="none" /><path d="M11 18h2c.55 0 1-.45 1-1s-.45-1-1-1h-2c-.55 0-1 .45-1 1s.45 1 1 1zM3 7c0 .55.45 1 1 1h16c.55 0 1-.45 1-1s-.45-1-1-1H4c-.55 0-1 .45-1 1zm4 6h10c.55 0 1-.45 1-1s-.45-1-1-1H7c-.55 0-1 .45-1 1s.45 1 1 1z" />
                                                    </svg>
                                                </a>
                                                {/* <Link target={"_blank"} to="/createProfile/basicDetails" > */}
                                                <a title="Add Candidate" data-toggle="modal" data-target="#addCandidate">
                                                    <svg xmlns="http://www.w3.org/2000/svg" height="18px" viewBox="0 0 24 24" width="18px" fill="#000000">
                                                        <path d="M0 0h24v24H0z" fill="none" /><path d="M15 12c2.21 0 4-1.79 4-4s-1.79-4-4-4-4 1.79-4 4 1.79 4 4 4zm-9-2V7H4v3H1v2h3v3h2v-3h3v-2H6zm9 4c-2.67 0-8 1.34-8 4v2h16v-2c0-2.66-5.33-4-8-4z" />
                                                    </svg>
                                                </a>
                                                {/* </Link> */}
                                                <a href="#" title="Bulk Upload">
                                                    <svg xmlns="http://www.w3.org/2000/svg" height="18px" viewBox="0 0 24 24" width="18px" fill="#000000">
                                                        <path d="M0 0h24v24H0V0z" fill="none" /><path d="M14 2H6c-1.1 0-1.99.9-1.99 2L4 20c0 1.1.89 2 1.99 2H18c1.1 0 2-.9 2-2V8l-6-6zm4 18H6V4h7v5h5v11zM8 15.01l1.41 1.41L11 14.84V19h2v-4.16l1.59 1.59L16 15.01 12.01 11z" />
                                                    </svg>
                                                </a>

                                            </div>
                                        </div>
                                    </div>

                                    <div className="ProfileBasicDetail">
                                        <div className="collapse" id="filterCollapse">
                                            <div className="Candidate-profile-card ViewProfileDetail">
                                                <div className="form-row">
                                                    <div className="form-group col-md-4">
                                                        <label>Skills</label>
                                                        <input className="form-control" placeholder="Skills" type="text" value={this.state.skills} onChange={(event) => this.onChangeHandler("skills", event)} />
                                                    </div>
                                                    <div className="form-group col-md-4">
                                                        <label>Designation</label>
                                                        <input className="form-control" placeholder="Designation" type="text"  value={this.state.designation} onChange={(event) => this.onChangeHandler("designation", event)}/>
                                                    </div>
                                                    <div className="form-group col-md-4">
                                                        <label>Location</label>
                                                        <input className="form-control" placeholder="Location" type="text" value={this.state.location} onChange={(event) => this.onChangeHandler("location", event)} />
                                                    </div>
                                                </div>
                                                <div className="btn-group NextFormButtons ModalNextFormButtons justify-content-end">
                                                    <button className="common-btn commonOutlineBtn" onClick={this.reset}>Reset All</button>
                                                    <button className="common-btn commonBlueBtn ml-3" onClick={this.handleSubmit}>Search</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="ProfileBasicDetail">
                                        <div className="form-row">
                                            {this.props.myCandidatesFields.map(res => (
                                                
                                                <div className="col-md-6" >
                                                    <Link onClick={()=>this.view(res.Id.Id)} target={"_blank"} to='/viewprofile' >
                                                    <div class="Candidate-profile-card ViewProfileDetail VieweditableDetails" >
                                                        <div class="Candidate-profile-pic">
                                                           <img src={res.ProfilePic.ProfilePic!==""?res.ProfilePic.ProfilePic:userPhotoDefault} class="profileImg" />
                                                        </div>
                                                        <div class="Candidate-profile-details">
                                                            <h3 className="font-weight-bold">{res.FirstName.FirstName} {res.LastName.LastName}</h3>

                                                            <div class="row mt-2">
                                                                <div className="col-auto col-md-6">
                                                                    <svg xmlns="http://www.w3.org/2000/svg" height="14px" viewBox="0 0 24 24" width="14px" fill="#FA852B">
                                                                        <path d="M0 0h24v24H0z" fill="none" /><path d="M20 6h-4V4c0-1.11-.89-2-2-2h-4c-1.11 0-2 .89-2 2v2H4c-1.11 0-1.99.89-1.99 2L2 19c0 1.11.89 2 2 2h16c1.11 0 2-.89 2-2V8c0-1.11-.89-2-2-2zm-6 0h-4V4h4v2z" />
                                                                    </svg>
                                                                    <span className="ml-2">{this.cat(res) ? this.cat(res) : "N/A"}</span>
                                                                </div>
                                                                <div className="col-auto col-md-6">
                                                                    <svg xmlns="http://www.w3.org/2000/svg" enable-background="new 0 0 24 24" height="16px" viewBox="0 0 24 24" width="16px" fill="#FA852B"><g>
                                                                        <path d="M0,0h24v24H0V0z" fill="none" /></g><g><path d="M12,2c-4.2,0-8,3.22-8,8.2c0,3.32,2.67,7.25,8,11.8c5.33-4.55,8-8.48,8-11.8C20,5.22,16.2,2,12,2z M12,12c-1.1,0-2-0.9-2-2 c0-1.1,0.9-2,2-2c1.1,0,2,0.9,2,2C14,11.1,13.1,12,12,12z" /></g>
                                                                    </svg>
                                                                    <span className="ml-2">{res.Location.Location ? res.Location.Location : "N/A"}</span>
                                                                </div>
                                                                <div className="col-auto col-md-6">
                                                                    <svg xmlns="http://www.w3.org/2000/svg" height="16px" viewBox="0 0 24 24" width="16px" fill="#FA852B">
                                                                        <path d="M0 0h24v24H0z" fill="none" /><path d="M17 3H7c-1.1 0-1.99.9-1.99 2L5 21l7-3 7 3V5c0-1.1-.9-2-2-2z" />
                                                                    </svg>
                                                                    <span className="ml-2">{res.SavedJobs.SavedJobs} Saved Jobs</span>
                                                                </div>
                                                                <div className="col-auto col-md-6">
                                                                    <svg xmlns="http://www.w3.org/2000/svg" height="14px" viewBox="0 0 24 24" width="14px" fill="#FA852B">
                                                                        <path d="M0 0h24v24H0z" fill="none" /><path d="M19 3h-4.18C14.4 1.84 13.3 1 12 1c-1.3 0-2.4.84-2.82 2H5c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zm-7 0c.55 0 1 .45 1 1s-.45 1-1 1-1-.45-1-1 .45-1 1-1zm-2 14l-4-4 1.41-1.41L10 14.17l6.59-6.59L18 9l-8 8z" />
                                                                    </svg>
                                                                    <span className="ml-2">{res.AppliedJobs.AppliedJobs} Applied Jobs</span>
                                                                </div>
                                                            </div>

                                                        </div>

                                                    </div>
                                                    </Link>
                                                </div>
                                            
                                            ))}


                                        </div>
                                    </div>
                                </div>
                            </div>



                            <div class="col-md-3 DisNoneMob">
                                <div class="EmploynmentCondition">
                                    <div class="EmploynmentConditionHead">
                                        <h4>Default Details</h4>

                                        <div class="editEmploynmentCondition" data-toggle="modal" data-target="#defaultDetails" onClick={() => { this.props.changeModal("editVendorDefualtDetails") }}>
                                            <svg xmlns="http://www.w3.org/2000/svg" height="18px" viewBox="0 0 24 24" width="18px" fill="#007BFF">
                                                <path d="M0 0h24v24H0z" fill="none" /><path d="M3 17.25V21h3.75L17.81 9.94l-3.75-3.75L3 17.25zM20.71 7.04c.39-.39.39-1.02 0-1.41l-2.34-2.34c-.39-.39-1.02-.39-1.41 0l-1.83 1.83 3.75 3.75 1.83-1.83z" />
                                            </svg>
                                        </div>
                                    </div>


                                    {/* <div class="editEmploynmentConditionInner" class="collapse" id="ConditionShow" aria-expanded="false"> */}
                                    <div className="scroll-items">
                                        <div class="Conditions disF">
                                            <svg xmlns="http://www.w3.org/2000/svg" height="24px" viewBox="0 0 24 24" width="24px" fill="#FA852B">
                                                <path d="M0 0h24v24H0V0z" fill="none" /><path d="M4 13h6c.55 0 1-.45 1-1V4c0-.55-.45-1-1-1H4c-.55 0-1 .45-1 1v8c0 .55.45 1 1 1zm0 8h6c.55 0 1-.45 1-1v-4c0-.55-.45-1-1-1H4c-.55 0-1 .45-1 1v4c0 .55.45 1 1 1zm10 0h6c.55 0 1-.45 1-1v-8c0-.55-.45-1-1-1h-6c-.55 0-1 .45-1 1v8c0 .55.45 1 1 1zM13 4v4c0 .55.45 1 1 1h6c.55 0 1-.45 1-1V4c0-.55-.45-1-1-1h-6c-.55 0-1 .45-1 1z" />
                                            </svg>

                                            <div class="employenmentsec">
                                                <h6>Category</h6>
                                                <p>{this.props.vendorCandidateDetailsFields.Category ? this.props.vendorCandidateDetailsFields.Category : "N/A"}</p>
                                            </div>
                                        </div>

                                        <div class="Conditions disF">
                                            <svg xmlns="http://www.w3.org/2000/svg" height="24px" viewBox="0 0 24 24" width="24px" fill="#FA852B">
                                                <path d="M0 0h24v24H0V0z" fill="none" /><path d="M4 13h6c.55 0 1-.45 1-1V4c0-.55-.45-1-1-1H4c-.55 0-1 .45-1 1v8c0 .55.45 1 1 1zm0 8h6c.55 0 1-.45 1-1v-4c0-.55-.45-1-1-1H4c-.55 0-1 .45-1 1v4c0 .55.45 1 1 1zm10 0h6c.55 0 1-.45 1-1v-8c0-.55-.45-1-1-1h-6c-.55 0-1 .45-1 1v8c0 .55.45 1 1 1zM13 4v4c0 .55.45 1 1 1h6c.55 0 1-.45 1-1V4c0-.55-.45-1-1-1h-6c-.55 0-1 .45-1 1z" />
                                            </svg>

                                            <div class="employenmentsec">
                                                <h6>Sub-Category</h6>
                                                <p>{this.props.vendorCandidateDetailsFields.Sub_Category.Sub_Category ? this.props.vendorCandidateDetailsFields.Sub_Category.Sub_Category : "N/A"}</p>
                                            </div>
                                        </div>

                                        <div class="Conditions disF">
                                            <svg xmlns="http://www.w3.org/2000/svg" enable-background="new 0 0 24 24" height="24px" viewBox="0 0 24 24" width="24px" fill="#FA852B">
                                                <g><rect fill="none" height="24" width="24" /><path d="M19,3H5C3.89,3,3,3.9,3,5v14c0,1.1,0.89,2,2,2h14c1.1,0,2-0.9,2-2V5C21,3.9,20.11,3,19,3z M19,19H5V7h14V19z M13.5,13 c0,0.83-0.67,1.5-1.5,1.5s-1.5-0.67-1.5-1.5c0-0.83,0.67-1.5,1.5-1.5S13.5,12.17,13.5,13z M12,9c-2.73,0-5.06,1.66-6,4 c0.94,2.34,3.27,4,6,4s5.06-1.66,6-4C17.06,10.66,14.73,9,12,9z M12,15.5c-1.38,0-2.5-1.12-2.5-2.5c0-1.38,1.12-2.5,2.5-2.5 c1.38,0,2.5,1.12,2.5,2.5C14.5,14.38,13.38,15.5,12,15.5z" />
                                                </g>
                                            </svg>

                                            <div class="employenmentsec">
                                                <h6>ID Proof</h6>
                                                <p>{this.props.vendorCandidateDetailsFields.ID_Proof.ID_Proof ? this.props.vendorCandidateDetailsFields.ID_Proof.ID_Proof : "N/A"}</p>
                                            </div>
                                        </div>

                                        <div class="Conditions disF">
                                            <svg xmlns="http://www.w3.org/2000/svg" height="24px" viewBox="0 0 24 24" width="24px" fill="#FA852B">
                                                <path d="M0 0h24v24H0V0z" fill="none" /><path d="M20 5h-9.12L10 2H4c-1.1 0-2 .9-2 2v13c0 1.1.9 2 2 2h7l1 3h8c1.1 0 2-.9 2-2V7c0-1.1-.9-2-2-2zM7.17 14.59c-2.25 0-4.09-1.83-4.09-4.09s1.83-4.09 4.09-4.09c1.04 0 1.99.37 2.74 1.07l.07.06-1.23 1.18-.06-.05c-.29-.27-.78-.59-1.52-.59-1.31 0-2.38 1.09-2.38 2.42s1.07 2.42 2.38 2.42c1.37 0 1.96-.87 2.12-1.46H7.08V9.91h3.95l.01.07c.04.21.05.4.05.61 0 2.35-1.61 4-3.92 4zm6.03-1.71c.33.6.74 1.18 1.19 1.7l-.54.53-.65-2.23zm.77-.76h-.99l-.31-1.04h3.99s-.34 1.31-1.56 2.74c-.52-.62-.89-1.23-1.13-1.7zM21 20c0 .55-.45 1-1 1h-7l2-2-.81-2.77.92-.92L17.79 18l.73-.73-2.71-2.68c.9-1.03 1.6-2.25 1.92-3.51H19v-1.04h-3.64V9h-1.04v1.04h-1.96L11.18 6H20c.55 0 1 .45 1 1v13z" />
                                            </svg>

                                            <div class="employenmentsec">
                                                <h6>Language</h6>
                                                <p>{this.props.vendorCandidateDetailsFields.Language.Language ? this.props.vendorCandidateDetailsFields.Language.Language : "N/A"}</p>
                                            </div>
                                        </div>

                                        <span id="dots"></span>
                                        <span id="more">
                                            <div class="collapse" id="vendorCollapse">
                                                <div class="Conditions disF">
                                                    <svg xmlns="http://www.w3.org/2000/svg" enable-background="new 0 0 24 24" height="22px" viewBox="0 0 24 24" width="22px" fill="#FA852B"><g>
                                                        <path d="M0,0h24v24H0V0z" fill="none" /></g><g><path d="M12,2c-4.2,0-8,3.22-8,8.2c0,3.32,2.67,7.25,8,11.8c5.33-4.55,8-8.48,8-11.8C20,5.22,16.2,2,12,2z M12,12c-1.1,0-2-0.9-2-2 c0-1.1,0.9-2,2-2c1.1,0,2,0.9,2,2C14,11.1,13.1,12,12,12z" /></g>
                                                    </svg>

                                                    <div class="employenmentsec">
                                                        <h6>Current Location</h6>
                                                        <p>{this.props.vendorCandidateDetailsFields.CurrentLocation.CurrentLocation ? this.props.vendorCandidateDetailsFields.CurrentLocation.CurrentLocation : 'N/A'}</p>
                                                    </div>
                                                </div>


                                                <div class="Conditions disF">
                                                    <svg xmlns="http://www.w3.org/2000/svg" height="24px" viewBox="0 0 24 24" width="24px" fill="#FA852B"><path d="M0 0h24v24H0z" fill="none" />
                                                        <path d="M11.99 2c-5.52 0-10 4.48-10 10s4.48 10 10 10 10-4.48 10-10-4.48-10-10-10zm3.61 6.34c1.07 0 1.93.86 1.93 1.93 0 1.07-.86 1.93-1.93 1.93-1.07 0-1.93-.86-1.93-1.93-.01-1.07.86-1.93 1.93-1.93zm-6-1.58c1.3 0 2.36 1.06 2.36 2.36 0 1.3-1.06 2.36-2.36 2.36s-2.36-1.06-2.36-2.36c0-1.31 1.05-2.36 2.36-2.36zm0 9.13v3.75c-2.4-.75-4.3-2.6-5.14-4.96 1.05-1.12 3.67-1.69 5.14-1.69.53 0 1.2.08 1.9.22-1.64.87-1.9 2.02-1.9 2.68zM11.99 20c-.27 0-.53-.01-.79-.04v-4.07c0-1.42 2.94-2.13 4.4-2.13 1.07 0 2.92.39 3.84 1.15-1.17 2.97-4.06 5.09-7.45 5.09z" />
                                                    </svg>

                                                    <div class="employenmentsec">
                                                        <h6>Recommendation</h6>
                                                        <p>{this.props.recommendationFields.length > 0 ? this.props.recommendationFields.map((val, index) => { return val.Name.Name + (index !== this.props.recommendationFields.length - 1 ? ", " : "") }) : "N/A"}</p>
                                                    </div>
                                                </div>

                                                <div class="Conditions disF">
                                                    <svg xmlns="http://www.w3.org/2000/svg" enable-background="new 0 0 24 24" height="24px" viewBox="0 0 24 24" width="24px" fill="#FA852B"><g>
                                                        <rect fill="none" height="24" width="24" /></g><g><g /><path d="M12,3c-0.46,0-0.93,0.04-1.4,0.14C7.84,3.67,5.64,5.9,5.12,8.66c-0.48,2.61,0.48,5.01,2.22,6.56C7.77,15.6,8,16.13,8,16.69 V19c0,1.1,0.9,2,2,2h0.28c0.35,0.6,0.98,1,1.72,1s1.38-0.4,1.72-1H14c1.1,0,2-0.9,2-2v-2.31c0-0.55,0.22-1.09,0.64-1.46 C18.09,13.95,19,12.08,19,10C19,6.13,15.87,3,12,3z M14,19h-4v-1h4V19z M14,17h-4v-1h4V17z M12.5,11.41V14h-1v-2.59L9.67,9.59 l0.71-0.71L12,10.5l1.62-1.62l0.71,0.71L12.5,11.41z" /></g>
                                                    </svg>

                                                    <div class="employenmentsec">
                                                        <h6>Skills</h6>
                                                        <p>{this.props.skills.length > 0 ? this.props.skills.map((skill, index) => { return skill.skillName + (index !== this.props.skills.length - 1 ? ", " : "") }) : "N/A"}</p>
                                                    </div>
                                                </div>

                                                <div class="Conditions disF">
                                                    <svg xmlns="http://www.w3.org/2000/svg" height="24px" viewBox="0 0 24 24" width="24px" fill="#FA852B">
                                                        <path d="M0 0h24v24H0z" fill="none" /><path d="M20 19.59V8l-6-6H6c-1.1 0-1.99.9-1.99 2L4 20c0 1.1.89 2 1.99 2H18c.45 0 .85-.15 1.19-.4l-4.43-4.43c-.8.52-1.74.83-2.76.83-2.76 0-5-2.24-5-5s2.24-5 5-5 5 2.24 5 5c0 1.02-.31 1.96-.83 2.75L20 19.59zM9 13c0 1.66 1.34 3 3 3s3-1.34 3-3-1.34-3-3-3-3 1.34-3 3z" />
                                                    </svg>

                                                    <div class="employenmentsec">
                                                        <h6>Job Search Status</h6>
                                                        <p>{this.props.vendorCandidateDetailsFields.JobStatus.JobStatus ? this.props.vendorCandidateDetailsFields.JobStatus.JobStatus : "N/A"}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </span>
                                    </div>



                                    <div className="expand Toggle">
                                        <a className="trigger-more collapsed" role="button" data-toggle="collapse" href="#vendorCollapse" aria-expanded="false" aria-controls="vendorCollapse">See More</a>
                                    </div>


                                </div>

                            </div>
                            <div className="modal fade" id="enterDetails" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">

                                <div className="modal-dialog modal-dialog-centered multistepModal CreateProfileModal" role="document">

                                    <Modal />

                                </div>

                            </div>
                            <div className="modal fade" id="defaultDetails" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">

                                <div className="modal-dialog modal-dialog-centered multistepModal CreateProfileModal" role="document">

                                    <Modal />

                                </div>

                            </div>


                        </div>

                    </div>

                </section>
            </div>
        )

    }



}

const mapStateToProps = state => {
    return {
        vendor: state.vendorBasicDetails,
        recommendationFields: state.recommendations.recommendations,
        skills: state.skills.skills,
        defaultDetailsShowHide: state.vendorDefaultDetails,
        vendorBasicDetailsFields: state.vendorBasicDetails.fields,
        vendorCandidateDetailsFields: state.vendorCandidateDetails.fields,
        myCandidatesFields: state.vendorCandidates.vendorCandidates,
    }
}

const mapDispatchToProps = dispatch => {
    return {

        changeModal: (modal) => dispatch({ type: "CHANGE_MODAL", modal: modal }),
        mapDatabaseToLocal: (name, res) => dispatch({ type: "MAP_DATABASE_TO_LOCAl", name: name, res: res, }),
        DefaultDetails: () => dispatch({ type: "VENDOR_DEFAULT_DETAILS" }),

    }
}

export default connect(mapStateToProps, mapDispatchToProps)(vendorProfile);