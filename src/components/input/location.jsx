import React from "react";

import Input from "./input";

class Location extends React.Component{
    render(){
        return (
            <Input label={this.props.label} divClass={this.props.divClass}
            config = {this.props.config}
            value = {this.props.value}
            invalid = {this.props.invalid}
            change = {this.props.change}
            elementType="select" name="Location"
            options = {["Select", "Hyderabad", "Mumbai", "Delhi", 
            "Noida", "Others"] }
            />
        )
    }
}

export default Location;