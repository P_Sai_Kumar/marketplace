import React from 'react';
import {withRouter, Route,BrowserRouter,Switch, Redirect} from 'react-router-dom';

import CreateProfile from './createProfile/createProfile';
import AdditionalDetails from './createProfile/additionalDetails/additionalDetails';
import BasicDetails from './createProfile/basicDetails';
import VendorBasicDetails from './createProfile/vendorCreateProfile/basicDetails'
import VendorCandidateDetails from './createProfile/vendorCreateProfile/candidateDetails'
import EmploymentQues from './createProfile/employmentQues';
import { Home } from './home/home';
import ViewProfile from './viewProfile/viewProfile';
import Search from './searchCandidate/search';
import viewCandidateProfile from './searchCandidate/viewCandidateProfile';
import axios from 'axios'
import jobFilter from './searchJob/jobSearch';
import jobDetails from './searchJob/viewJobDetails';
import SavedJobs from './savedJobs/savedJobs';
import AppliedJobs from './AppliedJobs/appliedJobs';
import VendorProfile from './vendorProfile/vendorProfile';
import EmployerBasicDetails from './createProfile/employerCreateProfile/basicDetails';
import EmployerViewProfile from './viewProfile/employerViewProfile';
// import XmlHttpRequest from 'xmlhttprequest/lib/XMLHttpRequest'

class Page extends React.Component{
    constructor(props)
    {
        super(props);

    }

     
    componentWillMount() {
        if (window.opener && window.opener !== window) {
            const code = this.getCodeFromWindowURL(window.location.href);
            window.opener.postMessage({'type': 'code', 'code': code}, '*')
            window.close();
          }
        //window.addEventListener('message', this.handlePostMessage);
      }
      // handlePostMessage = event => {
      //   if (event.data.type === 'code') {
      //     const { code } = event.data;
      //     console.log(code)
      //     axios.get(`api/linkedin/${code}`)
      //     .then((response)=>{
      //       console.log(response)
      //       response.data[1].localizedFirstName
      //       response.data[1].localizedLastName
      //       response.data[1].profilePicture.displayImage
      //               localStorage.setItem("email",response.data[0].elements[0]["handle~"].emailAddress);
      //               const data = {         
      //                   "email": response.data[0].elements[0]["handle~"].emailAddress,
      //                   "login": response.data[0].elements[0]["handle~"].emailAddress,
      //                   "activated": true,
      //                 }
      //                 axios.post("api/register/thirdparty", data, 
      //                 {headers : {'Content-Type': 'application/json'}}).then(async(res) => {
      //                     console.log(res)
      //                     await axios.post("api/authenticate", {"username" : response.data[0].elements[0]["handle~"].emailAddress, "password": "1234"}).then(
      //                       (res) => {console.log(res) ; localStorage.setItem("token", res.data.id_token); console.log(localStorage.getItem("token"))}
      //                   ).catch(err => console.log(err))
      //                   localStorage.setItem("user",JSON.stringify(res.data.user))
      //                   localStorage.setItem("userID", res.data.user.id)
      //                   axios.get("api/workers/get/" + localStorage.getItem('userID'),{headers : {'Content-Type': 'application/json','Authorization':'Bearer '+localStorage.getItem("token")}}).then(
      //                     (res1) => {
      //                         console.log(res1) ;
      //                         localStorage.setItem("WorkerID", res1.data.id);
      //                         this.props.history.push('/viewProfile')
      //                       })
      //                 .catch(err => this.props.history.push('/createProfile/basicDetails'))
      //           })
      //     })
      //   }
      // };
    
      getCodeFromWindowURL = url => {
        const popupWindowURL = new URL(url);
        return popupWindowURL.searchParams.get("code");
      };
    
    render(){
    return (
        <BrowserRouter >
            <Switch>
                <Route path="/vendor/createProfile/basicDetails" render={(props) => 
                    ((localStorage.getItem("token") !== null) ? <VendorBasicDetails {...props}/>: <Redirect to="/" />)}/>
                <Route path="/vendor/createProfile/candidateDetails" render={(props) => 
                    ((localStorage.getItem("token") !== null) ? <VendorCandidateDetails {...props}/>: <Redirect to="/" />)}/>
                <Route path="/vendor/viewProfile" render={(props)=>
                ((localStorage.getItem("token") !== null)?<VendorProfile {...props}/>: <Redirect to="/"/> )}/>

                <Route path="/employer/createProfile/basicDetails" render={(props) => 
                ((localStorage.getItem("token") !== null) ? <EmployerBasicDetails {...props}/>: <Redirect to="/" />)}/>   
                <Route path="/employer/viewProfile" render={(props) => 
                ((localStorage.getItem("token") !== null) ? <EmployerViewProfile {...props}/>:<Redirect to="/" />)}/>

                <Route path="/createProfile/basicDetails" render={(props) => 
                    ((localStorage.getItem("token") !== null) ? <BasicDetails {...props}/>: <Redirect to="/" />)}/>               
                <Route path="/createProfile/additionalDetails" render={(props) => 
                    ((localStorage.getItem("token") !== null) ? <AdditionalDetails {...props}/>: <Redirect to="/" />)}/>
                 <Route path="/createProfile/employmentDetails" render={(props) => 
                    ((localStorage.getItem("token") !== null) ? <EmploymentQues {...props}/>: <Redirect to="/" />)}/>
                <Route path="/viewProfile" render={(props) =>
                    ((localStorage.getItem("token") !== null) ? <ViewProfile {...props}/>: <Redirect to="/" />)}/>
                <Route path="/" exact component={Home} />
                {/* <Route path="/createProfile/basicDetails" component={BasicDetails}/>
                <Route path="/createProfile/additionalDetails" component={AdditionalDetails}/>
                <Route path="/createProfile/employmentDetails" component={EmploymentQues}/>
                <Route path="/viewProfile" component={ViewProfile}/> */}
                <Route path="/searchCandidate" component={Search}/>
                {/* <Route path="/recommendedCandidates" component={search}/> */}
                <Route path="/recommendedCandidates" render={(props) =>
                    ((localStorage.getItem("token") !== null) ? <Search {...props} key="1"/>: <Redirect to="/" />)}/>
                <Route path="/candidateDetails/:id" component={withRouter(viewCandidateProfile)} />
                <Route path="/searchJob" component={jobFilter}></Route>
                <Route path="/recommendedJobs" component={jobFilter}></Route>
                <Route path="/jobDetails/:id" component={jobDetails} />
                <Route path="/savedJobs" render={(props) =>
                    ((localStorage.getItem("token") !== null) ? <SavedJobs {...props}/>: <Redirect to="/" />)}/>
                 
                
                <Route path="/appliedJobs" render={(props) =>
                    ((localStorage.getItem("token") !== null) ? <AppliedJobs {...props}/>: <Redirect to="/" />)}/>

            </Switch>
        </BrowserRouter>
    )
    }
}


export default withRouter(Page);