import React from 'react';
import {connect} from 'react-redux'
import axios from 'axios';
import $ from 'jquery';

import './viewProfile.css';
import Header from '../header/Header';
import userPhotoDefault from "../../imgaes/userPhotoDefault.png";
import UploadImage from '../uploadImage/uploadImage';
import EmployerCandidates from './employerCandidates';
import Modal from '../createProfile/additionalDetails/modal';

class EmployerViewProfile extends React.Component
{
    // employerCandidates = null
    componentWillMount = async () => {
        try{
            $('.modal-backdrop').hide();
            // $('#signUp').click();
            // window.location.reload(false);
          } catch(err){
  
          }

        await axios.get('api/recruiters/'+localStorage.getItem('EmployerID'))
        .then((res)=>{
            this.props.mapDatabaseToLocal("employerBasicDetails",res.data)
        })
        await axios.get("api/workers/recruiter").then(async (res)=> {
            // this.employerCandidates = res.data
            console.log(res)
            await this.props.setEmployerCandidatesResponse(res.data)
            
        })
        
    }
    render()
    {
        console.log(this.props.employerCandidatesResponse)
        // if(!this.props.employerCandidatesResponse){
        //     this.forceUpdate()
        // }
        return(<div>
            <Header/>
            <section class="mainbgColor view-profile-section">
                    <div class="container-fluid">
                        <div class="row mobMar0">
                            <div class="col-md-12 mobPadd0">
                                <div class="ProfileDetail">
                                    <div class="ProfileBasicDetail">
                                    <UploadImage type='cover_pic' file={this.props.fields.coverPhoto} employer={true}/>
                                        <div class="ViewProfileDetail marT-30">
                                            <div class="ProfilePicMain">
                                            {this.props.fields.profilePic ? <img src={this.props.fields.profilePic} class="profileImg" /> : <img src={userPhotoDefault} class="profileImg"/>}
                                            </div>
                                            <div class="profileActionButtons">

                                                {/*  <div class="status online"><span class="onlinedot"></span>Active</div> */}

                                                {/* <button type="button" class="btn commonOutlineBtn EditViewedProfile">
                                                    <svg xmlns="http://www.w3.org/2000/svg" height="13px" viewBox="0 0 24 24" width="13px" fill="#007BFF">
                                                        <path d="M0 0h24v24H0z" fill="none" /><path d="M3 17.25V21h3.75L17.81 9.94l-3.75-3.75L3 17.25zM20.71 7.04c.39-.39.39-1.02 0-1.41l-2.34-2.34c-.39-.39-1.02-.39-1.41 0l-1.83 1.83 3.75 3.75 1.83-1.83z" />
                                                    </svg>
                                                    Edit Profile
                                                </button> */}
                                                <button type="button" className="commonOutlineBtn EditViewedProfile" data-toggle="modal" data-target="#enterDetails" onClick={()=>{this.props.changeModal("editEmployerBasicDetails")}}>
                                                    <svg xmlns="http://www.w3.org/2000/svg" height="13px" viewBox="0 0 24 24" width="13px" fill="#007BFF">
                                                        <path d="M0 0h24v24H0z" fill="none"/><path d="M3 17.25V21h3.75L17.81 9.94l-3.75-3.75L3 17.25zM20.71 7.04c.39-.39.39-1.02 0-1.41l-2.34-2.34c-.39-.39-1.02-.39-1.41 0l-1.83 1.83 3.75 3.75 1.83-1.83z"/>
                                                    </svg>
                                                    Edit Profile</button>
                                            </div>

                                            <h2 class="FontBold">{this.props.fields.Name.Name}</h2>

                                            <div class="ProfileBasic">
                                                <ul class="ProfileBasicInfo">
                                                    <li>
                                                        <span>
                                                            <svg xmlns="http://www.w3.org/2000/svg" height="15px" viewBox="0 0 24 24" width="15px" fill="#FA852B">
                                                                <path d="M0 0h24v24H0z" fill="none" /><path d="M20.01 15.38c-1.23 0-2.42-.2-3.53-.56-.35-.12-.74-.03-1.01.24l-1.57 1.97c-2.83-1.35-5.48-3.9-6.89-6.83l1.95-1.66c.27-.28.35-.67.24-1.02-.37-1.11-.56-2.3-.56-3.53 0-.54-.45-.99-.99-.99H4.19C3.65 3 3 3.24 3 3.99 3 13.28 10.73 21 20.01 21c.71 0 .99-.63.99-1.18v-3.45c0-.54-.45-.99-.99-.99z" />
                                                            </svg>
                                                            {this.props.fields.PhoneNumber.PhoneNumber}
                                                        </span>
                                                    </li>

                                                    <li>
                                                        <span>
                                                            <svg xmlns="http://www.w3.org/2000/svg" height="15px" viewBox="0 0 24 24" width="15px" fill="#FA852B">
                                                                <path d="M0 0h24v24H0z" fill="none" /><path d="M20 4H4c-1.1 0-1.99.9-1.99 2L2 18c0 1.1.9 2 2 2h16c1.1 0 2-.9 2-2V6c0-1.1-.9-2-2-2zm0 4l-8 5-8-5V6l8 5 8-5v2z" />
                                                            </svg>
                                                            {this.props.fields.Email.Email}
                                                        </span>
                                                    </li>
                                                </ul>

                                                <ul class="ProfileBasicInfo">
                                                    <li>
                                                        <span>
                                                            <svg xmlns="http://www.w3.org/2000/svg" height="16px" viewBox="0 0 24 24" width="16px" fill="#FA852B">
                                                                <path d="M0 0h24v24H0V0z" fill="none" /><path d="M12 12c2.21 0 4-1.79 4-4s-1.79-4-4-4-4 1.79-4 4 1.79 4 4 4zm0 2c-2.67 0-8 1.34-8 4v1c0 .55.45 1 1 1h14c.55 0 1-.45 1-1v-1c0-2.66-5.33-4-8-4z" />
                                                            </svg>
                                                            {this.props.fields.EmployerName.EmployerName}
                                                        </span>
                                                    </li>
                                                    <li>
                                                        <span>
                                                            <svg xmlns="http://www.w3.org/2000/svg" enable-background="new 0 0 24 24" height="15px" viewBox="0 0 24 24" width="15px" fill="#FA852B"><g><rect fill="none" height="24" width="24" y="0" /></g>
                                                                <g><path d="M20,7h-5V4c0-1.1-0.9-2-2-2h-2C9.9,2,9,2.9,9,4v3H4C2.9,7,2,7.9,2,9v11c0,1.1,0.9,2,2,2h16c1.1,0,2-0.9,2-2V9 C22,7.9,21.1,7,20,7z M9,12c0.83,0,1.5,0.67,1.5,1.5c0,0.83-0.67,1.5-1.5,1.5s-1.5-0.67-1.5-1.5C7.5,12.67,8.17,12,9,12z M12,18H6 v-0.43c0-0.6,0.36-1.15,0.92-1.39C7.56,15.9,8.26,15.75,9,15.75s1.44,0.15,2.08,0.43c0.55,0.24,0.92,0.78,0.92,1.39V18z M13,9h-2V4 h2V9z M17.25,16.5h-2.5c-0.41,0-0.75-0.34-0.75-0.75v0c0-0.41,0.34-0.75,0.75-0.75h2.5c0.41,0,0.75,0.34,0.75,0.75v0 C18,16.16,17.66,16.5,17.25,16.5z M17.25,13.5h-2.5c-0.41,0-0.75-0.34-0.75-0.75v0c0-0.41,0.34-0.75,0.75-0.75h2.5 c0.41,0,0.75,0.34,0.75,0.75v0C18,13.16,17.66,13.5,17.25,13.5z" /></g>
                                                            </svg>
                                                            {this.props.fields.EmployeeID.EmployeeID}
                                                        </span>
                                                    </li>
                                                    <li>
                                                        <span>
                                                            <svg xmlns="http://www.w3.org/2000/svg" height="15px" viewBox="0 0 24 24" width="15px" fill="#FA852B">
                                                                <path d="M0 0h24v24H0V0z" fill="none" /><path d="M11.99 2C6.47 2 2 6.48 2 12s4.47 10 9.99 10C17.52 22 22 17.52 22 12S17.52 2 11.99 2zm6.93 6h-2.95c-.32-1.25-.78-2.45-1.38-3.56 1.84.63 3.37 1.91 4.33 3.56zM12 4.04c.83 1.2 1.48 2.53 1.91 3.96h-3.82c.43-1.43 1.08-2.76 1.91-3.96zM4.26 14C4.1 13.36 4 12.69 4 12s.1-1.36.26-2h3.38c-.08.66-.14 1.32-.14 2s.06 1.34.14 2H4.26zm.82 2h2.95c.32 1.25.78 2.45 1.38 3.56-1.84-.63-3.37-1.9-4.33-3.56zm2.95-8H5.08c.96-1.66 2.49-2.93 4.33-3.56C8.81 5.55 8.35 6.75 8.03 8zM12 19.96c-.83-1.2-1.48-2.53-1.91-3.96h3.82c-.43 1.43-1.08 2.76-1.91 3.96zM14.34 14H9.66c-.09-.66-.16-1.32-.16-2s.07-1.35.16-2h4.68c.09.65.16 1.32.16 2s-.07 1.34-.16 2zm.25 5.56c.6-1.11 1.06-2.31 1.38-3.56h2.95c-.96 1.65-2.49 2.93-4.33 3.56zM16.36 14c.08-.66.14-1.32.14-2s-.06-1.34-.14-2h3.38c.16.64.26 1.31.26 2s-.1 1.36-.26 2h-3.38z" />
                                                            </svg>
                                                            {this.props.fields.EmployerWebsite.EmployerWebsite}
                                                        </span>
                                                    </li>
                                                </ul>

                                            </div>

                                        </div>
                                    </div>

                                    <div class="ProfileBasicDetail">
                                        <div class="vendorHead Candidate-profile-card ViewProfileDetail disF">
                                            <h2 class="CandidateProfileHead">My Candidates</h2>
                                            <div class="rightIcons">
                                                <a href="#">
                                                    <svg xmlns="http://www.w3.org/2000/svg" height="18px" viewBox="0 0 24 24" width="18px" fill="#000000">
                                                        <path d="M0 0h24v24H0z" fill="none" /><path d="M15.5 14h-.79l-.28-.27C15.41 12.59 16 11.11 16 9.5 16 5.91 13.09 3 9.5 3S3 5.91 3 9.5 5.91 16 9.5 16c1.61 0 3.09-.59 4.23-1.57l.27.28v.79l5 4.99L20.49 19l-4.99-5zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5 14 7.01 14 9.5 11.99 14 9.5 14z" />
                                                    </svg>
                                                </a>
                                                <a href="#">
                                                    <svg xmlns="http://www.w3.org/2000/svg" height="18px" viewBox="0 0 24 24" width="18px" fill="#000000">
                                                        <path d="M0 0h24v24H0V0z" fill="none" /><path d="M11 18h2c.55 0 1-.45 1-1s-.45-1-1-1h-2c-.55 0-1 .45-1 1s.45 1 1 1zM3 7c0 .55.45 1 1 1h16c.55 0 1-.45 1-1s-.45-1-1-1H4c-.55 0-1 .45-1 1zm4 6h10c.55 0 1-.45 1-1s-.45-1-1-1H7c-.55 0-1 .45-1 1s.45 1 1 1z" />
                                                    </svg>
                                                </a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="ProfileBasicDetail">
                                        <div class="row">
                                        {this.props.employerCandidatesResponse?this.props.employerCandidatesResponse.map((val,id) => (<EmployerCandidates val = {val} key={id} id={val.id} />)): ""}
                                            {console.log(this.props.employerCandidatesResponse)}


                                            

                                        </div>

                                    </div>





                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <div class="modal fade" id="enterDetails" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered multistepModal CreateProfileModal" role="document">
                            <Modal/>
                    </div>
                </div>
        </div>)
    }
}
const mapStateToProps = state => {
    return {
        fields : state.employerBasicDetails.fields,
        employerCandidatesResponse : state.employerCandidates,
    }
}

const mapDispatchToProps = dispatch => {
    return {
         
        mapDatabaseToLocal : (name,res) => dispatch({type : "MAP_DATABASE_TO_LOCAl", name:name, res: res,}),
        changeState : (name,val)=> dispatch({type:"CHANGE_FIELD",name:name,val:val,data : 'employerBasicDetails'}),
        setEmployerCandidatesResponse : (val) => dispatch({type:"CANDIDATE_RESPONSE", val: val}),
        changeModal : (modal)=> dispatch({type:"CHANGE_MODAL", modal : modal})
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(EmployerViewProfile)