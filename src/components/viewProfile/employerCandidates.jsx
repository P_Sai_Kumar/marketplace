import React from 'react';
import { Link } from "react-router-dom";

import userPhotoDefault from "../../imgaes/userPhotoDefault.png";

class EmployerCandidates extends React.Component{
    render(){
        return(
            <div class="col-md-4 paddR8">
                <div class="Candidate-profile-card ViewProfileDetail VieweditableDetails">
            <Link to={"/candidateDetails/"+this.props.val.id}><>
            <div class="Candidate-profile-pic">
            {this.props.val.photo?<img src={this.props.val.photo.picContentType+","+this.props.val.photo.pic}/>:<img src={userPhotoDefault} alt="Img" />}
            </div>
            <div class="Candidate-profile-details">
                <h3>{this.props.val.firstName} {this.props.val.lastName}</h3>

                <ul class="searchDetails disF">
                    <li>
                        <svg xmlns="http://www.w3.org/2000/svg" height="14px" viewBox="0 0 24 24" width="14px" fill="#FA852B">
                            <path d="M0 0h24v24H0z" fill="none" /><path d="M20 6h-4V4c0-1.11-.89-2-2-2h-4c-1.11 0-2 .89-2 2v2H4c-1.11 0-1.99.89-1.99 2L2 19c0 1.11.89 2 2 2h16c1.11 0 2-.89 2-2V8c0-1.11-.89-2-2-2zm-6 0h-4V4h4v2z" />
                        </svg>
                        <span>{(this.props.val.employments&&this.props.val.employments.jobTitle) ? this.props.val.employments.jobTitle : "N/A"}</span>
                    </li>
                    <li>
                        <svg xmlns="http://www.w3.org/2000/svg" enable-background="new 0 0 24 24" height="16px" viewBox="0 0 24 24" width="16px" fill="#FA852B"><g>
                            <path d="M0,0h24v24H0V0z" fill="none" /></g><g><path d="M12,2c-4.2,0-8,3.22-8,8.2c0,3.32,2.67,7.25,8,11.8c5.33-4.55,8-8.48,8-11.8C20,5.22,16.2,2,12,2z M12,12c-1.1,0-2-0.9-2-2 c0-1.1,0.9-2,2-2c1.1,0,2,0.9,2,2C14,11.1,13.1,12,12,12z" /></g>
                        </svg>
                        <span>{this.props.val.workerLocation ? this.props.val.workerLocation : "N/A"}</span>
                    </li>
                    {/* <li>
                        <svg xmlns="http://www.w3.org/2000/svg" height="16px" viewBox="0 0 24 24" width="16px" fill="#FA852B">
                            <path d="M0 0h24v24H0z" fill="none" /><path d="M17 3H7c-1.1 0-1.99.9-1.99 2L5 21l7-3 7 3V5c0-1.1-.9-2-2-2z" />
                        </svg>
                        <span>8 Saved Jobs</span>
                    </li>
                    <li>
                        <svg xmlns="http://www.w3.org/2000/svg" height="14px" viewBox="0 0 24 24" width="14px" fill="#FA852B">
                            <path d="M0 0h24v24H0z" fill="none" /><path d="M19 3h-4.18C14.4 1.84 13.3 1 12 1c-1.3 0-2.4.84-2.82 2H5c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zm-7 0c.55 0 1 .45 1 1s-.45 1-1 1-1-.45-1-1 .45-1 1-1zm-2 14l-4-4 1.41-1.41L10 14.17l6.59-6.59L18 9l-8 8z" />
                        </svg>
                        <span>12 Applied Jobs</span>
                    </li> */}
                </ul>

            </div>
            </></Link>
            </div>
            </div>
        )
    }
}

export default EmployerCandidates;