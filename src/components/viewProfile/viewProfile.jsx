import React from 'react';
import {connect} from 'react-redux'
import axios from 'axios';
import $ from 'jquery';
import { withRouter } from 'react-router';

import './viewProfile.css';
import Header from '../header/Header';
import Modal from '../createProfile/additionalDetails/modal';

import certification from '../../imgaes/certification.png'
import userPhotoDefault from "../../imgaes/userPhotoDefault.png";
import Workplace from '../../imgaes/Workplace.png'
import template from '../../imgaes/template_3.jpg'
import PdfIcon from '../../imgaes/icons/pdf_icon.jpg'
import DocIcon from '../../imgaes/icons/doc_icon.png'
import CollapsedCertifications from '../createProfile/additionalDetails/collapsedDetails/collapsedCertifications';
import CollapsedEducation from '../createProfile/additionalDetails/collapsedDetails/collapsedEducation'
import CollapsedWorkDetails from '../createProfile/additionalDetails/collapsedDetails/collapsedWorkDetails'
import CollapsedSkills from '../createProfile/additionalDetails/collapsedDetails/collapsedSkills';
import AudioRecorder from '../recorder/audioRecorder';
import UploadImage from '../uploadImage/uploadImage';
import AudiorecorderV2 from '../recorder/audiorecorderV2';

class ViewProfile extends React.Component
{

    SubCategoryID = null
    SubCategory = null
    Category=null
    audioURL = null
    audio = null
    WorkerID=localStorage.getItem("VendorID")?(sessionStorage.getItem("WorkerID")?sessionStorage.getItem("WorkerID"):localStorage.getItem("VendorCandidateID")):localStorage.getItem("WorkerID")
    headers = {
        'Content-Type': 'application/json',
      }
      handleKeyPress = async (event) => {
        // event.preventDefault()
        if (event.key === 'Enter') {

            var data = null

            const headers = {
                'Content-Type': 'application/json',
              }
                data = {
                    skillName: event.target.value,
                    worker : {
                        id: this.WorkerID,
                    }
                }            
                

                await axios.post('api/skills-masters', data, {headers : headers})
                .then(async(response) => 
                {console.log(response); await axios.patch("api/worker/skills/"+this.WorkerID,
                 data=response.data, {headers : {"Content-Type" : "application/merge-patch+json"}})}).catch((e) => console.log(e))

                axios.get('api/skills-masters/worker/'+this.WorkerID).then((res) => 
                {
                    console.log(res)
                    this.props.mapDatabaseToLocal('skills',res.data)
                })
          }
    }

     
    componentWillMount = async () => {
      
        try{
          $('.modal-backdrop').hide();
          // $('#signUp').click();
          // window.location.reload(false);

        } catch(err){

        }
        
        if(!sessionStorage.getItem("WorkerID"))
        {
          sessionStorage.setItem("WorkerID",localStorage.getItem("VendorCandidateID"))
        }
        // await axios.get('api/photos/'+this.WorkerID)
        //         .then((res)=>{
        //             console.log(res.data.pic)
        //             //this.props.setProfilePic(JSON.parse(res.data.name))
        //             this.props.changeState('profilePic',res.data.picContentType+', '+res.data.pic)
        //         })
        await axios.get('api/employments/worker/'+this.WorkerID, {headers : this.headers}).then((res) => 
        {
            console.log(res.data)
            this.props.mapDatabaseToLocal("workDetails",res.data)
        })
        await axios.get('api/educations/worker/'+this.WorkerID, {headers : this.headers}).then((res) => 
        {
            this.props.mapDatabaseToLocal("educationalDetails",res.data)
        })
        await axios.get('api/certificates/worker/'+this.WorkerID, {headers : this.headers}).then((res) => 
        {
            this.props.mapDatabaseToLocal('certifications',res.data)
        })
        await axios.get('api/skills-masters/worker/'+this.WorkerID, {headers : this.headers}).then((res) => 
        {
            this.props.mapDatabaseToLocal("skills",res.data)
        })
        await axios.get('api/workers/'+this.WorkerID, {headers : this.headers}).then(async (res) => 
        {
          console.log(res)
          await this.props.mapDatabaseToLocal("fields",res.data)
          //  axios.get('api/photos/'+)
          //       .then(async(res)=>{
          //           console.log(res.data.pic)
          //           //this.props.setProfilePic(JSON.parse(res.data.name))
          //           this.props.changeState('profilePic',res.data.picContentType+', '+res.data.pic)
          //           let res1 = await fetch(res.data.picContentType+', '+res.data.pic);
          //           let blob = await res1?.blob();
          //           console.log(blob,URL.createObjectURL(blob))
                     
          //       })
                await axios.get('api/files/worker/'+this.WorkerID)
                .then(async(res)=>{
                console.log(res.data);
                if(res.data.length!==0)
                {
                  for(var i=0; i< res.data.length; i++){
                    if(res.data[i].filenameContentType.includes("audio")){
                      // let res1 = await fetch(res.data[i].filenameContentType+', '+res.data[i].audioresume);
                      // let blob = await res1?.blob();
                      localStorage.getItem("VendorID")?sessionStorage.setItem("audioURL",res.data[i].filenameContentType+', '+res.data[i].audioresume):localStorage.setItem("audioURL",res.data[i].filenameContentType+', '+res.data[i].audioresume)
                      this.audio = new Audio(localStorage.getItem("VendorID")?sessionStorage.getItem("audioURL"):localStorage.getItem("audioURL"))
                    } else if(res.data[i].filenameContentType.includes("video")){
                      
                    }
                    else if(res.data[i].filenameContentType.includes("image")){
                      localStorage.getItem("VendorID")?sessionStorage.setItem('coverPhotoID',res.data[i].id):localStorage.setItem('coverPhotoID',res.data[i].id)
                      let res1 = await fetch(res.data[i].filenameContentType+', '+res.data[i].filename);
                        let blob = await res1?.blob();
                      this.props.changeState('coverPhoto',URL.createObjectURL(blob))
                    }
                     else{
                      localStorage.getItem("VendorID")?sessionStorage.setItem('fileID',res.data[i].id):localStorage.setItem('fileID',res.data[i].id)
                        this.props.changeState('resume',res.data[i].filenameContentType+', '+res.data[i].filename)
                        let res1 = await fetch(res.data[i].filenameContentType+', '+res.data[i].filename);
                        let blob = await res1?.blob();
                        localStorage.getItem("VendorID")?sessionStorage.setItem('resumeURL',URL.createObjectURL(blob)):sessionStorage.setItem('resumeURL',URL.createObjectURL(blob))
                        let resumeContentType
                     if(res.data[i].filenameContentType.includes('pdf'))
                     {
                        resumeContentType='Pdf'
                     }
                     else if(res.data[i].filenameContentType.includes('docs'))
                     {
                        resumeContentType='Docs'
                     }
                     else{
                        resumeContentType='Doc'
                     }
                     this.props.changeState('resumeContentType',resumeContentType)
                    }
                  }
                        
                }
                })
                
        })
        await axios.get('api/workers/profile/'+this.WorkerID, {headers : this.headers}).then(async (res) => 
        {
            console.log(res.data)
            if(res.data.photo) {localStorage.getItem("VendorID")?sessionStorage.setItem('photoID',res.data.photo.id):localStorage.setItem('photoID',res.data.photo.id) 
            this.props.changeState('profilePic',res.data.photo.picContentType+', '+res.data.photo.pic)
              if(res.data.photo.mimetype)
              {
                this.props.changeState('summary',res.data.photo.mimetype)
              }
            }
            localStorage.getItem("VendorID")?sessionStorage.setItem("subCat",JSON.stringify(res.data.category[res.data.category.length-1])):localStorage.setItem("subCat",JSON.stringify(res.data.category[res.data.category.length-1]))
            try{
            if(res.data.category[res.data.category.length-1]){
              this.SubCategoryID = res.data.category[res.data.category.length-1].id
              this.SubCategory = res.data.category[res.data.category.length-1].name
              this.Category=res.data.category[res.data.category.length-1].parent.name
            }
          }catch(err){}
          if (res.data.jobPreference.length !== 0){
            await this.props.mapDatabaseToLocal("employmentDetails",res.data)
        }
        })
        this.props.viewProfileLoader()
    }

    saveSummary=()=>
    {
        if(this.props.basicDetailsFields.summary!=='')
            {
                let photoID=localStorage.getItem("VendorID")?sessionStorage.getItem('photoID'):localStorage.getItem('photoID')
                if(photoID==null)
                {
                    axios.post('api/photos',{worker:{id:Number(this.WorkerID)},mimetype:this.props.basicDetailsFields.summary}) 
                        .then((res)=>{
                            console.log(res)
                            localStorage.getItem("VendorID")?sessionStorage.setItem('photoID',res.data.id):localStorage.setItem('photoID',res.data.id)
                        })
                }
                else{
                    axios.patch('api/photos/'+photoID,{id:Number(photoID),worker:{id:Number(this.WorkerID)},mimetype:this.props.basicDetailsFields.summary},{headers : {'Content-Type': 'application/merge-patch+json'}}) 
                    .then((res)=>{
                        console.log(res)
                    }) 
                }
            }
    }
    togglePlay = async () => {
      await this.props.togglePlay() 
      // var audio = new Audio(localStorage.getItem("audioURL"))
      this.props.play ? this.audio.play() : this.audio.pause();
    }
    
    render()
    {
        // console.log(this.props.basicDetailsFields)
        // if (!this.props.loader){
        //   return null
        // }
        

        $('.trigger-more').click(function() {
            if($(this).hasClass('collapsed')) {
              $(this).text('See Less');
            } else {
              $(this).text('See More');
            }
        });

        return (
            <div>
                <Header/>
                {this.props.loader ? 
                <section class="mainbgColor view-profile-section">
        <div class="container-fluid">
            <div class="row mobMar0">
                <div class="col-md-9 mobPadd0">
                   <div class="ProfileDetail">
                     <div class="ProfileBasicDetail">
                        <UploadImage type='cover_pic' file={this.props.basicDetailsFields.coverPhoto}/>
                        <div class="ViewProfileDetail marT-30">
                          <div class="ProfilePicMain">
                          {this.props.basicDetailsFields.profilePic ? <img src={this.props.basicDetailsFields.profilePic} class="profileImg" /> : <img src={userPhotoDefault} class="profileImg"/>}
                          </div>
                          <div class="profileActionButtons">
                            
                            {this.props.employmentQues.JobStatus.JobStatus==='Actively Applying'?
                            <div class="status online"><span class="onlinedot"></span>Actively Applying</div>
                            :this.props.employmentQues.JobStatus.JobStatus==='Casually Looking'?
                            <div class="status casual"><span class="onlinedot"></span>Casually Looking</div>
                            :<div class="status explore"><span class="onlinedot"></span>Exploring Marketplace</div>}

                            <button type="button" className="commonOutlineBtn EditViewedProfile" data-toggle="modal" data-target="#enterDetails" onClick={()=>{this.props.changeModal("basicDetails")}}>
                              <svg xmlns="http://www.w3.org/2000/svg" height="13px" viewBox="0 0 24 24" width="13px" fill="#007BFF">
                                <path d="M0 0h24v24H0z" fill="none"/><path d="M3 17.25V21h3.75L17.81 9.94l-3.75-3.75L3 17.25zM20.71 7.04c.39-.39.39-1.02 0-1.41l-2.34-2.34c-.39-.39-1.02-.39-1.41 0l-1.83 1.83 3.75 3.75 1.83-1.83z"/>
                              </svg>
                              &nbsp;Edit Profile</button>
                          </div>

                          <h2 class="FontBold">{this.props.basicDetailsFields.Name.Name+" "+this.props.basicDetailsFields.LastName.LastName+" "}
                          {(localStorage.getItem("audioURL")) ? <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg"  x="0px" y="0px" height="20px" width="20px" fill={this.props.play ? "#007bff" : ""} onClick={this.togglePlay} style={{cursor : 'pointer'}}
                            viewBox="0 0 489.8 489.8" >
                              <g>
                                <path d="M355.8,340.1c3,2.1,6.5,3.2,9.9,3.2c5.4,0,10.7-2.5,14-7.2c18.9-26.6,28.9-58.1,28.9-91.1c0-32.9-10-64.4-28.9-91.1
                                  c-5.5-7.7-16.2-9.5-23.9-4.1c-7.7,5.5-9.5,16.2-4.1,23.9c14.8,20.8,22.6,45.4,22.6,71.2c0,25.8-7.8,50.4-22.6,71.2
                                  C346.3,323.9,348.1,334.6,355.8,340.1z"/>
                                <path d="M418.8,397.5c3,2.1,6.5,3.2,9.9,3.2c5.4,0,10.7-2.5,14-7.2c30.8-43.4,47.1-94.8,47.1-148.5s-16.3-105-47.1-148.5
                                  c-5.5-7.7-16.2-9.5-23.9-4.1c-7.7,5.5-9.5,16.2-4.1,23.9c26.7,37.6,40.8,82.1,40.8,128.6s-14.1,91-40.8,128.6
                                  C409.3,381.3,411.1,392,418.8,397.5z"/>
                                <path d="M52.4,140.7C23.5,140.7,0,164.2,0,193.1v103.5C0,325.5,23.5,349,52.4,349h105.7l92.5,66.8c0.5,0.3,1,0.7,1.5,0.9
                                  c5,2.9,10.7,4.4,16.4,4.4l0,0c8.5,0,16.7-3.3,22.9-9.2c6.5-6.2,10.1-14.7,10.1-23.7V101.7c0-18.2-14.8-33-33-33
                                  c-5.7,0-11.4,1.5-16.4,4.4c-0.4,0.2-0.8,0.5-1.2,0.7l-96.7,66.8H52.4V140.7z M169.2,172l97.9-67.6v281.2L173.7,318
                                  c-2.9-2.1-6.4-3.2-10-3.2H52.4c-10,0-18.1-8.1-18.1-18.1V193.2c0-10,8.1-18.1,18.1-18.1h107.1C163,175,166.4,174,169.2,172z"/>
                              </g>

                          </svg> : ""}
                          </h2> 

                          <div class="ProfileBasic">
                            <ul class="ProfileBasicInfo">
                              <li>
                                <span>
                                  <svg xmlns="http://www.w3.org/2000/svg" height="15px" viewBox="0 0 24 24" width="15px" fill="#FA852B">
                                    <path d="M0 0h24v24H0z" fill="none"/><path d="M20 6h-4V4c0-1.11-.89-2-2-2h-4c-1.11 0-2 .89-2 2v2H4c-1.11 0-1.99.89-1.99 2L2 19c0 1.11.89 2 2 2h16c1.11 0 2-.89 2-2V8c0-1.11-.89-2-2-2zm-6 0h-4V4h4v2z"/>
                                  </svg>
                                  {this.Category}/{this.SubCategory}
                                </span>
                              </li>
                              <li>
                                <span>
                                  <svg xmlns="http://www.w3.org/2000/svg" enable-background="new 0 0 24 24" height="15px" viewBox="0 0 24 24" width="15px" fill="#FA852B"><g>
                                    <path d="M0,0h24v24H0V0z" fill="none"/></g><g><path d="M12,2c-4.2,0-8,3.22-8,8.2c0,3.32,2.67,7.25,8,11.8c5.33-4.55,8-8.48,8-11.8C20,5.22,16.2,2,12,2z M12,12c-1.1,0-2-0.9-2-2 c0-1.1,0.9-2,2-2c1.1,0,2,0.9,2,2C14,11.1,13.1,12,12,12z"/></g>
                                  </svg>
                                  {this.props.basicDetailsFields.CurrentLocation.CurrentLocation}
                                </span>
                              </li>
                            </ul>
                            <ul class="ProfileBasicInfo">
                              <li>
                                <span>
                                  <svg xmlns="http://www.w3.org/2000/svg" height="15px" viewBox="0 0 24 24" width="15px" fill="#FA852B">
                                    <path d="M0 0h24v24H0z" fill="none"/><path d="M20.01 15.38c-1.23 0-2.42-.2-3.53-.56-.35-.12-.74-.03-1.01.24l-1.57 1.97c-2.83-1.35-5.48-3.9-6.89-6.83l1.95-1.66c.27-.28.35-.67.24-1.02-.37-1.11-.56-2.3-.56-3.53 0-.54-.45-.99-.99-.99H4.19C3.65 3 3 3.24 3 3.99 3 13.28 10.73 21 20.01 21c.71 0 .99-.63.99-1.18v-3.45c0-.54-.45-.99-.99-.99z"/>
                                  </svg>
                                  +91 {this.props.basicDetailsFields.PhoneNumber.PhoneNumber}
                                </span>
                              </li>
                            
                              <li>
                                <span>
                                  <svg xmlns="http://www.w3.org/2000/svg" height="15px" viewBox="0 0 24 24" width="15px" fill="#FA852B">
                                    <path d="M0 0h24v24H0z" fill="none"/><path d="M20 4H4c-1.1 0-1.99.9-1.99 2L2 18c0 1.1.9 2 2 2h16c1.1 0 2-.9 2-2V6c0-1.1-.9-2-2-2zm0 4l-8 5-8-5V6l8 5 8-5v2z"/>
                                  </svg>
                                   {this.props.basicDetailsFields.Email.Email}
                                </span>
                              </li>
                            </ul>
                          </div>
                        </div>
                      </div>

                      <div class="ProfileBasicDetail">
                        <div class="ViewProfileDetail Summary">
                        <div class="EmploynmentConditionHead">
                          <h2>Summary</h2>
                          <div class="editEmploynmentCondition" onClick={() => {this.props.toggleSummary()}}>
                          <svg xmlns="http://www.w3.org/2000/svg" height="18px" viewBox="0 0 24 24" width="18px" fill="#007BFF">
                                <path d="M0 0h24v24H0z" fill="none"/><path d="M3 17.25V21h3.75L17.81 9.94l-3.75-3.75L3 17.25zM20.71 7.04c.39-.39.39-1.02 0-1.41l-2.34-2.34c-.39-.39-1.02-.39-1.41 0l-1.83 1.83 3.75 3.75 1.83-1.83z"/>
                          </svg>
                          </div>
                          </div>
                          {(this.props.isSummaryEditable)?<>
                            <br/><textarea className='form-control' cols='100' value={this.props.basicDetailsFields.summary} onChange={(e)=>{this.props.changeState('summary',e.target.value)}}></textarea><br/>
                            <div className="btn-group NextFormButtons ModalNextFormButtons d-flex justify-content-center">
                                <button className="common-btn commonBlueBtn" onClick={()=>{this.saveSummary()}}>Save</button>
                            </div>
                          </>:
                          <p>{this.props.basicDetailsFields.summary?this.props.basicDetailsFields.summary:null}</p>}
                        </div>
                      </div>

                      <div class="ProfileBasicDetail">
                        <div class="ViewProfileDetail VieweditableDetails">
                          <div class="ViewProfileDetailHead disF">
                            <h2>Work History</h2>

                            <div class="expand ViewHistory">
                            {this.props.workDetailsFields.length>1 && <a style={{textDecoration:'none'}} role="button" class="collapsed" data-toggle="collapse" href="#ViewAllHistory" aria-expanded="false" aria-controls="ViewAllHistory">View All {'>'}</a>}
                            </div>
                            <a style={{marginLeft:'auto',marginRight:'0px',cursor:'pointer'}} class="addDetails" data-toggle="modal" data-target="#enterDetails" onClick={() => {this.props.resetForm(this.props.modalSelected); this.props.editAction(this.props.modalSelected); this.props.changeModal("workDetails")}}>
                            <svg xmlns="http://www.w3.org/2000/svg" height="15px" viewBox="0 0 24 24" width="15px" fill="#007BFF">
                                <path d="M0 0h24v24H0z" fill="none"/><path d="M19 13h-6v6h-2v-6H5v-2h6V5h2v6h6v2z"/>
                            </svg>
                            </a>
                          </div>
                           {/* <div class="EmploynmentConditionHead">
                            <h2>Work History</h2>
    
                            <div class="editEmploynmentCondition">
                              <svg xmlns="http://www.w3.org/2000/svg" height="18px" viewBox="0 0 24 24" width="18px" fill="#007BFF">
                                <path d="M0 0h24v24H0z" fill="none"/><path d="M3 17.25V21h3.75L17.81 9.94l-3.75-3.75L3 17.25zM20.71 7.04c.39-.39.39-1.02 0-1.41l-2.34-2.34c-.39-.39-1.02-.39-1.41 0l-1.83 1.83 3.75 3.75 1.83-1.83z"/>
                              </svg>
                            </div>
                          </div>  */}
                          {this.props.workDetailsFields.length>0 && <CollapsedWorkDetails val = {this.props.workDetailsFields[0]} id={this.props.workDetailsFields[0].id} modal="workDetails"/>}
                          <div id='ViewAllHistory' className='collapse' aria-expanded="false">
                          {this.props.workDetailsFields.map((val,id) => id!==0&&(<CollapsedWorkDetails val = {val} key={id} id={val.id} modal="workDetails"/>))}
                            {/* {this.props.workDetailsFields.map((val,id)=>( 
                          <div class="addedDetail paddX15 disF">
                            <div class="workIconImg">
                              <img src={Workplace} />
                            </div>

                            <div class="addedDetailContent">
                            <p>{val.Designation.Designation}</p>
                            <span>{val.EmployerName.EmployerName}, Full Time, {val.StartDate.StartDate.split("-")[0]} - {val.CurrentlyStudying.CurrentlyStudying===false?val.EndDate.EndDate.split("-")[0]:'currently working'}</span>
                            </div>

                            <div class="actionBtns">
                             <a href="#" class="editDetails" data-toggle="modal" data-target="#enterDetails" onClick={()=>{this.props.editDetails(val.id.id,"workDetails");this.props.changeModal("workDetails")}}>
                               <svg xmlns="http://www.w3.org/2000/svg" height="18px" viewBox="0 0 24 24" width="18px" fill="#007BFF">
                                 <path d="M0 0h24v24H0z" fill="none"/><path d="M3 17.25V21h3.75L17.81 9.94l-3.75-3.75L3 17.25zM20.71 7.04c.39-.39.39-1.02 0-1.41l-2.34-2.34c-.39-.39-1.02-.39-1.41 0l-1.83 1.83 3.75 3.75 1.83-1.83z"/>
                               </svg>
                             </a>
                           </div>

                          </div>
                            ))} */}
                        </div>
                        </div>
                      </div>



                      <div class="ProfileBasicDetail">
                        <div class="ViewProfileDetail VieweditableDetails">
                        <div class="ViewProfileDetailHead disF">
                          <h2>Educational Details</h2>
                          <div class="expand ViewHistory">
                          {this.props.fields.length>1 &&<a style={{textDecoration:'none'}} role="button" class="collapsed" data-toggle="collapse" href="#ViewAllEducationalDetails" aria-expanded="false" aria-controls="ViewAllEducationalDetails">View All {'>'}</a>}
                            </div>
                            <a style={{marginLeft:'auto',marginRight:'0px',cursor:'pointer'}} class="addDetails" data-toggle="modal" data-target="#enterDetails" onClick={() => {this.props.resetForm(this.props.modalSelected); this.props.editAction(this.props.modalSelected); this.props.changeModal("educationalDetails")}}>
                            <svg xmlns="http://www.w3.org/2000/svg" height="15px" viewBox="0 0 24 24" width="15px" fill="#007BFF">
                                <path d="M0 0h24v24H0z" fill="none"/><path d="M19 13h-6v6h-2v-6H5v-2h6V5h2v6h6v2z"/>
                            </svg>
                            </a>
                            </div>
                          {/* <div class="EmploynmentConditionHead">
                            <h2>Work History</h2>
    
                            <div class="editEmploynmentCondition">
                              <svg xmlns="http://www.w3.org/2000/svg" height="18px" viewBox="0 0 24 24" width="18px" fill="#007BFF">
                                <path d="M0 0h24v24H0z" fill="none"/><path d="M3 17.25V21h3.75L17.81 9.94l-3.75-3.75L3 17.25zM20.71 7.04c.39-.39.39-1.02 0-1.41l-2.34-2.34c-.39-.39-1.02-.39-1.41 0l-1.83 1.83 3.75 3.75 1.83-1.83z"/>
                             </svg>
                            </div>
                          </div>  */}
                          {this.props.fields.length>0 && <CollapsedEducation val = {this.props.fields[0]} id={this.props.fields[0].id} modal="educationalDetails"/>}
                            <div id='ViewAllEducationalDetails' className='collapse' aria-expanded="false">
                            {this.props.fields.map((val,id) => id!==0&&(<CollapsedEducation val = {val} key={id} id={val.id} modal="educationalDetails"/>))}
                            {/* {this.props.fields.map((val,id)=>( 
                          <div class="addedDetail paddX15 disF">
                            <div class="addedDetailImg"></div>

                            <div class="addedDetailContent">
                            <p>{val.Degree.Degree}</p>
                            <span>{val.University.University}, CGPA {val.Grade.Grade}, {val.PassingYear.PassingYear}</span>
                            </div>

                            <div class="actionBtns">
                             <a href="#" class="editDetails" data-toggle="modal" data-target="#enterDetails" onClick={()=>{this.props.editDetails(val.id.id,"educationalDetails");this.props.changeModal("educationalDetails")}}>
                               <svg xmlns="http://www.w3.org/2000/svg" height="18px" viewBox="0 0 24 24" width="18px" fill="#007BFF">
                                 <path d="M0 0h24v24H0z" fill="none"/><path d="M3 17.25V21h3.75L17.81 9.94l-3.75-3.75L3 17.25zM20.71 7.04c.39-.39.39-1.02 0-1.41l-2.34-2.34c-.39-.39-1.02-.39-1.41 0l-1.83 1.83 3.75 3.75 1.83-1.83z"/>
                               </svg>
                             </a>
                           </div>

                          </div>))} */}
                        </div>
                        </div>
                      </div>

                      <div class="ProfileBasicDetail">
                        <div class="ViewProfileDetail VieweditableDetails">
                          <div class="ViewProfileDetailHead disF">
                            <h2>Certifications</h2>

                            <div class="expand ViewHistory">
                            {this.props.certificationFields.length>1 && <a style={{textDecoration:'none'}} role="button" class="collapsed" data-toggle="collapse" href="#ViewAllCertifications" aria-expanded="false" aria-controls="ViewAllCertifications">View All {'>'}</a>}
                            </div>
                            <a style={{marginLeft:'auto',marginRight:'0px',cursor:'pointer'}} class="addDetails" data-toggle="modal" data-target="#enterDetails" onClick={() => {this.props.resetForm(this.props.modalSelected); this.props.editAction(this.props.modalSelected); this.props.changeModal("certifications")}}>
                            <svg xmlns="http://www.w3.org/2000/svg" height="15px" viewBox="0 0 24 24" width="15px" fill="#007BFF">
                                <path d="M0 0h24v24H0z" fill="none"/><path d="M19 13h-6v6h-2v-6H5v-2h6V5h2v6h6v2z"/>
                            </svg>
                            </a>
                          </div>
                          {/* <div class="EmploynmentConditionHead">
                            <h2>Work History</h2>
    
                            <div class="editEmploynmentCondition">
                              <svg xmlns="http://www.w3.org/2000/svg" height="18px" viewBox="0 0 24 24" width="18px" fill="#007BFF">
                                <path d="M0 0h24v24H0z" fill="none"/><path d="M3 17.25V21h3.75L17.81 9.94l-3.75-3.75L3 17.25zM20.71 7.04c.39-.39.39-1.02 0-1.41l-2.34-2.34c-.39-.39-1.02-.39-1.41 0l-1.83 1.83 3.75 3.75 1.83-1.83z"/>
                              </svg>
                            </div>
                          </div> */}
                          {this.props.certificationFields.length>0 && <CollapsedCertifications val = {this.props.certificationFields[0]} id={this.props.certificationFields[0].id} modal="certifications"/>}
                          <div id='ViewAllCertifications' className='collapse' aria-expanded="false">
                          {/* <div id={this.props.href} class="panel-collapse collapse in" role="tabpanel" aria-labelledby={this.props.id}>
                          <div class="panel-body"> */}
                          
                          {this.props.certificationFields.map((val,id) => id!==0&&(<CollapsedCertifications val = {val} key={id} id={val.id} modal="certifications"/>))}

                          {/* </div> */}
                        {/* {this.props.certificationFields.map((val,id) => (
                          <div class="addedDetail paddX15 disF">
                            <div class="workIconImg">
                              <img src={certification} />
                            </div>

                            <div class="addedDetailContent">
                            <p>{val.Name.Name}</p>
                            <span>{val.Issuer.Issuer}</span>
                            </div>

                            <div class="actionBtns">
                             <a href="#" class="editDetails" data-toggle="modal" data-target="#enterDetails" onClick={()=>{this.props.editDetails(val.id.id,"certifications");this.props.changeModal("certifications")}}>
                               <svg xmlns="http://www.w3.org/2000/svg" height="18px" viewBox="0 0 24 24" width="18px" fill="#007BFF">
                                 <path d="M0 0h24v24H0z" fill="none"/><path d="M3 17.25V21h3.75L17.81 9.94l-3.75-3.75L3 17.25zM20.71 7.04c.39-.39.39-1.02 0-1.41l-2.34-2.34c-.39-.39-1.02-.39-1.41 0l-1.83 1.83 3.75 3.75 1.83-1.83z"/>
                               </svg>
                             </a>
                           </div>

                          </div>))} */}
                            </div>
                        </div>
                      </div>


                      <div class="ProfileBasicDetail">
                        <div class="ViewProfileDetail">
                          <div class="EmploynmentConditionHead">
                            <h2>Skills</h2>
    
                            <div class="editEmploynmentCondition" onClick={()=>this.props.toggleSkills()}>
                              <svg xmlns="http://www.w3.org/2000/svg" height="18px" viewBox="0 0 24 24" width="18px" fill="#007BFF">
                                <path d="M0 0h24v24H0z" fill="none"/><path d="M3 17.25V21h3.75L17.81 9.94l-3.75-3.75L3 17.25zM20.71 7.04c.39-.39.39-1.02 0-1.41l-2.34-2.34c-.39-.39-1.02-.39-1.41 0l-1.83 1.83 3.75 3.75 1.83-1.83z"/>
                              </svg>
                            </div>
                          </div>
                          <div class="skills">
                            {(this.props.isSkillsEditable)?<div className='VieweditableDetails'>
                            <div class="AddSkillsTextbox paddX15" id="AddSkills">
                            <input type="text" class="form-control" id="inputAddPortfolio" placeholder={this.props.placeholder} onKeyPress={this.handleKeyPress}/>
                            </div>
                            {this.props.skills.map((item, id) => (<CollapsedSkills item={item} key={id} id={item.id} data='skills'/>))}
                            </div>
                            :this.props.skills.map((val)=>(
                                <span>{val.skillName}</span>
                            ))}
                          </div>
                        
                        </div>
                      </div>

                      <div class="ProfileBasicDetail">
                        <div class="ViewProfileDetail">
                          <div class="EmploynmentConditionHead">
                            <h2>Documents</h2>
    
                            {/* <div class="editEmploynmentCondition">
                              <svg xmlns="http://www.w3.org/2000/svg" height="18px" viewBox="0 0 24 24" width="18px" fill="#007BFF">
                                <path d="M0 0h24v24H0z" fill="none"/><path d="M3 17.25V21h3.75L17.81 9.94l-3.75-3.75L3 17.25zM20.71 7.04c.39-.39.39-1.02 0-1.41l-2.34-2.34c-.39-.39-1.02-.39-1.41 0l-1.83 1.83 3.75 3.75 1.83-1.83z"/>
                              </svg>
                            </div> */}
                          </div>
                         
                          <div class="document">
                          {/* <div class="addedDetail paddX15 disF">
                            <div class="addedDetailImg">
                              <img src={this.props.basicDetailsFields.resumeContentType==='Pdf'?PdfIcon:DocIcon}></img>
                            </div>
                            <div class="addedDetailContent">
                            <a style={{textDecoration:'none'}} href={localStorage.getItem("resumeURL")} target='_blank'><p>My Resume</p></a>
                            <p id='resumeContentType'>{this.props.basicDetailsFields.resumeContentType}</p>   
                            </div>
                            </div> */}
                            <AudiorecorderV2 />
                            <UploadImage type='resume' fileContentType={this.props.basicDetailsFields.resumeContentType} file={localStorage.getItem('VendorID')?sessionStorage.getItem("resumeURL"):localStorage.getItem("resumeURL")}/>
                          </div>
                        
                        </div>
                      </div>


                   </div>
                </div>

                <div class="col-md-3">
                    <div class="EmploynmentCondition">
                      <div class="EmploynmentConditionHead">
                        <h4>Employment Conditions</h4>

                        <div class="editEmploynmentCondition" data-toggle="modal" data-target="#enterDetails" onClick={()=>{this.props.changeModal("employmentDetails")}}>
                          <svg xmlns="http://www.w3.org/2000/svg" height="18px" viewBox="0 0 24 24" width="18px" fill="#007BFF">
                            <path d="M0 0h24v24H0z" fill="none"/><path d="M3 17.25V21h3.75L17.81 9.94l-3.75-3.75L3 17.25zM20.71 7.04c.39-.39.39-1.02 0-1.41l-2.34-2.34c-.39-.39-1.02-.39-1.41 0l-1.83 1.83 3.75 3.75 1.83-1.83z"/>
                          </svg>
                        </div>
                      </div>
                      

                      {/* <div class="editEmploynmentConditionInner" class="collapse" id="ConditionShow" aria-expanded="false"> */}
                      <div class="editEmploynmentConditionInner">
                      <div class="Conditions disF">
                        <svg xmlns="http://www.w3.org/2000/svg" enable-background="new 0 0 24 24" height="24px" viewBox="0 0 24 24" width="24px" fill="#FA852B"><rect fill="none" height="24" width="24"/>
                          <path d="M7.76,16.24C6.67,15.16,6,13.66,6,12s0.67-3.16,1.76-4.24l1.42,1.42C8.45,9.9,8,10.9,8,12c0,1.1,0.45,2.1,1.17,2.83 L7.76,16.24z M16.24,16.24C17.33,15.16,18,13.66,18,12s-0.67-3.16-1.76-4.24l-1.42,1.42C15.55,9.9,16,10.9,16,12 c0,1.1-0.45,2.1-1.17,2.83L16.24,16.24z M12,10c-1.1,0-2,0.9-2,2s0.9,2,2,2s2-0.9,2-2S13.1,10,12,10z M20,12 c0,2.21-0.9,4.21-2.35,5.65l1.42,1.42C20.88,17.26,22,14.76,22,12s-1.12-5.26-2.93-7.07l-1.42,1.42C19.1,7.79,20,9.79,20,12z M6.35,6.35L4.93,4.93C3.12,6.74,2,9.24,2,12s1.12,5.26,2.93,7.07l1.42-1.42C4.9,16.21,4,14.21,4,12S4.9,7.79,6.35,6.35z"/>
                        </svg>

                        <div class="employenmentsec">
                          <h6>Availability</h6>
                          <p>{this.props.employmentQues.AvailableFrom.AvailableFrom} to {this.props.employmentQues.AvailableTill.AvailableTill}</p>
                        </div>
                      </div>

                      <div class="Conditions disF">
                        <svg xmlns="http://www.w3.org/2000/svg" height="21px" viewBox="0 0 24 24" width="21px" fill="#FA852B">
                          <path d="M0 0h24v24H0z" fill="none"/><path d="M20 6h-4V4c0-1.11-.89-2-2-2h-4c-1.11 0-2 .89-2 2v2H4c-1.11 0-1.99.89-1.99 2L2 19c0 1.11.89 2 2 2h16c1.11 0 2-.89 2-2V8c0-1.11-.89-2-2-2zm-6 0h-4V4h4v2z"/>
                        </svg>

                        <div class="employenmentsec">
                          <h6>Work Type</h6>
                          <p>{this.props.employmentQues.WorkType.WorkType}</p>
                        </div>
                      </div>

                      <div class="Conditions disF">
                        <svg xmlns="http://www.w3.org/2000/svg" height="20px" viewBox="0 0 24 24" width="20px" fill="#FA852B">
                          <path d="M0 0h24v24H0z" fill="none"/><path d="M18 16.08c-.76 0-1.44.3-1.96.77L8.91 12.7c.05-.23.09-.46.09-.7s-.04-.47-.09-.7l7.05-4.11c.54.5 1.25.81 2.04.81 1.66 0 3-1.34 3-3s-1.34-3-3-3-3 1.34-3 3c0 .24.04.47.09.7L8.04 9.81C7.5 9.31 6.79 9 6 9c-1.66 0-3 1.34-3 3s1.34 3 3 3c.79 0 1.5-.31 2.04-.81l7.12 4.16c-.05.21-.08.43-.08.65 0 1.61 1.31 2.92 2.92 2.92 1.61 0 2.92-1.31 2.92-2.92s-1.31-2.92-2.92-2.92z"/>
                        </svg>

                        <div class="employenmentsec">
                          <h6>Employment Type</h6>
                          <p>{this.props.employmentQues.EmploymentType.EmploymentType}</p>
                        </div>
                      </div>

                      <div class="Conditions disF">
                        <svg xmlns="http://www.w3.org/2000/svg" height="24px" viewBox="0 0 24 24" width="24px" fill="#FA852B">
                          <path d="M0 0h24v24H0z" fill="none"/><path d="M11.8 10.9c-2.27-.59-3-1.2-3-2.15 0-1.09 1.01-1.85 2.7-1.85 1.78 0 2.44.85 2.5 2.1h2.21c-.07-1.72-1.12-3.3-3.21-3.81V3h-3v2.16c-1.94.42-3.5 1.68-3.5 3.61 0 2.31 1.91 3.46 4.7 4.13 2.5.6 3 1.48 3 2.41 0 .69-.49 1.79-2.7 1.79-2.06 0-2.87-.92-2.98-2.1h-2.2c.12 2.19 1.76 3.42 3.68 3.83V21h3v-2.15c1.95-.37 3.5-1.5 3.5-3.55 0-2.84-2.43-3.81-4.7-4.4z"/>
                        </svg>

                        <div class="employenmentsec">
                          <h6>Pay Rate</h6>
                          <p>{this.props.employmentQues.Rate.Rate} {this.props.employmentQues.Currency.Currency} {this.props.employmentQues.RateType.RateType==='Daily Rate'&&'/ day'}{this.props.employmentQues.RateType.RateType==='Hourly Rate'&&'/ hour'}{this.props.employmentQues.RateType.RateType==='Monthly Rate'&&'/ month'}{this.props.employmentQues.RateType.RateType==='Yearly Rate'&&'/ year'}</p>
                        </div>
                      </div>

                      <div class="Conditions disF">
                        <svg xmlns="http://www.w3.org/2000/svg" enable-background="new 0 0 24 24" height="22px" viewBox="0 0 24 24" width="22px" fill="#FA852B"><g>
                          <path d="M0,0h24v24H0V0z" fill="none"/></g><g><path d="M12,2c-4.2,0-8,3.22-8,8.2c0,3.32,2.67,7.25,8,11.8c5.33-4.55,8-8.48,8-11.8C20,5.22,16.2,2,12,2z M12,12c-1.1,0-2-0.9-2-2 c0-1.1,0.9-2,2-2c1.1,0,2,0.9,2,2C14,11.1,13.1,12,12,12z"/></g>
                        </svg>

                        <div class="employenmentsec">
                          <h6>Work Location</h6>
                          <p>{this.props.employmentQues.WorkLocation.WorkLocation==='Both'?'Home and Office':this.props.employmentQues.WorkLocation.WorkLocation}</p>
                        </div>
                      </div>
                      


                      <div class="collapse" id="collapseExample">
                         <div class="Conditions disF">
                         <svg xmlns="http://www.w3.org/2000/svg" enable-background="new 0 0 24 24" height="22px" viewBox="0 0 24 24" width="22px" fill="#FA852B"><g>
                           <path d="M0,0h24v24H0V0z" fill="none"/></g><g><path d="M12,2c-4.2,0-8,3.22-8,8.2c0,3.32,2.67,7.25,8,11.8c5.33-4.55,8-8.48,8-11.8C20,5.22,16.2,2,12,2z M12,12c-1.1,0-2-0.9-2-2 c0-1.1,0.9-2,2-2c1.1,0,2,0.9,2,2C14,11.1,13.1,12,12,12z"/></g>
                         </svg>
 
                         <div class="employenmentsec">
                           <h6>Location Preference</h6>
                           <p>{this.props.employmentQues.LocationPreference.LocationPreference}</p>
                         </div>
                       </div>
 
                       <div class="Conditions disF">
                         <svg xmlns="http://www.w3.org/2000/svg" enable-background="new 0 0 24 24" height="22px" viewBox="0 0 24 24" width="22px" fill="#FA852B"><g>
                           <path d="M0,0h24v24H0V0z" fill="none"/></g><g><path d="M12,2c-4.2,0-8,3.22-8,8.2c0,3.32,2.67,7.25,8,11.8c5.33-4.55,8-8.48,8-11.8C20,5.22,16.2,2,12,2z M12,12c-1.1,0-2-0.9-2-2 c0-1.1,0.9-2,2-2c1.1,0,2,0.9,2,2C14,11.1,13.1,12,12,12z"/></g>
                         </svg>
 
                         <div class="employenmentsec">
                           <h6>Current Location</h6>
                           <p>{this.props.basicDetailsFields.CurrentLocation.CurrentLocation}</p>
                         </div>
                       </div>
                       </div>



                    </div>
                    <div class="expand Toggle">
                        {/* <button type="button" className="btn btn-sm border pl-3" onClick={this.props.employeeConditions}>
                        {!this.props.employeeConditionsShowHide ? <> See Less <img className="img-fluid svg-sm" src={downArrow} alt="More" /></> : <> See More <img className="img-fluid svg-sm" src={upArrow} alt="More" /></> }
                        </button> */}

                        <a class="trigger-more collapsed" role="button" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">See More</a>
                    </div>

                    {/* <div class="expand Toggle">
                      <a role="button" class="collapsed" data-toggle="collapse" href="#ConditionShow" aria-expanded="false" aria-controls="ConditionShow"></a>
                    </div> */}

                    </div>
                </div>
            </div>
        </div>
    </section>
    : <div class="cssload-container" style={{ marginTop: '12%' }}>
        <div class="cssload-speeding-wheel" style={{ alignContent: 'center' }}></div>
      </div>}
                <div class="modal fade" id="enterDetails" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered multistepModal CreateProfileModal" role="document">
                        <Modal subCategoryID = {this.SubCategoryID}/>
                    </div>
                </div>
                <div className="modal fade" id="enterDetails" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div className="modal-dialog modal-dialog-centered multistepModal CreateProfileModal" role="document">
                      <Modal />
                    </div>
                </div>
            </div>
        )
    }
}
const mapStateToProps = state => {
    return {
        fields : state.educationalDetails.educationalDetails,
        workDetailsFields : state.workDetails.workDetails,
        certificationFields : state.certifications.certifications,
        recommendationFields : state.recommendations.recommendations,
        skills : state.skills.skills,
        employmentQues : state.employmentQues.fields,
        token: state.token,
        basicDetailsFields : state.fields.fields,
        isSkillsEditable: state.isSkillsEditable,
        isSummaryEditable: state.isSummaryEditable,
        play: state.play,
        loader : state.viewProfileLoader,
        employeeConditionsShowHide: state.employeeConditions,
        modalSelected : state.modalSelected,
    }
}

const mapDispatchToProps = dispatch => {
    return {
        editDetails : (id,name) => dispatch({type : "EDIT_DETAILS", id : id, name:name}),
        changeModal : (modal)=> dispatch({type:"CHANGE_MODAL", modal : modal}),
        mapDatabaseToLocal : (name,res) => dispatch({type : "MAP_DATABASE_TO_LOCAl", name:name, res: res,}),
        changeState : (name,val)=> dispatch({type:"CHANGE_FIELD",name:name,val:val,data : 'fields'}),
        toggleSkills:()=>dispatch({type:'TOGGLE_SKILLS'}),
        toggleSummary:()=>dispatch({type:'TOGGLE_SUMMARY'}),
        togglePlay : () => dispatch({type: "TOGGLE_PLAY"}),
        viewProfileLoader : () => dispatch({type : "VIEW_PROFILE_LOADER"}),
        employeeConditions : () => dispatch({type: "EMPLOYEE_CONDITIONS"}),
        resetForm : (data) => dispatch({type : "RESET_FORM", data : data}),
        editAction : (data) => dispatch({type : "EDIT_ACTION", data : data}),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(ViewProfile));