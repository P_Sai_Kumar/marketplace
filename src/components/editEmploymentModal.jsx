import React from "react";
import { connect } from "react-redux";
import axios from 'axios';
import $ from "jquery";

import Input from "./input/input";
import Location from './input/location'


class EditEmploymentModal extends React.Component {

    CurrencyOptions=[]
    WorkerID=localStorage.getItem("VendorID")?sessionStorage.getItem("WorkerID"):localStorage.getItem("WorkerID")
    componentDidMount() {
        axios.get('api/workers/profile/'+this.WorkerID,).then((res) => 
        {
            console.log(res.data)
            if (res.data.jobPreference.length !== 0){
                this.props.mapDatabaseToLocal("employmentDetails",res.data)
            }
        })
        axios.get('api/job-preferences/Currency').then((res)=>{
            console.log(res)
            this.CurrencyOptions = ["Select"].concat(res.data)
            this.forceUpdate()
        })
    }
    preventMinus = (e) => {
        if (e.code === 'Minus') {
            e.preventDefault();
        }
    };
    handleChange(field, event)
    {        
        var obj = {}
        obj[field] = event.target.value 
        this.props.changeState(field, obj)
    }
    
    handleSubmit(){
        var monthlyRate = 0
        var hourlyRate = 0
        var dailyRate = 0
        var yearlyRate = 0
        var engagementType = null, employmentType = null, locationType = null
        switch (this.props.fields.RateType.RateType){
            
            case "Daily Rate":
                dailyRate = Number(this.props.fields.Rate.Rate)
                break;
            case "Monthly Rate":
                monthlyRate = Number(this.props.fields.Rate.Rate)
                break;
            case  "Hourly Rate":
                hourlyRate = Number(this.props.fields.Rate.Rate)
                break;
            case "Yearly Rate":
                yearlyRate = Number(this.props.fields.Rate.Rate)
                break;
            default:
                break;
        }

        if (this.props.fields.WorkType.WorkType){engagementType = this.props.fields.WorkType.WorkType}
        if (this.props.fields.EmploymentType.EmploymentType){employmentType = this.props.fields.EmploymentType.EmploymentType}
        if (this.props.fields.WorkLocation.WorkLocation){locationType = this.props.fields.WorkLocation.WorkLocation}
        var data = {
            jobSearchStatus : this.props.fields.JobStatus.JobStatus,
            availabilityStatus : this.props.fields.JobStatus.JobStatus === "Actively Applying" ? this.props.fields.AvailabilityStatus.AvailabilityStatus : "",
            availableFrom: this.props.fields.AvailableFrom.AvailableFrom,
            availableTo: this.props.fields.AvailableTill.AvailableTill,
            engagementType: engagementType,
            employmentType: employmentType,
            locationType: locationType,
            hourPerDay: Number(this.props.fields.WorkingHours.WorkingHours),
            currencyType:this.props.fields.Currency.Currency,
            monthlyRate : monthlyRate,
            dailyRate : dailyRate,
            hourlyRate : hourlyRate,
            yearlyRate : yearlyRate,
            worker : {
                id : this.WorkerID,
            },
        }
        if(localStorage.getItem("VendorID")?sessionStorage.getItem("subCat"):localStorage.getItem("subCat"))
        {
            data['subCategory'] = JSON.parse(localStorage.getItem("VendorID")?sessionStorage.getItem("subCat"):localStorage.getItem("subCat"))
        }

        console.log(data)


        const headers = {
            'Content-Type': 'application/json',
          }
          

          axios.get('api/workers/profile/'+this.WorkerID).then((res) =>{
            if(res.data.jobPreference.length!==0)
            {
                data['id']=res.data.jobPreference[0].id
                axios.put('api/job-preferences/'+res.data.jobPreference[0].id, data, {headers : headers})
                .then(async(response) => {
                    console.log(response)
                    console.log({city : this.props.fields.LocationPreference.LocationPreference, })
                    await axios.patch('api/locations/'+res.data.locationpreference[0].location.id, 
                {id:res.data.locationpreference[0].location.id,city : this.props.fields.LocationPreference.LocationPreference}, {headers:{'Content-Type': 'application/merge-patch+json'}})
                .then(async(res1) => {
                    console.log(res1); console.log({worker : response.data, location : res1.data, prefrenceOrder : 1});
                    await axios.put('api/location-prefrences/'+res.data.locationpreference[0].id, {id:res.data.locationpreference[0].id,worker : response.data, location : res1.data, prefrenceOrder : 1})})
                    .catch((e) => console.log(e))
                }).catch((e) => console.log(e))
            }
            else{
                axios.post('api/job-preferences/', data, {headers : headers})
                .then(async(response) => {
                    console.log(response)
                    console.log({city : this.props.fields.LocationPreference.LocationPreference, })
                    await axios.post('api/locations', 
                {city : this.props.fields.LocationPreference.LocationPreference}, {headers : headers})
                .then(async(res) => {
                    console.log(res); console.log({worker : response.data, location : res.data, prefrenceOrder : 1});await axios.post('api/location-prefrences', {worker : response.data, location : res.data, prefrenceOrder : 1})})
                    .catch((e) => console.log(e))
                }).catch((e) => console.log(e))
            }
        })

        $('#enterDetails').click();
    }

    render (){
        return (
            <div class="modal-content">
                        <div class="FormSec employerDetails">
                            <form>
                                <div class="form-row">

                                    <div className="form-group">
                                        <label htmlFor="inputGender">Job Search Status</label>
                                        <div className="RadioBtn">
                                                <div className="form-check form-check-inline">
                                                    <input className="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="Actively Applying" checked={this.props.fields.JobStatus.JobStatus === "Actively Applying"} onChange={this.handleChange.bind(this,"JobStatus")}/>
                                                    <label className="form-check-label" htmlFor="inlineRadio1">Actively Applying</label>
                                                </div>
                                                <div className="form-check form-check-inline">
                                                    <input className="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="Casually Looking" checked={this.props.fields.JobStatus.JobStatus === "Casually Looking"} onChange={this.handleChange.bind(this,"JobStatus")}/>
                                                    <label className="form-check-label" htmlFor="inlineRadio2">Casually Looking</label>
                                                </div>
                                                <div className="form-check form-check-inline">
                                                    <input className="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="Exploring Marketplace" checked={this.props.fields.JobStatus.JobStatus === "Exploring Marketplace"} onChange={this.handleChange.bind(this,"JobStatus")}/>
                                                    <label className="form-check-label" htmlFor="inlineRadio2">Exploring Marketplace</label>
                                                </div>
                                        </div>
                                    </div>

                                    {this.props.fields.JobStatus.JobStatus === "Actively Applying" && <div>
                                    <div className="form-group">
                                    <label htmlFor="inputGender">Availability Status</label>
                                    <div className="RadioBtn">
                                            <div className="form-check form-check-inline">
                                                <input className="form-check-input" type="radio" name="inlineRadioOptionsAvailable" id="inlineRadio1" value="Can join immediately" checked={this.props.fields.AvailabilityStatus.AvailabilityStatus === "Can join immediately"} onChange={this.handleChange.bind(this,"AvailabilityStatus")}/>
                                                <label className="form-check-label" htmlFor="inlineRadio1">Can join immediately</label>
                                            </div>
                                            <div className="form-check form-check-inline">
                                                <input className="form-check-input" type="radio" name="inlineRadioOptionsAvailable" id="inlineRadio2" value="Service notice period" checked={this.props.fields.AvailabilityStatus.AvailabilityStatus === "Service notice period"} onChange={this.handleChange.bind(this,"AvailabilityStatus")}/>
                                                <label className="form-check-label" htmlFor="inlineRadio2">Service notice period</label>
                                            </div>
                                            <div className="form-check form-check-inline">
                                                <input className="form-check-input" type="radio" name="inlineRadioOptionsAvailable" id="inlineRadio2" value="Not resigned yet" checked={this.props.fields.AvailabilityStatus.AvailabilityStatus === "Not resigned yet"} onChange={this.handleChange.bind(this,"AvailabilityStatus")}/>
                                                <label className="form-check-label" htmlFor="inlineRadio2">Not resigned yet</label>
                                            </div>
                                    </div>
                                    </div>

                                    {this.props.fields.AvailabilityStatus.AvailabilityStatus !== "Not resigned yet" && <div class="form-row">
                                    <Input 
                                    divClass="form-group col-md-6" label="Available From" 
                                    config = {{className :"form-control" ,
                                            placeholder : "Enter Date", 
                                            type:"date"}}
                                    value = {this.props.fields.AvailableFrom.AvailableFrom}
                                    change={this.handleChange.bind(this,"AvailableFrom")}
                                    elementType="input" 
                                    />
                                    <Input 
                                    divClass="form-group col-md-6" label="Available Till" 
                                    config = {{className :"form-control" ,
                                            placeholder : "Enter Date", 
                                            type:"date"}}
                                    value = {this.props.fields.AvailableTill.AvailableTill}
                                    change={this.handleChange.bind(this,"AvailableTill")}
                                    elementType="input" 
                                    />
                                    </div>
                                    }
                                    </div>
                                    }       
                                </div>
                                
                                <div class="form-row">
                                    <div class="form-group col-md-4">
                                        <label for="inputworkType">Work Type</label>
                                        <div class="RadioBtn">
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="WorkTypeOptions" id="fullTime" value="FullTime" checked={this.props.fields.WorkType.WorkType=== "FullTime"} onChange={this.handleChange.bind(this,"WorkType",)}/>
                                                <label class="form-check-label" for="fullTime">Full Time</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="WorkTypeOptions" id="partTime" value="PartTime" checked={this.props.fields.WorkType.WorkType=== "PartTime"} onChange={this.handleChange.bind(this,"WorkType",)}/>
                                                <label class="form-check-label" for="partTime">Part Time</label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group col-md-4">
                                        <label for="inputEmployType">Employnment Type</label>
                                        <div class="RadioBtn">
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="EmploynmentTypeOptions" id="permanent" value="Permanent" checked={this.props.fields.EmploymentType.EmploymentType=== "Permanent"} onChange={this.handleChange.bind(this,"EmploymentType")}/>
                                                <label class="form-check-label" for="permanent">Permanent</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="EmploynmentTypeOptions" id="inlineRadio2" value="Contractual" checked={this.props.fields.EmploymentType.EmploymentType=== "Contractual"} onChange={this.handleChange.bind(this,"EmploymentType")}/>
                                                <label class="form-check-label" for="inlineRadio2">Contract</label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group col-md-4">
                                        <label for="inputWorkLocation">Work Location</label>
                                        <div class="RadioBtn">
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="inlineRadioOptions1" id="inlineRadio1" value="WorkFromHome" checked={this.props.fields.WorkLocation.WorkLocation=== "WorkFromHome"} onChange={this.handleChange.bind(this,"WorkLocation")}/>
                                                <label class="form-check-label" for="inlineRadio1">Home</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="inlineRadioOptions1" id="inlineRadio2" value="WorkFromOffice" checked={this.props.fields.WorkLocation.WorkLocation=== "WorkFromOffice"} onChange={this.handleChange.bind(this,"WorkLocation")}/>
                                                <label class="form-check-label" for="inlineRadio2">Office</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="inlineRadioOptions1" id="inlineRadio3" value="Both" checked={this.props.fields.WorkLocation.WorkLocation=== "Both"} onChange={this.handleChange.bind(this,"WorkLocation")}/>
                                                <label class="form-check-label" for="inlineRadio3">Both</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-row">

                                    {/* <Input 
                                    divClass="form-group col-md-6" label="Location Preference" 
                                    config = {{className :"form-control form-select" ,}}
                                    elementType="select"
                                    options = {["Select", "Hyderabad", "Noida",] }
                                    value = {this.props.fields.LocationPreference.LocationPreference}
                                    change={this.handleChange.bind(this,"LocationPreference",)}
                                        /> */}
                                    <Location 
                                    divClass="form-group col-md-6" label="Location Preference" 
                                    config = {{className :"form-control form-select" ,}}
                                    value={this.props.fields.LocationPreference.LocationPreference}
                                    change={this.handleChange.bind(this,"LocationPreference",)}/>
                                    <Input 
                                    divClass="form-group col-md-6" label="Working hours" 
                                    config = {{className :"form-control",type:"number",min:0,onKeyPress:this.preventMinus}}
                                    elementType="input"
                                    value = {this.props.fields.WorkingHours.WorkingHours}
                                    change={this.handleChange.bind(this,"WorkingHours",)}
                                    />
                                  </div>

                                  <div class="form-row">
                                  <Input 
                                    divClass="form-group col-md-4" label="Currency" 
                                    config = {{className :"form-control form-select" ,}}
                                    elementType="select"
                                    options = {this.CurrencyOptions}
                                    value = {this.props.fields.Currency.Currency}
                                    change={this.handleChange.bind(this,"Currency",)}
                                    />

                                    <Input 
                                    divClass="form-group col-md-2" label="Rate" 
                                    config = {{className :"form-control",type:"number",min:0,onKeyPress:this.preventMinus}}
                                    elementType="input"
                                    value = {this.props.fields.Rate.Rate}
                                    change={this.handleChange.bind(this,"Rate",)}
                                    />

                                    {/* <Input 
                                    divClass="form-group col-md-4" label="Rate Type" 
                                    config = {{className :"form-control form-select" ,}}
                                    elementType="select"
                                    options = {["Select", "Daily Rate", "Monthly Rate", "Hourly Rate"] }
                                    value = {this.props.fields.RateType.RateType}
                                    change={this.handleChange.bind(this,"RateType",)}
                                    /> */}
                                    <div class="form-group col-md-6">
                                        <label for="inputRateType">Rate Type</label>
                                        <div class="RadioBtn">
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="inlineRadioOptions2" id="inlineRadio1" value="Daily Rate" checked={this.props.fields.RateType.RateType=== "Daily Rate"} onChange={this.handleChange.bind(this,"RateType")}/>
                                                    <label class="form-check-label" for="inlineRadio1">Daily</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="inlineRadioOptions2" id="inlineRadio2" value="Hourly Rate" checked={this.props.fields.RateType.RateType=== "Hourly Rate"} onChange={this.handleChange.bind(this,"RateType")}/>
                                                    <label class="form-check-label" for="inlineRadio2">Hourly</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="inlineRadioOptions2" id="inlineRadio3" value="Monthly Rate" checked={this.props.fields.RateType.RateType=== "Monthly Rate"} onChange={this.handleChange.bind(this,"RateType")}/>
                                                    <label class="form-check-label" for="inlineRadio3">Monthly</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="inlineRadioOptions2" id="inlineRadio4" value="Yearly Rate" checked={this.props.fields.RateType.RateType=== "Yearly Rate"} onChange={this.handleChange.bind(this,"RateType")}/>
                                                    <label class="form-check-label" for="inlineRadio4">Yearly</label>
                                                </div>
                                            </div>
                                        </div>
                                  </div>

                            </form>
                        </div>
                        <div class="modal-footer">
                        <div class="btn-group NextFormButtons ModalNextFormButtons ">
                            <button class="common-btn commonBlueBtn" 
                            onClick = {() => {setTimeout(() => this.handleSubmit(),5)}}>Save</button>
                        </div>
                        </div>
        </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        fields: state.employmentQues.fields,
        errors : state.fields.errors,
        formValid : state.fields.formValid,
        token: state.token
    }
}

const mapDispatchToProps = dispatch => {
    return {
        mapDatabaseToLocal : (name,res,) => dispatch({type : "MAP_DATABASE_TO_LOCAl", name:name, res: res,}),
        changeState : (name,val)=> dispatch({type:"CHANGE_FIELD", name:name, val:val, data : 'employmentQues'}), 
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(EditEmploymentModal);