import { useState ,useEffect} from "react"
import { useHistory, useLocation } from "react-router-dom";
import { connect } from 'react-redux'
import axios from 'axios'
import PdfIcon from '../../imgaes/icons/pdf_icon.jpg'
import DocIcon from '../../imgaes/icons/doc_icon.png'
import MicrosoftTeamsImage from '../../imgaes/MicrosoftTeams-image.png'
import $ from 'jquery';

const UploadImage=(props)=> {
    const history = useHistory();
    const location = useLocation();
    const [selectedFile, setSelectedFile] = useState()
    const [preview, setPreview] = useState()
    
    let WorkerID=localStorage.getItem("VendorID")?(props.vendor?localStorage.getItem("WorkerID"):sessionStorage.getItem("WorkerID")):localStorage.getItem("WorkerID")
      useEffect(() => {
          if (!selectedFile) {
              setPreview(undefined)
              return
          }
          console.log(selectedFile)
          const objectUrl = URL.createObjectURL(selectedFile)
          setPreview(objectUrl)
          console.log(objectUrl)
          return () => URL.revokeObjectURL(objectUrl)
      }, [selectedFile])

      const handleChange= async (field, event)=>
    {
        let fileID=localStorage.getItem("VendorID")?sessionStorage.getItem('fileID'):localStorage.getItem('fileID')
        if(event===null)
        {
            console.log("del")
            props.changeState(field,'')
            if(fileID!==null)
                {
                    axios.delete('api/files/'+fileID)
                    .then((res)=>{
                    })
                    localStorage.removeItem('fileID')
                    localStorage.removeItem('resumeURL')
                    sessionStorage.removeItem('fileID')
                    sessionStorage.removeItem('resumeURL')
                }
            return
        }
        if(field==='resume')
        {
            var reader = new FileReader();
            reader.readAsDataURL(event.target.files[0]);
            reader.onloadend =  async()=> {
            var base64String = reader.result;
            console.log(base64String.split(",")[1]);
            props.changeState(field,base64String)
            console.log({
                "file_name": event.target.files[0].name,
                "raw":base64String.split(",")[1]})
            if((!location.pathname.includes("/viewProfile"))||location.pathname.includes("vendor/viewProfile"))
            {
            await axios.post("api/resume/parse",{
                "file_name": event.target.files[0].name,
                "raw":base64String.split(",")[1]})
            .then(async(res)=>{
                console.log(res)
                if(res.data.error){
                    document.getElementById('error1').innerHTML='Parsing error Occured. Try uploading another'
                }else {
                    var qualifications=res.data.candidate.qualifications
                    let experiences=res.data.candidate.experiences
                    let d={id:Number(WorkerID),firstName:res.data.candidate.first_name,lastName:res.data.candidate.last_name}
                    if(localStorage.getItem('VendorID'))
                    {
                        d['vendor']={id:Number(localStorage.getItem('VendorID'))}
                    }
                    await axios.patch('api/workers/'+WorkerID,  d,{headers:{'Content-Type': 'application/merge-patch+json'}})
                    .then((response) => {console.log(response);}).catch((e) => console.log(e))
                    for(let q of qualifications)
                    {
                        if(q.qualification_type.code=="SKILL")
                        {
                            data = {
                                skillName: q.name,
                                worker : {
                                    id: WorkerID,
                                }
                            }            
                            await axios.post('api/skills-masters', data)
                            .then(async(response) => 
                            {console.log(response); await axios.patch("api/worker/skills/"+WorkerID,
                            data=response.data, {headers : {"Content-Type" : "application/merge-patch+json"}})}).catch((e) => console.log(e))
                        }
                        else if(q.qualification_type.code=="EDUCATION")
                        {
                            var data = {
                                degreeName: q.degree,
                                institute: q.institution
                            }
                            try{
                                data['yearOfPassing']=Number(q.year)
                                data['isComplete']=(new Date()).getFullYear()<=Number(q.year)
                            }catch(msg){}
                            try{
                                data['marks']=Number(q.score)
                            }catch(msg){}
                            data["worker"] = {id :WorkerID}
                            await axios.post('api/educations', data)
                            .then((response) => {console.log(response)}).catch((e) => console.log(e))
                        }
                    }

                    if(location.pathname.includes("/viewProfile")&&(!location.pathname.includes("vendor/viewProfile")))
                {
                  await axios.get('api/educations/worker/'+WorkerID).then((res) => 
                  {
                      props.mapDatabaseToLocal("educationalDetails",res.data)
                  })
                  await axios.get('api/skills-masters/worker/'+WorkerID).then((res) => 
                  {
                      props.mapDatabaseToLocal("skills",res.data)
                  })
                }
                for(let e of experiences)
                {
                    var data = {
                        companyName: e.organization,
                        jobTitle: e.title,
                        startDate: "",
                        endDate: "",
                        isCurrent:false,
                    }
                    try{
                        data['isCurrent']=(new Date()).getFullYear()<=Number(e.end_date.slice(0,4))
                    }catch(msg){}
                    data["worker"] = {id : WorkerID}
                    console.log(data)
                    await axios.post('api/employments', data)
                    .then((response) => {console.log(response)}).catch((e) => console.log(e))
                }
                await axios.get('api/employments/worker/'+ WorkerID).then((res) => 
                {
                    props.mapDatabaseToLocal("workDetails",res.data)
                })
                if (location.pathname !== "/viewProfile"){
                    if(localStorage.getItem('VendorID'))
                    {
                        props.setResumeLoading(false)
                        $('#addCandidate').click()
                        localStorage.setItem('newWorkerID',WorkerID)
                        const newWindow = window.open("../../createProfile/basicDetails", '_blank', 'noopener,noreferrer')
                        if (newWindow) newWindow.opener = null
                    }
                    else{
                        history.push("/createProfile/basicDetails")
                    }
                    
                }
                }
                
                // await axios.get('api/educations/worker/'+ localStorage.getItem("WorkerID")).then((res) => 
                // {
                //     props.mapDatabaseToLocal("educationalDetails",res.data)
                // })
                
            }).catch((e) => {
              console.log(e)
                document.getElementById('error1').innerHTML='Error Occured. Try again later'})
            }   
            
            if(field==='resume')
            {
                let res1 = await fetch(base64String);
                let blob = await res1?.blob();
                if(localStorage.getItem("VendorID"))
                {
                  sessionStorage.setItem('resumeURL',URL.createObjectURL(blob))
                }
                else{
                  localStorage.setItem('resumeURL',URL.createObjectURL(blob))
                }
                console.log(URL.createObjectURL(blob))
                props.changeState('resume',base64String)
                let resumeContentType
                     if(base64String.includes('pdf'))
                     {
                        resumeContentType='Pdf'
                     }
                     else if(base64String.includes('docs'))
                     {
                        resumeContentType='Docs'
                     }
                     else{
                        resumeContentType='Doc'
                     }
                     props.changeState('resumeContentType',resumeContentType)      

            }
                if(fileID==null)
                {
                    await axios.post('api/files',{worker:{id:Number(WorkerID)},filename:base64String.split(",")[1],filenameContentType:base64String.split(",")[0],isResume:true})
                    .then((res)=>{
                        console.log(res)
                        if(localStorage.getItem("VendorID"))
                        {
                          sessionStorage.setItem('fileID',res.data.id)
                        }
                        else{
                          localStorage.setItem('fileID',res.data.id)
                        }
                    })
                }
                else{
                    await axios.patch('api/files/'+fileID,{id:Number(fileID),worker:{id:Number(WorkerID)},filename:base64String.split(",")[1],filenameContentType:base64String.split(",")[0],isResume:true},{headers : {"Content-Type":"application/merge-patch+json"}})
                    .then((res)=>{
                        })
                }
            }
        }

    }
      
      const onDelete=e=>{
        setSelectedFile(undefined)
        document.getElementById('exampleFormControlFile2').value=''
        localStorage.removeItem("resumeURL")
        sessionStorage.removeItem("resumeURL")
        handleChange("resume",null)
      }

      const onSelectFile = async (e) => {
        if (!e.target.files || e.target.files.length === 0) {
            setSelectedFile(undefined)
            return
        }
        if(props.type==='pic'||props.type==='cover_pic')
        {
        var fileInput = (props.type==='pic')?document.getElementById('exampleFormControlFile1'):document.getElementById('myfile');
      
            var filePath = fileInput.value;
        
            // Allowing file type
            var allowedExtensions = /(\.jpg|\.jpeg|\.png)$/i;
            const fsize =Math.round((e.target.files[0].size / 1024));
            if (!allowedExtensions.exec(filePath)) {
                document.getElementById('error').innerHTML='Invalid file type<br/>Allowed Extensions are .jpg, .jpeg, .png'
                fileInput.value = '';
                return false;
            } 
            else if(fsize>=5120)
           {
                document.getElementById('error').innerHTML="File too Big, please select a file less than 5mb"
                fileInput.value = '';
                return false;
           }
            else{
                document.getElementById('error').innerHTML=''
            }
            if(props.type==='pic')
            {
              props.onChange(e)
            }
            if(props.type==='cover_pic')
            {
              var reader = new FileReader();
              reader.readAsDataURL(e.target.files[0]);
              reader.onloadend =  async()=> {
              var base64String = reader.result;
              console.log(base64String.split(",")[1]);
              let res1 = await fetch(base64String);
              let blob = await res1?.blob();
              props.changeState('coverPhoto',URL.createObjectURL(blob))
              let coverPhotoID=localStorage.getItem('VendorID')?(props.vendor?localStorage.getItem('coverPhotoID'):sessionStorage.getItem('coverPhotoID')):localStorage.getItem('coverPhotoID')
              if(props.vendor==="vendor")
                {
                    console.log(props.vendor)
                    var data={
                        "id":localStorage.getItem("VendorID"),
                        "coverPhoto":base64String.split(",")[1],
                        "coverPhotoContentType":base64String.split(",")[0],
                    }
                    axios.patch('api/vendors/'+localStorage.getItem("VendorID"),data,{headers : {'Content-Type': 'application/merge-patch+json'}}
                    ).then((response) => {console.log(response);}).catch((e) => console.log(e))
                }
                else if(props.employer)
                {
                    var data={
                        "id":localStorage.getItem("EmployerID"),
                        "coverPhoto":base64String.split(",")[1],
                        "coverPhotoContentType":base64String.split(",")[0],
                    }
                    axios.patch('api/recruiters',data,{headers : {'Content-Type': 'application/merge-patch+json'}}
                    ).then((response) => {console.log(response);}).catch((e) => console.log(e))
                }
               else{     
              if(coverPhotoID==null)
                {
                    console.log(WorkerID)
                    axios.post('api/files',{worker:{id:Number(WorkerID)},filetype:"Other",filename:base64String.split(",")[1],filenameContentType:base64String.split(",")[0]})
                    .then((res)=>{
                        console.log(res)
                        localStorage.getItem('VendorID')?(props.vendor?localStorage.setItem('coverPhotoID',res.data.id):sessionStorage.setItem('coverPhotoID',res.data.id)):localStorage.setItem('coverPhotoID',res.data.id)
                    })
                
                }
                else{
                    axios.patch('api/files/'+coverPhotoID,{id:Number(coverPhotoID),worker:{id:Number(WorkerID)},filetype:"Other",filename:base64String.split(",")[1],filenameContentType:base64String.split(",")[0]},{headers : {"Content-Type":"application/merge-patch+json"}})
                    .then((res)=>{
                        })
                }
            }
              }
            }
        }
        else{
            var fileInput = document.getElementById('exampleFormControlFile2');
      
            var filePath = fileInput.value;
        
            // Allowing file type
            var allowedExtensions = /(\.pdf|\.doc|\.docs|\.docx)$/i;
            const fsize =Math.round((e.target.files[0].size / 1024));
            if (!allowedExtensions.exec(filePath)) {
                document.getElementById('error1').innerHTML='Invalid file type<br/>Allowed Extensions are .pdf, .doc, .docs'
                fileInput.value = '';
                return false;
            } 
            else if(fsize>2048)
           {
                document.getElementById('error1').innerHTML="File too Big, please select a file less than 2mb"
                fileInput.value = '';
                return false;
           }
            else{
                document.getElementById('error1').innerHTML=''
            }
            if (!WorkerID){
                if(location.pathname.includes("vendor/viewProfile"))
                {
                    await axios.get('api/workers/profile/'+localStorage.getItem("WorkerID")).then(async(res) => 
                    {
                        console.log(res)
                        await axios.post('api/workers', {email : localStorage.getItem("email"),firstName: "", lastName: "",vendor:{id:localStorage.getItem("VendorID")},idProof: res.data.worker.idProof,
                        language:  res.data.worker.language,
                        workerLocation: res.data.worker.workerLocation})
                        .then(async(response) => {console.log(response);WorkerID=response.data.id
                        if (res.data.jobPreference.length !== 0){
                            console.log(res.data)
                            let d = [res.data.jobPreference[res.data.jobPreference.length-1]].map((item, id) => {
                                 
                                return ({jobSearchStatus : item.jobSearchStatus,
                                    availabilityStatus : item.availabilityStatus,
                                    engagementType : item.engagementType,
                                    employmentType : item.employmentType,
                                    currencyType : item.currencyType,
                                    monthlyRate : item.monthlyRate,
                                    dailyRate : item.dailyRate,
                                    hourlyRate : item.hourlyRate,
                                    yearlyRate : item.yearlyRate,
                                    locationType : item.locationType,
                                    hourPerDay : item.hourPerDay,
                                    worker:{id:response.data.id}
                                                    })
                                })
                            d=d[d.length-1]
                        if(res.data.category[res.data.category.length-1])
                        {
                            d['subCategory']=res.data.category[res.data.category.length-1]
                        }
                        await axios.post('api/job-preferences/',d)
                        .then(async(response) => {
                            console.log(response)
                            await axios.post('api/locations', 
                        {city : ''})
                        .then(async(res) => {
                            console.log(res);  await axios.post('api/location-prefrences', {worker : response.data, location : res.data, prefrenceOrder : 1})})
                            .catch((e) => console.log(e))
                        }).catch((e) => console.log(e))
                    }
                    }).catch((e) => console.log(e))
                    })
                }
                else{
                    await axios.post('api/workers', {email : localStorage.getItem("email"),firstName: "", lastName: "",user:JSON.parse(localStorage.getItem("user"))},)
                            .then((response) => {console.log(response); localStorage.setItem("Worker", JSON.stringify(response.data));localStorage.setItem("WorkerID", response.data.id);WorkerID=response.data.id}).catch((e) => console.log(e))
                }
            }
            if((!location.pathname.includes("/viewProfile"))||location.pathname.includes("vendor/viewProfile")){
              props.setResumeLoading(true)
            }
            handleChange("resume",e)
        }
          setSelectedFile(e.target.files[0])
      }

      
      
      return (
          <>
        {(props.type==='pic')?
          <div className="col-md-3">
          <div className="profilePic"> 
          {console.log(props.file)}
                   <div className="uploadProfilePic">
                  {selectedFile?<img src={preview} className="profilePicImage"/> :props.file!==''? <img src={props.file} className="profilePicImage"/>: <svg xmlns="http://www.w3.org/2000/svg" height="48px" viewBox="0 0 24 24" width="48px" fill="#007BFF">
                  <path d="M24 24H0V0h24v24z" fill="none"/><path d="M21 3H3C2 3 1 4 1 5v14c0 1.1.9 2 2 2h18c1 0 2-1 2-2V5c0-1-1-2-2-2zM5 17l3.5-4.5 2.5 3.01L14.5 11l4.5 6H5z"/>
              </svg> }
                    <h6>+ Choose Photo</h6>
                  <span>Please select the photo you want to upload</span>
                  <input type="file" accept='image/*' className="form-control-file choose-pic" id="exampleFormControlFile1" onChange={onSelectFile} />
              </div>
            <div className='error' id='error'></div>
              
          </div>
          </div>
       :
       (props.type==='cover_pic')?
       <div class="coverPhoto" style={{backgroundImage:selectedFile?`url(${preview})`:props.file!=''?`url(${props.file})`:MicrosoftTeamsImage,backgroundRepeat:'no-repeat'}}>
       <div class="uploadCoverPhoto">
         <svg xmlns="http://www.w3.org/2000/svg" height="18px" viewBox="0 0 24 24" width="18px" fill="#000000">
           <path d="M0 0h24v24H0z" fill="none"/><circle cx="12" cy="12" r="3.2"/><path d="M9 2L7.17 4H4c-1.1 0-2 .9-2 2v12c0 1.1.9 2 2 2h16c1.1 0 2-.9 2-2V6c0-1.1-.9-2-2-2h-3.17L15 2H9zm3 15c-2.76 0-5-2.24-5-5s2.24-5 5-5 5 2.24 5 5-2.24 5-5 5z"/>
         </svg>
         <input type="file" accept='image/*' id="myfile" name="myfile" onChange={onSelectFile}/>
       </div>
       <div className='error' id='error'></div>
     </div>
       :
       <div>
        {(location.pathname.includes("/viewProfile")&&!location.pathname.includes("vendor/viewProfile")) &&
        (selectedFile ? <div class="paddX15 disF">{<div class="addedDetailImg"><img src={selectedFile.name.includes('pdf')?PdfIcon:DocIcon}></img></div>}<div class="addedDetailContent"><a target='_blank' href={preview}>My Resume</a><p id='resumeContentType'>{selectedFile.name.includes('pdf')?'Pdf':'Doc'}</p></div><a onClick={onDelete}><svg xmlns="http://www.w3.org/2000/svg" height="20px" viewBox="0 0 24 24" width="20px" fill="#f67980">
                        <path d="M0 0h24v24H0z" fill="none"/><path d="M6 19c0 1.1.9 2 2 2h8c1.1 0 2-.9 2-2V7H6v12zM19 4h-3.5l-1-1h-5l-1 1H5v2h14V4z"/>
                    </svg></a></div>:props.file!==null && <div class="paddX15 disF">{(props.fileContentType&&props.fileContentType!='')&&<div class="addedDetailImg"><img src={props.fileContentType==='Pdf'?PdfIcon:DocIcon}></img></div>}
                    <div class="addedDetailContent"><a target='_blank' href={props.file}>My Resume</a><p id='resumeContentType'>{props.fileContentType==='Pdf'?'Pdf':'Doc'}</p></div><a onClick={onDelete}><svg xmlns="http://www.w3.org/2000/svg" height="20px" viewBox="0 0 24 24" width="20px" fill="#f67980">
                        <path d="M0 0h24v24H0z" fill="none"/><path d="M6 19c0 1.1.9 2 2 2h8c1.1 0 2-.9 2-2V7H6v12zM19 4h-3.5l-1-1h-5l-1 1H5v2h14V4z"/>
                    </svg></a></div>)}
      <div className='error' id='error1'></div>
      <div className="text-center">
      <label className="Social-login-btn common-lightblue-button continue-create-profile" htmlFor="exampleFormControlFile2">
        Upload Resume
        <svg viewBox="0 0 384.97 384.97" xmlns="http://www.w3.org/2000/svg" role="img" className='fileUpload' width="25px" height="25px">
        <g>
        <path d="M372.939,264.641c-6.641,0-12.03,5.39-12.03,12.03v84.212H24.061v-84.212c0-6.641-5.39-12.03-12.03-12.03
          S0,270.031,0,276.671v96.242c0,6.641,5.39,12.03,12.03,12.03h360.909c6.641,0,12.03-5.39,12.03-12.03v-96.242
          C384.97,270.019,379.58,264.641,372.939,264.641z"/>
        <path d="M117.067,103.507l63.46-62.558v235.71c0,6.641,5.438,12.03,12.151,12.03c6.713,0,12.151-5.39,12.151-12.03V40.95
          l63.46,62.558c4.74,4.704,12.439,4.704,17.179,0c4.74-4.704,4.752-12.319,0-17.011l-84.2-82.997
          c-4.692-4.656-12.584-4.608-17.191,0L99.888,86.496c-4.752,4.704-4.74,12.319,0,17.011
          C104.628,108.211,112.327,108.211,117.067,103.507z"/>
	      </g>
        </svg>
      </label>
      <input type="file" style={{display:'none'}} className="form-control-file" id="exampleFormControlFile2" onChange={onSelectFile}/>
      </div>
      </div>}
      </>
      )
    }
  const mapStateToProps = state => {
    return {
        fields: state.fields.fields,
    }
  }
  const mapDispatchToProps = dispatch => {
      return {
          changeState : (name,val)=> dispatch({type:"CHANGE_FIELD",name:name,val:val,data : 'fields'}),
          mapDatabaseToLocal : (name,res) => dispatch({type : "MAP_DATABASE_TO_LOCAl", name:name, res: res})
      }
  }  
  export default  connect(mapStateToProps,mapDispatchToProps)(UploadImage);